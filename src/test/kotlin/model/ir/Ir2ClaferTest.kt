package model.ir

import ir.IrGraph
import ir.createFromClasspath
import clafer.ChocoConsistencyChecker
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import org.junit.Assert.assertEquals
import org.junit.Test

import java.util.concurrent.TimeUnit

internal class Ir2ClaferTest {

    @Test
    fun basicTest() {
        runTest("/clafer/basicAbc.cfr")
    }

    @Test
    fun deepDrefPath() {
        runTest("/clafer/deepDrefPath.cfr")
    }

    @Test
    fun fullClafer() {
        runTest("/clafer/positive/ACCDemo_fullClafer.cfr")
    }


    @Test
    fun NestedInheritanceAndReference2() {
        runTest("/clafer/positive/NestedInheritanceAndReference2.cfr")
    }

    @Test
    @Throws(Exception::class)
    fun testPositive() {
        val dirPath = "/clafer/positive/"
        val claferFileNames = ResourceUtil.getResourceFiles(dirPath)
        for (claferFileName in claferFileNames.filter { it.endsWith(".cfr") }) {
            println("> Testing clafer model " + claferFileName)
            runTest(dirPath + claferFileName)
        }
    }


    fun runTest(cpSpec: String) {
        val absPath = Ir2ClaferTest::class.java.getResource(cpSpec).file
        val exp = ChocoConsistencyChecker().check(absPath, 5, 180, TimeUnit.SECONDS)

        val graph = IrGraph.createFromClasspath(cpSpec)
        val actual = ChocoConsistencyChecker().check(graph, 5, 180, TimeUnit.SECONDS)
//        println(graph.convert2Clafer())
        assertEquals(exp, actual)

    }

}