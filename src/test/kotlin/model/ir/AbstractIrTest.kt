package model.ir


import ir.*
import ir.constraints.*
import org.cardygan.boundanalyzer.antlr.ClaferCompiler
import org.cardygan.boundanalyzer.antlr.ClaferParser
import org.cardygan.boundanalyzer.antlr.ParserTest
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import java.io.File
import java.io.PrintWriter
import java.util.*


abstract class AbstractIrTest {

    protected lateinit var graph: IrGraph

    protected lateinit var root: Node

    protected fun n(name: String): Node = graph.getNode(name)

    protected fun chkGraph(expNodeNo: Int = 0, expNestNo: Int = 0, expInhNo: Int = 0, expRefNo: Int = 0,
                           expSetRefNo: Int = 0, expBagRefNo: Int = 0, expCstrNo: Int = 0) {
        assertEquals(expNodeNo, graph.nodes.size)
        assertEquals(expNestNo, graph.incoming.values.flatten().filter { it.type == Edge.EdgeType.P }.size)
        assertEquals(expInhNo, graph.incoming.values.flatten().filter { it.type == Edge.EdgeType.A }.size)
        assertEquals(expRefNo, graph.incoming.values.flatten().filter { it.type == Edge.EdgeType.R }.size)
        assertEquals(expSetRefNo, graph.setRefs.size)
        assertEquals(expBagRefNo, graph.bagRefs.size)
    }

    protected infix fun Node.inheritsFrom(superNode: Node) {
        assertTrue(graph.getSub(superNode).contains(this))
    }

    protected infix fun String.references(trg: String) {
        n(this) references n(trg)
    }

    protected infix fun Node.references(trg: Node) {
        assertTrue(graph.getRefsTo(trg).contains(this))
    }

    protected infix fun String.references(trg: Node.NodeType) {
        n(this) references trg
    }

    protected infix fun Node.references(trg: Node.NodeType) {
        when (trg) {
            Node.NodeType.STR -> assertTrue(graph.hasStringRef(this))
            Node.NodeType.DBL -> assertTrue(graph.hasDoubleRef(this))
            Node.NodeType.INT -> assertTrue(graph.hasIntRef(this))
            else -> {
                throw IllegalStateException("Use this function to check a reference to a node representing a string, int or double value.")
            }
        }
    }

    protected infix fun String.isNestedIn(trg: Node) {
        n(this) isNestedIn trg
    }


    protected infix fun String.isNestedIn(trg: String) {
        n(this) isNestedIn n(trg)
    }

    protected infix fun Node.isNestedIn(trg: Node) {
        assertTrue(graph.getNested(trg).contains(this))
    }


    protected infix fun String.hasCstr(boolExpr: String) {
        n(this) hasCstr boolExpr
    }

    protected infix fun Node.hasCstr(boolExpr: String) {
        val ret = (ClaferParser.parseFromString("[ $boolExpr ]").accept(ExpressionCreator())) as BoolExpr
        val toBeChecked = replaceLocalIds(simplifyIds(ret))
        assertTrue(graph.cstrs[this]!!.map { simplifyIds(it) }.any { it == toBeChecked })
    }

    protected infix fun String.hasSetRef(setExpr: String) {
        n(this) hasSetRef setExpr
    }

    protected infix fun Node.hasSetRef(setExpr: String) {
        val ret = (ClaferParser.parseFromString("[ #($setExpr) < 0 ]").accept(ExpressionCreator())) as BoolExpr
        val castToSetExpr = ((ret as Lt).lhs as Card).set
        val toBeChecked = replaceLocalIds(simplifyIds(castToSetExpr))
        assertTrue(simplifyIds(graph.setRefs[this]!!) == toBeChecked)
    }


    protected infix fun Node.hasBagRef(bagExpr: String) {
        assertTrue(prettyPrint(simplifyIds(graph.setRefs[this]!!)) == bagExpr)
    }

    private fun replaceLocalIds(setExpr: AstElement): AstElement =
            (replaceAstElement(setExpr, { Optional.of(Identifier((it as LocalIdentifier).id)) }, { it is LocalIdentifier })).get()


    protected fun resolvesTo(ctx: String, path: String, expNodes: List<Node>) {
        val res = graph.resolvePath(path, ctx)
        assertTrue(res.containsAll(expNodes))
    }

    protected fun resolvesToInt(ctx: String, path: String) = resolvesToType(ctx, path, Node.NodeType.INT)

    protected fun resolvesToType(ctx: String, path: String, type: Node.NodeType) {
        val res = graph.resolvePath(path, ctx)
        assertTrue(res.all { it.type == type })
    }

    protected fun loadDesugaredTestSpec(file: String): IrGraph {
        val desugared = ResourceUtil.transformStreamToString(ParserTest::class.java.getResourceAsStream(file))

        val parser = ClaferParser.parseFromString(desugared)
        val graph = IrCreator(parser).create()

        root = graph.cRoot

        return graph
    }

    protected fun loadTestSpec(file: String): IrGraph {
        val model = ResourceUtil.transformStreamToString(ParserTest::class.java.getResourceAsStream(file))
        val desugared = ClaferCompiler.compile(model)
        println(desugared)
        val parser = ClaferParser.parseFromString(desugared)
        val graph = IrCreator(parser).create()

        root = graph.cRoot

        return graph
    }

    protected fun genDotFile(file: String, graph: IrGraph) {
        val dotFileName = File(file).let {
            it.name.replace(".${it.extension}", "")
        }

        // TODO generalize / remove!!!
        PrintWriter("/users/markus/Desktop/dot/tmpDotFile.dot").use { out -> out.println(graph.toDot()) }
        val proc = Runtime.getRuntime().exec("dot -Tpng /users/markus/Desktop/dot/tmpDotFile.dot -o /users/markus/Desktop/dot/$dotFileName.png")
        proc.waitFor()

        File("/users/markus/Desktop/dot/tmpDotFile.dot").delete()
    }
}


