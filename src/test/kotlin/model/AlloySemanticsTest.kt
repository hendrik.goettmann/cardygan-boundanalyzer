package model

import ilp.IlpAnalyzer
import ir.*
import ir.constraints.*
import ir.transform.flatten
import main.BoundAnalyzer
import mu.KotlinLogging
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import org.junit.Test
import org.junit.runners.Parameterized


class AlloySemanticsTest() {

    private val minimalUnboundedRange = 4


    private val logger = KotlinLogging.logger {}

    companion object {

        @JvmStatic
        @Parameterized.Parameters(name = "\"{0}\"")
        fun data(): Collection<String> {
            val dirPath = "/clafer/positive/"
            val claferFileNames = ResourceUtil.getResourceFiles(dirPath)
            val fileNames = mutableListOf<String>()
            for (claferFileName in claferFileNames) {
                fileNames.add(dirPath + claferFileName)
            }

            return fileNames
        }
    }

    @Parameterized.Parameter
    lateinit var specName: String

    @Test
    fun testPositive() {
        testSpec(IrGraph.createFromClasspath(specName), minimalUnboundedRange)

//    }
    }


    @Test
    fun testBasicA() {
        testSpec(IrGraph.createFromClasspath("/clafer/basicA.cfr"), minimalUnboundedRange)
    }

    @Test
    fun testAbcWithNested() {
        testSpec(IrGraph.createFromClasspath("/clafer/abcWithNested.cfr"), minimalUnboundedRange)
    }

    @Test
    fun testAbstractWithAbstractRef() {
        testSpec(IrGraph.createFromClasspath("/clafer/abstractWithAbstractRef.cfr"), minimalUnboundedRange)
    }

    @Test
    fun testAbstractWithRef() {
        testSpec(IrGraph.createFromClasspath("/clafer/abstractWithRef.cfr"), minimalUnboundedRange)
    }

    @Test
    fun testBasicAbc() {
        testSpec(IrGraph.createFromClasspath("/clafer/basicAbc.cfr"), minimalUnboundedRange)
    }

    @Test
    fun testBasicAbcInheritance() {
        testSpec(IrGraph.createFromClasspath("/clafer/basicAbcInheritance.cfr"), minimalUnboundedRange)
    }

    @Test
    fun testBasicAbcInheritanceWithConstraint() {
        testSpec(IrGraph.createFromClasspath("/clafer/basicAbcInheritanceWithConstraint.cfr"), minimalUnboundedRange)
    }

    @Test
    fun testBasicAbcInheritanceWithConstraint2() {
        testSpec(IrGraph.createFromClasspath("/clafer/basicAbcInheritanceWithConstraint2.cfr"), minimalUnboundedRange)
    }

    @Test
    fun testBasicAbcInheritanceWithConstraint3() {
        testSpec(IrGraph.createFromClasspath("/clafer/basicAbcInheritanceWithConstraint3.cfr"), minimalUnboundedRange)
    }

    @Test
    fun complexInheritance() {
        testSpec(IrGraph.createFromClasspath("/clafer/complexInheritance.cfr"), minimalUnboundedRange)
    }

    @Test
    fun complexNamespaces() {
        testSpec(IrGraph.createFromClasspath("/clafer/complexNamespaces.cfr"), minimalUnboundedRange)
    }

    @Test
    fun inheritanceNumExpr() {
        testSpec(IrGraph.createFromClasspath("/clafer/inheritanceNumExpr.cfr"), minimalUnboundedRange)
    }

    @Test
    fun inheritanceRefinement() {
        testSpec(IrGraph.createFromClasspath("/clafer/inheritanceRefinement.cfr"), minimalUnboundedRange)
    }

    @Test
    fun inheritanceRefinement2() {
        testSpec(IrGraph.createFromClasspath("/clafer/inheritanceRefinement2.cfr"), minimalUnboundedRange)
    }

    @Test
    fun inheritanceRefinement3() {
        testSpec(IrGraph.createFromClasspath("/clafer/inheritanceRefinement3.cfr"), minimalUnboundedRange)
    }

    @Test
    fun minimalRefInit() {
        testSpec(IrGraph.createFromClasspath("/clafer/minimalRefInit.cfr"), minimalUnboundedRange)
    }

    @Test
    fun minimalRelatives() {
        testSpec(IrGraph.createFromClasspath("/clafer/minimalRelatives.cfr"), minimalUnboundedRange)
    }

    @Test
    fun parentPathRef() {
        testSpec(IrGraph.createFromClasspath("/clafer/parentPathRef.cfr"), minimalUnboundedRange)
    }

    @Test
    fun personRelatives1() {
        testSpec(IrGraph.createFromClasspath("/clafer/personRelatives1.cfr"), minimalUnboundedRange)
    }

    @Test
    fun simpleQuantified() {
        testSpec(IrGraph.createFromClasspath("/clafer/simpleQuantified.cfr"), minimalUnboundedRange)
    }

    @Test
    fun simpleQuantified2() {
        testSpec(IrGraph.createFromClasspath("/clafer/simpleQuantified2.cfr"), minimalUnboundedRange)
    }

    @Test
    fun simpleQuantified3() {
        testSpec(IrGraph.createFromClasspath("/clafer/simpleQuantified3.cfr"), minimalUnboundedRange)
    }

    @Test
    fun simpleQuantified4() {
        testSpec(IrGraph.createFromClasspath("/clafer/simpleQuantified4.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedGeq() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedGeq.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedGeq_binary() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedGeq_binary.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedGt() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedGt.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedGt_binary() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedGt_binary.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedLeq() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedLeq.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedLeq_binary() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedLeq_binary.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedLt() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedLt.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedLt_binary() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedLt_binary.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedEq() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedEq.cfr"), minimalUnboundedRange)
    }

    @Test
    fun nestedEq_binary() {
        testSpec(IrGraph.createFromClasspath("/clafer/nestedEq_binary.cfr"), minimalUnboundedRange)
    }

    @Test
    fun constraints() {
        testSpec(IrGraph.createFromClasspath("/clafer/positive/constraints.cfr"), minimalUnboundedRange)
    }

    @Test
    fun i19() {
        testSpec(IrGraph.createFromClasspath("/clafer/positive/i19.cfr"), minimalUnboundedRange)
    }

    @Test
    fun i49_resolve_ancestor() {
        testSpec(IrGraph.createFromClasspath("/clafer/positive/i49_resolve_ancestor.cfr"), minimalUnboundedRange)
    }


    @Test
    fun nestedInheritanceWithReference() {
        testSpec(IrGraph.createFromClasspath("/clafer/positive/nestedInheritanceWithReference.cfr"), minimalUnboundedRange)
    }

    @Test
    fun multisetOps1() {
        testSpec(IrGraph.createFromClasspath("/clafer/multisetOps1.cfr"), minimalUnboundedRange)
    }

    private fun testSpec(graph: IrGraph, minimalUnboundedRange: Int) {
        graph.flatten()

        for (node in graph.nodes) {
//        if (flatMod.clafers.isNotEmpty()) {
//            val c = flatMod.clafers.first()
            if (!node.isAbstract) {
                val (lb, ubPre) = graph.cmult(node)

                val ub = if (ubPre < 0) lb + minimalUnboundedRange else ubPre

                for (ivVal in lb..ub) {
                    logger.info { "Checking clafer ${node.name} for interval value $ivVal." }


                    val modifiedGraph = createModifiedModel(graph, node, ivVal)

//                    val alloyConsistent = AlloyConsistencyChecker().check(modifiedMod, 14, 180, TimeUnit.SECONDS)
//                    logger.info { "Alloy detected ${if (alloyConsistent) "consistency" else "inconsistency"} of spec of clafer ${c.name.id} for sub-interval $ivVal." }
//
//
//                    val chocoConsistent = ChocoConsistencyChecker().check(modifiedMod, 5, 30, TimeUnit.SECONDS)
//                    logger.info { "Choco detected ${if (chocoConsistent) "consistency" else "inconsistency"} of spec of clafer ${c.name.id} for sub-interval $ivVal." }
                    val path = graph.pathFromRootAsPath(node)
                    val res = BoundAnalyzer().analyzeBounds(path, IlpAnalyzer.AnalysisTarget.CLAFER_MULT, modifiedGraph)
//                    logger.info { "Boundanalyzer detected ${res.lbIlpRes.statistics.status} for spec of clafer ${node.name} for sub-interval $ivVal." }
//                    logger.info { "Boundanalyzer detected ${res.lbIlpRes.statistics.status} forclafer ${node.name} for sub-interval $ivVal." }


                    //TODO Fix
//                    Assertions.assertTrue((/*alloyConsistent && chocoConsistent && */res.lbIlpRes.statistics.status == Result.SolverStatus.OPTIMAL && (res.ubIlpRes.statistics.s || res.ubIlpRes.statistics.isUnbounded)) ||
//                            (/*!alloyConsistent && !chocoConsistent &&*/ !res.lbIlpRes.statistics.isFeasible && !res.ubIlpRes.statistics.isFeasible))


                }
            }

        }
    }

    fun createModifiedModel(graph: IrGraph, node: Node, intervalVal: Int): IrGraph {
        val fixClaferCstr = Some(SetElement(graph.pathFromRootAsPath(node)))

        if (graph.getParent(node) == null) {
            val fixClaferNoCstr = Eq(Card(SetElement(graph.pathFromRootAsPath(node))), IntVal(intervalVal))

            val copy = graph.copy()
            val cstrs = copy.cstrs.getOrPut(copy.cRoot) { emptyList() }
            copy.cstrs[copy.cRoot] = cstrs + listOf(fixClaferNoCstr)

            return copy
        } else {

            val fixClaferNoCstr = Eq(Card(SetElement(Path(listOf(ThisIdentifier, Identifier(node.name))))), IntVal(intervalVal))

            val parent = graph.getParent(node)!!

            val copy = graph.copy()
            val cstrs = copy.cstrs.getOrPut(parent) { emptyList() }
            copy.cstrs[parent] = cstrs + listOf(fixClaferNoCstr)

            if (intervalVal != 0) {
                val copy = graph.copy()
                val cstrs = copy.cstrs.getOrPut(copy.cRoot) { emptyList() }
                copy.cstrs[copy.cRoot] = cstrs + listOf(fixClaferCstr)
            }
            return copy
        }

    }

}