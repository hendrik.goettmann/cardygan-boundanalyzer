package model

import org.cardygan.boundanalyzer.antlr.ClaferCompiler
import org.cardygan.boundanalyzer.antlr.ClaferParser
import org.cardygan.boundanalyzer.antlr.ParserTest
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import ir.IrCreator
import org.junit.Test

class TreeCreatorTest {

    @Test
    @Throws(Exception::class)
    fun testPositive() {
        val dirPath = "/clafer/positive/"
        val claferFileNames = ResourceUtil.getResourceFiles(dirPath)
        for (claferFileName in claferFileNames) {
            println("> Testing clafer model " + claferFileName)
            testSpec(dirPath + claferFileName)
        }
    }

    @Test
    fun testMinimalRefInit() {
        testSpec("/clafer/minimalRefInit.cfr")
    }


    @Test
    fun refDisambiguationII() {
        testSpec("/clafer/positive/i205-ref-disambiguation-II.cfr")
    }

    @Test
    fun NestedInheritanceAndReference2() {
        testSpec("/clafer/positive/NestedInheritanceAndReference2.cfr")
    }

    fun testSpec(file: String) {
        val model = ResourceUtil.transformStreamToString(ParserTest::class.java.getResourceAsStream(file))
        val parser = ClaferParser.parseFromString(ClaferCompiler.compile(model))

        println(ClaferCompiler.compile(model))

        val mod = IrCreator(parser).create()
    }

}