package model

import ir.*
import ir.constraints.VisitorParams
import ir.constraints.visitAst
import ir.transform.*
import model.ir.*
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test
import java.io.File


class TreeFlattenerTest : AbstractIrTest() {

    private fun runTest(path: String, isDesugared: Boolean = false, withDot: Boolean = true) {
        graph = if (isDesugared) loadDesugaredTestSpec(path) else loadTestSpec(path)

        discardUnsupportedConstraints(graph)

        val fileNameWithoutExtension = File(path).let {
            it.name.replace(".${it.extension}", "")
        }
        if (withDot) genDotFile("${fileNameWithoutExtension}_0_orig.cfr", graph)

        pushDownOutgoingReferences(graph)

        if (withDot) genDotFile("${fileNameWithoutExtension}_1_pushedDownOutgoing.cfr", graph)

        pushDownIncomingReferences(graph)

        if (withDot) genDotFile("${fileNameWithoutExtension}_2_pushedDownIncoming.cfr", graph)

        flattenSubtree(graph.cRoot, graph)

        if (withDot) genDotFile("${fileNameWithoutExtension}_3_flat.cfr", graph)

        flattenRefs(graph)

        flattenConstraints(graph)

        flattenNumExprs(graph)

        if (withDot) genDotFile("${fileNameWithoutExtension}_4_cstrs.cfr", graph)

        removeAbstractNodes(graph)

        if (withDot) genDotFile("${fileNameWithoutExtension}_5_after.cfr", graph)
    }

    @Test
    fun testbasicAbc() {
        runTest("/clafer/basicAbc.cfr")
        n("a") isNestedIn graph.cRoot
        n("b") isNestedIn n("a")
        n("c") isNestedIn n("a")
    }

    @Test
    fun testnumRefInheritance() {
        runTest("/clafer/numRefInheritance.cfr")
        n("c") isNestedIn graph.cRoot
        n("a") isNestedIn root
        n("b") isNestedIn root
        n("a.x") isNestedIn n("a")
        n("b.x") isNestedIn n("b")
        root hasCstr "a.x.dref > 4"
        root hasCstr "b.x.dref > 4"
    }

    @Test
    fun deepDrefPath_example() {
        runTest("/clafer/deepDrefPath_example.cfr")
    }


    @Test
    fun pathWithParent() {
        runTest("/clafer/pathWithParent.cfr")
        chkGraph(6, 5, 0, 1, 1, 0, 1)
        n("e") isNestedIn graph.cRoot
        n("b") isNestedIn n("e")
        n("c") isNestedIn graph.cRoot
        n("d") isNestedIn n("c")
        n("d") references n("e.b")
        n("d") hasSetRef "e.b"
        n("c") hasCstr "some this.d.dref.parent"
    }


    @Test
    fun cloneReferencingClone() {
        runTest("/clafer/cloneReferencingClone.cfr")
        chkGraph(12, 11, 0, 8, 4, 0, 0)
        n("r") isNestedIn graph.cRoot
        n("i") isNestedIn graph.cRoot
        n("g") isNestedIn n("r")
        n("h") isNestedIn n("g")
        n("r.g.f") isNestedIn n("g")
        n("r.g.h.f") isNestedIn n("h")
        n("c") isNestedIn n("i")
        n("e") isNestedIn n("i")
        n("i.c.b") isNestedIn n("c")
        n("i.e.b") isNestedIn n("e")
        n("r.g.f") references n("i.c.b")
        n("r.g.f") references n("i.e.b")
        n("r.g.h.f") references n("i.c.b")
        n("r.g.h.f") references n("i.e.b")
        n("i.c.b") references n("r.g.f")
        n("i.e.b") references n("r.g.f")
        n("i.c.b") references n("r.g.h.f")
        n("i.e.b") references n("r.g.h.f")
        n("i.c.b") hasSetRef "r.g.f ++ r.g.h.f"
        n("i.e.b") hasSetRef "r.g.f ++ r.g.h.f"
        n("r.g.h.f") hasSetRef "i.c.b ++ i.e.b"
        n("r.g.f") hasSetRef "i.c.b ++ i.e.b"

    }

    @Test
    fun pathWithAbstractNesting() {
        runTest("/clafer/pathWithAbstractNesting.cfr")

        chkGraph(8, 7, 0, 3, 3, 0, 1)
        "a" isNestedIn root
        "g" isNestedIn "a"
        "e" isNestedIn "a"
        "c" isNestedIn root
        "d" isNestedIn "g"
        "d" references "c"
        "e" references "g"
        "f" references "g"
        root hasCstr "#a.g.d.dref < 2"

    }


    @Test
    fun deepDrefPathWithParent4() {
        runTest("/clafer/deepDrefPathWithParent4.cfr")

        chkGraph(12, 11, 0, 10, 7, 0, 1)
        "r" isNestedIn root
        "c" isNestedIn "r"
        "e" isNestedIn "r"
        "r.c.b" isNestedIn "c"
        "r.e.b" isNestedIn "e"
        "i" isNestedIn root
        "g" isNestedIn root
        "h" isNestedIn "g"
        "g.f" isNestedIn "g"
        "g.h.f" isNestedIn "h"
        "r.c.b" references "g.h.f"
        "r.c.b" references "g.f"
        "r.e.b" references "g.h.f"
        "r.e.b" references "g.f"
        "g" references "i"
        "h" references "i"
        "i" references "c"
        "i" references "e"
        "r.c.b" hasSetRef "g.f ++ g.h.f"
        "r.e.b" hasSetRef "g.f ++ g.h.f"
        "g.f" hasSetRef "i"
        "g.h.f" hasSetRef "i"
        "i" hasSetRef "c ++ e"
        "g" hasSetRef "i"
        "h" hasSetRef "i"
        "r" hasCstr "some this.c.b.dref.dref.dref.b.dref.dref.dref.b.dref.parent.dref ++ this.e.b.dref.dref.dref.b.dref.dref.dref.b.dref.parent.dref"
    }

    @Test
    fun deepDrefPathWithParent3() {
        runTest("/clafer/deepDrefPathWithParent3.cfr")

        chkGraph(12, 11, 0, 8, 4, 0, 1)
        "r" isNestedIn root
        "f" isNestedIn "r"
        "e" isNestedIn "r"
        "r.f.b" isNestedIn "f"
        "r.e.b" isNestedIn "e"
        "g" isNestedIn root
        "h" isNestedIn "g"
        "i" isNestedIn "h"
        "g.d" isNestedIn "g"
        "g.h.i.d" isNestedIn "i"
        "r.f.b" references "g"
        "r.e.b" references "i"
        "g.d" references "f"
        "g.h.i.d" references "e"
        "r.f.b" hasSetRef "g ++ i"
        "r.e.b" hasSetRef "g ++ i"
        "g.d" hasSetRef "e ++ f"
        "g.h.i.d" hasSetRef "e ++ f"

        "r" hasCstr "some this.e.b.dref.d.dref.b.dref.parent ++ this.f.b.dref.d.dref.b.dref.parent"
    }

    @Test
    fun deepDrefPathWithParent2() {
        runTest("/clafer/deepDrefPathWithParent2.cfr")

        chkGraph(13, 12, 0, 8, 4, 0, 1)
        "r" isNestedIn root
        "f" isNestedIn "r"
        "e" isNestedIn "r"
        "r.f.b" isNestedIn "f"
        "r.e.b" isNestedIn "e"
        "j" isNestedIn root
        "k" isNestedIn "j"
        "g" isNestedIn "j"
        "i" isNestedIn "j"
        "j.i.d" isNestedIn "i"
        "j.g.d" isNestedIn "g"
        "r.f.b" references "i"
        "r.f.b" references "g"
        "r.f.b" hasSetRef "g ++ i"
        "r.e.b" references "i"
        "r.e.b" references "g"
        "r.e.b" hasSetRef "g ++ i"
        "j.i.d" references "e"
        "j.i.d" references "f"
        "j.i.d" hasSetRef "e ++ f"
        "j.g.d" references "e"
        "j.g.d" references "f"
        "j.g.d" hasSetRef "e ++ f"
        "r" hasCstr "some this.e.b.dref.d.dref.b.dref.parent ++ this.f.b.dref.d.dref.b.dref.parent"
    }

    @Test
    fun deepDrefPathWithParent() {
        runTest("/clafer/deepDrefPathWithParent.cfr")

        chkGraph(10, 9, 0, 6, 4, 0, 1)

        "c" isNestedIn root
        "e" isNestedIn "c"
        "d" isNestedIn "c"
        "g" isNestedIn "e"
        "c.e.b" isNestedIn "e"
        "c.e.b.f" isNestedIn "c.e.b"
        "c.d.b" isNestedIn "d"
        "c.d.b.f" isNestedIn "c.d.b"
        "c.e.b.f" references "g"
        "c.e.b.f" hasSetRef "g"
        "c.d.b.f" references "g"
        "c.d.b.f" hasSetRef "g"
        "c.d.b" references "e"
        "c.d.b" references "d"
        "c.d.b" hasSetRef "d ++ e"
        "c.e.b" references "e"
        "c.e.b" references "d"
        "c.e.b" hasSetRef "d ++ e"
        "c" hasCstr "some this.d.b.dref.b ++ this.e.b.dref.b"
    }

    @Test
    fun deepDrefPath() {
        runTest("/clafer/deepDrefPath.cfr")

        chkGraph(7, 6, 0, 4, 2, 0, 1)
        "c" isNestedIn root
        "d" isNestedIn "c"
        "e" isNestedIn "c"
        "c.d.b" isNestedIn "d"
        "c.e.b" isNestedIn "e"
        "c.d.b" references "d"
        "c.d.b" references "e"
        "c.d.b" hasSetRef "d ++ e"
        "c.e.b" references "d"
        "c.e.b" references "e"
        "c.e.b" hasSetRef "d ++ e"
        "c" hasCstr "some this.d.b.dref.b.dref.b ++ this.e.b.dref.b.dref.b"
    }


    @Test
    fun testInheritanceRefinement3() {
        runTest("/clafer/inheritanceRefinement3.cfr")

        chkGraph(6, 5, 0, 2, 2, 0, 0)
        "b" isNestedIn root
        "e" isNestedIn root
        "c0_c" isNestedIn "b"
        "c1_c" isNestedIn "b"
        "c1_c" references "e"
        "c0_c" references "e"
        "c1_c" hasSetRef "e"
        "c0_c" hasSetRef "e"

    }

    @Test
    fun testInheritanceNumExpr() {
        runTest("/clafer/inheritanceNumExpr.cfr")

        chkGraph(12, 11, 0, 4, 0, 0, 4)
        "c" isNestedIn root
        "d" isNestedIn root
        "c.b" isNestedIn "c"
        "d.b" isNestedIn "d"
        "g" isNestedIn root
        "h" isNestedIn root
        "h.f" isNestedIn "h"
        "g.f" isNestedIn "g"
        "c.b" references Node.NodeType.INT
        "d.b" references Node.NodeType.INT
        "h.f" references Node.NodeType.INT
        "g.f" references Node.NodeType.INT
        root hasCstr "c.b.dref < 4 && h.f.dref < 2"
        root hasCstr "c.b.dref < 4 && g.f.dref < 2"
        root hasCstr "d.b.dref < 4 && h.f.dref < 2"
        root hasCstr "d.b.dref < 4 && g.f.dref < 2"
    }

    @Test
    fun testInheritanceNumExpr2() {
        runTest("/clafer/inheritanceNumExpr2.cfr")

        chkGraph(13, 12, 0, 4, 0, 0, 4)
        "r" isNestedIn root
        "c" isNestedIn "r"
        "d" isNestedIn "r"
        "r.c.b" isNestedIn "c"
        "r.d.b" isNestedIn "d"
        "g" isNestedIn "r"
        "h" isNestedIn "r"
        "r.h.f" isNestedIn "h"
        "r.g.f" isNestedIn "g"
        "r.c.b" references Node.NodeType.INT
        "r.d.b" references Node.NodeType.INT
        "r.h.f" references Node.NodeType.INT
        "r.g.f" references Node.NodeType.INT
        "r" hasCstr "r.c.b.dref < 4 && r.h.f.dref < 2"
        "r" hasCstr "r.c.b.dref < 4 && r.g.f.dref < 2"
        "r" hasCstr "r.d.b.dref < 4 && r.h.f.dref < 2"
        "r" hasCstr "r.d.b.dref < 4 && r.g.f.dref < 2"
    }

    @Test
    fun testInheritanceNumExpr3() {
        runTest("/clafer/inheritanceNumExpr3.cfr")

        chkGraph(17, 16, 0, 8, 0, 0, 15)
        "r" isNestedIn root
        "c" isNestedIn "r"
        "d" isNestedIn "r"
        "r.c.b" isNestedIn "c"
        "r.d.b" isNestedIn "d"
        "g" isNestedIn "r"
        "h" isNestedIn "r"
        "r.h.f" isNestedIn "h"
        "r.g.f" isNestedIn "g"
        "j" isNestedIn "r"
        "r" references Node.NodeType.INT
        "j" references Node.NodeType.INT
        "r.c.b" references Node.NodeType.INT
        "r.d.b" references Node.NodeType.INT
        "r.h.f" references Node.NodeType.INT
        "r.g.f" references Node.NodeType.INT
        root hasCstr "r.j.dref > 3"
        "r" hasCstr "r.c.b.dref < 4 && r.h.f.dref < 2"
        "r" hasCstr "r.c.b.dref < 4 && r.g.f.dref < 2"
        "r" hasCstr "r.d.b.dref < 4 && r.h.f.dref < 2"
        "r" hasCstr "r.d.b.dref < 4 && r.g.f.dref < 2"
        "r" hasCstr "this.dref > 5"
        "r" hasCstr " #((this.c ++ this.d) ++ (this.g ++ this.h)) > r.c.b.dref"
        "r" hasCstr " #((this.c ++ this.d) ++ (this.g ++ this.h)) > r.d.b.dref"
        "c" hasCstr "this.dref > 3"
        "c" hasCstr "this.b.dref < 3"
        "c" hasCstr "this.b.dref > 2"
        "d" hasCstr "this.b.dref < 2"
        "d" hasCstr "this.b.dref > 2"
        "g" hasCstr "this.f.dref < 1"
        "h" hasCstr "this.f.dref < 3"

    }

    @Test
    fun testbasicAbcConstraint() {
        runTest("/clafer/basicAbcWithConstraint.cfr")

        chkGraph(5, 4, 0, 0, 0, 0, 0)
        "a" isNestedIn root
        "b" isNestedIn "a"
        "c" isNestedIn "a"
    }

    @Test
    fun testbasicAbcInheritanceWithConstraint() {
        runTest("/clafer/basicAbcInheritanceWithConstraint.cfr")

        chkGraph(4, 3, 0, 0, 0, 0, 1)
        "c" isNestedIn root
        "b" isNestedIn "c"

        root hasCstr "(#c >= #c.b) && (#c >= #c.b)"

    }

    @Test
    fun testbasicAbcInheritanceWithConstraint2() {
        runTest("/clafer/basicAbcInheritanceWithConstraint2.cfr")

        chkGraph(6, 5, 0, 0, 0, 0, 1)
        "c" isNestedIn root
        "d" isNestedIn root
        "c.b" isNestedIn "c"
        "d.b" isNestedIn "d"
        root hasCstr "#(c ++ d) >= #(c.b ++ d.b)"
    }

    @Test
    fun testbasicAbcInheritanceWithConstraint3() {
        runTest("/clafer/basicAbcInheritanceWithConstraint3.cfr")

        chkGraph(10, 9, 0, 0, 0, 0, 1)
        "c" isNestedIn root
        "c.b" isNestedIn "c"
        "c.b.e" isNestedIn "c.b"
        "c.b.e.f" isNestedIn "c.b.e"
        "d" isNestedIn root
        "d.b" isNestedIn "d"
        "d.b.e" isNestedIn "d.b"
        "d.b.e.f" isNestedIn "d.b.e"
        root hasCstr "#(c ++d) >= #(c.b.e.f ++ d.b.e.f) + #(c.b.e ++ d.b.e)"
    }

    @Test
    fun testinheritanceRefinement2() {
        runTest("/clafer/inheritanceRefinement2.cfr")

        chkGraph(5, 4, 0, 0, 0, 0, 0)
        "c" isNestedIn root
        "f" isNestedIn "c"
        "d" isNestedIn "c"
    }

    @Test
    fun testbasicAbcInheritanceWithRefs() {
        runTest("/clafer/basicAbcInheritanceWithRefs.cfr")

        chkGraph(6, 5, 0, 4, 2, 0, 0)
        "c" isNestedIn root
        "d" isNestedIn root
        "c.b" isNestedIn "c"
        "d.b" isNestedIn "d"
        "c.b" references "c"
        "c.b" references "d"
        "c.b" hasSetRef "c ++ d"
        "d.b" references "d"
        "d.b" references "c"
        "d.b" hasSetRef "c ++ d"
    }

    @Test
    fun testComplexInheritance() {
        runTest("/clafer/complexInheritance.cfr")

        chkGraph(26, 25, 0, 0, 0, 0, 0)
        "f" isNestedIn root
        "f.g" isNestedIn "f"
        "f.g.n" isNestedIn "f.g"
        "f.s" isNestedIn "f"
        "f.s.t" isNestedIn "f.s"
        "f.s.t.v" isNestedIn "f.s.t"
        "f.s.t.v.w" isNestedIn "f.s.t.v"
        "f.s.t.v.w.x" isNestedIn "f.s.t.v.w"
        "f.j" isNestedIn "f"
        "f.j.m" isNestedIn "f.j"
        "f.j.r" isNestedIn "f.j"
        "f.j.r.q" isNestedIn "f.j.r"
        "k" isNestedIn root
        "k.g" isNestedIn "k"
        "k.g.n" isNestedIn "k.g"
        "k.s" isNestedIn "k"
        "k.s.t" isNestedIn "k.s"
        "k.s.t.v" isNestedIn "k.s.t"
        "k.s.t.v.w" isNestedIn "k.s.t.v"
        "k.s.t.v.w.x" isNestedIn "k.s.t.v.w"
        "k.j" isNestedIn "k"
        "k.j.m" isNestedIn "k.j"
        "k.j.r" isNestedIn "k.j"
        "k.j.r.q" isNestedIn "k.j.r"
    }

    @Test
    fun nestedAbstract() {
        try {
            runTest("/clafer/nestedAbstract.cfr")
            fail("Exception is expected.")
        } catch (e: AssertionError) {

        }
    }

    @Test
    fun nestedInheritanceWithReference() {
        runTest("/clafer/positive/nestedInheritanceWithReference.cfr")
        chkGraph(11, 10, 0, 10, 2, 0, 2)
        "SystemX" isNestedIn root
        "SystemY" isNestedIn root
        "SystemX.con1" isNestedIn "SystemX"
        "SystemX.con2" isNestedIn "SystemX"
        "SystemY.con3" isNestedIn "SystemY"
        "SystemY.con4" isNestedIn "SystemY"
        "SystemY.con4" isNestedIn "SystemY"
        "SystemX.connections" isNestedIn "SystemX"
        "SystemY.connections" isNestedIn "SystemY"
        "SystemX.connections" references "SystemX.con1"
        "SystemX.connections" references "SystemX.con2"
        "SystemX.connections" references "SystemY.con3"
        "SystemY.connections" references "SystemY.con4"
        "SystemY.connections" references "SystemY.con5"
        "SystemY.connections" references "SystemX.con1"
        "SystemY.connections" references "SystemX.con2"
        "SystemY.connections" references "SystemY.con3"
        "SystemY.connections" references "SystemY.con4"
        "SystemY.connections" references "SystemY.con5"
        "SystemX.connections" hasSetRef "con1 ++ con2 ++ con3 ++ con4 ++ con5"
        "SystemY.connections" hasSetRef "con1 ++ con2 ++ con3 ++ con4 ++ con5"
        "SystemX" hasCstr "this.connections.dref = this.con1 ++ this.con2"
        "SystemY" hasCstr "this.connections.dref = this.con3 ++ this.con4 ++ this.con5"
    }

    @Test
    fun `EAST-ADL-PowerWindow`() {
        runTest("/clafer/positive/EAST-ADL-PowerWindow.cfr")
    }

    @Test
    fun AADL_Complete() {
        runTest("/clafer/eval_des/AADL_Complete.cfr.des", isDesugared = true, withDot = false)
    }

    @Test
    fun ACCDemo_fullClafer() {
        runTest("/clafer/positive/ACCDemo_fullClafer.cfr")

    }


    @Test
    fun ACCDemo_attributedFeatureModels() {
        runTest("/clafer/positive/ACCDemo_attributedFeatureModels.cfr")

    }

    @Test
    fun ClaferTools_ToolingArchitecture() {
        runTest("/clafer/positive/ClaferTools_ToolingArchitecture.cfr")
    }

    @Test
    fun cyclesNestingInheritance() {
        runTest("/clafer/positive/i101.cfr")
    }

    @Test
    fun cyclesNestingInheritance2() {
        runTest("/clafer/cyclesNestingInheritance2.cfr")
    }


    @Test
    fun Telematics_PLA() {
        runTest("/clafer/positive/Telematics_PLA.cfr")
    }

    @Test
    fun EDMPoster_Intro() {
        runTest("/clafer/positive/EDMPoster_Intro.cfr")
        chkConsistency()
    }

    @Test
    fun i122_CVL() {
        runTest("/clafer/positive/i122-CVL.cfr")
        chkConsistency()
    }

    @Test
    fun i50_stop_following_references() {
        runTest("/clafer/positive/i50_stop_following_references.cfr")
        chkConsistency()
    }


    @Test
    @Throws(Exception::class)
    fun testPositive() {
        val dirPath = "/clafer/positive/"

        // list of specs of which flattening detects that they do not yield valid instances
//        val skip = listOf("nestedInheritanceWithReference.cfr", "NestedInheritanceAndReference2.cfr")
        val skip = emptyList<String>()

        val claferFileNames = ResourceUtil.getResourceFiles(dirPath)
        for (claferFileName in claferFileNames - skip) {
            println("> Testing clafer model " + claferFileName)
            runTest(dirPath + claferFileName)
            chkConsistency()

        }
    }

    private fun chkConsistency() {
        assertTrue(graph.nodes.none { it.isAbstract })
        assertTrue(graph.incoming.values.flatten().none { it.type == Edge.EdgeType.A })

        graph.cstrs.forEach { (ctx, cstrs) ->
            cstrs.forEach { cstr ->
                visitAst(cstr, VisitorParams(visitPath = { path ->
                    val evalGraph = graph.retrieveEvalGraph(path, ctx)
                    assertTrue(evalGraph.edges.flatten()
                            .map { elem -> setOf(elem.edge.src, elem.edge.trg) }
                            .flatten().none { n -> n.isAbstract })
                }))
            }

        }
    }

}
