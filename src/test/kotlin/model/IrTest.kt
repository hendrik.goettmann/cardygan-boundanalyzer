package model

import model.ir.AbstractIrTest
import ir.Node
import org.junit.Test

class IrTest : AbstractIrTest() {

    @Test
    fun basicTest() {
        graph = loadTestSpec("/clafer/basicAbc.cfr")

        chkGraph(4, 3)
        n("b") isNestedIn n("a")
        n("c") isNestedIn n("a")

        resolvesTo("b", "this.parent.c", listOf(n("c")))
        resolvesTo("c", "this.parent.b", listOf(n("b")))
        resolvesTo("c", "this.parent.c", listOf(n("c")))
        resolvesTo("b", "this.parent", listOf(n("a")))
        resolvesTo("c", "this", listOf(n("c")))
    }

    @Test
    fun basicAbcInheritanceWithRefs() {
        graph = loadTestSpec("/clafer/basicAbcInheritanceWithRefs.cfr")

        chkGraph(5, 4, 2, 1, 1)
        n("b") isNestedIn n("a")
        n("c") inheritsFrom n("a")
        n("d") inheritsFrom n("a")
        n("b") references n("a")

        resolvesTo("a", "this.b.dref", listOf(n("a")))
        resolvesTo("c", "this.b.dref", listOf(n("a")))
        resolvesTo("d", "this.b.dref", listOf(n("a")))
    }

    @Test
    fun nestedAbs() {
        graph = loadTestSpec("/clafer/nestedAbs.cfr")

        chkGraph(6, 5, 2, 1, 1)
        n("b") isNestedIn n("a")
        n("c") references n("b")
        n("d") inheritsFrom n("a")
        n("e") inheritsFrom n("a")

        resolvesTo("d", "this.parent.c.dref.parent", listOf(n("a")))
        resolvesTo("e", "this.b", listOf(n("b")))
        resolvesTo("c", "this.dref.parent.parent.d.b", listOf(n("b")))
    }

    @Test
    fun nestedGeq() {
        graph = loadTestSpec("/clafer/nestedGeq.cfr")

        chkGraph(4, 3, 0, 0, 0, 0, 2)
        n("b") isNestedIn n("a")
        resolvesTo("a", "this.b", listOf(n("b")))
    }

    @Test
    fun complexNamespaces() {
        graph = loadTestSpec("/clafer/complexNamespaces.cfr")

        chkGraph(30, 29, 9)
        n("w") isNestedIn n("x")
        n("v") isNestedIn n("w")
        n("r") isNestedIn n("v")
        n("c0") isNestedIn n("r")
        n("x.w.v.r.c0.j") isNestedIn n("c0")
        n("x.w.v.r.c0.j.ek") isNestedIn n("x.w.v.r.c0.j")
        n("x.w.v.r.c0.d0") isNestedIn n("c0")
        n("x.w.v.r.e") isNestedIn n("r")
        n("e3") isNestedIn n("r")
        n("e4") isNestedIn n("e3")
        n("y.z") isNestedIn n("y")
        n("y.z.e0") isNestedIn n("y.z")
        n("y.z.e0.j") isNestedIn n("y.z.e0")
        n("y.z.e0.j.ek") isNestedIn n("y.z.e0.j")
        n("y.z.e0.d0") isNestedIn n("y.z.e0")
        n("y.z.e") isNestedIn n("y.z")
        n("yy.z") isNestedIn n("yy")
        n("a") isNestedIn n("a0")
        n("a0.a.b") isNestedIn n("a")
        n("c.b") isNestedIn n("c")
        n("c.b.c") isNestedIn n("c.b")
        n("g") isNestedIn n("c.b.c")
        n("c.b.c.e") isNestedIn n("c.b.c")
        n("f") isNestedIn n("c.b.c.e")

        n("x.w.v.r.e") inheritsFrom n("c0")
        n("e3") inheritsFrom n("c0")
        n("y.z.e") inheritsFrom n("e0")
        n("c") inheritsFrom n("a0")
        n("c.b") inheritsFrom n("a0")
        n("c.b.c") inheritsFrom n("a")
        n("g") inheritsFrom n("a0")
        n("c.b.c.e") inheritsFrom n("a0.a.b")
        n("f") inheritsFrom n("a0")
    }

    @Test
    fun inheritanceNumExpr() {
        graph = loadTestSpec("/clafer/inheritanceNumExpr.cfr")

        chkGraph(11, 10, 4, 2, 0, 0, 1)
        n("b") isNestedIn n("a")
        n("f") isNestedIn n("e")
        n("c") inheritsFrom n("a")
        n("d") inheritsFrom n("a")
        n("g") inheritsFrom n("e")
        n("h") inheritsFrom n("e")
        n("b") references Node.NodeType.INT
        n("f") references Node.NodeType.INT

        resolvesToInt("c", "this.b.dref")
        resolvesToInt("d", "this.b.dref")
        resolvesToInt("g", "this.f.dref")
        resolvesToInt("h", "this.f.dref")
    }

    @Test
    fun refUnion() {
        graph = loadTestSpec("/clafer/refUnion.cfr")

        chkGraph(7, 6, 2, 3, 1)
        n("b") isNestedIn n("a")
        n("e") isNestedIn n("a")
        n("c") inheritsFrom n("a")
        n("d") inheritsFrom n("a")
        n("b") references n("c")
        n("b") references n("d")
        n("e") references Node.NodeType.INT

        resolvesTo("c", "this.b.dref", listOf(n("c"), n("d")))
        resolvesToInt("c", "this.e.dref")
    }


    @Test
    fun refUnionPaths() {
        graph = loadTestSpec("/clafer/refUnionPaths.cfr")

        chkGraph(8, 7, 2, 3, 1)
        n("b") isNestedIn n("a")
        n("e") isNestedIn n("a")
        n("c") isNestedIn n("x")
        n("d") isNestedIn n("x")
        n("c") inheritsFrom n("a")
        n("d") inheritsFrom n("a")
        n("b") references n("c")
        n("b") references n("d")
        n("e") references Node.NodeType.INT

        resolvesTo("x", "this.c.b.dref", listOf(n("c"), n("d")))
        resolvesToInt("x", "this.d.e.dref")
    }

    @Test
    fun refDisambiguationII() {
        graph = loadTestSpec("/clafer/positive/i205-ref-disambiguation-II.cfr")

        chkGraph(4, 3, 0, 1, 1, 0, 1)
        n("owner") isNestedIn n("Car")
        n("owner") references n("Person")
    }


    @Test
    fun NestedInheritanceAndReference2() {
        graph = loadTestSpec("/clafer/positive/NestedInheritanceAndReference2.cfr")

        chkGraph(15, 14, 6, 3, 2, 0, 7)
        n("Port") isNestedIn n("Component")
        n("TemperaturePort") isNestedIn n("Component")
        n("input") isNestedIn n("Port")
        n("output") isNestedIn n("Port")
        n("from") isNestedIn n("TemperatureConnector")
        n("to") isNestedIn n("TemperatureConnector")
        n("sensor.temperature") isNestedIn n("sensor")
        n("controller.temperature") isNestedIn n("controller")
        n("TemperaturePort") inheritsFrom n("Port")
        n("sensor") inheritsFrom n("Component")
        n("controller") inheritsFrom n("Component")
        n("sensor.temperature") inheritsFrom n("TemperaturePort")
        n("controller.temperature") inheritsFrom n("TemperaturePort")
        n("con1") inheritsFrom n("TemperatureConnector")
        n("TemperaturePort") references Node.NodeType.INT
        n("from") references n("TemperaturePort")
        n("to") references n("TemperaturePort")
    }


}