package ilp

import ilp.optimize.MsetOptimizer
import ir.Node
import mset.*
import mset.transform.MContext
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test


internal class MsetOptimizerTest {

    @Test
    fun testMvarReplacement() {
        val a = Node("a")
        val b = Node("b")
        val c = Node("c")

        val mC = CFunc(setOf(a))
        val mP = CFunc(setOf(a))
        val mQ = CFunc(setOf(a))
        val mR = CFunc(setOf(a))
        val mR2 = CFunc(setOf(b))
        val mS = CFunc(setOf(b))
        val mT = CFunc(setOf(b))
        val mU = CFunc(setOf(b))

        val mR3 = CFunc(setOf(c))

        val conds = mutableListOf<MBoolExpr>()

        conds.add(mC.getVar(a) eq mP.getVar(a))
        conds.add(mP.getVar(a) eq mQ.getVar(a))
        conds.add(mQ.getVar(a) eq mR.getVar(a))
        conds.add(mP.getVar(a) geq mC.getVar(a) + mP.getVar(a) + mQ.getVar(a) + mR.getVar(a) + IntLit(4))

        conds.add(mC.getVar(a) eq mR3.getVar(c))

        conds.add(mR2.getVar(b) eq mS.getVar(b))
        conds.add(mQ.getVar(a) eq mT.getVar(b))
        conds.add(mU.getVar(b) eq mR2.getVar(b))
        conds.add(mT.getVar(b) geq mR2.getVar(b) + mT.getVar(b) + mS.getVar(b) + mR.getVar(a) + mU.getVar(b) + IntLit(4))

        conds.shuffle()

        val mRefs = mutableMapOf(a to mR, b to mR2, c to mR3)

        val ctx = MContext(mC, mutableListOf(mC, mP, mQ, mR, mR2, mS, mT, mU, mR3), conds, mRefs, mutableMapOf(), mutableMapOf())

        MsetOptimizer(ctx).doOptimize()

        assertEquals(4, ctx.conds.size)
        assertTrue(ctx.conds.contains(mC.getVar(a) geq mC.getVar(a) + mC.getVar(a) + mC.getVar(a) + mC.getVar(a) + IntLit(4)))

        assertTrue(ctx.conds.contains(mC.getVar(a) geq mR2.getVar(b) + mC.getVar(a) + mR2.getVar(b) + mC.getVar(a) + mR2.getVar(b) + IntLit(4)))

        assertEquals(4, ctx.mAll.size)
    }


    @Test
    fun testParamReplacement() {
        val a = Node("a")

        val mC = CFunc(setOf(a))
        val mP = CFunc(setOf(a))
        val mQ = CFunc(setOf(a))
        val mR = CFunc(setOf(a))


        val conds = mutableListOf<MBoolExpr>()

        conds.add(mC.getVar(a) eq mP.getVar(a))
        conds.add(mP.getVar(a) eq mQ.getVar(a))
        conds.add(mQ.getVar(a) eq mR.getVar(a))
        conds.add(mR.getVar(a) eq IntLit(1))
        conds.add(mP.getVar(a) geq mC.getVar(a) + mP.getVar(a) + mQ.getVar(a) + mR.getVar(a) + IntLit(4))

        conds.shuffle()

        val mRefs = mutableMapOf(a to mR)

        val ctx = MContext(mC, mutableListOf(mC, mP, mQ, mR), conds, mRefs, mutableMapOf(), mutableMapOf())

        MsetOptimizer(ctx).doOptimize()

        assertEquals(3, ctx.conds.size)
        assertTrue(ctx.conds.contains(IntLit(1) geq IntLit(1) + IntLit(1) + IntLit(1) + IntLit(1) + IntLit(4)))

        assertEquals(2, ctx.mAll.size)
    }


    @Test
    fun testSimplification() {
        val a = Node("a")

        val mC = CFunc(setOf(a))


        val conds = mutableListOf<MBoolExpr>()


        conds.add((IntLit(3) geq mC.getVar(a)) and (IntLit(3) leq mC.getVar(a)))

        conds.shuffle()

        val ctx = MContext(mC, mutableListOf(mC), conds, mutableMapOf(), mutableMapOf(), mutableMapOf())

        MsetOptimizer(ctx).doOptimize()

        assertEquals(1, ctx.conds.size)
        assertTrue(ctx.conds.contains(mC.getVar(a) eq IntLit(3)))

        assertEquals(1, ctx.mAll.size)
    }
}