package phd

import ilp.IlpAnalyzer
import ir.*
import ir.constraints.*
import clafer.AlloyConsistencyChecker
import clafer.ChocoConsistencyChecker
import clafer.SolverRes
import ir.transform.flatten
import mu.KotlinLogging
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import org.junit.AfterClass
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import util.writeStatsToJsonFile
import java.io.BufferedWriter
import java.io.File
import java.lang.IllegalArgumentException
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit


private val reports = kotlin.collections.mutableListOf<EvalRQ1.Report>()

@RunWith(Parameterized::class)
class EvalRQ1 {

    @Parameterized.Parameter(0)
    lateinit var specFile: String

    @Parameterized.Parameter(1)
    lateinit var path: String

    private val defaultScope = 15 to 5
    private val timeout: Long = System.getProperty("SOLVER_TIMEOUT").toLong()


    private val logger = KotlinLogging.logger {}

    private val scopes = mutableMapOf(
            // "filepath" to (scope to bitwidth)
            "/clafer/positive/enforcingInverseReferences.cfr" to (35 to 8),
            "/clafer/positive/assertionsAndScopes.cfr" to (8 to 4),
            "/clafer/positive/i131-incorrect-scope-diag.cfr" to (15 to 5),
            "/clafer/positive/i131-incorrect-scope.cfr" to (8 to 8),
            "/clafer/positive/nestedInheritanceWithReference.cfr" to (20 to 8),
            "/clafer/positive/i71.cfr" to (20 to 8),
            "/clafer/positive/scopes.cfr" to (36 to 9),
            "/clafer/positive/ClaferTools_ToolingArchitecture.cfr" to (20 to 6),
            "/clafer/positive/ACCDemo_attributedFeatureModels.cfr" to (25 to 8)

    )

    companion object {

        private val logger = KotlinLogging.logger {}
        private val outputDir: String = System.getProperty("OUTPUT_DIR")
        private val stats = AstAnalyzerStats()

        @JvmStatic
        @Parameterized.Parameters(name = "\"{0}\" - {1}")
        fun specs(): List<Array<String>> {
            val claferFileNames = ResourceUtil.getResourceFiles("/clafer/positive/").map { "/clafer/positive/$it" }
                    .filter { !it.endsWith("ClaferTools_ToolingArchitecture.cfr") }
//                    .filter { it.endsWith("constraints.cfr") }//.filter { it == "assertionsAndScopes.cfr" || it == "i131-incorrect-scope.cfr" }
            //.filter { it != "ClaferTools_ToolingArchitecture.cfr" && it != "ACCDemo_attributedFeatureModels.cfr" }

//                    .filter { it.endsWith("ClaferTools_ToolingArchitecture.cfr") }

//            val claferFileNames2 = ResourceUtil.getResourceFiles("/clafer/").map { "/clafer/$it" }
            val claferFileNames2 = emptyList<String>()

            fun createArgs(cpSpec: String): List<Array<String>> {
                logger.info { "LOADING: " + cpSpec }

                val (_, paths) = getPathGraph(cpSpec)
                return paths.map { arrayOf(cpSpec, it) }
//                return paths.filter { it == "c0_claferIG.c0_generator.c0_desClafer" }.map { arrayOf(cpSpec, it) }
            }

            val ret = (claferFileNames + claferFileNames2).filter { it.endsWith(".cfr") }.map { createArgs(it) }.flatten()
            return ret
        }

        @AfterClass
        @JvmStatic
        internal fun afterAll() {
            assert(File(outputDir).isDirectory) { "$outputDir must be a directory." }

            val time = ZonedDateTime.now(ZoneId.of("Europe/Paris"))
                    .format(DateTimeFormatter.ofPattern("uuuu-MM-dd_HHmmss"))
            val resFile = File(outputDir + File.separator + "${time}_rq1CorrectnessResults.csv")
            resFile.createNewFile()

            resFile.bufferedWriter().use { out ->
                reports.forEach {
                    out.writeLn("${it.specFile},  ${it.path},${it.type}, ${it.alloyRes},${it.chocoRes},${it.boundanalyzerRes}")
                }
            }

            // write stats about models
            writeStatsToJsonFile(outputDir, "overall", stats)
        }


        private fun getPathGraph(cpSpec: String): Pair<IrGraph, Set<String>> {
            val graph = IrGraph.createFromClasspath(cpSpec)
            graph.flatten()

            graph.analyze(stats)


            val paths = graph.nodes.filter { it.type == Node.NodeType.STD && it != graph.emptyset && it != graph.cRoot }
                    .map { n ->
                        graph.pathFromRootAsPath(n, true).elements.joinToString(".") { it.id }
                    }

            return Pair(graph, paths.toSet())
        }

        fun BufferedWriter.writeLn(line: String) {
            this.write(line)
            this.newLine()
        }
    }


//    @Test
//    fun `i131-incorrect-scope`() {
//        testCorrectnessScopes("/clafer/positive/i131-incorrect-scope.cfr", "c0_Dog.c0_leg", IlpAnalyzer.AnalysisType.SCOPE, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }
//
//    @Test
//    fun i101() {
//        testCorrectnessScopes("/clafer/positive/i101.cfr", "c0_pth.c0_p", IlpAnalyzer.AnalysisType.SCOPE, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }
//
//    @Test
//    fun liftingNestedConstraints() {
//        testCorrectnessScopes("/clafer/positive/liftingNestedConstraints.cfr", "c0_a.c0_b.c0_c.c0_d", IlpAnalyzer.AnalysisType.SCOPE, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }
//
//    @Test
//    fun `NECSIS-Workshop_Modeling_Quality_4`() {
//        testCorrectnessScopes("/clafer/positive/NECSIS-Workshop_Modeling_Quality_4.cfr", "c0_aCar.c0_ABS", IlpAnalyzer.AnalysisType.SCOPE, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }
//
//    @Test
//    fun scopes() {
//        testCorrectnessScopes("/clafer/positive/scopes.cfr", "c0_f", IlpAnalyzer.AnalysisType.SCOPE, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }
//
//    @Test
//    fun reals() {
//        testCorrectnessScopes("/clafer/positive/reals.cfr", "c0_interest", IlpAnalyzer.AnalysisType.SCOPE, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }


//    @Test
//    fun reals() {
//        testCorrectnessScopes("/clafer/positive/i78_transitive-closure.cfr", "c0_Week1AggregatesTo", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }


    //    @Test
//    fun assertionsAndScopes() {
//        testCorrectnessScopes("/clafer/positive/assertionsAndScopes.cfr", "c0_a", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
//    }
//
////    @Test
////    fun ClaferToolsDemo_Adding_Quality() {
////        testCorrectnessScopes("/clafer/positive/ClaferToolsDemo_Adding_Quality.cfr", "c0_OptimalPhone", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
////    }
//
//    @Test
//    fun enforcingInverseReferences() {
//        testCorrectnessScopes("/clafer/positive/enforcingInverseReferences.cfr", "c0_B", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
//    }
//
//    @Test
//    fun i122CVL() {
//        testCorrectnessScopes("/clafer/positive/i122-CVL.cfr", "c0_Office.c0_Printer", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
//    }
//
//    @Test
//    fun individualScopes() {
//        testCorrectnessScopes("/clafer/positive/i83-individual-scope.cfr", "c0_d", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
//    }
//
//    @Test
//    fun ifThenElse() {
//        testCorrectnessScopes("/clafer/positive/ifthenelse.cfr", "c0_allYellow", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
//    }
//
////    @Test
////    fun `NECSIS-Workshop_Modeling_Quality_4`() {
////        testCorrectnessScopes("/clafer/positive/NECSIS-Workshop_Modeling_Quality_4.cfr", "c0_aCar", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
////    }
//
//    @Test
//    fun `nestedInheritance`() {
//        testCorrectnessScopes("/clafer/positive/nestedInheritanceWithReference.cfr", "c0_SystemY", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
//    }
//
////    @Test
////    fun scopes() {
////        testCorrectnessScopes("/clafer/positive/scopes.cfr", "c0_H1", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
////    }
//
//    //
////    @Test
//    fun disambiguation() {
//        testCorrectnessScopes("/clafer/positive/i205-ref-disambiguation-II.cfr", "c0_car.c0_owner", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }
//
//    @Test
//    fun individualScopes() {
//        testCorrectnessScopes("/clafer/positive/i83-individual-scope.cfr", "c0_d.c0_f", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }
//
//    @Test
//    fun scopes() {
//        testCorrectnessScopes("/clafer/positive/scopes.cfr", "c0_H1.c0_H12", IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//    }
//
//

    @Test
    fun testAllPositiveScope() {
        testCorrectnessScopes(specFile, path, IlpAnalyzer.AnalysisType.SCOPE, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
    }

    @Test
    fun testAllPositiveCmult() {
        testCorrectnessScopes(specFile, path, IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
    }

    @Test
    fun testAllPositiveGmult() {
        testCorrectnessScopes(specFile, path, IlpAnalyzer.AnalysisType.BOUND, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
    }


    fun testCorrectnessScopes(specFile: String, path: String, type: IlpAnalyzer.AnalysisType, target: IlpAnalyzer.AnalysisTarget) {
        if (type == IlpAnalyzer.AnalysisType.SCOPE && target != IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
            throw IllegalArgumentException("Can only do scope analysis for clafer multiplicity.")

        logger.info { "testing correctness of SCOPES in spec $specFile for clafer $path." }

        val graph = IrGraph.createFromClasspath(specFile)

        if (type == IlpAnalyzer.AnalysisType.BOUND) {
            try {
                graph.getNode(path)
            } catch (e: NoSuchElementException) {
                logger.info { "Path $path is not available in unflattend spec. Skipping." }
                return
            }
        }
        graph.flatten()

        val node = graph.getNode(path)

        // check correctness for group multiplicity only of node has children
        if (target == IlpAnalyzer.AnalysisTarget.GROUP_MULT && graph.getNested(node).isEmpty()) {
            logger.info { "Node $node has no children. There is nothing to do while checking group multiplicity. Skipping" }
            return
        }

        val (lb, ub) = when {
            type == IlpAnalyzer.AnalysisType.SCOPE -> graph.lamdaMin(node) to (graph.lamdaMax(node) ?: -1)
            target == IlpAnalyzer.AnalysisTarget.CLAFER_MULT -> graph.cmult(node)
            target == IlpAnalyzer.AnalysisTarget.GROUP_MULT -> graph.gmult(node)
            else -> error("Unknown analysis target/type combination.")
        }

        // set node where additional constraint are nested.
        val ctxNode = when {
            type == IlpAnalyzer.AnalysisType.SCOPE -> graph.cRoot
            target == IlpAnalyzer.AnalysisTarget.CLAFER_MULT -> graph.getParent(node) ?: graph.cRoot
            target == IlpAnalyzer.AnalysisTarget.GROUP_MULT -> node
            else -> error("Unknown analysis target/type combination.")
        }
        val ctxPath = ctxNode.let { graph.pathFromRootAsPath(it, true).asString() }

        val testRes: MutableList<Pair<String, Boolean>> = mutableListOf()

        val res = IlpAnalyzer(graph).analyze(node, target, type)

        if (res.interval.lb is IlpAnalyzer.Infeasible) {
            logger.info { "Detected lower bound to yield infeasible result." }
            val resExp = check(specFile)
            reports.add(report(specFile, path, "${type.name} LB ${target.name} INFEASIBLE", resExp.first, resExp.second, SolverRes.INFEASIBLE))

            testRes.add(Pair("anomaly lower bound: alloy=${resExp.first} choco=${resExp.second}",
                    resExp.first == SolverRes.INFEASIBLE && resExp.second == SolverRes.INFEASIBLE))

        } else if (res.interval.lb is IlpAnalyzer.Feasible) {
            val lbOpt = (res.interval.lb as IlpAnalyzer.Feasible).value

            if (lbOpt > lb) {
                logger.info { "Anomalie at lower bound: lb < lb* : $lb < $lbOpt" }
                val cstrs = when {
                    type == IlpAnalyzer.AnalysisType.SCOPE -> listOf(Pair(ctxPath, Lt(Card(SetElement(Path(path))), IntVal(lbOpt))))
                    type == IlpAnalyzer.AnalysisType.BOUND && target == IlpAnalyzer.AnalysisTarget.CLAFER_MULT -> {
                        if (ctxNode == graph.cRoot)
                            listOf(Pair(ctxPath, Lt(Card(SetElement(Path(listOf(Identifier(node.name))))), IntVal(lbOpt))))
                        else
                            listOf(Pair(ctxPath, Lt(Card(SetElement(Path(listOf(ThisIdentifier, Identifier(node.name))))), IntVal(lbOpt))))
                    }

                    type == IlpAnalyzer.AnalysisType.BOUND && target == IlpAnalyzer.AnalysisTarget.GROUP_MULT -> {
                        val children = if (ctxNode == graph.cRoot)
                            error("Root clafer cannot have group multiplicity.")
                        else
                            graph.getNested(node)
                                    .map { Card(SetElement(Path(listOf(ThisIdentifier, Identifier(it.name))))) as NumExpr }
                                    .reduce { res, elem -> Add(res, elem) }
                        // nest "some ctxNode" in root  and  child_1 + ... + child n < lbOpt
                        listOf(Pair("", Some(SetElement(Path(ctxPath)))), Pair(ctxPath, Lt(children, IntVal(lbOpt))))
                    }
                    else -> error("Unknown target/type combination.")
                }
                logger.info {
                    "Checking lower bound with choco/alloy with additional constraint ${cstrs.joinToString("; ")
                    { "${prettyPrint(it.second)}}} nested in ${it.first}\"" }}"
                }

                val resExp = check(specFile, cstrs)

                reports.add(report(specFile, path, "${type.name} LB ${target.name}", resExp.first, resExp.second, SolverRes.FEASIBLE))

                testRes.add(Pair("anomaly lower bound: alloy=${resExp.first} choco=${resExp.second}",
                        resExp.first == SolverRes.INFEASIBLE && resExp.second == SolverRes.INFEASIBLE))

            } else {
                logger.info { "Detected feasible bound. No anomaly found." }
//                val resExp = check(specFile)
//                reports.add(report(specFile, path, "${type.name} LB ${target.name} FEASIBLE", resExp.first, resExp.second, SolverRes.FEASIBLE))
//                testRes.add(Pair("anomaly lower bound: alloy=${resExp.first} choco=${resExp.second}",
//                        resExp.first == SolverRes.FEASIBLE && resExp.second == SolverRes.FEASIBLE))
            }
        } else error("Lower bound of $node cannot should never be unbounded.")


        if (res.interval.ub is IlpAnalyzer.Infeasible) {
            logger.info { "Detected upper bound to yield infeasible result." }
            val resExp = check(specFile)
            reports.add(report(specFile, path, "${type.name} UB ${target.name} INFEASIBLE", resExp.first, resExp.second, SolverRes.INFEASIBLE))

            testRes.add(Pair("anomaly upper bound: alloy=${resExp.first} choco=${resExp.second}",
                    resExp.first == SolverRes.INFEASIBLE && resExp.second == SolverRes.INFEASIBLE))

        } else if (res.interval.ub is IlpAnalyzer.Feasible) {
            val ubOpt = (res.interval.ub as IlpAnalyzer.Feasible).value

            if (ubOpt < ub || (ub == -1)) {
                logger.info { "Anomalie at upper bound: ub* < ub : $ubOpt < $ub" }
                val cstrs = when {
                    type == IlpAnalyzer.AnalysisType.SCOPE -> listOf(Pair(ctxPath, Gt(Card(SetElement(Path(path))), IntVal(ubOpt))))
                    type == IlpAnalyzer.AnalysisType.BOUND && target == IlpAnalyzer.AnalysisTarget.CLAFER_MULT -> {
                        if (ctxNode == graph.cRoot)
                            listOf(Pair(ctxPath, Gt(Card(SetElement(Path(listOf(Identifier(node.name))))), IntVal(ubOpt))))
                        else
                            listOf(Pair(ctxPath, Gt(Card(SetElement(Path(listOf(ThisIdentifier, Identifier(node.name))))), IntVal(ubOpt))))
                    }
                    type == IlpAnalyzer.AnalysisType.BOUND && target == IlpAnalyzer.AnalysisTarget.GROUP_MULT -> {
                        val children = if (ctxNode == graph.cRoot)
                            error("Root clafer cannot have group multiplicity.")
                        else
                            graph.getNested(node)
                                    .map { Card(SetElement(Path(listOf(ThisIdentifier, Identifier(it.name))))) as NumExpr }
                                    .reduce { res, elem -> Add(res, elem) }
                        // some ctxNode && child_1 + ... + child n > lbOpt
                        listOf(Pair("", Some(SetElement(Path(ctxPath)))), Pair(ctxPath, Gt(children, IntVal(ubOpt))))
                    }
                    else -> error("Unknown target/type combination.")
                }
                logger.info {
                    "Checking lower bound with choco/alloy with additional constraint ${cstrs.joinToString("; ")
                    { "${prettyPrint(it.second)}}} nested in ${it.first}\"" }}"
                }

                val resExp = check(specFile, cstrs)
                reports.add(report(specFile, path, "$type UB ${target.name}", resExp.first, resExp.second, SolverRes.FEASIBLE))

                testRes.add(Pair("anomaly upper bound: alloy=${resExp.first} choco=${resExp.second}",
                        resExp.first == SolverRes.INFEASIBLE && resExp.second == SolverRes.INFEASIBLE))

            } else {
                logger.info { "Detected feasible bound. No anomaly found." }
//                val resExp = check(specFile)
//                reports.add(report(specFile, path, "${type.name} UB ${target.name} FEASIBLE", resExp.first, resExp.second, SolverRes.FEASIBLE))
//                testRes.add(Pair("analysis of upper bound: alloy=${resExp.first} choco=${resExp.second}",
//                        resExp.first == SolverRes.FEASIBLE && resExp.second == SolverRes.FEASIBLE))
            }
        } else if (res.interval.ub is IlpAnalyzer.Unbounded) {
            logger.info { "Detected upper bound to be unbounded" }
//            val resExp = check(specFile)
//            reports.add(report(specFile, path, "${type.name} UB ${target.name} UNBOUNDED", resExp.first, resExp.second, SolverRes.FEASIBLE))
//            testRes.add(Pair("analysis of unboundedness of upper bound: alloy=${resExp.first} choco=${resExp.second}",
//                    resExp.first == SolverRes.FEASIBLE && resExp.second == SolverRes.FEASIBLE))
        } else error("Unknown result for upper bound: ${res.interval.ub}")

        testRes.map { it.first }.forEach { logger.info { "Result: $it" } }
        Assert.assertTrue(testRes.map { it.second }.joinToString(", "), testRes.none { !it.second })
    }


    private fun report(specFile: String, path: String, type: String, alloy: SolverRes, choco: SolverRes, boundanalyzer: SolverRes): Report {
        return Report(specFile, path, type, alloy.name, choco.name, boundanalyzer.name)
    }

    data class Report(val specFile: String,
                      val path: String,
                      val type: String,
                      val alloyRes: String,
                      val chocoRes: String,
                      val boundanalyzerRes: String)


    /**
     * Returns pair of solver results. The first result corresponds to Alloy, the second to Chocosolver
     */
    fun check(specFile: String, cstrs: List<Pair<String, BoolExpr>> = emptyList()): Pair<SolverRes, SolverRes> {
        val origGraph = IrGraph.createFromClasspath(specFile)

        if (cstrs.isNotEmpty()) {
            for (elem in cstrs) {
                val (ctxPath, cstr) = elem
                val ctxNode = origGraph.getNode(ctxPath)
                val cstrsTmp = origGraph.cstrs.getOrDefault(ctxNode, emptyList())
                origGraph.cstrs[ctxNode] = cstrsTmp + listOf(cstr)
            }
        }

        val (scope, bitwidth) = scopes[specFile] ?: defaultScope

        val resChoco = ChocoConsistencyChecker().check(origGraph, scope, timeout, TimeUnit.SECONDS)
//        val resAlloy = AlloyConsistencyChecker(bitwidth).check(origGraph, scope, timeout, TimeUnit.SECONDS)
//        return SolverRes.FEASIBLE to SolverRes.FEASIBLE

        return SolverRes.INFEASIBLE to resChoco
    }

}
