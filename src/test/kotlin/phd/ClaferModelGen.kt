package phd

import ir.*
import mu.KotlinLogging
import org.cardygan.boundanalyzer.antlr.ClaferCompiler
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import java.io.File

//private val outputDir = "/Users/markus/Developer/Projects/Cardygan/boundanalyzer/src/test/resources/clafer/eval_rq3/gen"
private val outputDir = "C:/Users/mweckesser/Dev/boundanalyzer/src/test/resources/clafer/eval_rq3/gen"
private val logger = KotlinLogging.logger {}

fun main() {

    val large: List<String> = listOf(
            "/clafer/eval_rq3/sources/BC.cfr.des"
    )

    val specs = ResourceUtil.getResourceFiles("/clafer/eval_rq3/sources")
            .map { "/clafer/eval_rq3/sources/$it" }
            .filter { it.endsWith(".cfr.des") || it.endsWith(".cfr") }
            .filter { !it.contains("doorLocks.cfr") }
            .filter { !it.contains("powerWindow.cfr") }
            .filter { !it.contains("BC.cfr") }


//    for (spec in specs) {
//        for (i in 1 until 11) {
//            logger.info { "Generating spec $spec in size $i" }
//            ClaferModelGen().genModel(spec, outputDir, i, false)
//            ClaferModelGen().genModel(spec, outputDir, i, true)
//        }
//    }


    for (spec in large) {
        for (i in 6 until 11) {
            logger.info { "Generating large spec $spec in size $i" }
            ClaferModelGen().genModel(spec, outputDir, i, false)
            ClaferModelGen().genModel(spec, outputDir, i, true)
        }
    }
    logger.info { "Done." }
}

class ClaferModelGen {

    fun genModel(specFile: String, outputDir: String, noOfClones: Int, replaceInts: Boolean = false) {
        val graph = if (specFile.endsWith(".des"))
            IrGraph.createFromClasspathDesugared(specFile)
        else IrGraph.createFromClasspath(specFile)

        val hasInts = graph.nodes.any { it.type == Node.NodeType.INT }

        // copy graph n times
        val clonedGraphs: MutableList<IrGraph> = mutableListOf()
        for (i in 0 until noOfClones) {
            val clone = graph.copy("_r$i")
            clonedGraphs.add(clone)
        }

        // merge graphs
        merge(graph, clonedGraphs)

        val spec = graph.convert2Clafer(true)

        val specName = if (replaceInts && hasInts) "${File(specFile).nameWithoutExtension}_gen${noOfClones}_real" else "${File(specFile).nameWithoutExtension}_gen${noOfClones}"

        // output to directory
        val desugared = if (replaceInts && hasInts) replaceInts(ClaferCompiler.compile(spec)) else ClaferCompiler.compile(spec)

        val outputFile = File("$outputDir/$specName.cfr.des")
        outputFile.createNewFile()
        outputFile.writeText(desugared)
    }

    fun merge(target: IrGraph, graphs: List<IrGraph>) {
        val root = target.cRoot

        for (g in graphs) {
            target.nodes.addAll(g.nodes.filter { it != root })
            target.incoming.putAll(g.incoming.filter { it.key != root })
            target.outgoing.putAll(g.outgoing.filter { it.key != root })

            if (g.incoming[g.cRoot] != null)
                target.incoming[root]!!.addAll(g.incoming[g.cRoot]!!)

            if (g.outgoing[g.cRoot] != null)
                target.outgoing[root]!!.addAll(g.outgoing[g.cRoot]!!)

            // add constraints
            target.cstrs.putAll(g.cstrs)

            // add references
            target.setRefs.putAll(g.setRefs)
            target.bagRefs.putAll(g.bagRefs)

            target.claferMult.putAll(g.claferMult)
            target.groupMult.putAll(g.groupMult)
        }
    }

    fun replaceInts(spec: String): String =
            spec.replace("integer", "real")


}

