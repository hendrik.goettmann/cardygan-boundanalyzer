package phd

import ir.IrGraph
import ir.IrCreator
import ir.Edge
import ir.constraints.visitAst
import mu.KotlinLogging
import org.cardygan.boundanalyzer.antlr.ClaferCompiler
import org.cardygan.boundanalyzer.antlr.ClaferParser
import org.cardygan.boundanalyzer.antlr.ParserTest
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import ir.constraints.VisitorParams
import ir.transform.flatten
import ir.constraints.simplifyId
import org.junit.Test

class SpecStats {

    private val logger = KotlinLogging.logger {}

    @Test
    fun runTestForPositive() {
        runTestForDir("/clafer/positive/")
    }

    @Test
    fun runTestForEval() {
        runTestForDir("/clafer/eval/")
    }

    @Test
    fun runTestForRoot() {
        runTestForDir("/clafer/")
    }

    private fun runTestForDir(dirPath: String) {

        // list of specs of which flattening detects that they do not yield valid instances
//        val skip = listOf("nestedInheritanceWithReference.cfr", "NestedInheritanceAndReference2.cfr")
        val skip = emptyList<String>()
//        val skip = listOf("AADL_Complete.cfr")

        val claferFileNames = ResourceUtil.getResourceFiles(dirPath)
        for (claferFileName in claferFileNames - skip) {
            if (claferFileName.endsWith(".cfr")) {
                runTest(dirPath + claferFileName, claferFileName)
            }
        }
    }

//    private fun desugarToFile(inputFileName: String, inputFilePath: String) {
//        val model = ResourceUtil.transformStreamToString(ParserTest::class.java.getResourceAsStream(inputFilePath))
//
//        logger.info { "Compiling spec $inputFilePath ..." }
//        val desugared = ClaferCompiler.compile(model)
//        File("/Users/markus/Developer/Projects/Cardygan/boundanalyzerRes/src/test/resources/clafer/eval_des/$inputFileName.des").writeText(desugared)
//
//    }

    private fun runTest(file: String, name: String) {
        val model = ResourceUtil.transformStreamToString(ParserTest::class.java.getResourceAsStream(file))

        logger.info { "Compiling spec $name ..." }
        val desugared = ClaferCompiler.compile(model)
//        println(desugared)

        logger.info { "Parsing spec ..." }
        val parser = ClaferParser.parseFromString(desugared)

        logger.info { "Creating dependency graph ..." }
        val graph = IrCreator(parser).create()

        logger.info { "flattening dependency graph ..." }
        graph.flatten()

        logger.info { "collect stats ..." }
        collectStats(graph, name)


        collectStats(graph, name)



        graph.cstrs.forEach { (ctx, cstrs) ->
            cstrs.forEach { cstr ->
                visitAst(cstr, param = VisitorParams(
                        visitPath = { path ->
                            val evalGraph = graph.retrieveEvalGraph(path.elements.map { it.id }, ctx)

                            // Check if all paths of eval graph have same length
                            assert(evalGraph.edges.map { it.size }.toSet().size == 1)
                            { "Multiple paths with different number of edges not allowed." }

                            // Check if for each eval step, edge type is same
                            for (i in 0 until evalGraph.edges.first().size) {
                                assert(evalGraph.edges.map {
                                    it[i].edge.type to it[i].isInverted
                                }.toSet().size == 1) { "All edges per eval step must have same type." }
                            }

                            // print out eval elements that have reference edge type
                            for (i in 0 until evalGraph.edges.first().size) {
                                if (evalGraph.edges.first()[i].edge.type == Edge.EdgeType.R) {
                                    val edges = mutableListOf<Edge>()
                                    evalGraph.edges.forEach {
                                        edges.add(it[i].edge)
                                    }
                                    println("EDGES: $edges")

                                    assert(evalGraph.edges.map {
                                        simplifyId(it[i].edge.src.name)
                                    }.toSet().size == 1) { "All edges per eval step must have same src name." }
                                }
                            }
                        }))
            }
        }

    }

    fun collectStats(graph: IrGraph, name: String) {

        // get number of clafers in flattened graph were upwards nesting hiearchy has only 0..1 clafer multiplicits
        val nodesUb = graph.nodes.map { node ->
            val parentVals = graph.getParentTransitive(node).map { n ->
                graph.cmult(node).second
            }
            if (parentVals.isNotEmpty())
                node to parentVals.reduce { prod, n -> if (prod == -1 || n == -1) -(Math.abs(prod) * Math.abs(n)) else n * prod }
            else
                node to 0
        }.toMap()
        val noUb = nodesUb.map { it.value }.filter { it < 0 }.size
        val noGt1 = nodesUb.map { it.value }.filter { it > 1 }.size + noUb
        val noLq1 = nodesUb.map { it.value }.filter { it in 0..1 }.size
        val total = nodesUb.size

        println("\ttotal: $total \t  >1: ${String.format("%.2f", (noGt1 / total.toDouble()))} % \t ≤1: ${String.format("%.2f", noLq1 / total.toDouble())} % \t <- $name")
    }

}