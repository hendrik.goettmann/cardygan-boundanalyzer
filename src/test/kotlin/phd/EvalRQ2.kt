package phd

import ilp.IlpAnalyzer
import ir.*
import ir.transform.flatten
import mu.KotlinLogging
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import util.writeStatsToJsonFile
import java.io.File

@RunWith(Parameterized::class)
class EvalRQ2 {

    private val outputDir: String = System.getProperty("OUTPUT_DIR")

    private val timeout: Long = System.getProperty("SOLVER_TIMEOUT").toLong()

    private val logger = KotlinLogging.logger {}

    private val specSolverMapping: Map<String, IlpAnalyzer.SolverType> = mapOf(
            "EDM.cfr" to IlpAnalyzer.SolverType.GUROBI,
            "BC.cfr" to IlpAnalyzer.SolverType.GUROBI
    )

    @Parameterized.Parameter(0)
    lateinit var specFile: String

    companion object {

        private val ignoreList = listOf(
                "ClaferTools_ToolingArchitecture.cfr",
                "AADL_Complete.cfr",
                "doorLocks.cfr",
                "powerWindow.cfr"
        //        "EDM.cfr",
        //        "TM.cfr",
        //        "BC.cfr"
        )
        private val whiteList: List<String> = listOf("SAS.cfr")

        @JvmStatic
        @Parameterized.Parameters(name = "\"{0}\"")
        fun specs(): List<Array<String>> {
            val claferFileNames = ResourceUtil.getResourceFiles("/clafer/eval_des").map { "/clafer/eval_des/$it" }
            return claferFileNames.filter { it.endsWith(".cfr.des") || it.endsWith(".cfr") }
                    .filter { f -> ignoreList.filter { f.contains(it) }.none() }
                    .filter { f -> if (whiteList.isNotEmpty()) whiteList.filter { f.contains(it) }.any() else true }
                    .map { arrayOf(it) }
        }

    }


    @Test
    fun runSpec() {
        logger.info { "Starting to analyze $specFile." }

        val specName = File(specFile).nameWithoutExtension

        val graph = when {
            specFile.endsWith(".cfr") -> IrGraph.createFromClasspath(specFile)
            specFile.endsWith(".des") -> IrGraph.createFromClasspathDesugared(specFile)
            else -> error("Unknown file type: $specFile")
        }

        val graphStats = graph.analyze()

        logger.info { "Discarding unsupported constraints." }
        discardUnsupportedConstraints(graph)
        val baseLangStats = graph.analyze()

        logger.info { "Flattening." }
        graph.flatten()
        val flattenedStats = graph.analyze()

        val results = mutableListOf<IlpAnalyzer.AnalysisResult>()

        val solverType = specSolverMapping.filter { specName.contains(it.key) }.values.singleOrNull()
                ?: IlpAnalyzer.SolverType.GUROBI

        val analyzer = IlpAnalyzer(graph, solverType, timeout)

        // analyze scopes and multiplicities
        val nodes = graph.nodes.filter { it.type == Node.NodeType.STD && it != graph.cRoot && it != graph.emptyset }
        var count = 1
        for (n in nodes) {
            logger.info { "Performing analysis for ${n.name} (${count++} of ${nodes.size} nodes)." }

            try {
                logger.info { "Analyzing clafer multiplicity interval of clafer ${n.name}." }
                val resBoundCmult = analyzer.analyzeInterval(n, IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
                logger.info { "Result of clafer multiplicity interval analysis of clafer ${n.name}: $resBoundCmult" }


                logger.info { "Analyzing group multiplicity interval of clafer ${n.name}." }
                val resBoundGmult = analyzer.analyzeInterval(n, IlpAnalyzer.AnalysisTarget.GROUP_MULT)
                logger.info { "Result of group multiplicity interval analysis of clafer ${n.name}: $resBoundGmult" }

                results.addAll(listOf(resBoundCmult, resBoundGmult))
            } catch (e: IlpAnalyzer.SolverTimeoutException) {
                logger.error { "Solver timed out." }
            }
        }

        // analyze references
        count = 1
        val nodesRef = graph.nodes.filter { graph.hasNumRef(it) }
        for (n in nodesRef) {
            try {
                logger.info { "Performing REF analysis for ${n.name} (${count++} of ${nodesRef.size} nodes)." }

                logger.info { "Analyzing bound of integer/real clafer ${n.name}." }
                val resRef = analyzer.analyzeInterval(n, IlpAnalyzer.AnalysisTarget.REF)
                logger.info { "Result of attribute analysis of clafer ${n.name}: $resRef" }
                results.add(resRef)
            } catch (e: IlpAnalyzer.SolverTimeoutException) {
                logger.error { "Solver timed out." }
            }
        }

        val stats = Statistics(specName, graphStats, baseLangStats, flattenedStats, results)

        logger.info { "Writing stats to directory $outputDir." }
        writeStatsToJsonFile(outputDir, specName, stats)

    }

    private data class Statistics(val specificationName: String,
                                  val originalStats: AstAnalyzerStats,
                                  val afterConstraintDropStats: AstAnalyzerStats,
                                  val afterFlattenigStats: AstAnalyzerStats,
                                  val analysisResults: List<IlpAnalyzer.AnalysisResult>)


}