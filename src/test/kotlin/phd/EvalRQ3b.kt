package phd

import ilp.IlpAnalyzer
import ir.*
import ir.transform.flatten
import mu.KotlinLogging
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import org.junit.Test
import util.writeStatsToJsonFile
import java.io.File
import java.util.concurrent.TimeUnit

class EvalRQ3b {

    private val outputDir: String = System.getProperty("OUTPUT_DIR")

    private val timeout: Long = System.getProperty("SOLVER_TIMEOUT").toLong()

    private val repetitions: Int = 5

    private val logger = KotlinLogging.logger {}


    @Test
    fun runEval() {

        val claferFileNames = ResourceUtil.getResourceFiles("/clafer/eval_rq3/gen")
                .map { "/clafer/eval_rq3/gen/$it" }
                .filter { it.endsWith(".des") || it.endsWith(".cfr") }
                .filter { !it.contains("doorLocks.cfr") }
                //.filter { !it.contains("powerWindow.cfr") }
                //.filter { !it.contains("BC.cfr") }
        val results: MutableList<RQ3bResult> = mutableListOf()
        for (specPath in claferFileNames) {
            results.add(analyzeSpec(specPath))
        }

        writeStatsToJsonFile(outputDir, "evalRQ3b", results)
    }

    fun analyzeSpec(specFile: String): RQ3bResult {

        logger.info { "Starting to analyze $specFile" }
        val specName = File(specFile).nameWithoutExtension

        val graph = when {
            specFile.endsWith(".cfr") -> IrGraph.createFromClasspath(specFile)
            specFile.endsWith(".des") -> IrGraph.createFromClasspathDesugared(specFile)
            else -> error("Unknown file type: $specFile")
        }

        discardUnsupportedConstraints(graph)

        val graphStats = graph.analyze()

        discardUnsupportedConstraints(graph)

        val baseLangStats = graph.analyze()

        graph.flatten()

        val flattenedStats = graph.analyze()


        var totalRuntimeMs: Long = 0
        var totalSolverRuntime: Long = 0
        var ilpStats: IlpAnalyzer.ModelStats? = null

        for (i in 0 until repetitions) {
            logger.info { "Starting $i/$repetitions iteration for analysis of $specFile" }
            val start = System.nanoTime()
            val res = IlpAnalyzer(graph, timeoutSeconds = timeout).analyzeScope(graph.cRoot)

            ilpStats = res.stats

            val currentRuntime = TimeUnit.MILLISECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS)
            totalRuntimeMs += currentRuntime
            totalSolverRuntime += res.stats.runtime
            logger.info { "Result for iteration: runtimeIteration=$currentRuntime runtimeSolver=${res.stats.runtime}" }
        }

        val avgRuntimeMs = totalRuntimeMs.toDouble() / repetitions
        val avgSolverRuntime = totalSolverRuntime.toDouble() / repetitions / 2 // divide by two because solver runtime includes analysis for lower AND upper bound.

        logger.info { "Finished analysis of spec. Overall results: avgRuntimeMs=$avgRuntimeMs avgSolverRuntime=$avgSolverRuntime" }
        return RQ3bResult(specName, graphStats, baseLangStats, flattenedStats, ilpStats!!, avgRuntimeMs, avgSolverRuntime)
    }

    data class RQ3bResult(val name: String, val specStat: AstAnalyzerStats,
                          val baseLangStat: AstAnalyzerStats,
                          val flattenedStat: AstAnalyzerStats,
                          val ilpStats: IlpAnalyzer.ModelStats,
                          val avgRuntimeMs: Double, val avgSolverRuntimeMs: Double)

}