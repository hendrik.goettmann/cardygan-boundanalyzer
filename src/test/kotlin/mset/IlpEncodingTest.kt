package mset

import ilp.optimize.MsetOptimizer
import ilp.transform.transformToIlp
import org.cardygan.ilp.api.solver.CplexSolver
import org.cardygan.ilp.api.model.Model
import org.cardygan.ilp.api.model.Sum
import ir.IrGraph
import ir.createFromString
import ir.transform.flatten
import mset.transform.transformToMset
import org.cardygan.ilp.api.Result
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test


class IlpEncodingTest {

    @Test
    fun basicModel() {
        val claferModel =
                """
                    a
                        b
                """.trimIndent()


        testClaferScope(claferModel, "a.b", Pair(1, 1))
        testClaferScope(claferModel, "a", Pair(1, 1), Pair(1, 1))
    }


    @Test
    fun basicModel2() {
        val claferModel =
                """
                    a 0..2
                        b 2..3
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(0, 2), Pair(0, 6))
        testClaferScope(claferModel, "a.b", Pair(0, 6))
    }

    @Test
    fun basicModelUnbounded() {
        val claferModel =
                """
                    a 0..*
                        b 2..3
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(0, -1))
        testClaferScope(claferModel, "a.b", Pair(0, -1))
    }


    @Test
    fun trivialModel() {
        val claferModel =
                """
                    a
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1))
    }


    @Test
    fun testModel1() {
        val claferModel =
                """
                    1..4 a
                        b 1..2
                        c 1..3
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1), Pair(2, 4))
        testClaferScope(claferModel, "a.b", Pair(1, 2))
        testClaferScope(claferModel, "a.c", Pair(1, 3))
    }


    @Test
    fun testModel2() {
        val claferModel =
                """
                    1..10 a
                        1..20 b 1..10
                            c 1..20
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1), Pair(1, 10))
        testClaferScope(claferModel, "a.b", Pair(1, 10), Pair(1, 200))
        testClaferScope(claferModel, "a.b.c", Pair(1, 200))

    }

    @Test
    fun testModel3() {
        val claferModel =
                """
                    1..10 a
                        1..5 b 1..8
                            c 3..4
                        1..2 d 1..3
                            e 0..2
                        f 0..1
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1), Pair(2, 10))
        testClaferScope(claferModel, "a.b", Pair(1, 8), Pair(3, 32))
        testClaferScope(claferModel, "a.b.c", Pair(3, 32))
        testClaferScope(claferModel, "a.d", Pair(1, 3), Pair(1, 6))
        testClaferScope(claferModel, "a.d.e", Pair(1, 6))
        testClaferScope(claferModel, "a.f", Pair(0, 1))
    }

    @Test
    fun testModel4() {
        val claferModel =
                """
                    a -> b 0..10
                    b 1..2
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(0, 2))
        testClaferScope(claferModel, "b", Pair(1, 2))
    }

    @Test
    fun testModel5() {
        val claferModel =
                """
                    a -> b ++ c 0..10
                    b 1..2
                    c 1..2
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(0, 4))
        testClaferScope(claferModel, "b", Pair(1, 2))
        testClaferScope(claferModel, "c", Pair(1, 2))
    }

    @Test
    fun testModel6() {
        val claferModel =
                """
                    a -> b ++ c 0..10
                    b 1..2
                    c 1..*
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(0, 10))
        testClaferScope(claferModel, "b", Pair(1, 2))
        testClaferScope(claferModel, "c", Pair(1, -1))
    }

    @Test
    fun testModel7() {
        val claferModel =
                """
                    a -> (b ++ c ++ d) ** d 0..10
                    b 1..2
                    c 1..*
                    d 2
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(0, 2))
        testClaferScope(claferModel, "b", Pair(1, 2))
        testClaferScope(claferModel, "c", Pair(1, -1))
        testClaferScope(claferModel, "d", Pair(2, 2))
    }

    @Test
    fun testModel8() {
        val claferModel =
                """
                    abstract a -> b
                    abstract b
                    c : b 1..2
                    d : b 1..2
                    e : a *
                """.trimIndent()

        testClaferScope(claferModel, "c", Pair(1, 2))
        testClaferScope(claferModel, "d", Pair(1, 2))
        testClaferScope(claferModel, "e", Pair(0, 4))
    }

    @Test
    fun testModel9() {
        val claferModel =
                """
                    a -> integer
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1))
    }

    @Test
    fun testModel10() {
        val claferModel =
                """
                    a -> integer
                        [this >= 3 && this <= 4]
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1))
        testNumericRef(claferModel, "a", 3, 4)
    }

    @Test
    fun testModel11() {
        val claferModel =
                """
                    a -> integer
                        [this >= 3]
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1))
        testNumericRef(claferModel, "a", 3, -1)
    }

    @Test
    fun basicAnd() {
        val claferModel =
                """
                    1..10 a
                        b 1..4
                        c 1..3
                    [2 <= #b && #b <= 3]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(2, 3))
        testClaferScope(claferModel, "a.c", Pair(1, 3))
    }

    @Test
    fun andOfSum() {
        val claferModel =
                """
                    1..10 a
                        b 1..4
                        c 1..3
                    [2 <= #b + #c && #b + #c <= 4]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(1, 3), assert = "#c=1")
        testClaferScope(claferModel, "a.b", Pair(1, 2), assert = "#c=2")
        testClaferScope(claferModel, "a.b", Pair(1, 1), assert = "#c=3")

        testClaferScope(claferModel, "a.c", Pair(1, 3))
        testClaferScope(claferModel, "a.c", Pair(1, 3), assert = "#b=1")
        testClaferScope(claferModel, "a.c", Pair(1, 2), assert = "#b=2")
        testClaferScope(claferModel, "a.c", Pair(1, 1), assert = "#b=3")
    }

    @Test
    fun basicOr() {
        val claferModel =
                """
                    1..10 a
                        b 1..4
                        c 1..3
                    [ #b <= 2 || 4 <= #b ]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(1, 4))
        testClaferScope(claferModel, "a.c", Pair(1, 3))
        testClaferScope(claferModel, "a.b", Pair(2, 2), assert = "#b=2")
    }

    @Test
    fun orOfSum() {
        val claferModel =
                """
                    1..10 a
                        b 1..4
                        c 1..3
                    [ 2 = #b + #c || #b + #c = 4 ]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(1, 3))
        testClaferScope(claferModel, "a.b", Pair(1, 3), assert = "#c=1")
        testClaferScope(claferModel, "a.b", Pair(2, 2), assert = "#c=2")
        testClaferScope(claferModel, "a.b", Pair(1, 1), assert = "#c=3")

        testClaferScope(claferModel, "a.c", Pair(1, 3))
        testClaferScope(claferModel, "a.c", Pair(1, 3), assert = "#b=1")
        testClaferScope(claferModel, "a.c", Pair(2, 2), assert = "#b=2")
        testClaferScope(claferModel, "a.c", Pair(1, 1), assert = "#b=3")
    }

    @Test
    fun basicMult() {
        val claferModel =
                """
                    1..10 a
                        b 1..6
                        c 1..3
                    [ 4 <= 2 * #b && 3 * #b <= 9 ]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(2, 3))
        testClaferScope(claferModel, "a.c", Pair(1, 3))
    }

    @Test
    fun basicImpl() {
        val claferModel =
                """
                    1..10 a
                        b 2..6
                        c 1..3
                    [ (2 <=  #b && #b <= 3) => 2 = #c ]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(2, 6))

        // if f2 != 2 -> implication cannot be active -> minimum f1 = 4
        testClaferScope(claferModel, "a.b", Pair(4, 6), assert = "#c=1")
        testClaferScope(claferModel, "a.b", Pair(4, 6), assert = "#c=3")

        // if f2 == 2 -> implication can be true -> minimum f1 = 2
        testClaferScope(claferModel, "a.b", Pair(2, 6), assert = "#c=2")
        testClaferScope(claferModel, "a.c", Pair(1, 3))

        testClaferScope(claferModel, "a.c", Pair(2, 2), assert = "#b=2")
        testClaferScope(claferModel, "a.c", Pair(2, 2), assert = "#b=3")
        testClaferScope(claferModel, "a.c", Pair(1, 3), assert = "#b=4")
        testClaferScope(claferModel, "a.c", Pair(1, 3), assert = "#b=5")
        testClaferScope(claferModel, "a.c", Pair(1, 3), assert = "#b=6")
    }

    @Test
    fun sumImpl() {
        val claferModel =
                """
                    1..15 a
                        b 1..6
                        c 1..3
                        d 1..4
                    [ (3 = #b + #c && #b = 1) => (#d = 3) ]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(1, 6))

        testClaferScope(claferModel, "a.d", Pair(3, 3),
                assert = "#b = 1 && #c = 2")

        testClaferScope(claferModel, "a.d", Pair(1, 4),
                assert = "#b = 2 && #c = 1")

        testClaferScope(claferModel, "a.c", Pair(1, 3),
                assert = "#b = 1 && #d = 3")
    }

    @Test
    fun ite_1() {
        val claferModel =
                """
                    a
                        b 1..6
                        c 1..3
                        d 1..4
                    [ if ( #b = 2 ) then (#d = 2) else (#d = 3) ]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(1, 6))

        testClaferScope(claferModel, "a.d", Pair(2, 2),
                assert = "#b = 2")
        testClaferScope(claferModel, "a.d", Pair(2, 3))
    }

    @Test
    fun ite_2() {
        val claferModel =
                """
                    a
                        b 1..6
                        c -> integer
                        d 1..4
                    [ c = (if ( #b = 2 ) then 17 else 42) ]
                """.trimIndent()

        testClaferScope(claferModel, "a.b", Pair(1, 6))

        testClaferScope(claferModel, "a.c", Pair(1, 1),
                assert = "#b = 2")
        testNumericRef(claferModel, "a.c", 17, 42)
        testClaferScope(claferModel, "a.d", Pair(1, 4))
    }

    @Test
    fun setExprs1() {
        val claferModel =
                """
                    a -> b ++ c
                    b 0..1
                    c 0..1
                    [ a.dref = b ]
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1))
        testClaferScope(claferModel, "b", Pair(1, 1))
        testClaferScope(claferModel, "c", Pair(0, 1))
    }


    @Test
    fun setExprSetEq() {
        val claferModel =
                """
                    a 0..1
                    b
	                    r -> a ++ b 1..2
                    [b.r.dref = a]

                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1))
        testClaferScope(claferModel, "b", Pair(1, 1))
    }


    @Test
    fun setExprSetEq2() {
        val claferModel =
                """
                    c
                    a
                    b
	                    r -> a ++ b ++ c 1..1
                    [b.r.dref = a]
                    [b.r.dref != b]
                """.trimIndent()

        testClaferScope(claferModel, "a", Pair(1, 1))
        testClaferScope(claferModel, "b", Pair(1, 1))
    }


    @Test
    fun personRelatives() {
        val claferModel =
                """
  0..* Alice  1..1
	  0..* age -> integer 1..1
	  1..1 maritalStatus  1..1
		  0..* neverMarried  0..1
		  0..* married  0..1
			  0..* spouse -> Alice ++ Bob ++ Carol 1..1
		  0..* divorced  0..1
	  0..* childs -> Alice ++ Bob ++ Carol 0..*
	  0..* parents -> Alice ++ Bob ++ Carol 0..2
  0..* Bob  1..1
	  0..* age -> integer 1..1
	  1..1 maritalStatus  1..1
		  0..* neverMarried  0..1
		  0..* married  0..1
			  0..* spouse -> Alice ++ Bob ++ Carol 1..1
		  0..* divorced  0..1
	  0..* childs -> Alice ++ Bob ++ Carol 0..*
	  0..* parents -> Alice ++ Bob ++ Carol 0..2
  0..* Carol  1..1
	  0..* age -> integer 1..1
	  1..1 maritalStatus  1..1
		  0..* neverMarried  0..1
		  0..* married  0..1
			  0..* spouse -> Alice ++ Bob ++ Carol 1..1
		  0..* divorced  0..1
	  0..* childs -> Alice ++ Bob ++ Carol 0..*
	  0..* parents -> Alice ++ Bob ++ Carol 0..2
[Alice.age.dref = 6]
[Alice.maritalStatus.married.spouse.dref = Bob]
[Alice.age.dref >= 0]
[some Alice.maritalStatus.married||some Alice.maritalStatus.divorced => Alice.age.dref >= 5]
[Alice.maritalStatus.married.spouse.dref != Alice]
[Bob.age.dref >= 0]
[some Bob.maritalStatus.married||some Bob.maritalStatus.divorced => Bob.age.dref >= 5]
[Bob.maritalStatus.married.spouse.dref != Bob]
[Carol.age.dref = 1]
[Carol.age.dref >= 0]
[some Carol.maritalStatus.married||some Carol.maritalStatus.divorced => Carol.age.dref >= 5]
[Carol.maritalStatus.married.spouse.dref != Carol]
[Alice.childs.dref ++ Bob.childs.dref ++ Carol.childs.dref = Carol]
                """.trimIndent()

        testClaferScope(claferModel, "Alice", Pair(1, 1))


    }


    @Test
    fun telematics() {
        val claferModel =
                """
0..* ECU1  1..1
  0..* version -> integer 1..1
  0..* display  1..2
    0..* server -> ECU1 ++ ECU2 1..1
    0..* version -> integer 1..1
    0..* options  1..1
      1..1 size  1..1
        0..* small  0..1
        0..* large  0..1
      0..* cache  0..1
        0..* size -> integer 1..1
          0..* fixed  0..1
0..* ECU2  0..1
  0..* master -> ECU1 1..1
  0..* version -> integer 1..1
  0..* display  1..2
    0..* server -> ECU1 ++ ECU2 1..1
    0..* version -> integer 1..1
    0..* options  1..1
      1..1 size  1..1
        0..* small  0..1
        0..* large  0..1
      0..* cache  0..1
        0..* size -> integer 1..1
          0..* fixed  0..1
0..* telematicsSystem  1..1
  1..1 channel  1..1
    0..* single  0..1
    0..* dual  0..1
  0..* extraDisplay  0..1
  1..1 size  1..1
    0..* small  0..1
    0..* large  0..1
[some ECU1 => (some ECU1.version => ECU1.version.dref = 1 + 2)]
[some ECU1.display => (ECU1.display.server.dref = ECU1 && no ECU1.display.options.cache)]
[some ECU1.display => (some ECU1.display.version => ECU1.display.version.dref = 1 + 2)]
[some ECU1.display.options => (some ECU1.display.options.size.small && some ECU1.display.options.cache => some ECU1.display.options.cache.size.fixed)]
[some ECU2 => (some ECU2.version => ECU2.version.dref = 1 + 2)]
[some ECU2.display => (ECU2.display.server.dref = ECU2 && no ECU2.display.options.cache)]
[some ECU2.display => (some ECU2.display.version => ECU2.display.version.dref = 1 + 2)]
[some ECU2.display.options => (some ECU2.display.options.size.small && some ECU2.display.options.cache => some ECU2.display.options.cache.size.fixed)]
[some telematicsSystem => (some telematicsSystem.channel.dual <=> some ECU2 && some telematicsSystem.extraDisplay <=> #ECU1.display = 2 && some telematicsSystem.extraDisplay <=> some ECU2 => #ECU2.display = 2 && some telematicsSystem.size.large <=> no ECU1.display.options.size.small ++ ECU2.display.options.size.small && some telematicsSystem.size.small <=> no ECU1.display.options.size.large ++ ECU2.display.options.size.large)]
[some telematicsSystem.channel.dual && some telematicsSystem.extraDisplay && some telematicsSystem.size.large]
                """.trimIndent()

        testClaferScope(claferModel, "ECU1", Pair(1, 1))

    }


//    @Test
//    fun modelsRunningExample() {
//        val claferModel =
//                """
//
//    """.trimIndent()
//
//        testClaferScope(claferModel, "PharmTnT_SPL", Pair(1, 1))
//
//
//    }

//    @Test
//    fun basicBiImpl() {
//        //@formatter:off
//        val root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf()
//        )
//        //@formatter:on
//        val fm = fm(root)
//        FmDsl.cstr(fm).expr(bi_impl(
//                and(leq(param(2), intVar("f1")), leq(intVar("f1"), param(3))),
//                and(eq(param(2), intVar("f2")))))
//
//        testFeature(fm, "f1", fiCard(1, 6))
//        testFeature(fm, "f2", fiCard(1, 3))
//
//        testFeature(fm, "f1", fiCard(2, 3), eq(intVar("f2"), param(2)))
//
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f1"), param(2)))
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f1"), param(3)))
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(4)))
//
//        testFeatureInfeasible(fm, "f1", and(
//                eq(intVar("f1"), param(3)),
//                eq(intVar("f2"), param(1))
//        ))
//    }
//
//    @Test
//    fun sumBiImpl() {
//        //@formatter:off
//        val root = f("f0").fi(1, 1).gi(1, 15).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf(),
//                f("f3").fi(1, 4).leaf()
//        )
//        //@formatter:on
//        val fm = fm(root)
//
//        // ((3 == f1 + f2) && f1 == 1) <-> (f3 == 3) && (f0 >= 1)
//        FmDsl.cstr(fm).expr(bi_impl(
//                and(eq(param(3), sum(intVar("f1"), intVar("f2"))), eq(intVar("f1"), param(1))),
//                and(eq(param(3), intVar("f3")), geq(intVar("f0"), param(1)))))
//
//        testFeature(fm, "f1", fiCard(1, 6))
//
//        testFeature(fm, "f3", fiCard(3, 3), and(
//                eq(intVar("f1"), param(1)),
//                eq(intVar("f2"), param(2))
//        ))
//
//
//        testFeature(fm, "f1", fiCard(1, 1), eq(intVar("f3"), param(3)))
//
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f3"), param(3)))
//
//        testFeatureInfeasible(fm, "f3", and(
//                eq(intVar("f1"), param(1)),
//                eq(intVar("f2"), param(2)),
//                eq(intVar("f3"), param(1))))
//
//
//        testFeatureInfeasible(fm, "f1", and(
//                eq(intVar("f1"), param(2)),
//                eq(intVar("f2"), param(1)),
//                eq(intVar("f3"), param(3))))
//    }
//
//    @Test
//    fun basicNot() {
//        //@formatter:off
//        val root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf()
//        )
//        //@formatter:on
//        val fm = fm(root)
//        FmDsl.cstr(fm).expr(and(
//                not(and(leq(param(2), intVar("f1")), leq(intVar("f1"), param(3)))),
//                not(and(eq(param(2), intVar("f2"))))))
//
//        testFeature(fm, "f1", fiCard(1, 6))
//        testFeature(fm, "f2", fiCard(1, 3))
//
//        testFeatureInfeasible(fm, "f1", 2)
//        testFeatureInfeasible(fm, "f1", 3)
//        testFeatureInfeasible(fm, "f2", 2)
//    }
//
//    @Test
//    fun sumNot() {
//        //@formatter:off
//        val root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf()
//        )
//        //@formatter:on
//        val fm = fm(root)
//        FmDsl.cstr(fm).expr(and(
//                not(eq(param(2), sum(intVar("f1"), intVar("f2")))),
//                not(eq(param(2), intVar("f2"))),
//                not(eq(param(3), intVar("f1"))),
//                not(eq(param(5), intVar("f1")))
//        ))
//
//        var res = testFeature(fm, "f1", fiCard(1, 6))
//        assertEquals(3.0, res.fi().lb().fInsVal("f2"))
//
//        res = testFeature(fm, "f2", fiCard(1, 3))
//        assertTrue(res.fi().lb().fInsVal("f1") > 1)
//
//        testFeatureInfeasible(fm, "f1", 3)
//        testFeatureInfeasible(fm, "f1", 5)
//        testFeatureInfeasible(fm, "f2", 2)
//    }
//
//    // TODO Feature Type!
//
//    @Test
//    fun rhs0Basic() {
//        //@formatter:off
//        val root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 4).leaf(),
//                f("f2").fi(1, 3).leaf()
//        )
//        //@formatter:on
//        val fm = fm(root)
//        FmDsl.cstr(fm).expr(leq(intVar("f1"), intVar("f2")))
//
//        testFeature(fm, "f1", fiCard(1, 3))
//        testFeatureInfeasible(fm, "f1", 4)
//
//        testFeature(fm, "f1", fiCard(1, 1), eq(intVar("f2"), param(1)))
//        testFeature(fm, "f1", fiCard(1, 2), eq(intVar("f2"), param(2)))
//        testFeature(fm, "f1", fiCard(1, 3), eq(intVar("f2"), param(3)))
//    }
//
//    @Test
//    fun rhs0BasicNot() {
//        //@formatter:off
//        val root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 4).leaf(),
//                f("f2").fi(1, 3).leaf()
//        )
//        //@formatter:on
//        val fm = fm(root)
//        FmDsl.cstr(fm).expr(not(leq(intVar("f1"), intVar("f2"))))
//
//        testFeature(fm, "f1", fiCard(2, 4))
//        testFeatureInfeasible(fm, "f1", 1)
//
//        testFeature(fm, "f1", fiCard(2, 4), eq(intVar("f2"), param(1)))
//        testFeature(fm, "f1", fiCard(3, 4), eq(intVar("f2"), param(2)))
//        testFeature(fm, "f1", fiCard(4, 4), eq(intVar("f2"), param(3)))
//
//        testFeature(fm, "f2", fiCard(1, 1), eq(intVar("f1"), param(2)))
//        testFeature(fm, "f2", fiCard(1, 2), eq(intVar("f1"), param(3)))
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(4)))
//    }

    fun testNumericRef(model: String, path: String, lb: Int, ub: Int) {

        val graph = IrGraph.createFromString(model)
        val node = graph.getNode(path)

        graph.flatten()
        val mCtx = transformToMset(graph)
        val ilpCtx = transformToIlp(mCtx)

        val decVar = when {
            mCtx.intRefs.contains(node) -> ilpCtx.intRefVars[mCtx.intRef(node)]
            mCtx.dblRefs.contains(node) -> ilpCtx.dblRefVars[mCtx.dblRef(node)]
            else -> error("Could not resolve decision variable for reference")
        }

        val ilp = ilpCtx.ilp

        // lb
        ilp.newObjective(false, decVar)
        val resultLb = solveIlp(ilp)
        assertTrue(resultLb.objVal.isPresent)
        assertEquals(lb.toDouble(), resultLb.objVal.get())

        // ub
        ilp.newObjective(true, decVar)
        val resultUb = solveIlp(ilp)

        // unbounded
        if (ub == -1) {
            assertTrue(resultUb.statistics.status == Result.SolverStatus.UNBOUNDED)
        } else {
            assertTrue(resultUb.objVal.isPresent)
            assertEquals(ub.toDouble(), resultUb.objVal.get(), (0).toDouble())
        }


    }

    fun testClaferScope(spec: String, path: String,
                        mult: Pair<Int, Int>, groupMult: Pair<Int, Int>? = null, assert: String? = null) {
        val graph = if (assert != null)
            IrGraph.createFromString("$spec\n[$assert]")
        else
            IrGraph.createFromString(spec)

//        discardUnsupportedConstraints(graph)
        graph.flatten()
        val node = graph.getNode(path)


        val mCtx = transformToMset(graph)
        MsetOptimizer(mCtx).doOptimize()
        val ilpCtx = transformToIlp(mCtx)

        val decVar = ilpCtx.mvars[mCtx.mCVar(node)]!!

        val ilp = ilpCtx.ilp

        val (lb, ub) = mult

        // lb
        ilp.newObjective(false, decVar)
        val resultLb = solveIlp(ilp)
        assertTrue(resultLb.objVal.isPresent)
        assertEquals(lb.toDouble(), resultLb.objVal.get())

        // ub
        ilp.newObjective(true, decVar)
        val resultUb = solveIlp(ilp)

        // unbounded
        if (ub >= 0) {
            assertTrue(resultUb.objVal.isPresent)
            assertEquals(ub.toDouble(), resultUb.objVal.get())
        } else
            assertTrue(resultUb.statistics.status == Result.SolverStatus.UNBOUNDED)

        if (groupMult != null) {
            val childDecVars = graph.getNested(node).map { ilpCtx.mvars[mCtx.mCVar(it)]!! }

            val (groupLb, groupUb) = groupMult
            // group lb
            ilp.newObjective(false, Sum(childDecVars))
            val resultGroupLb = solveIlp(ilp)
            assertTrue(resultGroupLb.objVal.isPresent)
            assertEquals(groupLb.toDouble(), resultGroupLb.objVal.get())

            // ub
            ilp.newObjective(true, Sum(childDecVars))
            val resultGroupUb = solveIlp(ilp)
            assertTrue(resultGroupUb.objVal.isPresent)

            // unbounded
            if (groupUb >= 0) {
                assertTrue(resultUb.objVal.isPresent)
                assertEquals(groupUb.toDouble(), resultGroupUb.objVal.get())
            } else
                assertTrue(resultGroupUb.statistics.status == Result.SolverStatus.UNBOUNDED)
        }

    }

    private fun solveIlp(ilp: Model) = ilp.solve(CplexSolver.create()
//        .withLogging(true)
            .withPresolve(false)
//        .withModelOutput("/Users/markus/Desktop/cplexModel.lp")
            .build())

}


