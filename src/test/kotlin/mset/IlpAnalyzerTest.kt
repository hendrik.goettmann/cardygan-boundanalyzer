package mset

import ilp.IlpAnalyzer
import ir.IrGraph
import ir.createFromClasspath
import org.junit.Test

class IlpAnalyzerTest {

    @Test
    fun testAbc() {
        testModel(listOf("a", "b", "c"), "/clafer/basicAbc.cfr")
    }

    @Test
    fun testAbcInheritanceConstraint() {
        testModel(listOf("c"), "/clafer/basicAbcInheritanceWithConstraint.cfr")
    }


    @Test
    fun testPersonRelatives() {
        testModel(listOf("Bob", "Alice", "Carol"), "/clafer/eval/personRelatives.cfr")
    }

    private fun testModel(paths: List<String>, path: String) {
        val graph = IrGraph.createFromClasspath(path)

        val node = graph.getNode(path)

        val res = IlpAnalyzer(graph).analyzeScope(node)


    }

}