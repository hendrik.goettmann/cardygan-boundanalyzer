package main

import ir.AstAnalyzerStats
import org.junit.Test

class AnalyzeModels {

    @Test
    fun analyzeRunningExample() {
        analyzeModel("/clafer/eval/runningExampleMODELS18.cfr")
    }

    @Test
    fun analyzeDoorLocks() {
        analyzeModel("/clafer/eval/doorLocks.cfr")
    }

    @Test
    fun analyzePowerWindow() {
        analyzeModel("/clafer/eval/powerWindow.cfr")
    }

    @Test
    fun analyzeDriverPowerWindow() {
        analyzeModel("/clafer/eval/driverPowerWindow.cfr")
    }

    @Test
    fun analyzeAADL_Complete() {
        analyzeModel("/clafer/eval/AADL_Complete.cfr")
    }

    @Test
    fun analyzeBMM() {
        analyzeModel("/clafer/eval/BMM_model.cfr")
    }

    @Test
    fun analyzeEDM_Intro() {
        analyzeModel("/clafer/eval/EDM_Intro.cfr")
    }

    @Test
    fun analyzeScotiabank() {
        analyzeModel("/clafer/eval/Scotiabank_Mortgages.cfr")
    }

    @Test
    fun analyzeTelematics() {
        analyzeModel("/clafer/eval/telematics.cfr")
    }

    @Test
    fun analyzePersonRelatives() {
        analyzeModel("/clafer/eval/personRelatives.cfr")
    }

    @Test
    fun analyzeClaferToolsArch() {
        analyzeModel("/clafer/eval/ClaferTools_ToolingArchitecture.cfr")
    }

    fun analyzeModel(path: String) {
        val stats = analyzeModels(path)
        println(stats)
    }

}


fun analyzeModels(path: String): AstAnalyzerStats =
        TODO()
