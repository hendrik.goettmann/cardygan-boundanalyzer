package main


import clafer.ChocoConsistencyChecker
import clafer.SolverRes
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.concurrent.TimeUnit


class ChocoConsistencyCheckerTest {

    @Test
    fun testInfeasible() {
        val spec =
                """
                    a
                        b
                    [ #b > 2]
                """.trimIndent()

        val res = ChocoConsistencyChecker().check(spec, 5, 60, TimeUnit.SECONDS)
        assertEquals(SolverRes.INFEASIBLE, res)
    }

    @Test
    fun testFeasible() {
        val spec =
                """
                    a
                        b
                """.trimIndent()

        val res = ChocoConsistencyChecker().check(spec, 5, 60, TimeUnit.SECONDS)
        assertEquals(SolverRes.FEASIBLE, res)
    }

    @Test
    fun testTimeOut() {

        val spec =
                """
                    a *
                        b 2000
                    [ #a > 100 ]
                """.trimIndent()

        val res = ChocoConsistencyChecker().check(spec, 10, 1, TimeUnit.SECONDS)
        assertEquals(SolverRes.TIMEOUT, res)

    }

}