package models18

import org.junit.Test


class EvalSubjectSystems {


    @Test
    fun DoorLocks() {
        runEval("doorLocks")
    }

    // TODO get running
    //needs lot of memory
//    @Test
//    fun AADL_Complete() {
//        runEval("AADL_Complete")
//    }

    @Test
    fun BMM_model() {
        runEval("BMM")
    }

    @Test
    fun ClaferTools_ToolingArchitecture() {
        runEval("ClaferTools_ToolingArchitecture")
    }

    @Test
    fun driverPowerWindow() {
        runEval("BC")
    }

    @Test
    fun EDM_Intro() {
        runEval("EDM")
    }

    @Test
    fun personRelatives() {
        runEval("PerR")
    }

    @Test
    fun powerWindow() {
        runEval("powerWindow")
    }

    @Test
    fun runningExampleMODELS18() {
        runEval("TnT")
    }

    @Test
    fun telematics() {
        runEval("TM")
    }

    private fun runEval(specName: String) {
//        main.main(arrayOf("-k", "BOUND", "-t", "ALL", "-c", "ALL", "-i", "$PATH_PREFIX/$specName.cfr", "-o", "\"/Users/markus/Work/Research/Projects/Paper/2017-08-28_MODELS2018/MODELS18_Repo/eval/jupyter/data/$specName.json\""))
    }
}