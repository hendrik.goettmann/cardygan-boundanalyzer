//package org.cardygan.boundanalyzerRes;
//
//import org.cardygan.boundanalyzerRes.model.Clafer;
//import org.cardygan.boundanalyzerRes.model.Container;
//import org.cardygan.boundanalyzerRes.model.util.FmDsl;
//import org.junit.Test;
//
//import static junit.framework.TestCase.assertEquals;
//import static junit.framework.TestCase.assertTrue;
//import static org.cardygan.boundanalyzerRes.FeatureTestUtil.*;
//import static org.cardygan.boundanalyzerRes.model.util.ExprDsl.*;
//import static org.cardygan.boundanalyzerRes.model.util.FmDsl.*;
//
//public class TestBoundedCstrLang {
//
//    @Test
//    public void basicAnd() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 4).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(and(leq(param(2), intVar("f1")), leq(intVar("f1"), param(3))));
//
//        testFeatureInfeasible(fm, "f1", 1);
//        testFeatureInfeasible(fm, "f1", 4);
//
//        testFeature(fm, "f1", fiCard(2, 3));
//        testFeature(fm, "f2", fiCard(1, 3));
//    }
//
//    @Test
//    public void andOfSum() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 4).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(and(leq(param(2), sum(intVar("f1"), intVar("f2"))), leq(sum(intVar("f1"), intVar("f2")), param(4))));
//
//        testFeature(fm, "f1", fiCard(1, 3));
//        testFeature(fm, "f1", fiCard(1, 3), eq(intVar("f2"), param(1)));
//        testFeature(fm, "f1", fiCard(1, 2), eq(intVar("f2"), param(2)));
//        testFeature(fm, "f1", fiCard(1, 1), eq(intVar("f2"), param(3)));
//
//        testFeature(fm, "f2", fiCard(1, 3));
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(1)));
//        testFeature(fm, "f2", fiCard(1, 2), eq(intVar("f1"), param(2)));
//        testFeature(fm, "f2", fiCard(1, 1), eq(intVar("f1"), param(3)));
//    }
//
//    @Test
//    public void basicOr() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 4).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(or(leq(intVar("f1"), param(2)), leq(param(4), intVar("f1"))));
//
//        testFeature(fm, "f1", fiCard(1, 4));
//        testFeature(fm, "f2", fiCard(1, 3));
//        testFeatureInfeasible(fm, "f1", 3);
//        testFeature(fm, "f1", fiCard(2, 2), eq(intVar("f1"), param(2)));
//    }
//
//    @Test
//    public void orOfSum() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 4).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(or(eq(param(2), sum(intVar("f1"), intVar("f2"))), eq(sum(intVar("f1"), intVar("f2")), param(4))));
//
//        testFeature(fm, "f1", fiCard(1, 3));
//        testFeature(fm, "f1", fiCard(1, 3), eq(intVar("f2"), param(1)));
//        testFeature(fm, "f1", fiCard(2, 2), eq(intVar("f2"), param(2)));
//        testFeature(fm, "f1", fiCard(1, 1), eq(intVar("f2"), param(3)));
//
//        testFeature(fm, "f2", fiCard(1, 3));
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(1)));
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f1"), param(2)));
//        testFeature(fm, "f2", fiCard(1, 1), eq(intVar("f1"), param(3)));
//    }
//
//    @Test
//    public void basicMult() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(and(leq(param(4), mult(param(2), intVar("f1"))), leq(mult(param(3), intVar("f1")), param(9))));
//
//        testFeature(fm, "f1", fiCard(2, 3));
//        testFeature(fm, "f2", fiCard(1, 3));
//    }
//
//    @Test
//    public void basicImpl() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(2, 6).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(impl(
//                and(leq(param(2), intVar("f1")), leq(intVar("f1"), param(3))),
//                eq(param(2), intVar("f2"))));
//
//
//        FeatureTestResult res = testFeature(fm, "f1", fiCard(2, 6));
//        assertEquals(2d, res.fi().lb().fInsVal("f2"));
//
//        // if f2 != 2 -> implication cannot be active -> minimum f1 = 4
//        testFeature(fm, "f1", fiCard(4, 6), eq(intVar("f2"), param(1)));
//        testFeature(fm, "f1", fiCard(4, 6), eq(intVar("f2"), param(3)));
//
//        // if f2 == 2 -> implication can be true -> minimum f1 = 2
//        testFeature(fm, "f1", fiCard(2, 6), eq(intVar("f2"), param(2)));
//
//
//        testFeature(fm, "f2", fiCard(1, 3));
//
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f1"), param(2)));
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f1"), param(3)));
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(4)));
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(5)));
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(6)));
//    }
//
//    @Test
//    public void sumImpl() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 15).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf(),
//                f("f3").fi(1, 4).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//
//        // ((3 == f1 + f2) && f1 == 1)-> (f3 == 3)
//        FmDsl.cstr(fm).expr(impl(
//                and(eq(param(3), sum(intVar("f1"), intVar("f2"))), eq(intVar("f1"), param(1))),
//                eq(param(3), intVar("f3"))));
//
//
//        testFeature(fm, "f1", fiCard(1, 6));
//
//        testFeature(fm, "f3", fiCard(3, 3), and(
//                eq(intVar("f1"), param(1)),
//                eq(intVar("f2"), param(2))));
//
//
//        testFeature(fm, "f3", fiCard(1, 4), and(
//                eq(intVar("f1"), param(2)),
//                eq(intVar("f2"), param(1))));
//
//        testFeature(fm, "f2", fiCard(1, 3), and(
//                eq(intVar("f1"), param(1)),
//                eq(intVar("f3"), param(3))));
//
//    }
//
//    @Test
//    public void basicBiImpl() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(bi_impl(
//                and(leq(param(2), intVar("f1")), leq(intVar("f1"), param(3))),
//                and(eq(param(2), intVar("f2")))));
//
//        testFeature(fm, "f1", fiCard(1, 6));
//        testFeature(fm, "f2", fiCard(1, 3));
//
//        testFeature(fm, "f1", fiCard(2, 3), eq(intVar("f2"), param(2)));
//
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f1"), param(2)));
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f1"), param(3)));
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(4)));
//
//        testFeatureInfeasible(fm, "f1", and(
//                eq(intVar("f1"), param(3)),
//                eq(intVar("f2"), param(1))
//        ));
//    }
//
//    @Test
//    public void sumBiImpl() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 15).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf(),
//                f("f3").fi(1, 4).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//
//        // ((3 == f1 + f2) && f1 == 1) <-> (f3 == 3) && (f0 >= 1)
//        FmDsl.cstr(fm).expr(bi_impl(
//                and(eq(param(3), sum(intVar("f1"), intVar("f2"))), eq(intVar("f1"), param(1))),
//                and(eq(param(3), intVar("f3")), geq(intVar("f0"), param(1)))));
//
//        testFeature(fm, "f1", fiCard(1, 6));
//
//        testFeature(fm, "f3", fiCard(3, 3), and(
//                eq(intVar("f1"), param(1)),
//                eq(intVar("f2"), param(2))
//        ));
//
//
//        testFeature(fm, "f1", fiCard(1, 1), eq(intVar("f3"), param(3)));
//
//        testFeature(fm, "f2", fiCard(2, 2), eq(intVar("f3"), param(3)));
//
//        testFeatureInfeasible(fm, "f3", and(
//                eq(intVar("f1"), param(1)),
//                eq(intVar("f2"), param(2)),
//                eq(intVar("f3"), param(1))));
//
//
//        testFeatureInfeasible(fm, "f1", and(
//                eq(intVar("f1"), param(2)),
//                eq(intVar("f2"), param(1)),
//                eq(intVar("f3"), param(3))));
//    }
//
//    @Test
//    public void basicNot() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(and(
//                not(and(leq(param(2), intVar("f1")), leq(intVar("f1"), param(3)))),
//                not(and(eq(param(2), intVar("f2"))))));
//
//        testFeature(fm, "f1", fiCard(1, 6));
//        testFeature(fm, "f2", fiCard(1, 3));
//
//        testFeatureInfeasible(fm, "f1", 2);
//        testFeatureInfeasible(fm, "f1", 3);
//        testFeatureInfeasible(fm, "f2", 2);
//    }
//
//    @Test
//    public void sumNot() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 6).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(and(
//                not(eq(param(2), sum(intVar("f1"), intVar("f2")))),
//                not(eq(param(2), intVar("f2"))),
//                not(eq(param(3), intVar("f1"))),
//                not(eq(param(5), intVar("f1")))
//        ));
//
//        FeatureTestResult res = testFeature(fm, "f1", fiCard(1, 6));
//        assertEquals(3d, res.fi().lb().fInsVal("f2"));
//
//        res = testFeature(fm, "f2", fiCard(1, 3));
//        assertTrue(res.fi().lb().fInsVal("f1") > 1);
//
//        testFeatureInfeasible(fm, "f1", 3);
//        testFeatureInfeasible(fm, "f1", 5);
//        testFeatureInfeasible(fm, "f2", 2);
//    }
//
//    // TODO Feature Type!
//
//    @Test
//    public void rhs0Basic() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 4).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr((leq(intVar("f1"), intVar("f2"))));
//
//        testFeature(fm, "f1", fiCard(1, 3));
//        testFeatureInfeasible(fm, "f1", 4);
//
//        testFeature(fm, "f1", fiCard(1, 1), eq(intVar("f2"), param(1)));
//        testFeature(fm, "f1", fiCard(1, 2), eq(intVar("f2"), param(2)));
//        testFeature(fm, "f1", fiCard(1, 3), eq(intVar("f2"), param(3)));
//    }
//
//    @Test
//    public void rhs0BasicNot() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//                f("f1").fi(1, 4).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        FmDsl.cstr(fm).expr(not(leq(intVar("f1"), intVar("f2"))));
//
//        testFeature(fm, "f1", fiCard(2, 4));
//        testFeatureInfeasible(fm, "f1", 1);
//
//        testFeature(fm, "f1", fiCard(2, 4), eq(intVar("f2"), param(1)));
//        testFeature(fm, "f1", fiCard(3, 4), eq(intVar("f2"), param(2)));
//        testFeature(fm, "f1", fiCard(4, 4), eq(intVar("f2"), param(3)));
//
//        testFeature(fm, "f2", fiCard(1, 1), eq(intVar("f1"), param(2)));
//        testFeature(fm, "f2", fiCard(1, 2), eq(intVar("f1"), param(3)));
//        testFeature(fm, "f2", fiCard(1, 3), eq(intVar("f1"), param(4)));
//    }
//
//
//}
