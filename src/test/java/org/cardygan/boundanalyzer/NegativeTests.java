//package org.cardygan.boundanalyzerRes;
//
//import org.cardygan.boundanalyzerRes.model.Container;
//import org.cardygan.boundanalyzerRes.model.Clafer;
//import org.junit.jupiter.api.Test;
//
//
//import static org.cardygan.boundanalyzerRes.model.util.FmDsl.*;
//import static org.hamcrest.Matchers.equalTo;
//import static org.hamcrest.Matchers.is;
//import static org.junit.Assert.assertThat;
//
//public class NegativeTests {
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..1 f1 @gi=0..20 @gt=0..1
//     *         0..3 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 0..0 f3 requires 0..0 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq0_UbEq0__Trg_LbEq0_UbEq0() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 1).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 3).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(0, 0, "f3").req(0, 0, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        assertThat(r.fi().lb().fTypeVal("f2"), equalTo(0d));
//
//        assertThat(r.fi().lb().fInsVal("f4"), equalTo(0d));
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//    }
//
//}
