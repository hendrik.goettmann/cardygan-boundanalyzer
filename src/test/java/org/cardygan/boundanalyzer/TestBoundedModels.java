//package org.cardygan.boundanalyzerRes;
//
//import static org.cardygan.boundanalyzerRes.model.util.FmDsl.*;
//
//import org.cardygan.boundanalyzerRes.model.Clafer;
//import org.cardygan.boundanalyzerRes.model.Container;
//import org.junit.jupiter.api.Test;
//
//public class TestBoundedModels {
//
//	@Test
//	public void trivialModel() {
//		Clafer root = f("f0").fi(1, 1).leaf();
//		Container fm = fm(root);
//
//		FeatureTestUtil.testFeature(fm, "f0", fiCard(1, 1));
//	}
//
//	/**
//	 * <pre>
//	 * 1..1 f0 @gi=1..4 @gt=1..3
//	 * 		1..2 f1
//	 * 		1..3 f2
//	 * </pre>
//	 */
//	@Test
//	public void testModel1() {
//		//@formatter:off
//		Clafer root = f("f0").fi(1, 1).gi(1, 4).gt(1, 3).children(
//							f("f1").fi(1, 2).leaf(),
//							f("f2").fi(1, 3).leaf()
//		);
//		//@formatter:on
//
//		Container fm = fm(root);
//		FeatureTestUtil.testFeature(fm, "f0", fiCard(1, 1), giCard(2, 4), gtCard(2, 2));
//		FeatureTestUtil.testFeature(fm, "f1", fiCard(1, 2));
//		FeatureTestUtil.testFeature(fm, "f2", fiCard(1, 3));
//	}
//
//	/**
//	 * <pre>
//	 * 1..1 f0 @gi=1..10 @gt=1..1
//	 * 		1..10 f1 @gi=1..20 @gt=1..1
//	 * 			1..20 f2
//	 * </pre>
//	 */
//	@Test
//	public void testModel2() {
//
//		//@formatter:off
//		Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 1).children(
//							f("f1").fi(1, 10).gi(1, 20).gt(1,1).children(
//								f("f2").fi(1, 20).leaf()
//						)
//		);
//		//@formatter:on
//
//		Container fm = fm(root);
//
//		FeatureTestUtil.testFeature(fm, "f0", fiCard(1, 1), giCard(1, 10), gtCard(1, 1));
//		FeatureTestUtil.testFeature(fm, "f1", fiCard(1, 10), giCard(1, 200), gtCard(1, 1));
//		FeatureTestUtil.testFeature(fm, "f2", fiCard(1, 200));
//	}
//
//	/**
//	 * <pre>
//	 * 1..1 f0 @gt=1..3 @gi=1..10
//	 *    1..8 f1 @gt=0..1 @gi=0..5
//	 *	     3..4 f2
//	 *    1..3 f3 @gt=0..1 @gi=0..2
//	 *       0..2 f4
//	 *    0..1 f5
//	 * </pre>
//	 */
//	@Test
//	public void testModel3() {
//		//@formatter:off
//		Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 3).children(
//							f("f1").fi(1, 8).gi(0, 5).gt(0,1).children(
//								f("f2").fi(3, 4).leaf()),
//							f("f3").fi(1, 3).gi(0, 2).gt(0, 1).children(
//								f("f4").fi(0, 2).leaf()),
//							f("f5").fi(0, 1).leaf()
//						  );
//		//@formatter:on
//
//		Container fm = fm(root);
//		FeatureTestUtil.testFeature(fm, "f0", fiCard(1, 1), giCard(2, 10), gtCard(2, 3));
//		FeatureTestUtil.testFeature(fm, "f1", fiCard(1, 8), giCard(3, 32), gtCard(1, 1));
//		FeatureTestUtil.testFeature(fm, "f2", fiCard(3, 32));
//		FeatureTestUtil.testFeature(fm, "f3", fiCard(1, 3), giCard(0, 6), gtCard(0, 1));
//		FeatureTestUtil.testFeature(fm, "f4", fiCard(0, 6));
//		FeatureTestUtil.testFeature(fm, "f5", fiCard(0, 1));
//	}
//
//}
