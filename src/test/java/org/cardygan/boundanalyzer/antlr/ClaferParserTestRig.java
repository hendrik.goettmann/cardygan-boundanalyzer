package org.cardygan.boundanalyzer.antlr;

import org.antlr.v4.gui.TestRig;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class ClaferParserTestRig {

    public static void main(String[] args) throws Exception {

        try {
            if (args.length == 1 && new File(args[0]).exists()) {

                String claferModel = ResourceUtil.transformStreamToString(new FileInputStream(new File(args[0])));
                String compiledModel = ClaferCompiler.compile(claferModel);

                File tmpFile = File.createTempFile("tmp", ".cfr_intermediate");

                try (PrintWriter out = new PrintWriter(tmpFile) ) {
                    out.println(compiledModel);
                }

                new TestRig(new String[]{"org.cardygan.boundanalyzerRes.clafer.ClaferRx", "module", "-gui", tmpFile.getAbsolutePath()}).process();

            } else {
                System.err.println("Invalid arguments first argument must be clafer input file, second argument must be target file.");
            }

        } catch (IOException e) {
            System.err.println("Could not read clafer model file.");
        }

    }
}
