package org.cardygan.boundanalyzer.antlr;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.junit.Test;

import java.io.*;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

import static org.junit.Assert.fail;


/**
 * Created by markus on 26.04.17.
 */
public class ParserTest {

    @Test
    public void basicAbcTest() throws Exception {
        testModel("/clafer/basicAbc.cfr");
    }

    @Test
    public void basicAbcWithConstraintTest() throws Exception {
        testModel("/clafer/basicAbcWithConstraint.cfr");
    }

    @Test
    public void basicAbcInheritanceTest() throws Exception {
        testModel("/clafer/basicAbcInheritance.cfr");
    }

    //
    @Test
    public void complexInheritanceTest() throws Exception {
        testModel("/clafer/complexInheritance.cfr");
    }

    @Test
    public void personRelativesTest() throws Exception {
        testModel("/clafer/personRelatives1.cfr");
    }

    @Test
    public void basicAbcInheritanceWithConstraintTest() throws Exception {
        testModel("/clafer/basicAbcInheritanceWithConstraint.cfr");
    }

    @Test
    public void basicAbcInheritanceWithConstraint2Test() throws Exception {
        testModel("/clafer/basicAbcInheritanceWithConstraint2.cfr");
    }

    @Test
    public void testPositive() throws Exception {
        String dirPath = "/clafer/positive/";
        List<String> claferFileNames = ResourceUtil.getResourceFiles(dirPath);
        for (String claferFileName : claferFileNames) {
            System.out.println("> Testing clafer model " + claferFileName);
            testModel(dirPath + claferFileName);
        }
    }

    private void testModel(String file) throws IOException {
        String model = ResourceUtil.transformStreamToString(ParserTest.class.getResourceAsStream(file));
        ClaferParser.parseFromString(ClaferCompiler.compile(model), Arrays.asList(new ANTLRErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                fail();
            }

            @Override
            public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, boolean exact, BitSet ambigAlts, ATNConfigSet configs) {

            }

            @Override
            public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, ATNConfigSet configs) {

            }

            @Override
            public void reportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, ATNConfigSet configs) {

            }
        }));
    }


}
