//package org.cardygan.boundanalyzerRes;
//
//import org.cardygan.boundanalyzerRes.model.Clafer;
//import org.cardygan.boundanalyzerRes.model.Container;
//import org.junit.Test;
//
//import static org.cardygan.boundanalyzerRes.model.util.FmDsl.*;
//import static org.hamcrest.Matchers.*;
//import static org.junit.Assert.assertThat;
//
//public class TestBoundedModelsWithReq {
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..4 @gt=1..3
//     * 		1..2 f1
//     * 		1..3 f2
//     * 2..2 f1 require 1..1 f2
//     * </pre>
//     */
//    @Test
//    public void testModel1() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 4).gt(1, 3).children(
//                f("f1").fi(1, 2).leaf(),
//                f("f2").fi(1, 3).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(2, 2, "f1").req(1, 1, "f2");
////
//        FeatureTestUtil.testFeature(fm, "f0", fiCard(1, 1), giCard(2, 4), gtCard(2, 2));
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f1", fiCard(1, 2));
//        assertThat(r.fi().ub().fInsVal("f2"), is(1d));
//
//        FeatureTestUtil.testFeature(fm, "f2", fiCard(1, 3));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gt=1..3 @gi=1..9
//     *    1..8 f1 @gt=0..1 @gi=0..5
//     * 	     3..4 f2
//     *    1..3 f3 @gt=0..1 @gi=0..2
//     *       1..1 f4
//     *    0..1 f5
//     *
//     *    1..8 f1 require 2..2 f4
//     *    2..2 f4 require 1..1 f5
//     * </pre>
//     */
//    @Test
//    public void testModel3() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 9).gt(1, 3).children(
//                f("f1").fi(1, 8).gi(0, 5).gt(0, 1).children(
//                        f("f2").fi(3, 4).leaf()),
//                f("f3").fi(1, 3).gi(0, 2).gt(0, 1).children(
//                        f("f4").fi(1, 1).leaf()),
//                f("f5").fi(0, 1).leaf()
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(1, 8, "f1").req(2, 2, "f4");
//        cstr(fm).src(2, 2, "f4").req(1, 1, "f5");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f1", fiCard(1, 6));
//
//        assertThat(r.fi().ub().fInsVal("f4"), is(2d));
//        assertThat(r.fi().ub().fInsVal("f5"), is(1d));
//
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=2..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         0..10 f4
//     * 1..4 f3 requires 1..4 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq1_UbBounded__Trg_LbEq1_UbBounded() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(2, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(0, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(1, 4, "f3").req(1, 4, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=2..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         0..10 f4
//     * 1..4 f3 requires 2..2 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq1_UbBounded__Trg_LbEq2_UbEq2() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(2, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(0, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(1, 4, "f3").req(2, 2, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
//        assertThat(r.fi().ub().fInsVal("f4"), equalTo(2d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 1..4 f3 requires 0..0 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq1_UbBounded__Trg_LbEq0_UbEq0() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(1, 4, "f3").req(0, 0, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        assertThat(r.fi().ub().fTypeVal("f2"), equalTo(0d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), equalTo(0d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 1..4 f3 requires 2..4 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq1_UbBounded__Trg_LbGt1_UbBounded() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(1, 4, "f3").req(2, 4, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), both(greaterThanOrEqualTo(2d)).and(lessThanOrEqualTo(4d)));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 1..4 f3 requires 1..1 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq1_UbBounded__Trg_LbEq1_UbEq1() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(1, 4, "f3").req(1, 1, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), is(1d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=2..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         0..10 f4
//     * 2..4 f3 requires 1..4 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Gt1_UbBounded__Trg_LbEq1_UbBounded() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(2, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(0, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(2, 4, "f3").req(1, 4, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), both(greaterThanOrEqualTo(1d)).and(lessThanOrEqualTo(4d)));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=2..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         0..10 f4
//     * 2..4 f3 requires 2..2 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Gt1_UbBounded__Trg_LbEq2_UbEq2() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(2, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(0, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(2, 4, "f3").req(2, 2, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
//        assertThat(r.fi().ub().fInsVal("f4"), equalTo(2d));
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 2..4 f3 requires 0..0 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Gt1_UbBounded__Trg_LbEq0_UbEq0() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(2, 4, "f3").req(0, 0, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        assertThat(r.fi().ub().fTypeVal("f2"), equalTo(0d));
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), equalTo(0d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 2..4 f3 requires 2..4 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Gt1_UbBounded__Trg_LbGt1_UbBounded() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(2, 4, "f3").req(2, 4, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), both(greaterThanOrEqualTo(2d)).and(lessThanOrEqualTo(4d)));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 2..4 f3 requires 1..1 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Gt1_UbBounded__Trg_LbEq1_UbEq1() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 2).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 2).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(2, 4, "f3").req(1, 1, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 4));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), is(1d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=2..2
//     *     0..1 f1 @gi=0..20 @gt=0..1
//     *         0..3 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         0..10 f4
//     * 0..0 f3 requires 1..4 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq0_UbEq0__Trg_LbEq1_UbBounded() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(2, 2).children(
//                f("f1").fi(0, 1).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 3).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(0, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(0, 0, "f3").req(1, 4, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().lb().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().lb().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().lb().fInsVal("f4"), both(greaterThanOrEqualTo(1d)).and(lessThanOrEqualTo(4d)));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=2..2
//     *     0..3 f1 @gi=0..20 @gt=0..1
//     *         0..1 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         0..10 f4
//     * 0..0 f3 requires 2..2 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq0_UbEq0__Trg_LbEq2_UbEq2() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(2, 2).children(
//                f("f1").fi(0, 3).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 1).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(0, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(0, 0, "f3").req(2, 2, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().lb().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().lb().fInsVal("f4"), equalTo(2d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..1 f1 @gi=0..20 @gt=0..1
//     *         0..3 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 0..0 f3 requires 0..0 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq0_UbEq0__Trg_LbEq0_UbEq0() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 1).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 3).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(0, 0, "f3").req(0, 0, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        assertThat(r.fi().lb().fTypeVal("f2"), equalTo(0d));
//
//        assertThat(r.fi().lb().fInsVal("f4"), equalTo(0d));
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..3 f1 @gi=0..20 @gt=0..1
//     *         0..1 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 1..4 f3 requires 2..4 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq0_UbEq0__Trg_LbGt1_UbBounded() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 3).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(1, 1).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(3, 3, "f3").req(2, 4, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(1, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), both(greaterThanOrEqualTo(2d)).and(lessThanOrEqualTo(4d)));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 3..3 f3 requires 1..1 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq0_UbEq0__Trg_LbEq1_UbEq1() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(3, 3).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(1, 1).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(3, 3, "f3").req(1, 1, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(3, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), is(1d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=2..2
//     *     0..1 f1 @gi=0..20 @gt=0..1
//     *         0..3 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         0..10 f4
//     * 3..3 f3 requires 1..4 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq3_UbEq3__Trg_LbEq1_UbBounded() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(2, 2).children(
//                f("f1").fi(0, 1).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 3).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(0, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(3, 3, "f3").req(1, 4, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), both(greaterThanOrEqualTo(1d)).and(lessThanOrEqualTo(4d)));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=2..2
//     *     0..3 f1 @gi=0..20 @gt=0..1
//     *         0..1 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         0..10 f4
//     * 3..3 f3 requires 2..2 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq3_UbEq3__Trg_LbEq2_UbEq2() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(2, 2).children(
//                f("f1").fi(0, 3).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 1).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(0, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(3, 3, "f3").req(2, 2, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), equalTo(2d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..1 f1 @gi=0..20 @gt=0..1
//     *         0..3 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 3..3 f3 requires 0..0 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq3_UbEq3__Trg_LbEq0_UbEq0() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 1).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(0, 3).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(3, 3, "f3").req(0, 0, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(0, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//        assertThat(r.fi().ub().fTypeVal("f2"), equalTo(0d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), equalTo(0d));
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..3 f1 @gi=0..20 @gt=0..1
//     *         0..1 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 1..4 f3 requires 2..4 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq3_UbEq3__Trg_LbGt1_UbBounded() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(0, 3).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(1, 1).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(3, 3, "f3").req(2, 4, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(1, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), both(greaterThanOrEqualTo(2d)).and(lessThanOrEqualTo(4d)));
//    }
//
//    /**
//     * <pre>
//     * 1..1 f0 @gi=1..10 @gt=1..2
//     *     0..2 f1 @gi=0..20 @gt=0..1
//     *         0..2 f3
//     *     0..10 f2 @gi=1..20 @gt=0..1
//     *         1..10 f4
//     * 3..3 f3 requires 1..1 f4
//     * </pre>
//     */
//    @Test
//    public void test__Src_Lb_Eq3_UbEq3__Trg_LbEq1_UbEq1() {
//        //@formatter:off
//        Clafer root = f("f0").fi(1, 1).gi(1, 10).gt(1, 2).children(
//                f("f1").fi(3, 3).gi(0, 20).gt(0, 1).children(
//                        f("f3").fi(1, 1).leaf()),
//                f("f2").fi(0, 10).gi(1, 20).gt(0, 1).children(
//                        f("f4").fi(1, 10).leaf())
//        );
//        //@formatter:on
//        Container fm = fm(root);
//        cstr(fm).src(3, 3, "f3").req(1, 1, "f4");
//
//        FeatureTestUtil.FeatureTestResult r = FeatureTestUtil.testFeature(fm, "f3", fiCard(3, 3));
//
//        FeatureTestUtil.checkBranch(fm, "f1", r.fi().lb());
//        FeatureTestUtil.checkBranch(fm, "f3", r.fi().ub());
//
//        FeatureTestUtil.checkBranch(fm, "f4", r.fi().ub());
//
////        BinaryVar r1 = r.fi().ub().getMCtx().getBinaryVar(fm.getCtConstraints().get(0), true);
////        assertThat(r.fi().ub().getResult().getSolutions().get(r1), is(1d));
//
//        assertThat(r.fi().ub().fInsVal("f4"), is(1d));
//    }
//
//}
