//package org.cardygan.boundanalyzerRes;
//
//import org.cardygan.boundanalyzerRes.model.*;
//import org.cardygan.boundanalyzerRes.model.cstrlang.MBoolExpr;
//import org.cardygan.boundanalyzerRes.model.util.FmDsl;
//import org.cardygan.boundanalyzerRes.model.util.FmDsl.*;
//import org.cardygan.boundanalyzerRes.clafer.ResourceUtil;
//import org.cardygan.boundanalyzerRes.IlpBuilder.Obj;
//import org.cardygan.ilp.api.CplexSolver;
//import org.cardygan.ilp.api.Model;
//import org.cardygan.ilp.api.Result;
//import org.cardygan.ilp.api.Solver;
//
//import org.junit.jupiter.api.Test;
//
//import static org.cardygan.boundanalyzerRes.model.util.ExprDsl.*;
//import static org.cardygan.boundanalyzerRes.model.util.ExprDsl.intVar;
//import static org.cardygan.boundanalyzerRes.model.util.ExprDsl.param;
//import static org.hamcrest.CoreMatchers.equalTo;
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.greaterThanOrEqualTo;
//import static org.hamcrest.Matchers.is;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//public class FeatureTestUtil {
//
//    //private final static String CPLEX_LIB = "/Users/markus/Applications/IBM/ILOG/CPLEX_Studio1263/cplex/bin/x86-64_osx/";
//
//
////    public static FeatureGroupTestResult testFeature(Container fm, String f, InstCard fi, GroupCard gi,
////                                                     GroupTypeCard gt) {
////
////        // Feature instance
////        CfmContext context = new CfmContext(fm);
////        CardTestResult fi_r = testCard(fm, context, IlpBuilder.build(fm, context).with(f).fi(), f, fi);
////
////        // Group instance
////        context = new CfmContext(fm);
////        CardTestResult gi_r = testCard(fm, context, IlpBuilder.build(fm, context).with(f).gi(), f, gi);
////
////        // Group type
////        context = new CfmContext(fm);
////        CardTestResult gt_r = testCard(fm, context, IlpBuilder.build(fm, context).with(f).gt(), f, gt);
////        return new FeatureGroupTestResult(fi_r, gi_r, gt_r);
////    }
//
////    public static FeatureTestResult testFeature(Container fm, String f, Card fi) {
////        return new FeatureTestResult(testCard(fm, IlpBuilder.build(fm).with(f).fi(), f, fi));
////    }
//
////    public static FeatureTestResult testFeature(Container fm, String f, InstCard fi) {
////        // Feature instance
////        return new FeatureTestResult(testCard(fm, IlpBuilder.build(fm).with(f).fi(), f, fi, Optional.of(Arrays.asList(cstr))));
////    }
//
////    public static FeatureTestResult testFeature(Container fm, String f, InstCard fi) {
////        // Feature instance
////        return new FeatureTestResult(testCard(fm, IlpBuilder.build(fm).with(f).fi(), f, fi));
////    }
//
////    public static void testFeatureInfeasible(Container fm, String f) {
//////        testFeatureInfeasible(fm, f, Arrays.asList(IlpDsl.cstr("tmp_test").sum(IlpDsl.coef(1, IlpDsl.i_var(f + "_f"))).eq(fVal)));
////
////        testFeatureInfeasible(fm, f);
////    }
//
//
//    public static void testFeatureInfeasible(Container fm, String f, MBoolExpr cstr) {
//        fm.getConstraints().add(cstr);
//
//        Model ilp = IlpBuilder.build(fm).with(f).fi().min();
//
////        new ProxyResolver(ilp, new CplexSolver(CPLEX_LIB)).resolve();
//
//        Result r = ilp.solve(CplexSolver.create().build());
//
//        assertFalse(r.getStatistics().isFeasible());
//
//        fm.getConstraints().remove(cstr);
//    }
//
//
//    public static void testFeatureInfeasible(Container fm, String f, int val) {
//        MBoolExpr cstr = FmDsl.cstr(fm).expr((eq(intVar(f), param(val))));
//
//        Model ilp = IlpBuilder.build(fm).with(f).fi().min();
//
////        new ProxyResolver(ilp, new CplexSolver(CPLEX_LIB)).resolve();
//
//        Result r = ilp.solve(CplexSolver.create().build());
//
//        assertFalse(r.getStatistics().isFeasible());
//
//        fm.getConstraints().remove(cstr);
//    }
//
//    public static void testFeatureInfeasible(Container fm, String f) {
//        Model ilp = IlpBuilder.build(fm).with(f).fi().min();
//
////        new ProxyResolver(ilp, new CplexSolver(CPLEX_LIB)).resolve();
//
//        Result r = ilp.solve(CplexSolver.create().build());
//
//        assertFalse(r.getStatistics().isFeasible());
//    }
//
//    public static FeatureTestResult testFeature(Container fm, String f, InstCard fi, MBoolExpr cstrs) {
//        // Feature instance
//
//        fm.getConstraints().add(cstrs);
//
//
//        CfmContext context = new CfmContext(fm);
//        FeatureTestResult res = new FeatureTestResult(testCard(fm, context, IlpBuilder.build(fm, context).with(f).fi(), f, fi));
//
//        fm.getConstraints().remove(cstrs);
//        return res;
//
//    }
//
//    public static FeatureTestResult testFeature(Container fm, String f, InstCard fi) {
//        // Feature instance
//        CfmContext context = new CfmContext(fm);
//        return new FeatureTestResult(testCard(fm, context, IlpBuilder.build(fm, context).with(f).fi(), f, fi));
//    }
//
////    public static CardTestResult testCard(Container fm, Obj prev_ilp, String f, Card expected) {
////        return testCard(fm, prev_ilp, f, expected);
////    }
//
//    public static CardTestResult testCard(Container fm, CfmContext context, Obj prev_ilp, String f, Card expected) {
//
//        BoundTestResult lb = testLbCard(fm, context, f, prev_ilp, expected.getLb());
//        BoundTestResult ub;
//        if (expected.getUb() instanceof IntUb) {
//            ub = testUbCard(fm, context, f, prev_ilp, (IntUb) expected.getUb());
//        } else if (expected.getUb() instanceof InfUb) {
//            ub = testUbCard(fm, context, f, prev_ilp);
//        } else {
//            throw new IllegalStateException("Unkown type of upper bound.");
//        }
//
//        return new CardTestResult(lb, ub);
//    }
//
//    public static void testFeasible(Result r) {
//        assertThat(r.getStatistics().isFeasible(), is(true));
//    }
//
//    public static BoundTestResult testLbCard(Container fm, CfmContext context, String f, Obj prev_ilp, Lb expected) {
//        Model ilp = prev_ilp.min();
//
//
//        Solver solver = CplexSolver.create().build();
//
////        new ProxyResolver(ilp, solver).resolve();
//
//
//        Result r = ilp.solve(solver);
//        testFeasible(r);
//        assertThat(r.getObjVal().get(), equalTo(new Double(expected.getVal())));
//
//        return new BoundTestResult(r, context);
//    }
//
//    public static BoundTestResult testUbCard(Container fm, CfmContext context, String f, Obj prev_ilp, IntUb expected) {
//        Model ilp = prev_ilp.max();
//
//        Solver solver = CplexSolver.create().build();
//
////        new ProxyResolver(ilp, solver).resolve();
//
//        Result r = ilp.solve(solver);
//        testFeasible(r);
//        assertThat(r.getObjVal().get(), equalTo(new Double(expected.getVal())));
//
//        return new BoundTestResult(r, context);
//    }
//
//    public static BoundTestResult testUbCard(Container fm, CfmContext context, String f, Obj prev_ilp) {
//        Model ilp = prev_ilp.max();
//
//        Solver solver = CplexSolver.create().build();
//
////        new ProxyResolver(ilp, solver).resolve();
//
//        Result r = ilp.solve(solver);
//
//        assertThat(r.getStatistics().isUnbounded(), is(true));
//
//        Clafer feature = ResourceUtil.getFeature(fm, f).get();
//
////        assertThat(r.getObjVal().get(), greaterThan(new Double(ilp.getContext().getTreeBound(feature))));
////        if (r.getStatistics().isFeasible()) {
////            assertTrue(context.isUnbounded(feature, r.getObjVal().get()));
////        }
//
//        return new BoundTestResult(r, context);
//    }
//
//    /**
//     * Checks if parent features' type variables in given result are set to 1
//     * and features' instance variable are greater than 1.
//     *
//     * @param fm
//     * @param fName
//     */
//    public static void checkBranch(Container fm, String fName, BoundTestResult result) {
//
//        assertThat(result.fTypeVal(fName), is(equalTo(1d)));
//        assertThat(result.fInsVal(fName), is(greaterThanOrEqualTo(1d)));
//
//        Clafer f = ResourceUtil.getFeature(fm, fName).get();
//        if (!ResourceUtil.isRoot(f)) {
//            checkBranch(fm, f.getParent().getName(), result);
//        }
//    }
//
//    public static class FeatureTestResult {
//
//        private final CardTestResult fi;
//
//        public FeatureTestResult(CardTestResult fi) {
//            this.fi = fi;
//        }
//
//        public CardTestResult fi() {
//            return fi;
//        }
//
//    }
//
//    public static class FeatureGroupTestResult extends FeatureTestResult {
//
//        private final CardTestResult gi;
//        private final CardTestResult gt;
//
//        public FeatureGroupTestResult(CardTestResult fi, CardTestResult gi, CardTestResult gt) {
//            super(fi);
//            this.gi = gi;
//            this.gt = gt;
//        }
//
//        public CardTestResult gi() {
//            return gi;
//        }
//
//        public CardTestResult gt() {
//            return gt;
//        }
//    }
//
//    public static class CardTestResult {
//        private final BoundTestResult lb;
//        private final BoundTestResult ub;
//
//        public CardTestResult(BoundTestResult lb, BoundTestResult ub) {
//            super();
//            this.lb = lb;
//            this.ub = ub;
//        }
//
//        public BoundTestResult lb() {
//            return lb;
//        }
//
//        public BoundTestResult ub() {
//            return ub;
//        }
//
//    }
//
//    public static class BoundTestResult {
//        private final Result r;
//        private final CfmContext mCtx;
//
//        public BoundTestResult(Result r, CfmContext mCtx) {
//            this.r = r;
//            this.mCtx = mCtx;
//        }
//
//        public double fInsVal(String fName) {
//            Clafer f = ResourceUtil.getFeature(mCtx.getFm(), fName).get();
//
//            return r.getSolutions().get(mCtx.getIntVar(f));
//        }
//
//        public double fTypeVal(String fName) {
//            Clafer f = ResourceUtil.getFeature(mCtx.getFm(), fName).get();
//
//            return r.getSolutions().get(mCtx.getBinaryVar(f));
//        }
//
//        public CfmContext getMCtx() {
//            return mCtx;
//        }
//
//        public Result getResult() {
//            return r;
//        }
//
//    }
//}
