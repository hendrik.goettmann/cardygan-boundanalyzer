grammar ClaferRx;

module
   : topLevelElement*
   ;

topLevelElement
    : element
    | objective
    | assertion
    ;

element 
	: clafer
	| constraint
	;

clafer
	: abstr? giMult claferName parent? reference? fiMult childElements
	;

abstr
	: ABSTR
	;

parent
	: ':' identifier
	;

reference
	: intSetRef
	| doubleSetRef
	| setRef
	| intBagRef
	| doubleBagRef
	| bagRef
	| stringSetRef
	| stringBagRef
	;

setRef
    : '->' setExpr
    ;


stringSetRef
    : '->' stringSet
    ;

stringBagRef
    : '->>' stringSet
    ;

intSetRef
    : '->' intSet
    ;

doubleSetRef
    : '->' realSet
    ;

bagRef
    : '->>' setExpr
    ;

intBagRef
    : '->>' intSet
    ;

doubleBagRef
    : '->>' realSet
    ;

childElements
	: empty
	| LEFT_CURLY element+ RIGHT_CURLY
	;


empty
	: LEFT_CURLY RIGHT_CURLY
	;


giMult
	: mult
	;

fiMult
	: mult
	;

mult
	: lb '..' ub
	;

lb
	: NUMBER
	;

ub
	: NUMBER
	| INF
	;

 objective
    : '<<' 'minimize' numericExpr '>>' #minObj
    | '<<' 'maximize' numericExpr '>>' #maxObj
    ;

 assertion
    : 'assert' '[' boolExpr ']'
    ;

 constraint
 	: '[' boolExpr ']'
 	;

 boolExpr
    : 'if' boolExpr 'then' boolExpr 'else' boolExpr #iteBool
    | quantifiedExpr #quantified
    | simpleQuantifiedExpr #simpleQuantified
    | '(' boolExpr ')' #boolBrackets
    | boolExpr '&&' boolExpr #And
    | boolExpr '||' boolExpr #Or
    | boolExpr 'xor' boolExpr #XOr
 	| boolExpr '<=>' boolExpr #BiImpl
 	| boolExpr '=>' boolExpr #Impl
    |'!' boolExpr #Not
 	| numericComp #Rel
    | setComp #setComparison
//        | boolExpr ('&&' boolExpr)+ #And

	;

simpleQuantifiedExpr
    : 'lone' setExpr #loneSimpleQuant
    | 'one' setExpr #oneSimpleQuant
    | 'some' setExpr #someSimpleQuant
    | ('no'|'not') setExpr #noSimpleQuant
    ;

quantifiedExpr
    : 'all' 'disj' localDecl+ '|' boolExpr #allDisjQuant
    | 'all' localDecl+ '|' boolExpr #allQuant
    | 'one' 'disj' localDecl+ '|'  boolExpr #oneDisjQuant
    | 'one' localDecl+ '|' boolExpr #oneQuant
    | 'some' 'disj' localDecl+ '|' boolExpr #someDisjQuant
    | 'some' localDecl+ '|' boolExpr #someQuant
    | 'no' 'disj' localDecl+ '|' boolExpr #noDisjQuant
    | 'no' localDecl+ '|' boolExpr #noQuant
    ;

localDecl
    : localIdentifier (';' localIdentifier )* ':' setExpr
    ;

numericComp
	: numericExpr '>=' numericExpr #ge
	| numericExpr '>' numericExpr #gt
	| numericExpr '<=' numericExpr #le
	| numericExpr '<' numericExpr #lt
	| numericExpr '=' numericExpr #eq
	| numericExpr '!=' numericExpr #neq
	;

setComp
    : setExpr '=' setExpr #setEq
    | setExpr '!=' setExpr #setUnEq
    | setExpr 'in' setExpr #setIn
    | setExpr 'not in' setExpr #setNotIn
    ;

setExpr
    : '('setExpr')' #setBrackets
    | setExpr '++' setExpr #setUnion
    | setExpr '--' setExpr #setDiff
    | setExpr '**' setExpr #setIntersec
    | 'if' boolExpr 'then' setExpr 'else' setExpr #iteSet
    | setElement #join
    | stringExpr #stringElement
    ;

setElement
    : path
    ;

path
    : identifier ('.' identifier)*
    ;

numericExpr
    : 'if' boolExpr 'then' numericExpr 'else' numericExpr #iteNumeric
    | numericExpr '*' numericExpr #multiply
    | numericExpr '/' numericExpr #div
    | numericExpr '+' numericExpr #add
    | numericExpr '-' numericExpr #substr
    | numericExpr '%' numericExpr #mod
    | '(' numericExpr ')' #parentheses
    | '-' numericExpr #neg
    | 'sum' setExpr #sum
    | 'product' setExpr #product
    | '#' setExpr #setCard
    | 'min' setExpr #min
    | 'max' setExpr #max
    | numVar #var
    | intLiteral #intLit
    | realLiteral #realLit
    ;

 numVar
    : path '.' DREF_KEYWORD
    ;

identifier
    : drefClafer
    | thisClafer
    | parentClafer
    | claferName
    | localIdentifier
    ;

claferName
    : CLAFER_ID
 	;

localIdentifier
    : ID
    ;

name
    : ID
    ;

drefClafer
    : DREF_KEYWORD
    ;

thisClafer :
    THIS_KEYWORD
    ;

parentClafer :
    PARENT_KEYWORD
    ;

realSet
    : REAL_KEYWORD
    | DOUBLE_KEYWORD
    ;

stringSet
    : STRING_KEYWORD
    ;


intSet
    : INTEGER_KEYWORD
    ;

realLiteral
	: NUMBER
	;

intLiteral
    : INTEGER
    ;

stringExpr
    : STRING
    ;

LEFT_CURLY
	: '{'
	;

RIGHT_CURLY
	: '}'
	;

ABSTR
	: 'abstract'
	;

INF 
	: '*'
	;

STRING_KEYWORD : 'string' ;

INTEGER_KEYWORD : 'integer' | 'int' ;

REAL_KEYWORD : 'real' ;

DOUBLE_KEYWORD : 'double';

DREF_KEYWORD : 'dref';

THIS_KEYWORD : 'this';

PARENT_KEYWORD : 'parent';

CLAFER_ID :
    'c'DIGIT+'_'ID
    ;

ID : ID_LETTER (ID_LETTER | DIGIT)* ; // From C language

STRING
   : '"' (ESC | ~ ["\\])* '"'
   ;

NUMBER
   : '-'? INT '.' [0-9] + EXP? | '-'? INT EXP | '-'? INT
   ;

INTEGER
    : INT
    ;

WS
   : [ \t\n\r] + -> skip
   ;

COMMENT
    : '/*' .*? '*/' -> skip
;

LINE_COMMENT
    : '//' ~[\r\n]* -> skip
;

fragment ID_LETTER : 'a'..'z'|'A'..'Z'|'_' ;

fragment DIGIT : '0'..'9' ;

fragment INT
   : '0' | [1-9] [0-9]*
   ;
// no leading zeros
fragment EXP
   : [Ee] [+\-]? INT
   ;
// \- since - means "range" inside [...]

fragment ESC
   : '\\' (["\\/bfnrt] | UNICODE)
   ;
fragment UNICODE
   : 'u' HEX HEX HEX HEX
   ;
fragment HEX
   : [0-9a-fA-F]
   ;