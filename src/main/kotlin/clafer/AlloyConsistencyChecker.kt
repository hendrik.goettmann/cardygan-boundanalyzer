package clafer

import edu.mit.csail.sdg.alloy4.A4Reporter
import edu.mit.csail.sdg.alloy4compiler.ast.Command
import edu.mit.csail.sdg.alloy4compiler.parser.AlloyCompiler
import edu.mit.csail.sdg.alloy4compiler.translator.A4Options
import edu.mit.csail.sdg.alloy4compiler.translator.TranslateAlloyToKodkod
import ir.IrGraph
import ir.convert2Clafer
import mu.KotlinLogging
import org.cardygan.boundanalyzer.antlr.ClaferCompiler
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import edu.mit.csail.sdg.alloy4compiler.ast.CommandScope
import java.util.ArrayList
import java.util.concurrent.TimeoutException


class AlloyConsistencyChecker(val bitwidth: Int = -1) : ConsistencyCheckerI {

    private val logger = KotlinLogging.logger {}

    override fun check(graph: IrGraph, scope: Int, timeout: Long, unit: TimeUnit): SolverRes =
            check(graph.convert2Clafer(false), scope, timeout, unit)

    override fun check(claferSpec: String, scope: Int, timeout: Long, unit: TimeUnit): SolverRes {


        val alloySpec = ClaferCompiler.compile(claferSpec, ClaferCompiler.OutputType.ALLOY)

        loadLibrary()
        val rep = A4Reporter()

        val options = A4Options()

        options.coreMinimization = 2
        options.solver = A4Options.SatSolver.MiniSatProverJNI

        val world = AlloyCompiler.parse(rep, alloySpec)
        var command = world.allCommands[0]

        val bitwidthTmp = if (bitwidth < 1) binlog(2 * scope + 1) else bitwidth

        logger.info { "Checking specification with scope $scope and bitwidth $bitwidthTmp and timeout $timeout ${unit.name}." }


        val newScope = ArrayList<CommandScope>()
        for (sig in world.allSigs) {
            val newVal = when {
                sig.isOne != null || sig.isLone != null -> 1
                else -> scope
            }
            val scopeCmd = command.getScope(sig) ?: CommandScope(sig, false, newVal)
            newScope.add(scopeCmd)
        }

        command = Command(command.pos, command.label, command.check, scope,
                bitwidthTmp, command.maxseq, command.expects, newScope,
                command.additionalExactScopes, command.formula, command.parent)

        var res = SolverRes.TIMEOUT

        val executor = Executors.newSingleThreadExecutor()

        val f = executor.submit(Callable {
            try {
                val ans = TranslateAlloyToKodkod.execute_command(rep, world.allReachableSigs, command, options)
                res = if (ans.satisfiable()) {
                    if (ans.allAtoms.any() || ans.next().satisfiable()) {
                        logger.info { "Valid instance found." }
                        SolverRes.FEASIBLE
                    } else {
                        logger.info { "NO valid instance found." }
                        SolverRes.INFEASIBLE
                    }
                } else {
                    logger.info { "NO valid instance found." }
                    SolverRes.INFEASIBLE
                }
            } catch (e: Exception) {
                res = SolverRes.EXCEPTION
                e.printStackTrace()
            }
        })
        try {
            f.get(timeout, unit)
        } catch (e: TimeoutException) {
            f.cancel(true)
        }
        executor.shutdown()

        return res
    }

    fun binlog(bits: Int) // returns 0 for bits=0
            : Int {
        var bits = bits
        var log = 0
        if (bits and -0x10000 != 0) {
            bits = bits ushr 16
            log = 16
        }
        if (bits >= 256) {
            bits = bits ushr 8
            log += 8
        }
        if (bits >= 16) {
            bits = bits ushr 4
            log += 4
        }
        if (bits >= 4) {
            bits = bits ushr 2
            log += 2
        }
        return log + bits.ushr(1)
    }


    private fun loadLibrary() {
        try {
            System.loadLibrary("minisatprover")
        } catch (e: UnsatisfiedLinkError) {
            System.loadLibrary("minisatproverx1")
        }

    }


}