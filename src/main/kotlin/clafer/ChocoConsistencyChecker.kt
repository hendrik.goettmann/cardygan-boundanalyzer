package clafer

import ir.IrGraph
import ir.convert2Clafer
import mu.KotlinLogging
import org.cardygan.boundanalyzer.antlr.ClaferCompiler
import org.clafer.ast.AstClafer
import org.clafer.compiler.ReachedLimitException
import org.clafer.javascript.Javascript
import org.clafer.scope.Scope
import java.io.File
import java.io.PrintWriter
import java.lang.Error
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException


class ChocoConsistencyChecker : ConsistencyCheckerI {

    private val logger = KotlinLogging.logger {}


    override fun check(graph: IrGraph, scope: Int, timeout: Long, unit: TimeUnit): SolverRes =
            check(graph.convert2Clafer(false), scope, timeout, unit)

    override fun check(claferSpec: String, scope: Int, timeout: Long, unit: TimeUnit): SolverRes {
        logger.info { "Checking specification with scope $scope and timeout $timeout ${unit.name}." }

        println(claferSpec)

        val tempFile: File? = File.createTempFile("prefix-", ".cfr")
        tempFile!!.deleteOnExit()

        PrintWriter(tempFile).use { out -> out.println(claferSpec) }

        val jsFile = ClaferCompiler.compile(claferSpec, ClaferCompiler.OutputType.JS)
        val model = Javascript.readModel(jsFile).model


//        fun getChildren(c: AstClafer): List<AstClafer> {
//            return c.children.map { getChildren(it) }.flatten()
//        }
//
//        val allClafers = model.children.map { getChildren(it) }.flatten() + model.children
//        val claferScope = Scope.defaultScope(scope)
//        allClafers.forEach { claferScope.setScope(it, 18) }

        val solver = org.clafer.compiler.ClaferCompiler.compile(model,
                Scope.defaultScope(scope)
                        .intLow(-10000).intHigh(10000))


        var res = SolverRes.TIMEOUT

        val executor = Executors.newSingleThreadExecutor()
        val f = executor.submit(Callable {

            try {
                res = if (solver.find()) {
                    try {
                        if (solver.instance().topClafers.isNotEmpty() || solver.find()) {
                            logger.info { "Valid instance found." }
//                                println(solver.instance().print())
//                                println(solver.instance().topClafers.map { "${it.id}  ${it.type.name}" }.joinToString("."))
                            SolverRes.FEASIBLE
                        } else {
                            logger.info { "NO valid instance found." }
                            SolverRes.INFEASIBLE
                        }
                    } catch (e: ReachedLimitException) {
                        logger.info { "NO valid instance found." }
                        SolverRes.INFEASIBLE
                    }
                } else {
                    logger.info { "NO valid instance found." }
                    SolverRes.INFEASIBLE
                }
            } catch (e: Error) {
                res = SolverRes.EXCEPTION
                e.printStackTrace()
            }

        })
        try {
            f.get(timeout, unit)
        } catch (e: TimeoutException) {
            f.cancel(true)
        }
        executor.shutdown()
        return res
    }


}