package clafer

import ir.IrGraph
import java.util.concurrent.TimeUnit

interface ConsistencyCheckerI {


//    fun check(claferSpec: String, scope: Int, timeout: Long, unit: TimeUnit): SolverRes

    fun check(graph: IrGraph, scope: Int, timeout: Long, unit: TimeUnit): SolverRes

    fun check(claferSpec: String, scope: Int, timeout: Long, unit: TimeUnit): SolverRes

}

enum class SolverRes {
    TIMEOUT, FEASIBLE, INFEASIBLE, EXCEPTION
}