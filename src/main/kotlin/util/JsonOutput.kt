package util

import com.google.gson.*
import ir.constraints.Path
import ir.constraints.prettyPrint
import org.cardygan.ilp.api.model.Model
import java.io.File
import java.io.FileWriter
import java.lang.reflect.Type

fun writeStatsToJsonFile(outputFilePath: String, specName: String, stats: Any) {
    val gsonBuilder = GsonBuilder()
    gsonBuilder.registerTypeAdapter(Path::class.java, PathSerializer())
    gsonBuilder.registerTypeAdapter(Model::class.java, IlpModelSerializer())

    val timestamp = (System.currentTimeMillis() / 1000).toString()

    val file = File(outputFilePath)
    val outputFile = if (file.isDirectory && file.canWrite())
        "$outputFilePath/$specName-$timestamp.json"
    else if (file.parentFile.canWrite()) {
        if (file.exists()) {
            file.renameTo(File(file.parentFile, "${file.nameWithoutExtension}_$timestamp.${file.extension}"))
        }
        outputFilePath
    } else
        throw RuntimeException("Output file/dir not accessible.")

    FileWriter(outputFile).use { writer ->
        val gson = gsonBuilder.setPrettyPrinting().create()
        gson.toJson(stats, writer)
    }
}

private class IlpModelSerializer : JsonSerializer<Model> {
    override fun serialize(src: Model, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        val obj = JsonObject()
        obj.addProperty("noDecVars", src.vars.size)
        obj.addProperty("noConstraints", src.constraints.size)
        return obj
    }
}

private class PathSerializer : JsonSerializer<Path> {
    override fun serialize(src: Path, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        return JsonPrimitive(prettyPrint(src))
    }
}