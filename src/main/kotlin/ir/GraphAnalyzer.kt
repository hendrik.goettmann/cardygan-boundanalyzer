package ir

import ir.constraints.*

fun IrGraph.analyze(stats: AstAnalyzerStats = AstAnalyzerStats()): AstAnalyzerStats {
    stats.noClafer = (this.nodes.filter { it.type == Node.NodeType.STD } - cRoot).count()
    stats.noSetReference = this.setRefs.count()
    stats.noBagReference = this.bagRefs.count()
    stats.noIntSetReference = this.nodes.filter { hasIntRef(it) }.count()
    stats.noDoubleSetReference = this.nodes.filter { hasDoubleRef(it) }.count()
    stats.noConstraint = this.cstrs.map { it.value.size }.count()
    stats.noAbsClafer = (this.nodes.filter { it.isAbstract && it.type == Node.NodeType.STD } - cRoot).count()
    stats.noUnboundedClaferMult = this.claferMult.filter { it.key != cRoot && it.value.ub is InfUb }.count()
    stats.noUnboundedGroupMult = this.groupMult.filter { it.key != cRoot && it.value.ub is InfUb }.count()
    stats.noClaferMult = this.claferMult.filter { it.key != cRoot }.count()
    stats.noGroupMult = this.groupMult.filter { it.key != cRoot }.count()
    cstrs.values.flatten().forEach { analyzeElement(it, stats) }
    return stats
}


fun analyzeElement(element: AstElement, stats: AstAnalyzerStats = AstAnalyzerStats()): AstAnalyzerStats = AstAnalyzer().analyze(element, stats)


class AstAnalyzer {

    fun analyze(element: AstElement, stats: AstAnalyzerStats): AstAnalyzerStats {
        visitAst(element, VisitorParams(
                visitPath = {
                    val drefsCount = it.elements.count { it == DrefIdentfier }

                    if (drefsCount > 0 && it.elements.last() != DrefIdentfier)
                        stats.noCenterDref++

                    when (drefsCount) {
                        0 -> stats.noSimplePath++
                        1 -> stats.no1stOrderDref++
                        2 -> stats.no2ndOrderDref++
                        3 -> stats.no3rdOrderDref++
                        4 -> stats.no4thOrderDref++
                        else -> stats.noHigherOrderDref++
                    }
                },
                visitSomeQuantExpr = {
                    stats.noSomeQuantExpr++
                    stats.noComplexQuant++
                },
                visitSomeDisjQuantExpr = {
                    stats.noSomeDisjQuantExpr++
                    stats.noComplexQuant++
                },
                visitOneQuantExpr = {
                    stats.noOneQuantExpr++
                    stats.noComplexQuant++
                },
                visitOneDisjQuantExpr = {
                    stats.noOneDisjQuantExpr++
                    stats.noComplexQuant++
                },
                visitNoDisjQuantExpr = {
                    stats.noNoDisjQuantExpr++
                    stats.noComplexQuant++
                },
                visitNoQuantExpr = {
                    stats.noNoQuantExpr++
                    stats.noComplexQuant++
                },
                visitAllQuantExpr = {
                    stats.noAllQuantExpr++
                    stats.noComplexQuant++
                },
                visitAllDisjQuantExpr = {
                    stats.noAllDisjQuantExpr++
                    stats.noComplexQuant++
                },
                visitSome = {
                    stats.noSome++
                    stats.noSimpleQuant++
                },
                visitOne = {
                    stats.noOne++
                    stats.noSimpleQuant++
                },
                visitNo = {
                    stats.noNo++
                    stats.noSimpleQuant++
                },
                visitLone = {
                    stats.noLone++
                    stats.noSimpleQuant++
                },
                visitIteNumExpr = {
                    stats.noIteNumExpr++
                },
                visitIteSet = { stats.noIteSet++ },
                visitIteBoolExpr = { stats.noIteBoolExpr++ },
                visitMax = { stats.noMax++ },
                visitMin = { stats.noMin++ },
                visitSetElement = { stats.noSetElement++ },
                visitNot = { stats.noNot++ },
                visitIdentifier = { stats.noIdentifier++ },
                visitNumVar = { stats.noNumVar++ },
                visitAnd = { stats.noAnd++ },
                visitAdd = { stats.noAdd++ },
                visitBiImpl = { stats.noBiImpl++ },
                visitCard = { stats.noCard++ },
                visitDiv = {
                    if (it.rhs !is NumLiteral)
                        stats.noComplexMult++
                    stats.noDiv++
                },
                visitDoubleVal = { stats.noDoubleVal++ },
                visitEq = {
                    stats.noEq++
                },
                visitEqSet = { stats.noEqSet++ },
                visitGeq = {
                    stats.noGeq++
                },
                visitGt = {
                    stats.noGt++
                },
                visitImpl = { stats.noImpl++ },
                visitInSet = { stats.noInSet++ },
                visitIntersect = { stats.noIntersect++ },
                visitIntVal = { stats.noIntVal++ },
                visitLeq = {
                    stats.noLeq++
                },
                visitLocalIdentifier = { stats.noLocalIdentifier++ },
                visitLocalSetDecl = { stats.noLocalSetDecl++ },
                visitLt = {
                    stats.noLt++
                },
                visitMinus = { stats.noMinus++ },
                visitModulo = { stats.noModulo++ },
                visitMult = {
                    if (it.lhs is NumLiteral || it.rhs is NumLiteral)
                        stats.noSimpleMult++
                    else
                        stats.noComplexMult++
                    stats.noMult++
                },
                visitNeg = { stats.noNeg++ },
                visitNotEqSet = { stats.noNotEqSet++ },
                visitNotInSet = { stats.noNotInSet++ },
                visitOr = { stats.noOr++ },
                visitProduct = { stats.noProduct++ },
                visitRemoval = { stats.noRemoval++ },
                visitStringSetElement = { stats.noStringSetElement++ },
                visitSum = { stats.noSum++ },
                visitUnion = { stats.noUnion++ },
                visitXor = { stats.noXor++ }
        ))
        return stats
    }

}

data class AstAnalyzerStats(
        var noClafer: Int = 0,
        var noAbsClafer: Int = 0,
        var noClaferMult: Int = 0,
        var noSetReference: Int = 0,
        var noBagReference: Int = 0,
        var noGroupMult: Int = 0,
        var noLb: Int = 0,
        var noIntUb: Int = 0,
        var noInfUb: Int = 0,
        var noConstraint: Int = 0,
        var noAssert: Int = 0,
        var noMinObjective: Int = 0,
        var noMaxObjective: Int = 0,
        var noIteBoolExpr: Int = 0,
        var noNot: Int = 0,
        var noAnd: Int = 0,
        var noOr: Int = 0,
        var noEq: Int = 0,
        var noLeq: Int = 0,
        var noGeq: Int = 0,
        var noLt: Int = 0,
        var noGt: Int = 0,
        var noLone: Int = 0,
        var noOne: Int = 0,
        var noNo: Int = 0,
        var noSome: Int = 0,
        var noEqSet: Int = 0,
        var noNotEqSet: Int = 0,
        var noInSet: Int = 0,
        var noNotInSet: Int = 0,
        var noIteNumExpr: Int = 0,
        var noMult: Int = 0,
        var noDiv: Int = 0,
        var noAdd: Int = 0,
        var noMinus: Int = 0,
        var noModulo: Int = 0,
        var noCard: Int = 0,
        var noSum: Int = 0,
        var noProduct: Int = 0,
        var noMin: Int = 0,
        var noMax: Int = 0,
        var noNumVar: Int = 0,
        var noIntVal: Int = 0,
        var noDoubleVal: Int = 0,
        var noIteSet: Int = 0,
        var noUnion: Int = 0,
        var noIntersect: Int = 0,
        var noRemoval: Int = 0,
        var noSetElement: Int = 0,
        var noIdentifier: Int = 0,
        var noAllDisjQuantExpr: Int = 0,
        var noAllQuantExpr: Int = 0,
        var noNoQuantExpr: Int = 0,
        var noNoDisjQuantExpr: Int = 0,
        var noOneQuantExpr: Int = 0,
        var noOneDisjQuantExpr: Int = 0,
        var noSomeQuantExpr: Int = 0,
        var noSomeDisjQuantExpr: Int = 0,
        var noLocalSetDecl: Int = 0,
        var noStringSetElement: Int = 0,
        var noNeg: Int = 0,
        var noBiImpl: Int = 0,
        var noXor: Int = 0,
        var noImpl: Int = 0,
        var noPath: Int = 0,
        var noLocalIdentifier: Int = 0,
        var noIntSetReference: Int = 0,
        var noDoubleSetReference: Int = 0,
        var noIntBagReference: Int = 0,
        var noDoubleBagReference: Int = 0,
        var noComplexDiv: Int = 0,
        var noComplexMult: Int = 0,
        var noSimpleMult: Int = 0,
        var noUnboundedClaferMult: Int = 0,
        var noUnboundedGroupMult: Int = 0,
        var noSimplePath: Int = 0,
        var noCenterDref: Int = 0,
        var no1stOrderDref: Int = 0,
        var no2ndOrderDref: Int = 0,
        var no3rdOrderDref: Int = 0,
        var no4thOrderDref: Int = 0,
        var noHigherOrderDref: Int = 0,
        var noComplexQuant: Int = 0,
        var noSimpleQuant: Int = 0

)