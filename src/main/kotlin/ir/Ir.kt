package ir

import ir.constraints.*
import org.cardygan.boundanalyzer.antlr.ClaferCompiler
import org.cardygan.boundanalyzer.antlr.ClaferParser
import org.cardygan.boundanalyzer.antlr.ResourceUtil
import java.io.InputStream
import java.util.*
import java.util.concurrent.atomic.AtomicInteger


private val idGen = AtomicInteger(0)

class IrGraph(val cRoot: Node = Node("cRoot"), val emptyset: Node = Node("cEmptyset")) {

    /**
     * Represents an empty set with clafer multiplicity interval 0..0
     */


    val setRefs: MutableMap<Node, SetExpr> = mutableMapOf()

    val bagRefs: MutableMap<Node, SetExpr> = mutableMapOf()

    val cstrs: MutableMap<Node, List<BoolExpr>> = mutableMapOf()

    val claferMult: MutableMap<Node, ClaferMult> = mutableMapOf()
    val groupMult: MutableMap<Node, GroupMult> = mutableMapOf()

    val nodes: MutableSet<Node> = mutableSetOf()
    val outgoing: MutableMap<Node, MutableSet<Edge>> = mutableMapOf()
    val incoming: MutableMap<Node, MutableSet<Edge>> = mutableMapOf()

    init {
        claferMult[cRoot] = ClaferMult(1, 1)
        groupMult[cRoot] = GroupMult(0)
        claferMult[emptyset] = ClaferMult(0, 0)
        groupMult[emptyset] = GroupMult(0)
        add(Edge(this.emptyset, this.cRoot, Edge.EdgeType.P))
    }

    /**
     * Returns pair of clafer multiplicities. Iff interval is unbound, second entry of pair is less than 0.
     */
    fun cmult(node: Node): Pair<Int, Int> {
        val bound = claferMult[node]!!
        val lb = bound.lb.lb
        val ub = if (bound.ub is IntUb) bound.ub.ub else -1
        return Pair(lb, ub)
    }

    /**
     * Returns pair of grouo multiplicities. Iff interval is unbound, second entry of pair is less than 0.
     */
    fun gmult(node: Node): Pair<Int, Int> {
        val bound = groupMult[node]!!
        val lb = bound.lb.lb
        val ub = if (bound.ub is IntUb) bound.ub.ub else -1
        return Pair(lb, ub)
    }

    fun add(edge: Edge) {
        nodes.add(edge.src)
        nodes.add(edge.trg)
        outgoing.getOrPut(edge.src) { mutableSetOf() }.add(edge)
        incoming.getOrPut(edge.trg) { mutableSetOf() }.add(edge)
    }

    fun remove(node: Node) {
        nodes.remove(node)
        outgoing.remove(node)
        incoming.remove(node)
        claferMult.remove(node)
        groupMult.remove(node)

        outgoing.forEach {
            it.value.removeIf { e -> e.trg == node }
        }

        incoming.forEach {
            it.value.removeIf { e -> e.src == node }
        }

        cstrs.remove(node)
        setRefs.remove(node)
        bagRefs.remove(node)
    }

    fun remove(edge: Edge) {
        assert(outgoing.contains(edge.src))
        assert(incoming.contains(edge.trg))
        outgoing[edge.src]!!.remove(edge)
        incoming[edge.trg]!!.remove(edge)

        // remove nodes of edges if nodes are not connected to any edges of the graph
        if (outgoing[edge.src]?.isEmpty() == true && incoming[edge.src]?.isEmpty() == true)
            remove(edge.src)

        if (outgoing[edge.trg]?.isEmpty() == true && incoming[edge.trg]?.isEmpty() == true)
            remove(edge.trg)

        // remove target node if target node is an auxiliary node and has no other clafer referencing it
        if (edge.trg.type != Node.NodeType.STD && incoming[edge.trg]?.isNotEmpty() == false)
            remove(edge.trg)
    }

    /**
     * Get node by name (if name is unique) or pathExpr.
     * Path may contain both prefixed clafer names c0_name or clafer names without prefix.
     * Returns root node if pathExpr is empty
     * Throws NoSuchElementException if no clafer is found on given pathExpr.
     */
    fun getNode(path: String): Node {

        // returns root node if pathExpr is empty
        if (path == "")
            return cRoot

        fun visitPath(curNode: Node, toBeVisited: List<String>): Node {

            val next = toBeVisited.firstOrNull() ?: return curNode

            // first try to find clafer for given (potentially prefixed) name
            val candidates = getNested(curNode).filter { it.name == next }

            if (candidates.size > 1)
                throw IllegalStateException("Two clafers with the same ID $next are nested in clafer $curNode.")
            if (candidates.size == 1)
                return visitPath(candidates.first(), toBeVisited.drop(1))

            // now try to find clafer for given name without prefixes
            return getNested(curNode)
                    .filter { matchClaferName(it.name, next) }
                    .let { if (it.isEmpty()) throw NoSuchElementException();it }
                    .let { if (it.size > 1) throw IllegalStateException("Two clafers with same ID were found.");it }
                    .map { visitPath(it, toBeVisited.drop(1)) }
                    .let { assert(it.size == 1);it.first() }
        }

        val remPath = path.split(".")

        // try to resolve globally first
        if (remPath.size == 1) {
            // again, first with prefix
            val candidates = nodes.filter { it.name == remPath.first() }
            if (candidates.size == 1)
                return candidates.first()

            // try again without prefix
            val res = nodes.filter { matchClaferName(it.name, remPath.first()) }.toSet()
            if (res.size == 1)
                return res.first()
        }

        // try to resolve as pathExpr
        return visitPath(cRoot, remPath)
    }


    fun getNested(node: Node) = incoming.getOrDefault(node, mutableSetOf())
            .filter { it.type == Edge.EdgeType.P }
            .map { it.src }

    fun getParent(node: Node) = outgoing.getOrDefault(node, mutableSetOf())
            .filter { it.type == Edge.EdgeType.P }
            .map { it.trg }
            .let { assert(it.size <= 1); it }
            .firstOrNull()

    fun getSuper(node: Node) = outgoing.getOrDefault(node, mutableSetOf())
            .filter { it.type == Edge.EdgeType.A }
            .map { it.trg }
            .let {
                assert(it.size <= 1)
                it
            }
            .firstOrNull()

    fun getSub(node: Node) = incoming.getOrDefault(node, mutableSetOf())
            .filter { it.type == Edge.EdgeType.A }
            .map { it.src }

    fun getRefsFrom(node: Node) = outgoing.getOrDefault(node, mutableSetOf())
            .filter { it.type == Edge.EdgeType.R }
            .map { it.trg }

    fun getRefsTo(node: Node) = incoming.getOrDefault(node, mutableSetOf())
            .filter { it.type == Edge.EdgeType.R }
            .map { it.src }

    fun getNestedInSuper(node: Node): Set<Node> {
        val ret = mutableSetOf<Node>()
        var superNode = getSuper(node)

        while (superNode != null) {
            ret.addAll(getNested(superNode))
            superNode = getSuper(superNode)
        }

        return ret
    }

    fun getConcreteSubs(absNode: Node): Set<Node> {
        assert(absNode.isAbstract) { "Node $absNode needs to be abstract." }

        fun visitSub(current: Node): Set<Node> {
            return if (!current.isAbstract)
                setOf(current)
            else
                getSub(current).map { visitSub(it) }.flatten().toSet()
        }

        return visitSub(absNode)
    }

    fun getSuperTransitive(n: Node): Set<Node> {
        var nextSuper: Node? = getSuper(n)
        val ret: MutableSet<Node> = mutableSetOf()

        // break cycles in inheritance hierarchy
        while (nextSuper != null && !ret.contains(nextSuper)) {
            ret.add(nextSuper)
            nextSuper = getSuper(nextSuper)
        }
        return ret
    }

    fun getNestedTransitive(n: Node): Set<Node> =
            getNested(n).map { setOf(it) + getNestedTransitive(it) }.flatten().toSet()

    fun getParentTransitive(n: Node): Set<Node> =
            (getParent(n)?.let { setOf(it) } ?: setOf()).map {
                setOf(it) + (getParent(it)?.let { p -> setOf(p) } ?: setOf())
            }.flatten().toSet()


    fun resolvePath(path: String, currentContext: String): List<Node> =
            resolvePath(path.split("."), currentContext.split("."))


    fun resolvePath(path: List<String>, currentContext: List<String>): List<Node> {
        val curNode = getNode(currentContext.joinToString("."))
        return resolvePath(path, curNode)
    }

    /**
     * Resolves pathExpr starting from given context node.
     * Path may contain both prefixed clafer names c0_name or clafer names without prefix.
     */
    fun resolvePath(path: List<String>, ctx: Node): List<Node> {
//        val toBeVisited = pathExpr.map { simplifyId(it) }
        val toBeVisited = path.map { it }
        return retrieveEvalGraph(toBeVisited, ctx).edges
                .map {
                    when {
                        // if EvalGraph contains no edges, pathExpr is resolved to node representing context clafer
                        it.isEmpty() -> ctx
                        it.last().isInverted -> it.last().edge.src
                        else -> it.last().edge.trg
                    }
                }
    }


    fun retrieveEvalGraph(pathExpr: Path, ctx: Node): EvalGraph = retrieveEvalGraph(pathExpr.elements.map { it.id }, ctx)


    /**
     * Retrieves the evaluation graph of a particular pathExpr expression and context node.
     */
    fun retrieveEvalGraph(path: List<String>, ctx: Node): EvalGraph {

        val allVisitedPaths: MutableSet<List<EvalGraphElem>> = mutableSetOf()
        val curPath: LinkedList<EvalGraphElem> = LinkedList()

        fun visitPath(curNode: Node, toBeVisited: List<String>, origPath: List<String>) {

            val next = toBeVisited.firstOrNull()

            if (next == null) {
                allVisitedPaths.add(curPath.toList())

            } else if (toBeVisited.size == origPath.size && next == origPath.first() && next != ThisIdentifier.id) {
                // search globally first, works if name of next is unique
                val cands = nodes.filter { it.name == next }.let { assert(it.isNotEmpty());it }
                if (cands.size == 1) {
                    // if clafer is unique in the specification add pathExpr from root to unique clafer
                    val pathFromRoot = pathFromRoot(cands.first()).let { assert(it.isNotEmpty());it }
                    curPath.addAll(pathFromRoot)
                    visitPath(pathFromRoot.last().edge.src, toBeVisited.drop(1), origPath)

                } else {
                    val nextEdge = incoming[cRoot]?.singleOrNull { it.type == Edge.EdgeType.P && matchClaferName(it.src.name, next) }
                            ?: throw IllegalStateException("Could not resolve pathExpr ${origPath.joinToString(".")} globally")
                    curPath.addLast(EvalGraphElem(nextEdge, true))
                    visitPath(nextEdge.src, toBeVisited.drop(1), origPath)

                }

            } else if (next == ThisIdentifier.id)
                visitPath(curNode, toBeVisited.drop(1), origPath)
            else if (next == ParentIdentfier.id) {
                val nextEdge = outgoing[curNode]?.single { it.type == Edge.EdgeType.P }
                        ?: throw IllegalStateException("Could not resolve pathExpr")
                curPath.addLast(EvalGraphElem(nextEdge))
                visitPath(nextEdge.trg, toBeVisited.drop(1), origPath)

            } else if (next == DrefIdentfier.id) {
                val nextEdges = outgoing[curNode]?.filter { it.type == Edge.EdgeType.R }
                        ?: throw IllegalStateException("Could not resolve pathExpr.")
                nextEdges.forEach {
                    curPath.addLast(EvalGraphElem(it))
                    visitPath(it.trg, toBeVisited.drop(1), origPath)
                }

            } else {
                // locally nested? Try first without prefixed name
                val candidates = incoming[curNode]?.filter { it.type == Edge.EdgeType.P && matchClaferName(it.src.name, next) }

                if (candidates == null) {
                    // search in super
                    val superEdge = outgoing[curNode]?.singleOrNull { it.type == Edge.EdgeType.A }
                            ?: throw IllegalStateException("Could not resolve pathExpr.")
                    curPath.addLast(EvalGraphElem(superEdge))
                    visitPath(superEdge.trg, toBeVisited, origPath)
                } else {

                    val nextEdge = if (candidates.size == 1) {
                        candidates.first()
                    } else {
                        // if more than one candidate is found, try to uniquely identify nested clafer with prefix
                        incoming[curNode]?.singleOrNull { it.type == Edge.EdgeType.P && it.src.name == next }
                                ?: error("Could not resolve pathExpr: More than one clafer found with name $next.")
                    }
                    curPath.addLast(EvalGraphElem(nextEdge, true))
                    visitPath(nextEdge.src, toBeVisited.drop(1), origPath)

                }


            }
            curPath.pollLast()
        }

        visitPath(ctx, path, path)

        return EvalGraph(path, ctx, allVisitedPaths.toSet())
    }


    /**
     * Retrieves edges from root to given node.
     */
    fun pathFromRoot(node: Node): List<EvalGraphElem> {
        val ret = LinkedList<EvalGraphElem>()

        fun visitPath(next: Node) {
            if (next != cRoot) {
                val nextEdge = outgoing[next]?.single { it.type == Edge.EdgeType.P }
                        ?: error("Node must be nested in exactly one parent node.")
                ret.addFirst(EvalGraphElem(nextEdge, true))
                visitPath(nextEdge.trg)
            }

        }
        visitPath(node)
        return ret
    }

    /**
     * Retrieves path from root to given node.
     * If force = true: do not resolve to global id even if unique id exists.
     */
    fun pathFromRootAsPath(node: Node, force: Boolean = false): Path =
            if (node == cRoot) Path(emptyList()) else transToPathExpr(pathFromRoot(node), force)


    /**
     * Transforms a list of elements of an evaluation graph to a pathExpr expression.
     */
    fun transToPathExpr(edges: List<EvalGraphElem>, force: Boolean = false): Path {
        assert(edges.none { it.edge.type == Edge.EdgeType.A })
        { "At this point no eval graph element should contain an abstract edge." }

        assert(edges.isNotEmpty()) { "At least one edge must exist." }

        val ctx = if (edges.first().isInverted) edges.first().edge.trg else edges.first().edge.src

        if (!force) {
            // if pathExpr is context independent and contains no edge representing a parent or dref keyword,
            // then try to resolve to global/unique ID instead
            if (edges.none {
                        it.edge.type == Edge.EdgeType.R
                                || (it.edge.type == Edge.EdgeType.P && !it.isInverted)
                    } && ctx == cRoot) {
                val lastEdge = edges.last()
                val lastNode = if (lastEdge.isInverted) lastEdge.edge.src else lastEdge.edge.trg
                if (nodes.filter { it.name == lastNode.name }.size == 1)
                    return Path(lastNode.name)
            }
        }

        val path = mutableListOf<AbstractIdentifier>()

        fun visitPath(rem: List<EvalGraphElem>) {
            val next = rem.firstOrNull()

            if (next != null) {
                when (next.edge.type) {
                    Edge.EdgeType.R -> {
                        if (edges == rem && ctx == next.edge.src)
                            path.add(ThisIdentifier)
                        path.add(DrefIdentfier)
                    }
                    Edge.EdgeType.P -> {
                        // parent
                        if (!next.isInverted) {
                            if (edges == rem && ctx == next.edge.src)
                                path.add(ThisIdentifier)
                            path.add(ParentIdentfier)
                        } else {
                            // nested
                            if (edges == rem && ctx == next.edge.trg && next.edge.trg != cRoot)
                                path.add(ThisIdentifier)
                            path.add(Identifier(next.edge.src.name))
                        }
                    }
                    else -> error("Should never be here.")
                }
                visitPath(rem.drop(1))
            }
        }
        visitPath(edges)

        return Path(path)
    }

    fun matchClaferName(s1: String, s2: String): Boolean = simplifyId(s1) == simplifyId(s2)

    fun hasNumRef(src: Node): Boolean = hasIntRef(src) || hasDoubleRef(src)

    fun hasIntRef(src: Node): Boolean = outgoing[src]?.any { it.type == Edge.EdgeType.R && it.trg.type == Node.NodeType.INT } == true

    fun hasDoubleRef(src: Node): Boolean = outgoing[src]?.any { it.type == Edge.EdgeType.R && it.trg.type == Node.NodeType.DBL } == true

    fun hasStringRef(src: Node): Boolean = outgoing[src]?.any { it.type == Edge.EdgeType.R && it.trg.type == Node.NodeType.STR } == true

    fun isNumeric(path: Path, ctx: Node): Boolean {
        if (path.elements.first() is LocalIdentifier)
            return false

        /**
         *  This method also works for partially initialized dependency graphs
         */
        fun visitPath(curNode: Node, toBeVisited: List<String>, origPath: List<String>): List<Node> {

            val next = toBeVisited.firstOrNull() ?: return listOf(curNode)

            // search globally
            if (toBeVisited.size == origPath.size && next == origPath.first() && next != ThisIdentifier.id)
                return getNested(cRoot).filter { matchClaferName(it.name, next) }
                        .let { assert(it.size == 1) { "Could not resolve pathExpr ${origPath.joinToString(".")} globally" };it }
                        .map { visitPath(it, toBeVisited.drop(1), origPath) }
                        .flatten()

            if (next == ThisIdentifier.id)
                return visitPath(curNode, toBeVisited.drop(1), origPath)

            if (next == ParentIdentfier.id) {
                val parent = getParent(curNode) ?: throw IllegalStateException("Could not resolve pathExpr")
                return visitPath(parent, toBeVisited.drop(1), origPath)
            }

            if (next == DrefIdentfier.id) {
                return getRefsFrom(curNode).map { visitPath(it, toBeVisited.drop(1), origPath) }.flatten()
            }

            // search in nested clafers
            return (getNested(curNode) + getNestedInSuper(curNode)).filter { matchClaferName(it.name, next) }
                    .let { if (it.size > 1) throw IllegalStateException("Could not resolve remaining pathExpr $toBeVisited: Element $next is not unique in namespace.");it }
                    .let {
                        if (it.isEmpty())
                            throw IllegalStateException("Could not resolve remaining pathExpr $toBeVisited: No nested element $next found.");it
                    }
                    .map { visitPath(it, toBeVisited.drop(1), origPath) }
                    .flatten()
        }

        val foundRefs = visitPath(ctx, path.elements.map { it.id }, path.elements.map { it.id })

        return (foundRefs.any { it.type == Node.NodeType.INT || it.type == Node.NodeType.DBL })
    }

    fun isSet(path: Path, ctx: Node) = !isNumeric(path, ctx)


    @Deprecated("DO NOT USE. WILL BE REMOVED")
    fun isNumeric(path: Path, currentContext: List<Identifier>): Boolean {
        if (path.elements.first() is LocalIdentifier)
            return false

        val foundRefs = resolvePath(path.elements.map { it.id }, currentContext.map { it.id })
        return (foundRefs.any { it.type == Node.NodeType.INT || it.type == Node.NodeType.DBL })
    }

    @Deprecated("DO NOT USE. WILL BE REMOVED")
    fun isSet(path: Path, currentContext: List<Identifier>): Boolean {
        if (path.elements.first() is LocalIdentifier)
            return false

        val foundRefs = resolvePath(path.elements.map { it.id }, currentContext.map { it.id })
        return (foundRefs.all { it.type == Node.NodeType.STD })
    }

    /**
     * Returns true if and only if given node is (transitively) nested in an abstract clafer.
     */
    fun isNestedInAbstract(node: Node): Boolean =
            (pathFromRoot(node).map { setOf(it.edge.src, it.edge.trg) }.flatten() - listOf(node)).any { it.isAbstract }

    /**
     * Computes lambda max for given node. Returns null if any upper bound in transitive upper nesting hierarchy
     * of given node is unbound. lambda max is defined as follows:
     * <br/>
     * lambda_max(c) = u * Product_{c' in c' <>--^+ c } u'
     */
    fun lamdaMax(n: Node): Int? =
            if (!isSyntUb(n))
                (setOf(n) + getParentTransitive(n)).map { (claferMult[it]!!.ub as IntUb).ub }.reduce { product, next -> product * next }
            else null

    /**
     * Computes lambda min for given node. lambda min is defined as follows:
     * <br/>
     * lambda_min(c) = l * Product_{c' in c' <>--^+ c } l'
     */
    fun lamdaMin(n: Node): Int =
            (setOf(n) + getParentTransitive(n)).map { claferMult[it]!!.lb.lb }.reduce { product, next -> product * next }


    /**
     * Returns true if given node or any parent node is syntactically unbound.
     */
    fun isSyntUb(n: Node): Boolean = (listOf(n) + getParentTransitive(n)).any { claferMult[it]!!.ub is InfUb }

    companion object Factory

}

fun IrGraph.Factory.createFromClasspath(cpFile: String): IrGraph =
        createFromStream(IrGraph::class.java.getResourceAsStream(cpFile))

fun IrGraph.Factory.createFromClasspathDesugared(cpFile: String): IrGraph =
        createFromStreamDesugared(IrGraph::class.java.getResourceAsStream(cpFile))

fun IrGraph.Factory.createFromStreamDesugared(stream: InputStream): IrGraph =
        createFromStringDesugared(ResourceUtil.transformStreamToString(stream))

fun IrGraph.Factory.createFromStream(stream: InputStream): IrGraph =
        createFromString(ResourceUtil.transformStreamToString(stream))

fun IrGraph.Factory.createFromString(spec: String): IrGraph = createFromStringDesugared(ClaferCompiler.compile(spec))

fun IrGraph.Factory.createFromStringDesugared(desugared: String): IrGraph {
//    println(desugared)
    val parser = ClaferParser.parseFromString(desugared)
    return IrCreator(parser).create()
}

fun IrGraph.copy(newSuffix: String = ""): IrGraph {
    val copy = IrGraph(this.cRoot, this.emptyset)
    copy.nodes.toSet().filter { it != this.cRoot }.forEach { copy.remove(it) }

    if (newSuffix == "") {
        // copy nodes
        copy.nodes.addAll(this.nodes)

        // copy edges
        for (n in this.incoming.keys)
            copy.incoming[n] = this.incoming[n]!!.toMutableSet()
        for (n in this.outgoing.keys)
            copy.outgoing[n] = this.outgoing[n]!!.toMutableSet()

        // copy refs
        copy.setRefs.putAll(this.setRefs)
        copy.bagRefs.putAll(this.bagRefs)

        // copy cstrs
        for (n in this.cstrs.keys)
            copy.cstrs[n] = this.cstrs[n]!!.toMutableList()

        // copy multiplicities
        copy.claferMult.putAll(this.claferMult)
        copy.groupMult.putAll(this.groupMult)
    } else {
        val oldNewMapping: MutableMap<Node, Node> = mutableMapOf()

        // copy nodes
        copy.nodes.addAll(this.nodes.filter { it != this.cRoot }.map {
            if (it.type == Node.NodeType.STD) {
                val newNode = Node(it.name + newSuffix, it.isAbstract, it.type)
                oldNewMapping[it] = newNode
                newNode
            } else {
                oldNewMapping[it] = it
                it
            }
        })


        oldNewMapping[this.cRoot] = this.cRoot

        // copy edges
        for (n in this.incoming.keys) {
            if (this.incoming[n] != null)
                copy.incoming[oldNewMapping[n]!!] = this.incoming[n]!!.map { edge ->
                    Edge(oldNewMapping[edge.src]!!, oldNewMapping[edge.trg]!!, edge.type)
                }.toMutableSet()
        }
        for (n in this.outgoing.keys) {
            if (this.outgoing[n] != null)
                copy.outgoing[oldNewMapping[n]!!] = this.outgoing[n]!!.map { edge ->
                    Edge(oldNewMapping[edge.src]!!, oldNewMapping[edge.trg]!!, edge.type)
                }.toMutableSet()
        }

        fun replaceIdentifier(elem: AstElement): AstElement =
                replaceAstElements(elem, listOf(
                        TreeOp(
                                { it is Identifier && it != ThisIdentifier && it != DrefIdentfier && it != ParentIdentfier },
                                {
                                    it as Identifier
                                    Optional.of(it.copy(it.id + newSuffix))
                                }
                        )
                )).get()


        // copy refs
        val newSetRefs = this.setRefs.map { entry ->
            oldNewMapping[entry.key]!! to replaceIdentifier(entry.value) as SetExpr
        }.toMap()
        copy.setRefs.putAll(newSetRefs)

        val newBagRefs = this.bagRefs.map { entry ->
            oldNewMapping[entry.key]!! to replaceIdentifier(entry.value) as SetExpr
        }.toMap()
        copy.bagRefs.putAll(newBagRefs)

        // copy cstrs
        for (n in this.cstrs.keys)
            copy.cstrs[oldNewMapping[n]!!] = this.cstrs[n]!!.map { replaceIdentifier(it) as BoolExpr }

        // copy multiplicities
        copy.claferMult.putAll(this.claferMult.map { oldNewMapping[it.key]!! to it.value })
        copy.groupMult.putAll(this.groupMult.map { oldNewMapping[it.key]!! to it.value })
    }
    return copy
}


fun IrGraph.toDot(): String =
        ("""
        digraph G {
            rankdir=BT;
            ${this.outgoing.values.flatten().joinToString("\n") {
            "${it.src.name}${it.src.id} -> ${it.trg.name}${it.trg.id} [label=\"${it.type.name.toLowerCase()}\", arrowhead=${when (it.type) {
                Edge.EdgeType.A -> "onormal"
                Edge.EdgeType.P -> "diamond"
                Edge.EdgeType.R -> "vee"
            }
            }];"
        }}
            ${this.nodes.joinToString("\n") { "${it.name}${it.id} [shape=box ${if (it.isAbstract) ",style=dotted" else ""}];" }}
        """.trimIndent() + "\n".trimIndent() + "labeljust=\"l\"" +
                "label=\"${if (setRefs.isNotEmpty()) "Refs:\\l${setRefs.map { (ctx, ref) -> "${ctx.name} -> ${prettyPrint(ref)}" }.joinToString("\\l")}" else ""}" +
                (if (bagRefs.isNotEmpty()) "\\lBags:\\l${bagRefs.map { (ctx, ref) -> "${ctx.name} ->> ${prettyPrint(ref)}" }.joinToString("\\l")}" else "") +
                (if (cstrs.isNotEmpty()) "\\lConstraints:\\l${cstrs.map { (ctx, cstrs) ->
                    cstrs.joinToString("\\l") {
                        "${ctx.name} -> ${prettyPrint(it)}"
                    }
                }.joinToString("\\l")}" else "").trimIndent() + "\"}").trimIndent().replace("(", "&#40;").replace(")", "&#41;")

/**
 * Objects of this class represent the evaluation graph of a pathExpr expression starting from a given context node.
 * One particular pathExpr is represented by a sequence of edges.
 * One pathExpr expression may specify multiple paths and is therefore represented by set of sequences of edges.
 */
data class EvalGraph(val pathExpr: List<String>, val ctx: Node, val edges: Set<List<EvalGraphElem>>)

/**
 * The element of an EvalGraph is represented by a pair of a Boolean value and an edge.
 * The Boolean value is true if the edge is isInverted, false otherwise.
 */
data class EvalGraphElem(val edge: Edge, val isInverted: Boolean = false)

data class Node(val name: String, val isAbstract: Boolean = false, val type: NodeType, val id: Int) {
    constructor(name: String, isAbstract: Boolean = false) : this(name, isAbstract, NodeType.STD, idGen.getAndIncrement())
    constructor(name: String, isAbstract: Boolean = false, type: NodeType) : this(name, isAbstract, type, idGen.getAndIncrement())
    constructor(type: NodeType) : this("${type.name}${idGen.getAndIncrement()}", false, type, idGen.get())

    /**
     * STD corresponds to a regular node representing a clafer
     * DBL corresponds to a node representing a referenced double value
     * INT corresponds to a node representing a referenced integer value
     * STR corresponds to a node representing a referenced string value
     */
    enum class NodeType {
        STD, DBL, INT, STR
    }

    override fun toString(): String {
        return "$name($id)"
    }
}

data class Edge(val src: Node, val trg: Node, val type: EdgeType) {

    /**
     * R(eference) relation (edge from referencing clafer to referenced clafer)
     * A(bstract) relation (edge from sub clafer to super clafer)
     * P(arent) relation (edge from nested clafer to parent clafer)
     */
    enum class EdgeType {
        R, A, P
    }

    override fun toString(): String {
        return "$type: $src -> $trg"
    }
}


sealed class Multiplicity(open val lb: Lb, open val ub: Ub)
data class ClaferMult(override val lb: Lb, override val ub: Ub) : Multiplicity(lb, ub) {
    constructor(lb: Int) : this(Lb(lb), InfUb())
    constructor(lb: Int, ub: Int) : this(Lb(lb), IntUb(ub))
}

data class GroupMult(override val lb: Lb, override val ub: Ub) : Multiplicity(lb, ub) {
    constructor(lb: Int) : this(Lb(lb), InfUb())
    constructor(lb: Int, ub: Int) : this(Lb(lb), IntUb(ub))
}

data class Lb(val lb: Int)
sealed class Ub
data class IntUb(val ub: Int) : Ub()
data class InfUb(val symbol: String = "*") : Ub()



