package ir

import ir.constraints.*
import java.util.*

fun IrGraph.convert2Clafer(simplifyIds: Boolean = true): String = Ir2Clafer(this).convert(simplifyIds)

class Ir2Clafer(val graph: IrGraph) {

    fun convert(simplifyIds: Boolean): String {

        val indCount = mutableMapOf<Node, Int>()
        graph.getNested(graph.cRoot).forEach { indCount[it] = 0 }

        fun printClafer(node: Node): String {
            graph.getNested(node).forEach { indCount[it] = indCount[node]!! + 1 }

            val ind = "\t".repeat(indCount[node] ?: error("Node is expected to be included in indentation map."))
            val abs = if (node.isAbstract) "abstract " else ""
            val (lbC, ubC) = graph.cmult(node).let { it.first.toString() to if (it.second < 0) "*" else it.second.toString() }
            val (lbG, ubG) = graph.gmult(node).let { it.first.toString() to if (it.second < 0) "*" else it.second.toString() }
            val name = if (simplifyIds) simplifyId(node.name) else node.name
            val supC = graph.getSuper(node)?.let { ": ${if (simplifyIds) simplifyId(it.name) else it.name} " } ?: ""
            val ref = when {
                graph.bagRefs.contains(node) -> "->> ${if (simplifyIds) prettyPrint(simplifyIds(graph.bagRefs[node]!!)) else prettyPrint(graph.bagRefs[node]!!)} "
                graph.setRefs.contains(node) -> "-> ${if (simplifyIds) prettyPrint(simplifyIds(graph.setRefs[node]!!)) else prettyPrint(graph.setRefs[node]!!)} "
                graph.hasStringRef(node) -> "-> string "
                graph.hasDoubleRef(node) -> "-> double "
                graph.hasIntRef(node) -> "-> integer "
                else -> ""
            }
            val children = if (graph.getNested(node).isNotEmpty()) "\n" + graph.getNested(node).joinToString("\n") { printClafer(it) } else ""
            val cstrs = if (graph.cstrs[node] != null) "\n" + graph.cstrs[node]!!.joinToString("\n") { "$ind\t[${if (simplifyIds) prettyPrint(simplifyPathExpr(simplifyIds(it))) else prettyPrint(simplifyPathExpr(it))}]" } else ""
            return "$ind$abs$lbG..$ubG $name $supC$ref$lbC..$ubC$children$cstrs"
        }

        return graph.getNested(graph.cRoot).filter { it.type == Node.NodeType.STD }.joinToString("\n") { printClafer(it) } +
                if (graph.cstrs[graph.cRoot] != null) "\n" + graph.cstrs[graph.cRoot]!!.joinToString("\n") { "[${if (simplifyIds) prettyPrint(simplifyPathExpr(simplifyIds(it))) else prettyPrint(simplifyPathExpr(it))}]" } else ""
    }

    private fun simplifyPathExpr(element: AstElement): AstElement {
        val op = TreeOp({ it is Path },
                { path ->
                    path as Path
                    val pathElements = path.elements.toMutableList()
                    var i = 1
                    var done = false
                    while (!done) {
                        if (i >= pathElements.size)
                            done = true
                        else if (pathElements[i] == DrefIdentfier
                                && i + 1 <pathElements.size
                                && pathElements[i + 1] != DrefIdentfier
                                && pathElements[i + 1] != ParentIdentfier) {
                            pathElements.removeAt(i)
                        } else i++
                    }
                    Optional.of(Path(pathElements))
                })
        return replaceAstElements(element, listOf(op)).get()
//        return element
    }

}



