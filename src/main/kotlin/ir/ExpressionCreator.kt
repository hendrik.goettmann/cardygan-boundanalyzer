package ir

import ir.constraints.*
import org.cardygan.boundanalyzer.antlr.ClaferRxBaseVisitor
import org.cardygan.boundanalyzer.antlr.ClaferRxParser
import java.util.*
import java.util.regex.Pattern

class ExpressionCreator(private val ctxClafer: Node? = null, private val dg: IrGraph? = null) : ClaferRxBaseVisitor<Any>() {

    init {
        if (ctxClafer != null && dg == null || ctxClafer == null && dg != null)
            throw IllegalStateException("Both context clafer and dependency graph must be either initialized or null at the same time.")
    }

    override fun visitConstraint(ctx: ClaferRxParser.ConstraintContext): Any =
            ctx.boolExpr().accept(this) as BoolExpr


    override fun visitFiMult(ctx: ClaferRxParser.FiMultContext): Any {
        val lb: Lb = ctx.mult().lb().accept(this) as Lb
        val ub: Ub = ctx.mult().ub().accept(this) as Ub
        return ClaferMult(lb, ub)
    }

    override fun visitGiMult(ctx: ClaferRxParser.GiMultContext): Any {
        val lb: Lb = ctx.mult().lb().accept(this) as Lb
        val ub: Ub = ctx.mult().ub().accept(this) as Ub
        return GroupMult(lb, ub)
    }

    override fun visitLb(ctx: ClaferRxParser.LbContext): Any {
        return Lb(ctx.text.toInt())
    }

    override fun visitUb(ctx: ClaferRxParser.UbContext): Any {
        return if (ctx.NUMBER() != null)
            IntUb(ctx.text.toInt())
        else
            InfUb()
    }

    override fun visitReference(ctx: ClaferRxParser.ReferenceContext): Any {
        ctx.setRef()?.let { return it.accept(this) as SetExpr }
        ctx.bagRef()?.let { return it.accept(this) as SetExpr }
        return Optional.empty<SetExpr>()
    }


    override fun visitSetRef(ctx: ClaferRxParser.SetRefContext): Any {
        return ctx.setExpr().accept(this) as SetExpr
    }

    override fun visitBagRef(ctx: ClaferRxParser.BagRefContext): Any {
        return ctx.setExpr().accept(this) as SetExpr
    }

    override fun visitIteBool(ctx: ClaferRxParser.IteBoolContext): Any {
        val i = ctx.boolExpr(0).accept(this) as BoolExpr
        val t = ctx.boolExpr(1).accept(this) as BoolExpr
        val e = ctx.boolExpr(2).accept(this) as BoolExpr
        return IteBoolExpr(i, t, e)
    }

    override fun visitNot(ctx: ClaferRxParser.NotContext): Any {
        return Not(ctx.boolExpr().accept(this) as BoolExpr)
    }

    override fun visitBoolBrackets(ctx: ClaferRxParser.BoolBracketsContext): Any {
        return ctx.boolExpr().accept(this)
    }

    override fun visitAnd(ctx: ClaferRxParser.AndContext): Any {
        val lhs = ctx.boolExpr(0).accept(this) as BoolExpr
        val rhs = ctx.boolExpr(1).accept(this) as BoolExpr
        return And(lhs, rhs)
    }

    override fun visitOr(ctx: ClaferRxParser.OrContext): Any {
        val lhs = ctx.boolExpr(0).accept(this) as BoolExpr
        val rhs = ctx.boolExpr(1).accept(this) as BoolExpr
        return Or(lhs, rhs)
    }

    override fun visitXOr(ctx: ClaferRxParser.XOrContext): Any {
        val lhs = ctx.boolExpr(0).accept(this) as BoolExpr
        val rhs = ctx.boolExpr(1).accept(this) as BoolExpr
        return Xor(lhs, rhs)
    }

    override fun visitImpl(ctx: ClaferRxParser.ImplContext): Any {
        val lhs = ctx.boolExpr(0).accept(this) as BoolExpr
        val rhs = ctx.boolExpr(1).accept(this) as BoolExpr
        return Impl(lhs, rhs)
    }

    override fun visitBiImpl(ctx: ClaferRxParser.BiImplContext): Any {
        val lhs = ctx.boolExpr(0).accept(this) as BoolExpr
        val rhs = ctx.boolExpr(1).accept(this) as BoolExpr
        return BiImpl(lhs, rhs)
    }

    override fun visitLe(ctx: ClaferRxParser.LeContext): Any {
        val lhs = ctx.numericExpr(0).accept(this) as NumExpr
        val rhs = ctx.numericExpr(1).accept(this) as NumExpr
        return Leq(lhs, rhs)
    }

    override fun visitLt(ctx: ClaferRxParser.LtContext): Any {
        val lhs = ctx.numericExpr(0).accept(this) as NumExpr
        val rhs = ctx.numericExpr(1).accept(this) as NumExpr
        return Lt(lhs, rhs)
    }

    override fun visitGe(ctx: ClaferRxParser.GeContext): Any {
        val lhs = ctx.numericExpr(0).accept(this) as NumExpr
        val rhs = ctx.numericExpr(1).accept(this) as NumExpr
        return Geq(lhs, rhs)
    }

    override fun visitGt(ctx: ClaferRxParser.GtContext): Any {
        val lhs = ctx.numericExpr(0).accept(this) as NumExpr
        val rhs = ctx.numericExpr(1).accept(this) as NumExpr
        return Gt(lhs, rhs)
    }

    override fun visitEq(ctx: ClaferRxParser.EqContext): Any {
        val lhs = ctx.numericExpr(0).accept(this) as NumExpr
        val rhs = ctx.numericExpr(1).accept(this) as NumExpr

        if (ctxClafer == null || dg == null)
            throw IllegalStateException("Dependency graph is not initialized. Dependency graph is needed to infer type information for equals to expression.")

        // if both sides are NumVars, check case if it's actually a SetEq expression
        if (lhs is NumVar && rhs is NumVar) {
            if (dg.isSet(lhs.path, ctxClafer) && dg.isSet(rhs.path, ctxClafer))
                return EqSet(SetElement(lhs.path), SetElement(rhs.path))

            if (!(dg.isNumeric(lhs.path, ctxClafer) && dg.isNumeric(rhs.path, ctxClafer)))
                throw IllegalStateException("LHS and RHS are referencing both, sets and numvar at the same time.")
        }

        return Eq(lhs, rhs)
    }

    override fun visitNeq(ctx: ClaferRxParser.NeqContext): Any {
        val lhs = ctx.numericExpr(0).accept(this) as NumExpr
        val rhs = ctx.numericExpr(1).accept(this) as NumExpr

        if (ctxClafer == null || dg == null)
            throw IllegalStateException("Dependency graph is not initialized. Dependency graph is needed to infer type information for not equals to expression.")

        // if both sides are NumVars, check case if it's actually a NotEqSet expression
        if (lhs is NumVar && rhs is NumVar) {
            if (dg.isSet(lhs.path, ctxClafer) && dg.isSet(rhs.path, ctxClafer))
                return NotEqSet(SetElement(lhs.path), SetElement(rhs.path))

            if (!(dg.isNumeric(lhs.path, ctxClafer) && dg.isNumeric(rhs.path, ctxClafer)))
                throw IllegalStateException("LHS and RHS are referencing both, sets and numvar at the same time.")
        }

        return Not(Eq(lhs, rhs))
    }

    override fun visitParentheses(ctx: ClaferRxParser.ParenthesesContext): Any {
        return ctx.numericExpr().accept(this)
    }

    override fun visitSomeSimpleQuant(ctx: ClaferRxParser.SomeSimpleQuantContext): Any {
        return Some(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitOneSimpleQuant(ctx: ClaferRxParser.OneSimpleQuantContext): Any {
        return One(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitNoSimpleQuant(ctx: ClaferRxParser.NoSimpleQuantContext): Any {
        return No(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitLoneSimpleQuant(ctx: ClaferRxParser.LoneSimpleQuantContext): Any {
        return Lone(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitSetEq(ctx: ClaferRxParser.SetEqContext): Any {
        val lhs = ctx.setExpr(0).accept(this) as SetExpr
        val rhs = ctx.setExpr(1).accept(this) as SetExpr
        return EqSet(lhs, rhs)
    }

    override fun visitSetUnEq(ctx: ClaferRxParser.SetUnEqContext): Any {
        val lhs = ctx.setExpr(0).accept(this) as SetExpr
        val rhs = ctx.setExpr(1).accept(this) as SetExpr
        return NotEqSet(lhs, rhs)
    }

    override fun visitSetIn(ctx: ClaferRxParser.SetInContext): Any {
        val lhs = ctx.setExpr(0).accept(this) as SetExpr
        val rhs = ctx.setExpr(1).accept(this) as SetExpr
        return InSet(lhs, rhs)
    }

    override fun visitSetNotIn(ctx: ClaferRxParser.SetNotInContext): Any {
        val lhs = ctx.setExpr(0).accept(this) as SetExpr
        val rhs = ctx.setExpr(1).accept(this) as SetExpr
        return NotInSet(lhs, rhs)
    }


    override fun visitIdentifier(ctx: ClaferRxParser.IdentifierContext): Any {
        if (ctx.thisClafer() != null)
            return ctx.thisClafer()?.accept(this) as Identifier
        if (ctx.drefClafer() != null)
            return ctx.drefClafer()?.accept(this) as Identifier
        if (ctx.parentClafer() != null)
            return ctx.parentClafer()?.accept(this) as Identifier
        if (ctx.claferName() != null)
            return ctx.claferName()?.accept(this) as Identifier
        if (ctx.localIdentifier() != null)
            return ctx.localIdentifier()?.accept(this) as LocalIdentifier

        throw IllegalStateException("Could not match identifier!")
    }

    override fun visitClaferName(ctx: ClaferRxParser.ClaferNameContext): Any {
        val id = ctx.text
        val idPattern = "^(c\\d+_).*"
        assert(id.matches(Regex(idPattern)), { "Could not match $id as clafer name" })

        val pattern = Pattern.compile(idPattern)
        val m = pattern.matcher(id)
        if (m.find() && m.groupCount() == 1)
            return Identifier(id)
        else
            throw IllegalStateException("Invalid ID format. IDs are required to start with c<digit>+_.")

    }

    override fun visitNumVar(ctx: ClaferRxParser.NumVarContext): Any {
        return Path((ctx.path().accept(this) as Path).elements + DrefIdentfier)
    }

    override fun visitLocalIdentifier(ctx: ClaferRxParser.LocalIdentifierContext): Any {
        return LocalIdentifier(ctx.text)
    }

    override fun visitThisClafer(ctx: ClaferRxParser.ThisClaferContext): Any {
        return Identifier(ctx.text)
    }

    override fun visitDrefClafer(ctx: ClaferRxParser.DrefClaferContext): Any {
        return Identifier(ctx.text)
    }

    override fun visitParentClafer(ctx: ClaferRxParser.ParentClaferContext): Any {
        return Identifier(ctx.text)
    }


    override fun visitSetUnion(ctx: ClaferRxParser.SetUnionContext): Any {
        val lhs = ctx.setExpr()[0].accept(this) as SetExpr
        val rhs = ctx.setExpr()[1].accept(this) as SetExpr
        return Union(lhs, rhs)
    }

    override fun visitSetDiff(ctx: ClaferRxParser.SetDiffContext): Any {
        val lhs = ctx.setExpr()[0].accept(this) as SetExpr
        val rhs = ctx.setExpr()[1].accept(this) as SetExpr
        return Removal(lhs, rhs)
    }

    override fun visitSetIntersec(ctx: ClaferRxParser.SetIntersecContext): Any {
        val lhs = ctx.setExpr()[0].accept(this) as SetExpr
        val rhs = ctx.setExpr()[1].accept(this) as SetExpr
        return Intersect(lhs, rhs)
    }

    override fun visitSetBrackets(ctx: ClaferRxParser.SetBracketsContext): Any {
        return ctx.setExpr().accept(this)
    }

    override fun visitJoin(ctx: ClaferRxParser.JoinContext): Any {
        return ctx.setElement().accept(this) as SetElement
    }

    override fun visitIteSet(ctx: ClaferRxParser.IteSetContext): Any {
        val i = ctx.setExpr()[0].accept(this) as BoolExpr
        val t = ctx.setExpr()[1].accept(this) as SetExpr
        val e = ctx.setExpr()[2].accept(this) as SetExpr
        return IteSet(i, t, e)
    }

    override fun visitStringExpr(ctx: ClaferRxParser.StringExprContext): Any {
        return StringSetElement(ctx.text)
    }

    override fun visitSetElement(ctx: ClaferRxParser.SetElementContext): Any {
        return SetElement(ctx.path().accept(this) as Path)
    }

    override fun visitPath(ctx: ClaferRxParser.PathContext): Any {
        return Path(ctx.identifier().map { it.accept(this) as AbstractIdentifier }.toList())
    }

    override fun visitIntLit(ctx: ClaferRxParser.IntLitContext): Any {
        return IntVal(ctx.intLiteral().text.toInt())
    }

    override fun visitRealLit(ctx: ClaferRxParser.RealLitContext): Any {
        return DoubleVal(ctx.realLiteral().text.toDouble())
    }

    override fun visitIteNumeric(ctx: ClaferRxParser.IteNumericContext): Any {
        val i = ctx.boolExpr().accept(this) as BoolExpr
        val t = ctx.numericExpr()[0].accept(this) as NumExpr
        val e = ctx.numericExpr()[1].accept(this) as NumExpr
        return IteNumExpr(i, t, e)
    }

    override fun visitAdd(ctx: ClaferRxParser.AddContext): Any {
        val lhs = ctx.numericExpr()[0].accept(this) as NumExpr
        val rhs = ctx.numericExpr()[1].accept(this) as NumExpr
        return Add(lhs, rhs)
    }

    override fun visitSubstr(ctx: ClaferRxParser.SubstrContext): Any {
        val lhs = ctx.numericExpr()[0].accept(this) as NumExpr
        val rhs = ctx.numericExpr()[1].accept(this) as NumExpr
        return Minus(lhs, rhs)
    }

    override fun visitMultiply(ctx: ClaferRxParser.MultiplyContext): Any {
        val lhs = ctx.numericExpr()[0].accept(this) as NumExpr
        val rhs = ctx.numericExpr()[1].accept(this) as NumExpr
        return Multiply(lhs, rhs)
    }

    override fun visitDiv(ctx: ClaferRxParser.DivContext): Any {
        val lhs = ctx.numericExpr()[0].accept(this) as NumExpr
        val rhs = ctx.numericExpr()[1].accept(this) as NumExpr
        return Div(lhs, rhs)
    }

    override fun visitMod(ctx: ClaferRxParser.ModContext): Any {
        val lhs = ctx.numericExpr()[0].accept(this) as NumExpr
        val rhs = ctx.numericExpr()[1].accept(this) as NumExpr
        return Modulo(lhs, rhs)
    }

    override fun visitNeg(ctx: ClaferRxParser.NegContext): Any {
        return Neg(ctx.numericExpr().accept(this) as NumExpr)
    }

    override fun visitSum(ctx: ClaferRxParser.SumContext): Any {
        return Sum(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitProduct(ctx: ClaferRxParser.ProductContext): Any {
        return Product(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitSetCard(ctx: ClaferRxParser.SetCardContext): Any {
        return Card(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitMin(ctx: ClaferRxParser.MinContext): Any {
        return Min(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitMax(ctx: ClaferRxParser.MaxContext): Any {
        return Max(ctx.setExpr().accept(this) as SetExpr)
    }

    override fun visitVar(ctx: ClaferRxParser.VarContext): Any {
        return NumVar(ctx.numVar().accept(this) as Path)
    }

    override fun visitAllQuant(ctx: ClaferRxParser.AllQuantContext): Any {
        val localDecls = ctx.localDecl().map { it.accept(this) as LocalDecl }
        val pred = ctx.boolExpr().accept(this) as BoolExpr
        return AllQuantExpr(localDecls, pred)
    }

    override fun visitNoQuant(ctx: ClaferRxParser.NoQuantContext): Any {
        val localDecls = ctx.localDecl().map { it.accept(this) as LocalDecl }
        val pred = ctx.boolExpr().accept(this) as BoolExpr
        return NoQuantExpr(localDecls, pred)
    }

    override fun visitNoDisjQuant(ctx: ClaferRxParser.NoDisjQuantContext): Any {
        val localDecls = ctx.localDecl().map { it.accept(this) as LocalDecl }
        val pred = ctx.boolExpr().accept(this) as BoolExpr
        return NoDisjQuantExpr(localDecls, pred)
    }

    override fun visitAllDisjQuant(ctx: ClaferRxParser.AllDisjQuantContext): Any {
        val localDecls = ctx.localDecl().map { it.accept(this) as LocalDecl }
        val pred = ctx.boolExpr().accept(this) as BoolExpr
        return AllDisjQuantExpr(localDecls, pred)
    }

    override fun visitOneQuant(ctx: ClaferRxParser.OneQuantContext): Any {
        val localDecls = ctx.localDecl().map { it.accept(this) as LocalDecl }
        val pred = ctx.boolExpr().accept(this) as BoolExpr
        return OneQuantExpr(localDecls, pred)
    }

    override fun visitOneDisjQuant(ctx: ClaferRxParser.OneDisjQuantContext): Any {
        val localDecls = ctx.localDecl().map { it.accept(this) as LocalDecl }
        val pred = ctx.boolExpr().accept(this) as BoolExpr
        return OneDisjQuantExpr(localDecls, pred)
    }

    override fun visitSomeDisjQuant(ctx: ClaferRxParser.SomeDisjQuantContext): Any {
        val localDecls = ctx.localDecl().map { it.accept(this) as LocalDecl }
        val pred = ctx.boolExpr().accept(this) as BoolExpr
        return SomeDisjQuantExpr(localDecls, pred)
    }

    override fun visitSomeQuant(ctx: ClaferRxParser.SomeQuantContext): Any {
        val localDecls = ctx.localDecl().map { it.accept(this) as LocalDecl }
        val pred = ctx.boolExpr().accept(this) as BoolExpr
        return SomeQuantExpr(localDecls, pred)
    }

    override fun visitLocalDecl(ctx: ClaferRxParser.LocalDeclContext): Any {
        val identifiers = ctx.localIdentifier().map { it.accept(this) as LocalIdentifier }
        return LocalDecl(identifiers, ctx.setExpr().accept(this) as SetExpr)
    }
}