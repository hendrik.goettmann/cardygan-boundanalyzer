package ir


import ir.constraints.BoolExpr
import ir.constraints.SetExpr
import mu.KotlinLogging
import org.antlr.v4.runtime.tree.ParseTree
import org.cardygan.boundanalyzer.antlr.ClaferRxBaseVisitor
import org.cardygan.boundanalyzer.antlr.ClaferRxParser

class IrCreator(val ctx: ParseTree) {

    private val logger = KotlinLogging.logger {}

    fun create(): IrGraph {
        val graph = createInitialGraph()

        resolveInheritance(graph.cRoot, graph)
        resolveReferences(graph.cRoot, graph)

        addConstraints(graph)

        assert(graph.nodes.all { (listOf(graph.cRoot) + graph.getNestedTransitive(graph.cRoot)).contains(it) })
        { "Widow nodes found. All nodes must be transitively nested beneath root after tree was created." }

        return graph
    }


    private fun resolveInheritance(n: Node, graph: IrGraph) {

        val superNode = graph.getSuper(n)

        // check if super node is orphaned, ie., has no outgoing edges
        if (superNode != null && !graph.outgoing.contains(superNode)) {
            // global match?
            var found = graph.getNested(graph.cRoot).filter { it != n }.find { it.name == superNode.name }

            val parentNode = graph.getParent(n)

            // on same level match?
            if (found == null && parentNode != null)
                found = graph.getNested(parentNode).find { it != n && it.name == superNode.name }


            // in super hierarchy match?
            if (found == null && parentNode != null) {
                val parentSuper = graph.getSuper(parentNode)

                if (parentSuper != null)
                    found = graph.getNested(parentSuper).find { it.name == superNode.name }
            }

            // remove orphaned node
            if (found != null) {
                graph.add(Edge(n, found, Edge.EdgeType.A))
                graph.remove(superNode)
            } else
                throw IllegalStateException("Could not resolve inheritance hierarchy for node ${n.name}")
        }

        graph.getNested(n).forEach { resolveInheritance(it, graph) }
    }

    private fun resolveReferences(n: Node, graph: IrGraph) {

        fun walkPath(curNode: Node, remPath: List<Node>): Node {
            if (remPath.isEmpty())
                return curNode

            if (curNode.name == remPath.first().name)
                return walkPath(curNode, remPath.drop(1))

            val next = graph.getNested(curNode)
                    .filter { it.name == remPath.first().name }
                    .let { assert(it.size <= 1);it }
                    .firstOrNull()

            if (next != null)
                return walkPath(next, remPath.drop(1))

            throw IllegalStateException("Could not resolve pathExpr.")
        }


        fun findPaths(curNode: Node): List<Node> {
            val next = graph.getNested(curNode).let {
                assert(it.size <= 1) { "Path is expected to have only one child per node." }
                it
            }.firstOrNull()

            return if (next != null)
                listOf(curNode) + findPaths(next)
            else
                listOf(curNode)
        }

        fun findCandidates(curNode: Node, predicate: (Node) -> Boolean): List<Node> =
                if (predicate.invoke(curNode))
                    listOf(curNode) + graph.getNested(curNode).map { findCandidates(it, predicate) }.flatten()
                else
                    graph.getNested(curNode).map { findCandidates(it, predicate) }.flatten()


        val startPoints = graph.outgoing[n]
                ?.filter {
                    it.type == Edge.EdgeType.R
                            && it.trg.type != Node.NodeType.DBL
                            && it.trg.type != Node.NodeType.INT
                            && it.trg.type != Node.NodeType.STR
                }
                ?.map { it.trg }

        if (startPoints != null) {
            // found paths
            val foundPaths: List<List<Node>> = startPoints.map { findPaths(it) }

            for (remPath in foundPaths) {
                // candidates
                val candidates: Set<Node> = findCandidates(graph.cRoot) { it.name == remPath.first().name }.toSet()

                // search candidates
                val res = candidates.map { walkPath(it, remPath) }
                assert(res.size <= 1) { "Ambigious paths in tree were found." }

                // Add new reference edge
                val newRefTrg = res.first()
                graph.add(Edge(n, newRefTrg, Edge.EdgeType.R))

                // remove orphan nodes
                remPath.forEach { graph.remove(it) }
            }
        }
        graph.getNested(n).forEach { resolveReferences(it, graph) }
    }

    /**
     * Create initial (partial) dependency graph without constraints.
     */
    private fun createInitialGraph(): IrGraph {
        val graph = IrGraph()
        val visitor = object : ClaferRxBaseVisitor<Any>() {
            var parentNode = graph.cRoot
            var lastNode = graph.cRoot

            var inRef = false
            var firstRef = false
            var inAbs = false

            override fun visitClafer(ctx: ClaferRxParser.ClaferContext) {

                val isAbstract = ctx.abstr() != null

//                lastNode = Node(simplifyId(mCtx.claferName().text), isAbstract)
                lastNode = Node(ctx.claferName().text, isAbstract)

                fun lb(ctx: ClaferRxParser.LbContext): Lb {
                    return Lb(ctx.text.toInt())
                }

                fun ub(ctx: ClaferRxParser.UbContext): Ub {
                    return if (ctx.NUMBER() != null)
                        IntUb(ctx.text.toInt())
                    else
                        InfUb()
                }

                val currentNode = lastNode
                graph.add(Edge(currentNode, parentNode, Edge.EdgeType.P))

                graph.claferMult[currentNode] = ClaferMult(lb(ctx.fiMult().mult().lb()), ub(ctx.fiMult().mult().ub()))
                graph.groupMult[currentNode] = GroupMult(lb(ctx.giMult().mult().lb()), ub(ctx.giMult().mult().ub()))

                inRef = true
                firstRef = true
                ctx.reference()?.intBagRef()
                ctx.reference()?.intSetRef()
                ctx.reference()?.stringSetRef()
                ctx.reference()?.stringBagRef()
                ctx.reference()?.doubleBagRef()
                ctx.reference()?.doubleSetRef()
                ctx.reference()?.accept(this)
                inRef = false

                lastNode = currentNode
                inAbs = true
                ctx.parent()?.accept(this)
                inAbs = false

                // visit nested clafers and update context clafer
                val oldParent = parentNode
                parentNode = currentNode
                lastNode = currentNode
                ctx.childElements().element().filter { it.clafer() != null }.map { it.accept(this) }
                parentNode = oldParent
            }


            override fun visitPath(ctx: ClaferRxParser.PathContext) {
                val currentNode = lastNode
                ctx.identifier().map { it.accept(this) }
                firstRef = true
                lastNode = currentNode
            }


            override fun visitIdentifier(ctx: ClaferRxParser.IdentifierContext) {
                if (inAbs) {
                    assert(ctx.claferName() != null)
//                    val currentNode = Node(simplifyId(mCtx.claferName().text), true)
                    val currentNode = Node(ctx.claferName().text, true)
                    graph.add(Edge(lastNode, currentNode, Edge.EdgeType.A))
                    return
                }

                if (inRef && firstRef) {
                    assert(ctx.claferName() != null)
//                    val currentNode = Node(simplifyId(mCtx.claferName().text))
                    val currentNode = Node(ctx.claferName().text)
                    graph.add(Edge(lastNode, currentNode, Edge.EdgeType.R))
                    lastNode = currentNode
                    firstRef = false
                    return
                }

                if (inRef && !firstRef) {
//                    val currentNode = Node(simplifyId(mCtx.claferName().text))
                    val currentNode = Node(ctx.claferName().text)
                    graph.add(Edge(currentNode, lastNode, Edge.EdgeType.P))
                    lastNode = currentNode
                    return
                }

            }


            override fun visitSetRef(ctx: ClaferRxParser.SetRefContext) {
                val cstr = ExpressionCreator(lastNode, graph).visit(ctx) as SetExpr
                graph.setRefs[lastNode] = cstr
                ctx.setExpr().accept(this)
            }

            override fun visitBagRef(ctx: ClaferRxParser.BagRefContext) {
                val cstr = ExpressionCreator(lastNode, graph).visit(ctx) as SetExpr
                graph.bagRefs[lastNode] = cstr
                ctx.setExpr().accept(this)
            }


            override fun visitIntSet(ctx: ClaferRxParser.IntSetContext) {
                val trg = Node(Node.NodeType.INT)

                // nest auxiliary int node under root
                graph.add(Edge(trg, graph.cRoot, Edge.EdgeType.P))

                // add reference edge
                graph.add(Edge(lastNode, trg, Edge.EdgeType.R))

                firstRef = false
            }

            override fun visitRealSet(ctx: ClaferRxParser.RealSetContext) {
                val trg = Node(Node.NodeType.DBL)

                // nest auxiliary int node under root
                graph.add(Edge(trg, graph.cRoot, Edge.EdgeType.P))

                // add reference edge
                graph.add(Edge(lastNode, trg, Edge.EdgeType.R))

                firstRef = false
            }

            override fun visitStringSet(ctx: ClaferRxParser.StringSetContext) {
                val trg = Node(Node.NodeType.STR)

                // nest auxiliary int node under root
                graph.add(Edge(trg, graph.cRoot, Edge.EdgeType.P))

                // add reference edge
                graph.add(Edge(lastNode, trg, Edge.EdgeType.R))

                firstRef = false
            }
        }

        ctx.accept(visitor)
        return graph
    }

    /**
     * Update (partial) dependency graph with constraints
     */
    private fun addConstraints(graph: IrGraph) {
        val visitor = object : ClaferRxBaseVisitor<Any>() {

            private val currentPath: MutableList<String> = mutableListOf()

            override fun visitClafer(ctx: ClaferRxParser.ClaferContext) {
                val name = ctx.claferName().CLAFER_ID().text
                currentPath.add(name)
                ctx.childElements().element().filter { it.constraint() != null }.map { it.accept(this) }
                ctx.childElements().element().filter { it.clafer() != null }.map { it.accept(this) }
                currentPath.remove(name)
            }

            override fun visitConstraint(ctx: ClaferRxParser.ConstraintContext) {
//                val lastNode = graph.getNode(currentPath.map { simplifyId(it) }.joinToString("."))!!
                val lastNode = graph.getNode(currentPath.joinToString(".") { it })
                val cstr = ExpressionCreator(lastNode, graph).visit(ctx) as BoolExpr

                graph.cstrs.putIfAbsent(lastNode, emptyList())
                graph.cstrs[lastNode] = graph.cstrs[lastNode]!! + listOf(cstr)
            }
        }

        ctx.accept(visitor)
    }

}