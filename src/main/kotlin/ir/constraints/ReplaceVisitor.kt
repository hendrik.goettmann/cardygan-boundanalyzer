package ir.constraints

import java.util.*

//
//fun replaceAstElement(ast: Module, newSubtree: (AstElement) -> Optional<AstElement>, predicate: (AstElement) -> Boolean): Module {
//    val ret = ReplaceAstAstVisitor(listOf(TreeOp(predicate, newSubtree))).visit(ast)
//    assert(ret.isPresent, { "Replacement operation led to empty module. BAD!" })
//    return ret.get() as Module
//}

data class TreeOp(val predicate: (AstElement) -> Boolean,
                  val replacement: (AstElement) -> Optional<AstElement>)


@Suppress("UNCHECKED_CAST")
fun <T : AstElement> replaceAstElement(ast: T,
                                                      newSubtree: (AstElement) -> Optional<AstElement>,
                                                      predicate: (AstElement) -> Boolean): Optional<T> = ReplaceAstAstVisitor(listOf(TreeOp(predicate, newSubtree))).visit(ast) as Optional<T>

@Suppress("UNCHECKED_CAST")
fun <T : AstElement> replaceAstElements(ast: T,
                                                       treeOps: List<TreeOp>): Optional<T> = ReplaceAstAstVisitor(treeOps).visit(ast) as Optional<T>

@Suppress("UNCHECKED_CAST")
private class ReplaceAstAstVisitor(private val treeOps: List<TreeOp>) : AstVisitor<Optional<out AstElement>>() {

    fun executeTreeOperations(element: AstElement, treeOps: List<TreeOp>): Pair<Boolean, Optional<out AstElement>> {
        for (treeOp in treeOps) {
            if (treeOp.predicate(element)) {
                return Pair(true, treeOp.replacement.invoke(element))
            }
        }
        return Pair(false, Optional.empty())
    }

    override fun visit(element: AstElement): Optional<out AstElement> = when (element) {
        is IteBoolExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val i = element.i.accept(this)
                val t = element.t.accept(this)
                val e = element.e.accept(this)

                if (i.isPresent && t.isPresent && e.isPresent)
                    Optional.of(element.copy(i = i.get() as BoolExpr,
                            t = t.get() as BoolExpr,
                            e = e.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is Not -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val e = element.e.accept(this)
                if (e.isPresent)
                    Optional.of(element.copy(e = e.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is And -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as BoolExpr, rhs.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is Or -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as BoolExpr, rhs.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is BiImpl -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as BoolExpr, rhs.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is Impl -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as BoolExpr, rhs.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is Xor -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as BoolExpr, rhs.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is Eq -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as NumExpr, rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Neq -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as NumExpr, rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Leq -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as NumExpr, rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Geq -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as NumExpr, rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Lt -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as NumExpr, rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Gt -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs.get() as NumExpr, rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Lone -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is One -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is No -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is Some -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is EqSet -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as SetExpr, rhs = rhs.get() as SetExpr))
                else Optional.empty()
            }
        }
        is NotEqSet -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as SetExpr, rhs = rhs.get() as SetExpr))
                else Optional.empty()
            }
        }
        is InSet -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as SetExpr, rhs = rhs.get() as SetExpr))
                else Optional.empty()
            }
        }
        is NotInSet -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as SetExpr, rhs = rhs.get() as SetExpr))
                else Optional.empty()
            }
        }
        is IteNumExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val i = element.i.accept(this)
                val t = element.t.accept(this)
                val e = element.e.accept(this)

                if (i.isPresent && t.isPresent && e.isPresent)
                    Optional.of(element.copy(i = i.get() as BoolExpr,
                            t = t.get() as NumExpr,
                            e = e.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Neg -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val e = element.e.accept(this)
                if (e.isPresent) Optional.of(element.copy(e = e.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Multiply -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as NumExpr, rhs = rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Div -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as NumExpr, rhs = rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Add -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as NumExpr, rhs = rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Minus -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as NumExpr, rhs = rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Modulo -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as NumExpr, rhs = rhs.get() as NumExpr))
                else Optional.empty()
            }
        }
        is Card -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is Sum -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is Product -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is Min -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is Max -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                if (set.isPresent)
                    Optional.of(element.copy(set = set.get() as SetExpr))
                else Optional.empty()
            }
        }
        is NumVar -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val path = element.path.accept(this)
                if (path.isPresent) Optional.of(element.copy(path = path.get() as Path))
                else Optional.empty()
            }
        }

        is IntVal -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                Optional.of(element.copy())
            }
        }
        is DoubleVal -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else
                Optional.of(element.copy())
        }
        is IteSet -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val i = element.i.accept(this)
                val t = element.t.accept(this)
                val e = element.e.accept(this)

                if (i.isPresent && t.isPresent && e.isPresent)
                    Optional.of(element.copy(i = i.get() as BoolExpr,
                            t = t.get() as SetExpr,
                            e = e.get() as SetExpr))
                else Optional.empty()
            }
        }
        is Union -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as SetExpr, rhs = rhs.get() as SetExpr))
                else Optional.empty()
            }
        }
        is Intersect -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as SetExpr, rhs = rhs.get() as SetExpr))
                else Optional.empty()
            }
        }
        is Removal -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val lhs = element.lhs.accept(this)
                val rhs = element.rhs.accept(this)
                if (lhs.isPresent && rhs.isPresent)
                    Optional.of(element.copy(lhs = lhs.get() as SetExpr, rhs = rhs.get() as SetExpr))
                else Optional.empty()
            }
        }
        is SetElement -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val path = element.path.accept(this)
                if (path.isPresent())
                    Optional.of(element.copy(path = path.get() as Path))
                else Optional.empty()
            }
        }
        is Identifier -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                Optional.of(element.copy())
            }
        }
        is AllQuantExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val localDecls = element.localDecls.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalDecl }
                val pred = element.pred.accept(this)
                if (localDecls.isNotEmpty() && pred.isPresent)
                    Optional.of(element.copy(localDecls = localDecls, pred = pred.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is AllDisjQuantExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val localDecls = element.localDecls.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalDecl }
                val pred = element.pred.accept(this)
                if (localDecls.isNotEmpty() && pred.isPresent)
                    Optional.of(element.copy(localDecls = localDecls, pred = pred.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is NoQuantExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val localDecls = element.localDecls.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalDecl }
                val pred = element.pred.accept(this)
                if (localDecls.isNotEmpty() && pred.isPresent)
                    Optional.of(element.copy(localDecls = localDecls, pred = pred.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is NoDisjQuantExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val localDecls = element.localDecls.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalDecl }
                val pred = element.pred.accept(this)
                if (localDecls.isNotEmpty() && pred.isPresent)
                    Optional.of(element.copy(localDecls = localDecls, pred = pred.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is OneQuantExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val localDecls = element.localDecls.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalDecl }
                val pred = element.pred.accept(this)
                if (localDecls.isNotEmpty() && pred.isPresent)
                    Optional.of(element.copy(localDecls = localDecls, pred = pred.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is OneDisjQuantExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val localDecls = element.localDecls.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalDecl }
                val pred = element.pred.accept(this)
                if (localDecls.isNotEmpty() && pred.isPresent)
                    Optional.of(element.copy(localDecls = localDecls, pred = pred.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is SomeQuantExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val localDecls = element.localDecls.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalDecl }
                val pred = element.pred.accept(this)
                if (localDecls.isNotEmpty() && pred.isPresent)
                    Optional.of(element.copy(localDecls = localDecls, pred = pred.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is SomeDisjQuantExpr -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val localDecls = element.localDecls.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalDecl }
                val pred = element.pred.accept(this)
                if (localDecls.isNotEmpty() && pred.isPresent)
                    Optional.of(element.copy(localDecls = localDecls, pred = pred.get() as BoolExpr))
                else Optional.empty()
            }
        }
        is LocalDecl -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val set = element.set.accept(this)
                val localIdentifiers = element.localIdentifiers
                        .map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as LocalIdentifier }
                if (set.isPresent && localIdentifiers.isNotEmpty())
                    Optional.of(element.copy(
                            localIdentifiers = localIdentifiers,
                            set = set.get() as SetExpr))
                else Optional.empty()
            }
        }

        is StringSetElement -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                Optional.of(element.copy())
            }
        }
        is Path -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                val path = element.elements.map { it.accept(this) }
                        .filter { it.isPresent }
                        .map { it.get() as AbstractIdentifier }
                        .toList()
                if (path.isEmpty()) {
                    Optional.empty()
                } else {
                    Optional.of(element.copy(elements = path))
                }
            }
        }

        is LocalIdentifier -> {
            val (predicate, newsubtree) = executeTreeOperations(element, treeOps)
            if (predicate) {
                newsubtree
            } else {
                Optional.of(element.copy())
            }
        }
    }
}