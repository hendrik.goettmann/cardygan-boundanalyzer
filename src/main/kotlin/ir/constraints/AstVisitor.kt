package ir.constraints


abstract class AstVisitor<T> {
    abstract fun visit(element: AstElement): T
}

abstract class BoolExprVisitor<T> {
    abstract fun visit(elem: BoolExpr): T
}

data class VisitorParams(
        val visitIteBoolExpr: (IteBoolExpr) -> Unit = {},
        val visitNot: (Not) -> Unit = {},
        val visitAnd: (And) -> Unit = {},
        val visitOr: (Or) -> Unit = {},
        val visitNeq: (Neq) -> Unit = {},
        val visitEq: (Eq) -> Unit = {},
        val visitLeq: (Leq) -> Unit = {},
        val visitGeq: (Geq) -> Unit = {},
        val visitLt: (Lt) -> Unit = {},
        val visitGt: (Gt) -> Unit = {},
        val visitLone: (Lone) -> Unit = {},
        val visitOne: (One) -> Unit = {},
        val visitNo: (No) -> Unit = {},
        val visitSome: (Some) -> Unit = {},
        val visitEqSet: (EqSet) -> Unit = {},
        val visitNotEqSet: (NotEqSet) -> Unit = {},
        val visitInSet: (InSet) -> Unit = {},
        val visitNotInSet: (NotInSet) -> Unit = {},
        val visitIteNumExpr: (IteNumExpr) -> Unit = {},
        val visitMult: (Multiply) -> Unit = {},
        val visitDiv: (Div) -> Unit = {},
        val visitAdd: (Add) -> Unit = {},
        val visitMinus: (Minus) -> Unit = {},
        val visitModulo: (Modulo) -> Unit = {},
        val visitCard: (Card) -> Unit = {},
        val visitSum: (Sum) -> Unit = {},
        val visitProduct: (Product) -> Unit = {},
        val visitMin: (Min) -> Unit = {},
        val visitMax: (Max) -> Unit = {},
        val visitNumVar: (NumVar) -> Unit = {},
        val visitIntVal: (IntVal) -> Unit = {},
        val visitDoubleVal: (DoubleVal) -> Unit = {},
        val visitIteSet: (IteSet) -> Unit = {},
        val visitUnion: (Union) -> Unit = {},
        val visitIntersect: (Intersect) -> Unit = {},
        val visitRemoval: (Removal) -> Unit = {},
        val visitSetElement: (SetElement) -> Unit = {},
        val visitIdentifier: (Identifier) -> Unit = {},
        val visitAllDisjQuantExpr: (AllDisjQuantExpr) -> Unit = {},
        val visitAllQuantExpr: (AllQuantExpr) -> Unit = {},
        val visitNoQuantExpr: (NoQuantExpr) -> Unit = {},
        val visitNoDisjQuantExpr: (NoDisjQuantExpr) -> Unit = {},
        val visitOneQuantExpr: (OneQuantExpr) -> Unit = {},
        val visitOneDisjQuantExpr: (OneDisjQuantExpr) -> Unit = {},
        val visitSomeQuantExpr: (SomeQuantExpr) -> Unit = {},
        val visitSomeDisjQuantExpr: (SomeDisjQuantExpr) -> Unit = {},
        val visitLocalSetDecl: (LocalDecl) -> Unit = {},
        val visitStringSetElement: (StringSetElement) -> Unit = {},
        val visitNeg: (Neg) -> Unit = {},
        val visitBiImpl: (BiImpl) -> Unit = {},
        val visitXor: (Xor) -> Unit = {},
        val visitImpl: (Impl) -> Unit = {},
        val visitPath: (Path) -> Unit = {},
        val visitLocalIdentifier: (LocalIdentifier) -> Unit = {}
)

fun visitAst(element: AstElement,
             param: VisitorParams) = visitAst(element, { true }, param)

fun visitAst(element: AstElement,
             doVisitWhile: (AstElement) -> Boolean,
             param: VisitorParams
): Unit = when (element) {
    is IteBoolExpr -> {
        param.visitIteBoolExpr(element)
        if (doVisitWhile(element)) {
            visitAst(element.i, doVisitWhile, param)
            visitAst(element.t, doVisitWhile, param)
            visitAst(element.e, doVisitWhile, param)
        } else Unit
    }
    is Not -> {
        param.visitNot(element)
        if (doVisitWhile(element)) {
            visitAst(element.e, doVisitWhile, param)
        } else Unit
    }
    is And -> {
        param.visitAnd(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Or -> {
        param.visitOr(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is BiImpl -> {
        param.visitBiImpl(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Impl -> {
        param.visitImpl(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Xor -> {
        param.visitXor(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Eq -> {
        param.visitEq(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Neq -> {
        param.visitNeq(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Leq -> {
        param.visitLeq(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Geq -> {
        param.visitGeq(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Lt -> {
        param.visitLt(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Gt -> {
        param.visitGt(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Lone -> {
        param.visitLone(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is One -> {
        param.visitOne(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is No -> {
        param.visitNo(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is Some -> {
        param.visitSome(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is EqSet -> {
        param.visitEqSet(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is NotEqSet -> {
        param.visitNotEqSet(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is InSet -> {
        param.visitInSet(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is NotInSet -> {
        param.visitNotInSet(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is IteNumExpr -> {
        param.visitIteNumExpr(element)
        if (doVisitWhile(element)) {
            visitAst(element.i, doVisitWhile, param)
            visitAst(element.t, doVisitWhile, param)
            visitAst(element.e, doVisitWhile, param)
        } else Unit
    }
    is Multiply -> {
        param.visitMult(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Div -> {
        param.visitDiv(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Add -> {
        param.visitAdd(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Minus -> {
        param.visitMinus(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Modulo -> {
        param.visitModulo(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Card -> {
        param.visitCard(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is Sum -> {
        param.visitSum(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is Product -> {
        param.visitProduct(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is Min -> {
        param.visitMin(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is Max -> {
        param.visitMax(element)
        if (doVisitWhile(element)) {
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is NumVar -> {
        param.visitNumVar(element)
        if (doVisitWhile(element)) {
            visitAst(element.path, doVisitWhile, param)
        } else Unit
    }
    is IntVal -> {
        param.visitIntVal(element)
    }
    is DoubleVal -> {
        param.visitDoubleVal(element)
    }
    is IteSet -> {
        param.visitIteSet(element)
        if (doVisitWhile(element)) {
            visitAst(element.i, doVisitWhile, param)
            visitAst(element.t, doVisitWhile, param)
            visitAst(element.e, doVisitWhile, param)
        } else Unit
    }
    is Union -> {
        param.visitUnion(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Intersect -> {
        param.visitIntersect(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is Removal -> {
        param.visitRemoval(element)
        if (doVisitWhile(element)) {
            visitAst(element.lhs, doVisitWhile, param)
            visitAst(element.rhs, doVisitWhile, param)
        } else Unit
    }
    is SetElement -> {
        param.visitSetElement(element)
        if (doVisitWhile(element)) {
            visitAst(element.path, doVisitWhile, param)
        } else Unit
    }
    is Identifier -> {
        param.visitIdentifier(element)
    }
    is AllQuantExpr -> {
        param.visitAllQuantExpr(element)
        if (doVisitWhile(element)) {
            element.localDecls.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.pred, doVisitWhile, param)
        } else Unit
    }
    is AllDisjQuantExpr -> {
        param.visitAllDisjQuantExpr(element)
        if (doVisitWhile(element)) {
            element.localDecls.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.pred, doVisitWhile, param)
        } else Unit
    }
    is NoQuantExpr -> {
        param.visitNoQuantExpr(element)
        if (doVisitWhile(element)) {
            element.localDecls.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.pred, doVisitWhile, param)
        } else Unit
    }
    is NoDisjQuantExpr -> {
        param.visitNoDisjQuantExpr(element)
        if (doVisitWhile(element)) {
            element.localDecls.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.pred, doVisitWhile, param)
        } else Unit
    }
    is OneQuantExpr -> {
        param.visitOneQuantExpr(element)
        if (doVisitWhile(element)) {
            element.localDecls.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.pred, doVisitWhile, param)
        } else Unit
    }
    is OneDisjQuantExpr -> {
        param.visitOneDisjQuantExpr(element)
        if (doVisitWhile(element)) {
            element.localDecls.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.pred, doVisitWhile, param)
        } else Unit
    }
    is SomeQuantExpr -> {
        param.visitSomeQuantExpr(element)
        if (doVisitWhile(element)) {
            element.localDecls.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.pred, doVisitWhile, param)
        } else Unit
    }
    is SomeDisjQuantExpr -> {
        param.visitSomeDisjQuantExpr(element)
        if (doVisitWhile(element)) {
            element.localDecls.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.pred, doVisitWhile, param)
        } else Unit
    }
    is LocalDecl -> {
        param.visitLocalSetDecl(element)
        if (doVisitWhile(element)) {
            element.localIdentifiers.forEach { e -> visitAst(e, doVisitWhile, param) }
            visitAst(element.set, doVisitWhile, param)
        } else Unit
    }
    is StringSetElement -> {
        param.visitStringSetElement(element)
    }
    is Neg -> {
        param.visitNeg(element)
        if (doVisitWhile(element)) {
            visitAst(element.e, doVisitWhile, param)

        } else Unit
    }
    is Path -> {
        param.visitPath(element)
        if (doVisitWhile(element)) {
            element.elements.forEach { visitAst(it, doVisitWhile, param) }
        } else Unit
    }
    is LocalIdentifier ->
        param.visitLocalIdentifier(element)
}


