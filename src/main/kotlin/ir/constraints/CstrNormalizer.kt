package ir.constraints

import ir.constraints.*
import ir.constraints.Eq
import ir.constraints.IteNumExpr
import java.util.*


class CstrNormalizer(val cstr: BoolExpr) {

    fun doTransform(): BoolExpr {

        // transforms Equalities of the form
        // numVar = (if cond then val1 else val2) to if cond then numVar = val1 else numVar = val2
        val replaceNumIte = TreeOp({ it is Eq && it.lhs is NumVar && it.rhs is IteNumExpr },
                {
                    it as Eq
                    val numVar = it.lhs as NumVar
                    val rhs = it.rhs as IteNumExpr
                    val cond = rhs.i
                    val val1 = rhs.t
                    val val2 = rhs.e

                    Optional.of(IteBoolExpr(cond, Eq(numVar, val1), Eq(numVar, val2)))
                })

        // transforms Equalities of the form
        // <expr> / numVal to  <expr> * 1/numVal
        val replaceNumDiv = TreeOp({ it is Div && (it.rhs is DoubleVal || it.rhs is IntVal) },
                {
                    it as Div
                    val numVal = if (it.rhs is DoubleVal) it.rhs.v else {
                        it.rhs as IntVal
                        it.rhs.v.toDouble()
                    }
                    val expr = it.lhs

                    Optional.of(Multiply(DoubleVal(1 / numVal), expr))
                })


        return replaceAstElements(cstr, listOf(replaceNumIte, replaceNumDiv)).get()
    }
}