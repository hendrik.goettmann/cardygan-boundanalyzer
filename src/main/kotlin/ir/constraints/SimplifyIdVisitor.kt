package ir.constraints

import java.util.*
import java.util.regex.Pattern

fun <T : AstElement> simplifyIds(element: T): T =
        replaceAstElements(element, listOf(TreeOp(
                predicate = { it is Identifier && it != ThisIdentifier && it != DrefIdentfier && it != ParentIdentfier },
                replacement = {
                    it as Identifier
                    Optional.of(Identifier(simplifyId(it.id)))
                }
        ))).get()

fun simplifyId(id: String): String {
    val idPattern = "^(c\\d+_).*"

    // Already simplified, nothing to do.
    if (!id.matches(Regex(idPattern)))
        return id

    val pattern = Pattern.compile(idPattern)
    val m = pattern.matcher(id)
    if (m.find() && m.groupCount() == 1)
        return StringBuilder(id).replace(m.start(1), m.end(1), "").toString()
    else
        throw IllegalStateException("Invalid ID format. IDs are required to start with c<digit>+_.")
}