package ir.constraints

import java.util.*

sealed class AstElement {
    fun <T> accept(visitor: AstVisitor<T>): T = visitor.visit(this)
}

sealed class BoolExpr : AstElement() {
    fun <T> accept(visitor: BoolExprVisitor<T>): T = visitor.visit(this)
}

data class IteBoolExpr(val i: BoolExpr, val t: BoolExpr, val e: BoolExpr) : BoolExpr()
data class Not(val e: BoolExpr) : BoolExpr()
data class And(val lhs: BoolExpr, val rhs: BoolExpr) : BoolExpr()
data class Or(val lhs: BoolExpr, val rhs: BoolExpr) : BoolExpr()

sealed class BinaryBoolExpr(open val lhs: BoolExpr, open val rhs: BoolExpr) : BoolExpr()
data class BiImpl(override val lhs: BoolExpr, override val rhs: BoolExpr) : BinaryBoolExpr(lhs, rhs)
data class Impl(override val lhs: BoolExpr, override val rhs: BoolExpr) : BinaryBoolExpr(lhs, rhs)
data class Xor(override val lhs: BoolExpr, override val rhs: BoolExpr) : BinaryBoolExpr(lhs, rhs)

sealed class NumComp(open val lhs: NumExpr, open val rhs: NumExpr) : BoolExpr()
data class Eq(override val lhs: NumExpr, override val rhs: NumExpr) : NumComp(lhs, rhs)
data class Neq(override val lhs: NumExpr, override val rhs: NumExpr) : NumComp(lhs, rhs)
data class Leq(override val lhs: NumExpr, override val rhs: NumExpr) : NumComp(lhs, rhs)
data class Geq(override val lhs: NumExpr, override val rhs: NumExpr) : NumComp(lhs, rhs)
data class Lt(override val lhs: NumExpr, override val rhs: NumExpr) : NumComp(lhs, rhs)
data class Gt(override val lhs: NumExpr, override val rhs: NumExpr) : NumComp(lhs, rhs)

sealed class SimpleQuantifiedExpr(open val set: SetExpr) : BoolExpr()
data class Lone(override val set: SetExpr) : SimpleQuantifiedExpr(set)
data class One(override val set: SetExpr) : SimpleQuantifiedExpr(set)
data class No(override val set: SetExpr) : SimpleQuantifiedExpr(set)
data class Some(override val set: SetExpr) : SimpleQuantifiedExpr(set)

sealed class ComplexQuantExpr(open val localDecls: List<LocalDecl>, open val pred: BoolExpr) : BoolExpr()
data class AllQuantExpr(override val localDecls: List<LocalDecl>, override val pred: BoolExpr) : ComplexQuantExpr(localDecls, pred)
data class AllDisjQuantExpr(override val localDecls: List<LocalDecl>, override val pred: BoolExpr) : ComplexQuantExpr(localDecls, pred)
data class NoQuantExpr(override val localDecls: List<LocalDecl>, override val pred: BoolExpr) : ComplexQuantExpr(localDecls, pred)
data class NoDisjQuantExpr(override val localDecls: List<LocalDecl>, override val pred: BoolExpr) : ComplexQuantExpr(localDecls, pred)
data class OneQuantExpr(override val localDecls: List<LocalDecl>, override val pred: BoolExpr) : ComplexQuantExpr(localDecls, pred)
data class OneDisjQuantExpr(override val localDecls: List<LocalDecl>, override val pred: BoolExpr) : ComplexQuantExpr(localDecls, pred)
data class SomeQuantExpr(override val localDecls: List<LocalDecl>, override val pred: BoolExpr) : ComplexQuantExpr(localDecls, pred)
data class SomeDisjQuantExpr(override val localDecls: List<LocalDecl>, override val pred: BoolExpr) : ComplexQuantExpr(localDecls, pred)


data class LocalDecl(val localIdentifiers: List<LocalIdentifier>, val set: SetExpr) : AstElement()
//data class LocalSimpleDecl(val identifier: Identifier) : LocalDecl()

sealed class SetComp(open val lhs: SetExpr, open val rhs: SetExpr) : BoolExpr()
data class EqSet(override val lhs: SetExpr, override val rhs: SetExpr) : SetComp(lhs, rhs)
data class NotEqSet(override val lhs: SetExpr, override val rhs: SetExpr) : SetComp(lhs, rhs)
data class InSet(override val lhs: SetExpr, override val rhs: SetExpr) : SetComp(lhs, rhs)
data class NotInSet(override val lhs: SetExpr, override val rhs: SetExpr) : SetComp(lhs, rhs)

sealed class NumExpr : AstElement()
data class Neg(val e: NumExpr) : NumExpr()
data class IteNumExpr(val i: BoolExpr, val t: NumExpr, val e: NumExpr) : NumExpr()

sealed class BinaryNumExpr(open val lhs: NumExpr, open val rhs: NumExpr) : NumExpr()
data class Multiply(override val lhs: NumExpr, override val rhs: NumExpr) : BinaryNumExpr(lhs, rhs)
data class Div(override val lhs: NumExpr, override val rhs: NumExpr) : BinaryNumExpr(lhs, rhs)
data class Add(override val lhs: NumExpr, override val rhs: NumExpr) : BinaryNumExpr(lhs, rhs)
data class Minus(override val lhs: NumExpr, override val rhs: NumExpr) : BinaryNumExpr(lhs, rhs)
data class Modulo(override val lhs: NumExpr, override val rhs: NumExpr) : BinaryNumExpr(lhs, rhs)

data class Card(val set: SetExpr) : NumExpr()
data class Sum(val set: SetExpr) : NumExpr()

data class Product(val set: SetExpr) : NumExpr()
data class Min(val set: SetExpr) : NumExpr()
data class Max(val set: SetExpr) : NumExpr()
data class NumVar(val path: Path) : NumExpr()

sealed class NumLiteral : NumExpr()
data class IntVal(val v: Int) : NumLiteral()
data class DoubleVal(val v: Double) : NumLiteral()

sealed class SetExpr : AstElement()
data class StringSetElement(val s: String) : SetExpr()
data class IteSet(val i: BoolExpr, val t: SetExpr, val e: SetExpr) : SetExpr()

sealed class BinarySetExpr(open val lhs: SetExpr, open val rhs: SetExpr) : SetExpr()
data class Union(override val lhs: SetExpr, override val rhs: SetExpr) : BinarySetExpr(lhs, rhs)
data class Intersect(override val lhs: SetExpr, override val rhs: SetExpr) : BinarySetExpr(lhs, rhs)
data class Removal(override val lhs: SetExpr, override val rhs: SetExpr) : BinarySetExpr(lhs, rhs)

data class SetElement(val path: Path) : SetExpr()

data class Path(val elements: List<AbstractIdentifier>) : AstElement() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Path?
        return elements == that!!.elements
    }

    override fun hashCode(): Int {
        return Objects.hash(elements)
    }

    fun asList(): List<String> = elements.map { it.id }

    constructor(path: String) : this(path.split(".").map { Identifier(it) })

    override fun toString(): String {
        return elements.joinToString(".")
    }

    fun asString(): String {
        return elements.joinToString(".")
    }
}

sealed class AbstractIdentifier(open val id: String) : AstElement()
data class Identifier(override val id: String) : AbstractIdentifier(id) {
    override fun toString(): String {
        return id
    }
}

//data class SuperIdentifier(override val id: String) : AbstractIdentifier(id)
data class LocalIdentifier(override val id: String) : AbstractIdentifier(id)

val ThisIdentifier = Identifier("this")
val DrefIdentfier = Identifier("dref")
val ParentIdentfier = Identifier("parent")


fun union(elements: Set<SetExpr>): SetExpr {
    if (elements.size < 2)
        throw IllegalArgumentException("Union operation must be applied to at least two set expressions.")
    val ret = elements.reduce { lhs, rhs -> Union(lhs, rhs) }
    return ret
}


//fun Path.replaceSubPath(toBeReplaced: Path, replacement: Path): Path {
//    val toBeReplacedS = toBeReplaced.elements.joinToString(".")
//    val replacementS = replacement.elements.joinToString(".")
//    val searchString = this.elements.joinToString(".")
//    val pathPattern = "(^|.*\\.)($toBeReplacedS)($|\\..*$)"
//
//    assert(searchString.matches(Regex(pathPattern)), { "Could not find pathExpr which is to be replaced." })
//
//    val pattern = Pattern.compile(pathPattern)
//    val m = pattern.matcher(searchString)
//    if (m.find() && m.groupCount() == 3) {
//        return Path(StringBuilder(searchString)
//                .replace(m.start(2), m.end(2), replacementS)
//                .toString()
//                .split(".")
//                .map { Identifier(it) }
//        )
//    }
//
//    throw IllegalStateException("Could not find pathExpr which is to be replaced.")
//}

