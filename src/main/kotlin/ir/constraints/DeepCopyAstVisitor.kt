package ir.constraints

fun deepCopy(element: AstElement): AstElement {
    return DeepCopyAstVisitor().visit(element)
}

private class DeepCopyAstVisitor : AstVisitor<AstElement>() {

    override fun visit(element: AstElement): AstElement = when (element) {

        is IteBoolExpr -> element.copy(i = element.i.accept(this) as BoolExpr,
                t = element.t.accept(this) as BoolExpr,
                e = element.e.accept(this) as BoolExpr)
        is Not -> element.copy(e = element.e.accept(this) as BoolExpr)
        is And -> element.copy(element.lhs.accept(this) as BoolExpr, element.rhs.accept(this) as BoolExpr)
        is Or -> element.copy(element.lhs.accept(this) as BoolExpr, element.rhs.accept(this) as BoolExpr)
        is BiImpl -> element.copy(element.lhs.accept(this) as BoolExpr, element.rhs.accept(this) as BoolExpr)
        is Impl -> element.copy(element.lhs.accept(this) as BoolExpr, element.rhs.accept(this) as BoolExpr)
        is Xor -> element.copy(element.lhs.accept(this) as BoolExpr, element.rhs.accept(this) as BoolExpr)
        is Eq -> element.copy(element.lhs.accept(this) as NumExpr, element.rhs.accept(this) as NumExpr)
        is Neq -> element.copy(element.lhs.accept(this) as NumExpr, element.rhs.accept(this) as NumExpr)
        is Leq -> element.copy(element.lhs.accept(this) as NumExpr, element.rhs.accept(this) as NumExpr)
        is Geq -> element.copy(element.lhs.accept(this) as NumExpr, element.rhs.accept(this) as NumExpr)
        is Lt -> element.copy(element.lhs.accept(this) as NumExpr, element.rhs.accept(this) as NumExpr)
        is Gt -> element.copy(element.lhs.accept(this) as NumExpr, element.rhs.accept(this) as NumExpr)
        is Lone -> element.copy(set = element.set.accept(this) as SetExpr)
        is One -> element.copy(set = element.set.accept(this) as SetExpr)
        is No -> element.copy(set = element.set.accept(this) as SetExpr)
        is Some -> element.copy(set = element.set.accept(this) as SetExpr)
        is EqSet -> element.copy(lhs = element.lhs.accept(this) as SetExpr, rhs = element.rhs.accept(this) as SetExpr)
        is NotEqSet -> element.copy(lhs = element.lhs.accept(this) as SetExpr, rhs = element.rhs.accept(this) as SetExpr)
        is InSet -> element.copy(lhs = element.lhs.accept(this) as SetExpr, rhs = element.rhs.accept(this) as SetExpr)
        is NotInSet -> element.copy(lhs = element.lhs.accept(this) as SetExpr, rhs = element.rhs.accept(this) as SetExpr)
        is IteNumExpr -> element.copy(i = element.i.accept(this) as BoolExpr,
                t = element.t.accept(this) as NumExpr,
                e = element.e.accept(this) as NumExpr)
        is Neg -> element.copy(e = element.e.accept(this) as NumExpr)
        is Multiply -> element.copy(lhs = element.lhs.accept(this) as NumExpr, rhs = element.rhs.accept(this) as NumExpr)
        is Div -> element.copy(lhs = element.lhs.accept(this) as NumExpr, rhs = element.rhs.accept(this) as NumExpr)
        is Add -> element.copy(lhs = element.lhs.accept(this) as NumExpr, rhs = element.rhs.accept(this) as NumExpr)
        is Minus -> element.copy(lhs = element.lhs.accept(this) as NumExpr, rhs = element.rhs.accept(this) as NumExpr)
        is Modulo -> element.copy(lhs = element.lhs.accept(this) as NumExpr, rhs = element.rhs.accept(this) as NumExpr)
        is Card -> element.copy(set = element.set.accept(this) as SetExpr)
        is Sum -> element.copy(set = element.set.accept(this) as SetExpr)
        is Product -> element.copy(set = element.set.accept(this) as SetExpr)
        is Min -> element.copy(set = element.set.accept(this) as SetExpr)
        is Max -> element.copy(set = element.set.accept(this) as SetExpr)
        is NumVar -> element.copy(path = element.path.accept(this) as Path)
        is IntVal -> element.copy()
        is DoubleVal -> element.copy()
        is IteSet -> element.copy(i = element.i.accept(this) as BoolExpr,
                t = element.t.accept(this) as SetExpr,
                e = element.e.accept(this) as SetExpr)
        is Union -> element.copy(lhs = element.lhs.accept(this) as SetExpr, rhs = element.rhs.accept(this) as SetExpr)
        is Intersect -> element.copy(lhs = element.lhs.accept(this) as SetExpr, rhs = element.rhs.accept(this) as SetExpr)
        is Removal -> element.copy(lhs = element.lhs.accept(this) as SetExpr, rhs = element.rhs.accept(this) as SetExpr)
        is SetElement -> element.copy(path = element.path.accept(this) as Path)
        is Identifier -> element.copy()
        is AllQuantExpr -> element.copy(localDecls = element.localDecls.map { it.accept(this) as LocalDecl }, pred = element.pred.accept(this) as BoolExpr)
        is AllDisjQuantExpr -> element.copy(localDecls = element.localDecls.map { it.accept(this) as LocalDecl }, pred = element.pred.accept(this) as BoolExpr)
        is NoQuantExpr -> element.copy(localDecls = element.localDecls.map { it.accept(this) as LocalDecl }, pred = element.pred.accept(this) as BoolExpr)
        is NoDisjQuantExpr -> element.copy(localDecls = element.localDecls.map { it.accept(this) as LocalDecl }, pred = element.pred.accept(this) as BoolExpr)
        is OneQuantExpr -> element.copy(localDecls = element.localDecls.map { it.accept(this) as LocalDecl }, pred = element.pred.accept(this) as BoolExpr)
        is OneDisjQuantExpr -> element.copy(localDecls = element.localDecls.map { it.accept(this) as LocalDecl }, pred = element.pred.accept(this) as BoolExpr)
        is SomeQuantExpr -> element.copy(localDecls = element.localDecls.map { it.accept(this) as LocalDecl }, pred = element.pred.accept(this) as BoolExpr)
        is SomeDisjQuantExpr -> element.copy(localDecls = element.localDecls.map { it.accept(this) as LocalDecl }, pred = element.pred.accept(this) as BoolExpr)
        is LocalDecl -> element.copy(localIdentifiers = element.localIdentifiers.map { it.accept(this) as LocalIdentifier }, set = element.set.accept(this) as SetExpr)
        is StringSetElement -> element.copy()
        is Path -> element.copy(elements = element.elements.map { it.accept(this) as AbstractIdentifier }.toList())
        is LocalIdentifier -> element.copy()
    }

}