package ir.constraints


fun prettyPrint(element: AstElement): String {
    return PrettyPrinter().visit(element)
}

private class PrettyPrinter : AstVisitor<String>() {

    override fun visit(element: AstElement) = when (element) {

        is IteBoolExpr -> "(if (${element.i.accept(this)}) then ${element.t.accept(this)} else ${element.e.accept(this)})"
        is Not -> "!(${element.e.accept(this)})"
        is And -> "(${element.lhs.accept(this)} && ${element.rhs.accept(this)})"
        is Or -> "(${element.lhs.accept(this)} || ${element.rhs.accept(this)})"
        is BiImpl -> "(${element.lhs.accept(this)} <=> ${element.rhs.accept(this)})"
        is Impl -> "(${element.lhs.accept(this)} => ${element.rhs.accept(this)})"
        is Xor -> "(${element.lhs.accept(this)} xor ${element.rhs.accept(this)})"
        is Eq -> "(${element.lhs.accept(this)} = ${element.rhs.accept(this)})"
        is Neq -> "(${element.lhs.accept(this)} != ${element.rhs.accept(this)})"
        is Leq -> "(${element.lhs.accept(this)} <= ${element.rhs.accept(this)})"
        is Geq -> "(${element.lhs.accept(this)} >= ${element.rhs.accept(this)})"
        is Lt -> "(${element.lhs.accept(this)} < ${element.rhs.accept(this)})"
        is Gt -> "(${element.lhs.accept(this)} > ${element.rhs.accept(this)})"
        is Lone -> "(lone ${element.set.accept(this)})"
        is One -> "(one ${element.set.accept(this)})"
        is No -> "(no ${element.set.accept(this)})"
        is Some -> "(some ${element.set.accept(this)})"
        is AllQuantExpr -> "(all ${element.localDecls.map { it.accept(this) }.joinToString(";")} | ${element.pred.accept(this)})"
        is AllDisjQuantExpr -> "(all disj ${element.localDecls.map { it.accept(this) }.joinToString(";")} | ${element.pred.accept(this)})"
        is NoQuantExpr -> "(no ${element.localDecls.map { it.accept(this) }.joinToString(";")} | ${element.pred.accept(this)})"
        is NoDisjQuantExpr -> "(no disj ${element.localDecls.map { it.accept(this) }.joinToString(";")} | ${element.pred.accept(this)})"
        is OneQuantExpr -> "(one ${element.localDecls.map { it.accept(this) }.joinToString(";")} | ${element.pred.accept(this)})"
        is OneDisjQuantExpr -> "(one disj ${element.localDecls.map { it.accept(this) }.joinToString(";")} | ${element.pred.accept(this)})"
        is SomeQuantExpr -> "(some ${element.localDecls.map { it.accept(this) }.joinToString(";")} | ${element.pred.accept(this)})"
        is SomeDisjQuantExpr -> "(some disj ${element.localDecls.map { it.accept(this) }.joinToString(";")} | ${element.pred.accept(this)})"
        is EqSet -> "(${element.lhs.accept(this)} = ${element.rhs.accept(this)})"
        is NotEqSet -> "(${element.lhs.accept(this)} != ${element.rhs.accept(this)})"
        is InSet -> "(${element.lhs.accept(this)} in ${element.rhs.accept(this)})"
        is NotInSet -> "(${element.lhs.accept(this)} not in ${element.rhs.accept(this)})"
        is LocalDecl -> "${element.localIdentifiers.map { it.accept(this) }.joinToString("; ")} : ${element.set.accept(this)}"
        is Neg -> "-(${element.e.accept(this)})"
        is IteNumExpr -> "(if (${element.i.accept(this)}) then ${element.t.accept(this)} else ${element.e.accept(this)})"
        is Multiply -> "(${element.lhs.accept(this)} * ${element.rhs.accept(this)})"
        is Div -> "(${element.lhs.accept(this)} / ${element.rhs.accept(this)})"
        is Add -> "(${element.lhs.accept(this)} + ${element.rhs.accept(this)})"
        is Minus -> "(${element.lhs.accept(this)} - ${element.rhs.accept(this)})"
        is Modulo -> "(${element.lhs.accept(this)} % ${element.rhs.accept(this)})"
        is Card -> "#(${element.set.accept(this)})"
        is Sum -> "(sum ${element.set.accept(this)})"
        is Product -> "(product ${element.set.accept(this)})"
        is Min -> "(min ${element.set.accept(this)})"
        is Max -> "(max ${element.set.accept(this)})"
        is NumVar -> element.path.accept(this)
        is IntVal -> "${element.v}"
        // TODO: Fix for double vals
        is DoubleVal -> "${element.v.toInt()}"
//        is IntSetElement -> "${element.i}"
//        is DoubleSetElement -> "${element.d}"
        is StringSetElement -> element.s
        is IteSet -> "(if (${element.i.accept(this)}) then ${element.t.accept(this)} else ${element.e.accept(this)})"
        is Union -> "(${element.lhs.accept(this)} ++ ${element.rhs.accept(this)})"
        is Intersect -> "${element.lhs.accept(this)} ** ${element.rhs.accept(this)}"
        is Removal -> "${element.lhs.accept(this)} -- ${element.rhs.accept(this)}"
        is SetElement -> element.path.accept(this)
        is Identifier -> element.id
        is Path -> element.elements.map { it.accept(this) }.joinToString(".")
        is LocalIdentifier ->
            element.id
    }


}
