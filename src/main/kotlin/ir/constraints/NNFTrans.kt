package ir.constraints


/**
 * Provides functionality for transforming constraints to Negation Normal Form (NNF)
 */
class NNFTrans(val cstr: BoolExpr) {

    fun doTransform(): BoolExpr {
        // eliminate implications
        val elim = object : BoolExprVisitor<BoolExpr>() {
            override fun visit(elem: BoolExpr): BoolExpr = when (elem) {
                is IteBoolExpr -> And(visit(Impl(elem.i, elem.t)), visit(Impl(Not(elem.i), elem.e)))
                is Not -> Not(visit(elem.e))
                is And -> And(visit(elem.lhs), visit(elem.rhs))
                is Or -> Or(visit(elem.lhs), visit(elem.rhs))
                is BiImpl -> And(visit(Impl(elem.lhs, elem.rhs)), visit(Impl(elem.rhs, elem.lhs)))
                is Impl -> Or(Not(visit(elem.lhs)), visit(elem.rhs))
                is Xor -> Or(And(Not(visit(elem.lhs)), visit(elem.rhs)), And(Not(visit(elem.rhs)), visit(elem.lhs)))
                else -> elem
            }
        }

        // Push down "not"
        val nnf = object : BoolExprVisitor<BoolExpr>() {
            override fun visit(elem: BoolExpr): BoolExpr = when (elem) {
                is Not -> {
                    val nested = elem.e
                    when (nested) {
                        is Not -> visit(nested.e)
                        is And -> Or(Not(visit(nested.lhs)), visit(Not(nested.rhs)))
                        is Or -> And(Not(visit(nested.lhs)), visit(Not(nested.rhs)))
                        is Eq -> Neq(nested.lhs, nested.rhs)
                        is Neq -> Eq(nested.lhs, nested.rhs)
                        is Leq -> Gt(nested.lhs, nested.rhs)
                        is Geq -> Lt(nested.lhs, nested.rhs)
                        is Lt -> Geq(nested.lhs, nested.rhs)
                        is Gt -> Leq(nested.lhs, nested.rhs)
                        is Lone -> Gt(Card(nested.set), IntVal(1))
                        is One -> Or(No(nested.set), Gt(Card(nested.set), IntVal(1)))
                        is No -> Some(nested.set)
                        is Some -> No(nested.set)
                        is EqSet -> NotEqSet(nested.lhs, nested.rhs)
                        is NotEqSet -> EqSet(nested.lhs, nested.rhs)
                        is InSet -> NotInSet(nested.lhs, nested.rhs)
                        is NotInSet -> InSet(nested.lhs, nested.rhs)
                        else -> error("Element $nested should not be part of AST.")
                    }
                }
                is And -> And(visit(elem.lhs), visit(elem.rhs))
                is Or -> Or(visit(elem.lhs), visit(elem.rhs))
                else -> elem
            }
        }

        return cstr.accept(elim).accept(nnf)
    }

}