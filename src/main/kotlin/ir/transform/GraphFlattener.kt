package ir.transform

import ir.*
import ir.constraints.*
import mu.KotlinLogging
import java.util.*
import java.util.Collections.emptyList

private val logger = KotlinLogging.logger {}

fun IrGraph.flatten() {
    logger.info { "Starting to flatten specification." }

    discardUnsupportedConstraints(this)

    pushDownOutgoingReferences(this)

    pushDownIncomingReferences(this)

    flattenSubtree(this.cRoot, this)

    flattenRefs(this)

    flattenConstraints(this)

    flattenNumExprs(this)

    removeAbstractNodes(this)

}


fun flattenRefs(graph: IrGraph) {
    logger.info { "Starting to flatten references." }
    // flatten set references
    val transSetRefs = graph.setRefs
            .filter { (src, _) -> !src.isAbstract && !graph.isNestedInAbstract(src) }
            .map { (src, expr) ->
                src to transExpr(expr, graph.cRoot, graph)
            }.toMap()

    graph.setRefs.clear()
    graph.setRefs.putAll(transSetRefs)

    // flatten bag references
    val transBagRefs = graph.bagRefs.map {
        val (src, expr) = it
        src to transExpr(expr, graph.cRoot, graph)
    }.toMap()

    graph.bagRefs.clear()
    graph.bagRefs.putAll(transBagRefs)
}

fun flattenConstraints(graph: IrGraph) {
    logger.info { "Starting to flatten constraints." }
    val transCstrs: Map<Node, List<BoolExpr>> = graph.cstrs
            .filter { (ctx, _) -> !ctx.isAbstract && !graph.isNestedInAbstract(ctx) }
            .map { (ctx, cstrs) ->
                ctx to cstrs.map {
                    val expr = it
                    transExpr(expr, ctx, graph)
                }
            }.toMap()

    // replace original constraints by their transformed counterparts.
    graph.cstrs.clear()
    graph.cstrs.putAll(transCstrs)
}

private fun <T : AstElement> transExpr(expr: T, ctx: Node, graph: IrGraph): T {
    // create transformation operations for expression
    val treeOps = createTransOp(expr, ctx, graph)

    // apply transformation operations to expression and return
    return replaceAstElements(expr, treeOps)
            .orElseThrow { IllegalStateException("Expression needs to be replaced not deleted.") }
}


private fun createTransOp(elem: AstElement, ctx: Node, graph: IrGraph): List<TreeOp> {
    val transOps = mutableListOf<TreeOp>()
    visitAst(elem, param = VisitorParams(
            visitPath = { path ->
                val evalGraph = resolvePathInMixedGraph(path.elements.map { it.id }, ctx, graph)

                assert(evalGraph.edges.isNotEmpty())

                // Transform to union of paths, if more than one pathExpr is represented by pathExpr expression
                val paths = if (evalGraph.edges.first().isEmpty() && path.asString() == "this")
                    setOf(SetElement(path))
                else evalGraph.edges.map { SetElement(graph.transToPathExpr(it)) }.toSet()

                val newExpr = if (paths.size == 1)
                    paths.first()
                else {
                    union(paths)
                }
                transOps.add(TreeOp({ it is SetElement && it.path === path }, { Optional.of(newExpr) }))
            }
    ))
    return transOps
}


/**
 * Resolve pathExpr in dependency graph containing both inheritance relations and flattened nesting hierarchy.
 */
private fun resolvePathInMixedGraph(path: List<String>, ctx: Node, graph: IrGraph): EvalGraph {

    val allVisitedPaths: MutableSet<List<EvalGraphElem>> = mutableSetOf()
    val curPath: LinkedList<EvalGraphElem> = LinkedList()

    fun visitPath(curNode: Node, toBeVisited: List<String>, origPath: List<String>) {

        val next = toBeVisited.firstOrNull()

        if (next == null) {
            allVisitedPaths.add(curPath.toList())
        } else if (toBeVisited.size == origPath.size && next == origPath.first() && next != ThisIdentifier.id) {
            // search globally
            val nextEdge = graph.incoming[graph.cRoot]?.singleOrNull { it.type == Edge.EdgeType.P && graph.matchClaferName(it.src.name, next) }
                    ?: error("Could not resolve pathExpr ${origPath.joinToString(".")} globally")

            // if parent edge to root starts from an abstract node,
            // we need to resolve pathExpr from root to its concrete sub-clafers (which are not transitively nested in an abstract clafer)
            if (nextEdge.src.isAbstract) {
                val candidates = graph.getConcreteSubs(nextEdge.src).filter { !graph.isNestedInAbstract(it) }

                // if there is no concrete clafer for given clafer pathExpr expression points to an empty set
                if (candidates.isEmpty())
                    throw EmptySetException()

                candidates.forEach {
                    val pathFromRoot = graph.pathFromRoot(it)
                    curPath.addAll(pathFromRoot)
                    visitPath(it, toBeVisited.drop(1), origPath)

                    // drop all paths here, as in next iteration we will start from root node again
                    curPath.clear()
                }

            } else {
                curPath.addLast(EvalGraphElem(nextEdge, true))
                visitPath(nextEdge.src, toBeVisited.drop(1), origPath)
                curPath.pollLast()
            }

        } else if (next == ThisIdentifier.id) {
            assert(!curNode.isAbstract) { "Should not resolve pathExpr from abstract clafer." }
            visitPath(curNode, toBeVisited.drop(1), origPath)
            curPath.pollLast()
        } else if (next == ParentIdentfier.id) {
            val nextEdge = graph.outgoing[curNode]?.single { it.type == Edge.EdgeType.P }
                    ?: error("Could not resolve pathExpr")

            assert(!nextEdge.trg.isAbstract) { "Parent edge can never be abstract" }

            curPath.addLast(EvalGraphElem(nextEdge))
            visitPath(nextEdge.trg, toBeVisited.drop(1), origPath)
            curPath.pollLast()

        } else if (next == DrefIdentfier.id) {
            val nextEdges = graph.outgoing[curNode]?.filter { it.type == Edge.EdgeType.R && !graph.isNestedInAbstract(it.trg) }
                    ?: error("Could not resolve pathExpr.")
            assert(nextEdges.isNotEmpty())
            { "${curNode.name} needs to have at least one outgoing reference edge." }
            nextEdges.forEach {
                curPath.addLast(EvalGraphElem(it))
                visitPath(it.trg, toBeVisited.drop(1), origPath)
                curPath.pollLast()
            }

        } else {
            // locally nested?
            val nextEdge = graph.incoming[curNode]?.singleOrNull {
                it.type == Edge.EdgeType.P && it.src.name == next
            } ?: error("Could not resolve pathExpr.")

            // if source is abstract navigate over nesting edge to concrete sub clafers instead
            if (nextEdge.src.isAbstract) {
                val nestedCon = if (graph.getConcreteSubs(nextEdge.src).isNotEmpty())
                    graph.getConcreteSubs(nextEdge.src)
                else if (absMapping[nextEdge.src] != null)
                // if there is no concrete clafer try next to search for candidate in original mapping
                    graph.getConcreteSubs(absMapping[nextEdge.src]!!)
                else throw EmptySetException()

                val nextEdges = graph.incoming[curNode]?.filter { nestedCon.contains(it.src) }
                        ?: error("Could not resolve pathExpr")

                // if there is no concrete clafer for given clafer pathExpr expression points to an empty set
                if (nextEdges.isEmpty())
                    throw  EmptySetException()

                nextEdges.forEach {
                    curPath.addLast(EvalGraphElem(it, true))
                    visitPath(it.src, toBeVisited.drop(1), origPath)
                    curPath.pollLast()
                }
            } else {
                curPath.addLast(EvalGraphElem(nextEdge, true))
                visitPath(nextEdge.src, toBeVisited.drop(1), origPath)
                curPath.pollLast()
            }
        }

    }

    try {
        visitPath(ctx, path, path)
    } catch (e: EmptySetException) {
        val edgeEmptysetRoot = graph.outgoing[graph.emptyset]
                ?.singleOrNull { it.trg == graph.cRoot && it.type == Edge.EdgeType.P }
                ?: error("Initialization of graph failed. There is no special node representing an empty set.")

        return EvalGraph(path, ctx, setOf(listOf(EvalGraphElem(edgeEmptysetRoot, true))))
    }

    return EvalGraph(path, ctx, allVisitedPaths.toSet())
}

private class EmptySetException : Exception()


/**
 * Copy all outgoing reference edges of an abstract clafer to its concrete sub-clafer
 * and remove the original edges afterwards.
 */
fun pushDownOutgoingReferences(graph: IrGraph) {
    logger.info { "Pushing down outgoing references from abstract clafers to their concrete sub-clafers." }

    // copy all reference edges from abstract clafers to their concrete sub-clafers
    val outgoingReferences = graph.nodes.filter { it.isAbstract }
            .map { graph.outgoing[it]!!.filter { e -> e.type == Edge.EdgeType.R } }
            .flatten()

    for (edge in outgoingReferences) {
        val absSrc = edge.src
        val trg = edge.trg

        val concreteSubs = graph.getConcreteSubs(absSrc)

        // add reference edges from concrete to target node if concrete node does not already have a reference
        concreteSubs.filter { graph.getRefsFrom(it).isEmpty() }
                .forEach { graph.add(Edge(it, trg, Edge.EdgeType.R)) }

        // copy set/bag expressions
        fun copyExpr(map: MutableMap<Node, SetExpr>) {
            assert(map.contains(absSrc))
            val copy = deepCopy(map[absSrc]!!) as SetExpr

            // assign copy only if concrete clafer does not already have an expression assigned
            concreteSubs.filter { !map.contains(it) }.forEach { map[it] = copy }

            // remove copied set/bag expression for abstract clafer
            map.remove(absSrc)
        }

        when {
            graph.setRefs.contains(absSrc) -> copyExpr(graph.setRefs)
            graph.bagRefs.contains(absSrc) -> copyExpr(graph.bagRefs)
            else -> assert(graph.hasNumRef(absSrc) || graph.hasStringRef(absSrc)
            ) { "Node $absSrc must have reference edge pointing to a numeric or string node." }
        }

        // remove original edge
        graph.remove(edge)
    }
}

/**
 * Copy all reference edges pointing to an abstract clafer for each concrete sub-clafer.
 * The copy points at the concrete sub-clafers of the abstract clafer.
 * The original edge is removed afterwards from the dependency graph.
 *
 */
fun pushDownIncomingReferences(graph: IrGraph) {
    logger.info { "Pushing down incoming references from abstract clafers to their concrete sub-clafers." }

    // copy all reference edges pointing at abstract clafers such that their copies
    // point at the concrete sub-clafers of the abstract clafer
    val incomingRefs = graph.nodes
            .filter { it.isAbstract }
            .map { graph.incoming[it] ?: emptySet<Edge>() }
            .flatten()
            .filter { it.type == Edge.EdgeType.R }

    for (edge in incomingRefs) {
        val absTrg = edge.trg
        val concreteSubs = graph.getConcreteSubs(absTrg)

        concreteSubs.forEach {
            val copy = Edge(edge.src, it, Edge.EdgeType.R)
            graph.add(copy)
        }

        // remove the original reference edge from the dependency graph
        graph.remove(edge)
    }
}

fun flattenSubtree(n: Node, dg: IrGraph) {
    logger.trace { "Flattening structure of clafer $n." }

    // If clafer is abstract, there is nothing to do
    if (n.isAbstract)
        return

    // While super clafer is present, copy children of current super clafer and walk upwards through inheritance hierarchy
    val superClafers = dg.getSuperTransitive(n)

    // deep copy of children
    val clonedChildren = superClafers.map { dg.getNested(it) }.flatten().map { it.deepCopy(dg) }

    // copy nested constraints from abstract clafer to concrete clafer
    val curCstrs = dg.cstrs.getOrPut(n) { emptyList() }
    dg.cstrs[n] = curCstrs + superClafers.map { dg.cstrs[it] ?: listOf() }.flatten()

    // create edges representing parent relation from cloned nodes to node
    clonedChildren.forEach { dg.add(Edge(it, n, Edge.EdgeType.P)) }

    // flatten subtree recursively
    dg.getNested(n).forEach { flattenSubtree(it, dg) }
}

//private fun IrGraph.resolveSuperTransitive(node: Node): Set<Node> {
//
//
//    val ret: MutableSet<Node> = mutableSetOf()
//    var nextSuper = getSuperCand(node)
//    // break cycles in inheritance hierarchy
//    while (nextSuper != null && !ret.contains(nextSuper)) {
//        ret.add(nextSuper)
//        nextSuper = getSuperCand(nextSuper)
//    }
//    return ret
//}
//
//private fun IrGraph.getSuperCand(n: Node): Node? = outgoing.getOrDefault(n, mutableSetOf())
//        .filter { it.type == Edge.EdgeType.A }
//        .map { it.trg }
//        .let {
//            if (it.size > 1)
//                it.filter { isNestedInAbstract(it) }
//            else it
//        }.let { assert(it.size <= 1); it }
//        .firstOrNull()


private fun Node.deepCopy(dg: IrGraph): Node {
    logger.trace { "Making deep copy of node $this" }
    // create copy
    val copy = Node(this.name, this.isAbstract)

    // copy group and clafer multiplicity intervals
    dg.claferMult[copy] = dg.claferMult[this]!!
    dg.groupMult[copy] = dg.groupMult[this]!!

    // create copy of nested nodes recursively
    val copiedChildren = dg.getNested(this).map { it.deepCopy(dg) }

    // create nesting edges
    copiedChildren.forEach {
        dg.add(Edge(it, copy, Edge.EdgeType.P))
    }


    dg.incoming[this]?.filter { it.type == Edge.EdgeType.R }?.forEach {
        val edge = it

        // copy edge representing references to original node
        val copiedEdge = Edge(edge.src, copy, Edge.EdgeType.R)
        dg.add(copiedEdge)
    }


    // copy edges representing references from original node
    dg.getRefsFrom(this).forEach {
        dg.add(Edge(copy, it, Edge.EdgeType.R))
    }

    // copy set/bag expression or integer/double symbol referenced from original node
    if (dg.setRefs[this] != null)
        dg.setRefs[copy] = deepCopy(dg.setRefs[this]!!) as SetExpr

    if (dg.bagRefs[this] != null)
        dg.bagRefs[copy] = deepCopy(dg.bagRefs[this]!!) as SetExpr


    // copy edges representing inheritance relations from original node
    // only if original node is not transitively nested in super node
    // (to break cycles in nesting/inheritance hierarchy)
    if (dg.getSuper(this) != null && !dg.getNestedTransitive(dg.getSuper(this)!!).contains(this))
//    if (dg.getSuper(this) != null && !dg.getNestedTransitive(dg.getSuper(this)!!).contains(this))
        dg.add(Edge(copy, dg.getSuper(this)!!, Edge.EdgeType.A))

    //  copy edges representing inheritance relations to original node
    if (this.isAbstract) {
        absMapping[copy] = this

    }
    return copy
}

private val absMapping: MutableMap<Node, Node> = mutableMapOf()

fun flattenNumExprs(graph: IrGraph) =
        graph.cstrs.replaceAll { ctx, cstrs -> flattenNumExprInCstrs(ctx, cstrs, graph) }


private fun flattenNumExprInCstrs(ctx: Node, cstrs: List<BoolExpr>, graph: IrGraph): List<BoolExpr> {
    val newCstrs: MutableList<BoolExpr> = mutableListOf()
    for (cstr in cstrs) {
        val candidates: MutableList<List<PathCandidate>> = mutableListOf()

        visitAst(cstr, VisitorParams(visitNumVar = { numVar ->
            // retrieve available paths first
            val origPath = graph.retrieveEvalGraph(numVar.path, ctx)

            if (origPath.edges.flatten().map { setOf(it.edge.src, it.edge.trg) }.flatten().any { it.isAbstract }) {
                assert(origPath.edges.size == 1)
                { "Exactly one pathExpr must exist in eval graph for pathExpr expression ${numVar.path}" }

                val availPaths = resolvePathInMixedGraph(numVar.path.elements.map { it.id }, ctx, graph)
                val availCands = availPaths.edges.map { EvalGraph(availPaths.pathExpr, ctx, setOf(it)) }
                        .map { PathCandidate(numVar, origPath, it) }
                candidates.add(availCands)
            } else if (origPath.edges.size > 1) {
                // more than one path exists after flattening to Int node
                val availCands = origPath.edges.map { EvalGraph(origPath.pathExpr, ctx, setOf(it)) }
                        .map { PathCandidate(numVar, origPath, it) }
                candidates.add(availCands)
            }
        }))

        if (candidates.isEmpty()) {
            // no abstract paths found. Keep original constraint.
            newCstrs.add(cstr)
        } else {
            // generate a new constraint for every viable combination of paths
            val permutations: MutableList<List<PathCandidate>> = mutableListOf()
            generatePermutations(candidates, permutations, 0, emptyList())

            newCstrs.addAll(permutations.map { replacePaths(ctx, cstr, it, graph) })
        }
    }
    return newCstrs

}

private fun generatePermutations(candidates: List<List<PathCandidate>>,
                                 result: MutableList<List<PathCandidate>>,
                                 depth: Int, current: List<PathCandidate>) {
    if (depth == candidates.size) {
        result.add(current)
        return
    }

    for (i in 0 until candidates[depth].size) {
        generatePermutations(candidates, result, depth + 1, current + candidates[depth][i])
    }
}

private data class PathCandidate(val target: NumVar, val toBeReplaced: EvalGraph, val replacement: EvalGraph)

private fun replacePaths(ctx: Node, constraint: BoolExpr, permutation: List<PathCandidate>, graph: IrGraph): BoolExpr =
        replaceAstElements(constraint,
                permutation.map { pathCand ->
                    TreeOp(
                            predicate = {
                                it is NumVar && pathCand.target === it && graph.retrieveEvalGraph(it.path, ctx).edges == pathCand.toBeReplaced.edges
                            },
                            replacement = {
                                it as NumVar
                                assert(pathCand.replacement.edges.size == 1)
                                { "Expected to have exactly one viable replacement per pathExpr in one permutation." }

                                val lastEdge = pathCand.replacement.edges.first().last().edge
                                assert(lastEdge.type == Edge.EdgeType.R && lastEdge.trg.type != Node.NodeType.STD)
                                val newPath = graph.pathFromRootAsPath(pathCand.replacement.edges.first().last().edge.src)
                                Optional.of(NumVar(Path(newPath.elements + listOf(DrefIdentfier))))
                            })
                }
        ).get()


fun removeAbstractNodes(graph: IrGraph) {
    graph.nodes.toSet().filter { it.isAbstract }.forEach {
        val nestedTransitive = setOf(it) + graph.getNestedTransitive(it).toSet()
        nestedTransitive.forEach { n -> graph.remove(n) }
    }
}
