package ir

import ir.constraints.BoolExpr
import ir.constraints.prettyPrint
import mu.KotlinLogging

fun discardUnsupportedConstraints(graph: IrGraph) = ConstraintDiscarder(graph).discardCstrs()

class ConstraintDiscarder(private val graph: IrGraph) {

    private val logger = KotlinLogging.logger {}

    /**
     * Unsupported properties
     */
    private val discardableProps = listOf(
            AstAnalyzerStats::noStringSetElement,
            AstAnalyzerStats::noComplexMult,
            AstAnalyzerStats::noComplexMult,
            AstAnalyzerStats::noComplexDiv,
            AstAnalyzerStats::noIteSet,
            AstAnalyzerStats::noMax,
            AstAnalyzerStats::noMin,
            AstAnalyzerStats::noProduct,
            AstAnalyzerStats::noSum,
            AstAnalyzerStats::noComplexQuant,
            AstAnalyzerStats::noComplexDiv,
            AstAnalyzerStats::noModulo
    )

    /**
     * Discards unsupported constraints from graph.
     */
    fun discardCstrs() {
        graph.cstrs.replaceAll { ctx, ctrs ->
            ctrs.map { if (isDiscardable(it)) listOf() else listOf(it) }.flatten()
        }
    }


    private fun isDiscardable(cstr: BoolExpr): Boolean {

        // Discard constraint based on statistical properties
        val stats = analyzeElement(cstr)
        for (prop in discardableProps) {
            if (prop.get(stats) > 0) {
                logger.info { "Discarding constraint ${prettyPrint(cstr)} due to number of ${prop.name} greater than 1." }
                return true
            }
        }
        return false

    }

}