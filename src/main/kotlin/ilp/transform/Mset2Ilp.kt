package ilp.transform

import ilp.IlpDoubleVar
import ilp.IlpIntVar
import ilp.IlpModel
import ilp.optimize.MsetOptimizer
import mset.MVar
import mset.prettyPrint
import mset.transform.MContext
import mu.KotlinLogging

private val logger = KotlinLogging.logger {}


fun transformToIlp(ctx: MContext, withOptimization: Boolean = false): IlpContext {
    val ilpCtx = IlpContext()

    // Optimize Mset representation to eliminate redundant counting functions
    if (withOptimization) MsetOptimizer(ctx).doOptimize()

    // Create decision variables for variables of multisets
    logger.info { "Creating decision variables of multi sets." }
    ctx.mAll.map { it.vars.values }.flatten().forEach { ilpCtx.add(it) }

    // Create variables for numeric references
    logger.info { "Creating decision variables of numeric references." }
    ctx.intRefs().forEach { ilpCtx.addIntRef(it) }
    ctx.dblRefs().forEach { ilpCtx.addDblRef(it) }

    // Transform conditions to ILP
    logger.info { "Transforming ${ctx.conds.size} conditions to ILP." }
    ctx.conds.forEachIndexed { i, cond ->
        logger.debug { "Converting constraint ${prettyPrint(cond)}" }
        ilpCtx.ilp.newConstraint("c$i", MBoolExpr2Ilp(cond, ctx, ilpCtx).doTransform())
    }

    return ilpCtx
}


class IlpContext {

    fun add(mvar: MVar) {
        assert(!mvars.containsKey(mvar)) { "Cannot create variable in ILP for $mvar twice." }
        mvars[mvar] = ilp.newIntVar(0)
    }

    fun addIntRef(mvar: MVar) {
        assert(!intRefVars.containsKey(mvar)) { "Cannot create variable in ILP for $mvar twice." }
        intRefVars[mvar] = ilp.newIntVar()
    }

    fun addDblRef(mvar: MVar) {
        assert(!dblRefVars.containsKey(mvar)) { "Cannot create variable in ILP for $mvar twice." }
        dblRefVars[mvar] = ilp.newDoubleVar()
    }

    val ilp = IlpModel()
    val mvars: MutableMap<MVar, IlpIntVar> = mutableMapOf()
    val intRefVars: MutableMap<MVar, IlpIntVar> = mutableMapOf()
    val dblRefVars: MutableMap<MVar, IlpDoubleVar> = mutableMapOf()
}