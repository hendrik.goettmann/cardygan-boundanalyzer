package ilp.transform

import ilp.*
import mset.*
import mset.transform.MContext

class MBoolExpr2Ilp(private val expr: MBoolExpr, private val mCtx: MContext, private val ilpContext: IlpContext) {

    fun doTransform(): IlpBoolExpr = visit(expr)

    private fun visit(e: MBoolExpr): IlpBoolExpr = when (e) {
        is Not -> ilp.not(visit(e.element))
        is And -> visit(e.lhs) and visit(e.rhs)
        is Or -> visit(e.lhs) or visit(e.rhs)
        is Impl -> visit(e.lhs) impl visit(e.rhs)
        is Leq -> MNumExpr2Ilp(e.lhs, mCtx, ilpContext).doTransform() leq MNumExpr2Ilp(e.rhs, mCtx, ilpContext).doTransform()
        is Geq -> MNumExpr2Ilp(e.lhs, mCtx, ilpContext).doTransform() geq MNumExpr2Ilp(e.rhs, mCtx, ilpContext).doTransform()
        is Eq -> MNumExpr2Ilp(e.lhs, mCtx, ilpContext).doTransform() eq MNumExpr2Ilp(e.rhs, mCtx, ilpContext).doTransform()
    }
}