package ilp.transform

import ilp.*
import mset.*
import mset.transform.MContext
import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

class MNumExpr2Ilp(private val expr: MNumExpr, private val mCtx: MContext, private val ilpCtx: IlpContext) {

    fun doTransform(): IlpNumExpr = visit(expr)

    private fun visit(e: MNumExpr): IlpNumExpr = e.accept(NumExprToIlpVisitor(mCtx, ilpCtx))
}

private class NumExprToIlpVisitor(private val mCtx: MContext, private val ilpCtx: IlpContext) : NumExprVisitor<IlpNumExpr> {

    override fun visit(e: IteNumExpr): IlpNumExpr {
        val i = MBoolExpr2Ilp(e.i, mCtx, ilpCtx).doTransform()
        val t = e.t.accept(this)
        val el = e.e.accept(this)

        val tmpVar = ilpCtx.ilp.newDoubleVar()
        ilpCtx.ilp.newConstraint("iteNumExpr", (i impl (tmpVar eq t)) and (not(i) impl (tmpVar eq el)))
        return tmpVar
    }

    override fun visit(e: Mult): IlpNumExpr {
        val lhs = e.lhs.accept(this)
        val rhs = e.rhs.accept(this)
//        if (lhs !is IlpUnary)
//            logger.warn { "$lhs is no unary expression!" }
//        if (rhs !is IlpUnary)
//            logger.warn { "${rhs.prettyPrint()} is no unary expression!" }




        if (rhs is IlpSum && lhs is IlpParam) {
            val summands = flattenSum(rhs)
            assert(summands.all { it is IlpUnary })

            return IlpSum(summands.map { IlpMult(lhs, it as IlpUnary) })
        }


        return lhs as IlpUnary * rhs as IlpUnary
    }

    private fun flattenSum(expr: IlpNumExpr): List<IlpNumExpr> =
            if (expr is IlpSum)
                expr.summands.flatMap { flattenSum(it) }
            else
                listOf(expr)


    override fun visit(e: Add): IlpNumExpr {
        return e.lhs.accept(this) + e.rhs.accept(this)
    }

    override fun visit(e: Subtract): IlpNumExpr {
        return e.lhs.accept(this) - e.rhs.accept(this)
    }

    override fun visit(e: Neg): IlpNumExpr {
        return -e.e.accept(this)
    }


    override fun visit(e: IntLit): IlpNumExpr {
        return p(e.value)
    }

    override fun visit(e: DblLit): IlpNumExpr {
        return p(e.value)
    }

    override fun visit(e: Max): IlpNumExpr {

//        assertDecVarAvailable(e, ilpCtx)
        val lhs = e.lhs.accept(this)
        val rhs = e.rhs.accept(this)
        // create helping var
        val ret = ilpCtx.ilp.newIntVar()
        ilpCtx.ilp.newConstraint("countFuncMin_${ret.name}", ((lhs geq rhs) impl (ret eq lhs)) and (not(lhs geq rhs) impl (ret eq rhs)))
        return ret
    }

    override fun visit(e: Min): IlpNumExpr {
//        assertDecVarAvailable(e, ilpCtx)
        val lhs = e.lhs.accept(this)
        val rhs = e.rhs.accept(this)
        val ret = ilpCtx.ilp.newIntVar()
        ilpCtx.ilp.newConstraint("countFuncMin_${ret.name}", ((lhs leq rhs) impl (ret eq lhs)) and (not(lhs leq rhs) impl (ret eq rhs)))
        return ret
    }

    override fun visit(e: MVar): IlpNumExpr {
        assert(ilpCtx.mvars.filter { it.key == e }.size +
                ilpCtx.intRefVars.filter { it.key == e }.size +
                ilpCtx.dblRefVars.filter { it.key == e }.size == 1
        ) { "Cannot resolve variable $e (either it does not exist or it is not unique." }
        return when {
            ilpCtx.mvars.contains(e) -> ilpCtx.mvars[e]!!
            ilpCtx.intRefVars.contains(e) -> ilpCtx.intRefVars[e]!!
            ilpCtx.dblRefVars.contains(e) -> ilpCtx.dblRefVars[e]!!
            else -> error("Could not resolve variable.")
        }
    }


}

