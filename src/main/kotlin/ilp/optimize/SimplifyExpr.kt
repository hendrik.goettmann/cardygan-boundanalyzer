package ilp.optimize

import mset.*
import mset.transform.MContext
import java.lang.Exception

class SimplifyExpr(val ctx: MContext) : MOptOp {

    override fun doOptimize() {
        val newConds = ctx.conds.map { doSimplify(it) }
        ctx.conds.clear()
        ctx.conds.addAll(newConds)
    }

    private class NoMatchException : Exception()

    private fun doSimplify(e: MBoolExpr): MBoolExpr = when (e) {

        is And -> {
            val (lhs, rhs) = e.lhs to e.rhs

            val params = when {
                // a <= b && b <= a
                lhs is Leq && rhs is Leq -> listOf(lhs.lhs, rhs.rhs, lhs.rhs, rhs.lhs)

                // a >= b && b >= a
                lhs is Geq && rhs is Geq -> listOf(lhs.lhs, rhs.rhs, lhs.rhs, rhs.lhs)

                // a <= b && a >= b
                lhs is Leq && rhs is Geq -> listOf(lhs.lhs, rhs.lhs, lhs.rhs, rhs.rhs)

                // a >= b && a <= b
                lhs is Geq && rhs is Leq -> listOf(lhs.lhs, rhs.lhs, lhs.rhs, rhs.rhs)

                // no match
                else -> emptyList()

            }
            if (params.isNotEmpty()) {
                val (a1, a2, b1, b2) = params
                if (a1 == a2 && b1 == b2)
                    Eq(doSimplify(a1), doSimplify(b1))
                else
                    And(doSimplify(e.lhs), doSimplify(e.rhs))
            } else
                And(doSimplify(e.lhs), doSimplify(e.rhs))
        }
        is Or -> when {
            e.rhs is Geq && e.rhs.lhs is IntLit && e.rhs.rhs is IntLit
                    && e.rhs.lhs.value < e.rhs.rhs.value ->
                e.lhs
            e.rhs is Geq && e.rhs.lhs is DblLit && e.rhs.rhs is DblLit
                    && e.rhs.lhs.value < e.rhs.rhs.value ->
                e.lhs
            else -> Or(doSimplify(e.lhs), doSimplify(e.rhs))
        }
        is Impl -> when {
            e.lhs is Leq && e.lhs.lhs is IntLit && e.lhs.rhs is IntLit -> {
                val v1 = e.lhs.lhs
                val v2 = e.lhs.rhs
                assert(v1.value <= v2.value) { "Specicifation has no instance." }
                e.rhs
            }
            e.lhs is Geq && e.lhs.lhs is IntLit && e.lhs.rhs is IntLit -> {
                val v1 = e.lhs.lhs
                val v2 = e.lhs.rhs
                assert(v1.value >= v2.value) { "Specicifation has no instance." }
                e.rhs
            }
            e.rhs is Leq && e.rhs.lhs is IntLit && e.rhs.rhs is IntLit && e.rhs.lhs.value > e.rhs.rhs.value ->
                Not(e.lhs)
            e.rhs is Geq && e.rhs.lhs is IntLit && e.rhs.rhs is IntLit && e.rhs.lhs.value < e.rhs.rhs.value ->
                Not(e.lhs)
            e.rhs is Leq && e.rhs.lhs is DblLit && e.rhs.rhs is DblLit && e.rhs.lhs.value > e.rhs.rhs.value ->
                Not(e.lhs)
            e.rhs is Geq && e.rhs.lhs is DblLit && e.rhs.rhs is DblLit && e.rhs.lhs.value < e.rhs.rhs.value ->
                Not(e.lhs)
            else -> Impl(doSimplify(e.lhs), doSimplify(e.rhs))
        }
        is Not -> Not(doSimplify(e.element))
        is Leq -> Leq(doSimplify(e.lhs), doSimplify(e.rhs))
        is Geq -> Geq(doSimplify(e.lhs), doSimplify(e.rhs))
        is Eq -> Eq(doSimplify(e.lhs), doSimplify(e.rhs))
    }

    private fun doSimplify(e: MNumExpr): MNumExpr = when (e) {
        is MVar -> e
        is IteNumExpr -> IteNumExpr(doSimplify(e.i), doSimplify(e.t), doSimplify(e.e))
        is Mult -> {
            when {
                e.lhs is DblLit && e.lhs.value == (0).toDouble() -> IntLit(0)
                e.lhs is IntLit && e.lhs.value == 0 -> IntLit(0)
                e.lhs is DblLit && e.rhs is DblLit -> DblLit(e.lhs.value * e.rhs.value)
                e.lhs is IntLit && e.rhs is IntLit -> IntLit(e.lhs.value * e.rhs.value)
                e.lhs is IntLit && e.rhs is MVar && e.lhs.value == 1 -> e.rhs
                else -> e
            }
        }
        is Add -> Add(doSimplify(e.lhs), doSimplify(e.rhs))
        is Subtract -> Subtract(doSimplify(e.lhs), doSimplify(e.rhs))
        is Neg -> Neg(doSimplify(e.e))
        is Max -> Max(doSimplify(e.lhs), doSimplify(e.rhs))
        is Min -> Min(doSimplify(e.lhs), doSimplify(e.rhs))
        is IntLit -> e
        is DblLit -> e
    }
}