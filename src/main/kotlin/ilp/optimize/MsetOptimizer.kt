package ilp.optimize

import mset.*
import mset.transform.MContext
import mu.KotlinLogging

class MsetOptimizer(val ctx: MContext, val maxIterations: Int = 1) : MOptOp {

    private val logger = KotlinLogging.logger {}

    private var count = 0

    override fun doOptimize() {
        logger.info { "Performing optimization of multiset representation." }

        val condsBefore = ctx.conds.toList()

        // replace mVars by parameters
        //TODO

        // simplify conditions
        logger.info { "Simplify conditions." }
        SimplifyExpr(ctx).doOptimize()

        logger.info { "Remove redundancies in conditions." }
        removeRedundancy()


        // replace fixed eq param expression with param val
        ReplaceByParams(ctx).doOptimize()

        // replace local mVars by global mcVar
        ReplaceByGlobalMvarC(ctx).doOptimize()


        // remove redundant count functions, ignore referenced multi sets
        ctx.mAll.removeIf { cFunc ->
            !cFunc.vars.values.any { mVar -> ctx.conds.any { containsMvar(it, mVar) } }
                    && !ctx.mRefs.values.contains(cFunc) && cFunc != ctx.mC
        }

        // split ands
        val newConds = ctx.conds.map { if (it is And) splitAnds(it) else listOf(it) }.flatten()
        ctx.conds.clear()
        ctx.conds.addAll(newConds)

        ctx.conds.removeIf { it is Eq && it.lhs == it.rhs }

        removeRedundancy()

        if (condsBefore != ctx.conds && count < maxIterations) {
            count++
            doOptimize()
        }

    }

    private fun removeRedundancy() {
        // remove redundant equal expressions
        ctx.conds.removeIf { it is Eq && it.lhs == it.rhs }
        ctx.conds.removeIf { it is Leq && it.lhs == it.rhs }
        ctx.conds.removeIf { it is Geq && it.lhs == it.rhs }

        ctx.conds.removeIf {
            it is Impl && it.rhs is Leq
                    && it.rhs.lhs is IntLit && it.rhs.rhs is IntLit
                    && it.rhs.lhs.value <= it.rhs.rhs.value
        }

        // remove redundant inequalities
        ctx.conds.removeIf {
            it is Or && it.rhs is Geq && it.rhs.lhs is DblLit
                    && it.rhs.rhs is DblLit && it.rhs.lhs.value >= it.rhs.rhs.value
        }
        ctx.conds.removeIf {
            it is Or && it.rhs is Leq && it.rhs.lhs is DblLit
                    && it.rhs.rhs is DblLit && it.rhs.lhs.value <= it.rhs.rhs.value
        }
        ctx.conds.removeIf {
            it is Or && it.rhs is Geq && it.rhs.lhs is IntLit
                    && it.rhs.rhs is IntLit && it.rhs.lhs.value >= it.rhs.rhs.value
        }
        ctx.conds.removeIf {
            it is Or && it.rhs is Leq && it.rhs.lhs is IntLit
                    && it.rhs.rhs is IntLit && it.rhs.lhs.value <= it.rhs.rhs.value
        }


        ctx.conds.removeIf { it is Leq && it.lhs is IntLit && it.rhs is IntLit && it.lhs.value <= it.rhs.value }
        ctx.conds.removeIf { it is Geq && it.lhs is IntLit && it.rhs is IntLit && it.lhs.value >= it.rhs.value }
        ctx.conds.removeIf { it is Leq && it.lhs is DblLit && it.rhs is DblLit && it.lhs.value <= it.rhs.value }
        ctx.conds.removeIf { it is Geq && it.lhs is DblLit && it.rhs is DblLit && it.lhs.value >= it.rhs.value }
    }

    private fun splitAnds(e: And): List<MBoolExpr> {
        val ret = mutableListOf<MBoolExpr>()
        if (e.lhs is And)
            ret.addAll(splitAnds(e.lhs))
        else
            ret.add(e.lhs)
        if (e.rhs is And)
            ret.addAll(splitAnds(e.rhs))
        else
            ret.add(e.rhs)

        return ret
    }

    private fun containsMvar(e: MBoolExpr, mVar: MVar): Boolean = when (e) {
        is Not -> containsMvar(e.element, mVar)
        is And -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Or -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Impl -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Leq -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Geq -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Eq -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
    }

    private fun containsMvar(e: MNumExpr, mVar: MVar): Boolean = when (e) {
        is MVar -> e == mVar
        is IteNumExpr -> containsMvar(e.i, mVar) || containsMvar(e.t, mVar) || containsMvar(e.e, mVar)
        is Mult -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Add -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Subtract -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Neg -> containsMvar(e.e, mVar)
        is Max -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is Min -> containsMvar(e.lhs, mVar) || containsMvar(e.rhs, mVar)
        is IntLit -> false
        is DblLit -> false
    }
}