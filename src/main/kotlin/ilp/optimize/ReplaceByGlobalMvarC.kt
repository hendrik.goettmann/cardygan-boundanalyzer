package ilp.optimize

import mset.*
import mset.transform.MContext
import mu.KotlinLogging
import org.jgrapht.alg.TransitiveClosure
import org.jgrapht.graph.DefaultEdge
import org.jgrapht.graph.SimpleDirectedGraph

class ReplaceByGlobalMvarC(val ctx: MContext) : MOptOp {

    private val graph: SimpleDirectedGraph<MVar, DefaultEdge> = SimpleDirectedGraph(DefaultEdge::class.java)

    private val logger = KotlinLogging.logger {}

    override fun doOptimize() {
        logger.info { "Replace fixed global mC expressions with parameter values." }

        // collect candidates
        ctx.conds.forEach { doVisit(it) }

        // compute transitive closure
        TransitiveClosure.INSTANCE.closeSimpleDirectedGraph(graph)


        val newConds = ctx.conds
                // ignore cVar == cRef assignments
                .filter { e ->
                    !(e is Eq && e.lhs is MVar && e.rhs is MVar && listOf(e.lhs, e.rhs).all { inCvar(it) || inRef(it) })
                }
        ctx.conds.removeAll(newConds)
        ctx.conds.addAll(newConds.map { doReplace(it) })

    }

    private fun doVisit(e: MBoolExpr): Unit = when (e) {
        is And -> {
            doVisit(e.lhs)
            doVisit(e.rhs)
        }
        is Eq -> {
            if (e.lhs is MVar && e.rhs is MVar) {
                graph.addVertex(e.lhs)
                graph.addVertex(e.rhs)
                graph.addEdge(e.lhs, e.rhs)
                graph.addEdge(e.rhs, e.lhs)
            }
            Unit
        }
        else -> Unit
    }


    private fun doReplace(e: MBoolExpr): MBoolExpr = when (e) {
        is Not -> Not(doReplace(e.element))
        is And -> And(doReplace(e.lhs), doReplace(e.rhs))
        is Or -> Or(doReplace(e.lhs), doReplace(e.rhs))
        is Impl -> Impl(doReplace(e.lhs), doReplace(e.rhs))
        is Leq -> Leq(doReplace(e.lhs), doReplace(e.rhs))
        is Geq -> Geq(doReplace(e.lhs), doReplace(e.rhs))
        is Eq -> Eq(doReplace(e.lhs), doReplace(e.rhs))
    }

    private fun doReplace(e: MNumExpr): MNumExpr = when (e) {
        is MVar -> getReplacement(e)
        is IteNumExpr -> IteNumExpr(doReplace(e.i), doReplace(e.t), doReplace(e.e))
        is Mult -> Mult(e.lhs, doReplace(e.rhs))
        is Add -> Add(doReplace(e.lhs), doReplace(e.rhs))
        is Subtract -> Subtract(doReplace(e.lhs), doReplace(e.rhs))
        is Neg -> Neg(doReplace(e.e))
        is Max -> Max(doReplace(e.lhs), doReplace(e.rhs))
        is Min -> Min(doReplace(e.lhs), doReplace(e.rhs))
        is IntLit -> e
        is DblLit -> e
    }

    private fun inCvar(mvar: MVar): Boolean = ctx.mC.vars.values.contains(mvar)

    private fun inRef(mvar: MVar): Boolean = ctx.mRefs.map { it.value.vars.values }.flatten().contains(mvar)

    private fun getReplacement(mvar: MVar): MNumExpr = when {
        graph.containsVertex(mvar) -> {
            val ret = (listOf(mvar) + graph.outgoingEdgesOf(mvar)
                    .map { graph.getEdgeTarget(it) })
                    .sortedWith(
                            compareBy<MVar> { it.id }.thenComparator { v1, v2 ->
                                when {
                                    // v1 in cVar, v2 not in cVar
                                    inRef(v1) && !inRef(v2) -> 1
                                    // v1 not in cVar, v2 in cVar
                                    !inRef(v1) && inRef(v2) -> -1
                                    else -> 0
                                }
                            }.thenComparator { v1, v2 ->
                                when {
                                    // v1 in cVar, v2 not in cVar
                                    inCvar(v1) && !inCvar(v2) -> 1
                                    // v1 not in cVar, v2 in cVar
                                    !inCvar(v1) && inCvar(v2) -> -1
                                    else -> 0
                                }
                            }
                    )
            ret.first()

        }
        else -> mvar
    }


}