package ilp.optimize

import mset.*
import mset.transform.MContext
import mu.KotlinLogging
import org.jgrapht.alg.TransitiveClosure
import org.jgrapht.graph.DefaultEdge
import org.jgrapht.graph.DirectedAcyclicGraph

class ReplaceByParams(val ctx: MContext) : MOptOp {

    private val graph: DirectedAcyclicGraph<MVar, DefaultEdge> = DirectedAcyclicGraph(DefaultEdge::class.java)

    private val fixedParams: MutableMap<MVar, NumLiteral> = mutableMapOf()
    private val tmpConds: MutableSet<MBoolExpr> = mutableSetOf()

    private val logger = KotlinLogging.logger {}

    override fun doOptimize() {
        logger.info { "Replace fixed eq param expressions with parameter values." }

        // collect candidates
        logger.info { "Collect candidates that are fixed to literals." }
        ctx.conds.forEach { doVisitLiterals(it) }

        // build up graph connecting elements to fixed literals
        logger.info { "build up graph connecting elements to fixed literals." }
        ctx.conds.forEach { doVisit(it) }

        // compute transitive closure
//        logger.info { "Computing transitive closure of graph." }
//        TransitiveClosure.INSTANCE.closeDirectedAcyclicGraph(graph)

        logger.info { "Replace conditions." }
        val newConds = ctx.conds
                // ignore cVar == cRef assignments
                .filter { e ->
                    !(e is Eq && e.lhs is MVar && e.rhs is MVar && listOf(e.lhs, e.rhs).all { inCvar(it) || inRef(it) })
                }
        ctx.conds.removeAll(newConds)
        ctx.conds.addAll(newConds.map { doReplace(it) })

        // add additional conditions for mC and mRef
        ctx.conds.addAll(tmpConds)

    }

    private fun doVisitLiterals(e: MBoolExpr): Unit = when (e) {
        is And -> {
            doVisitLiterals(e.lhs)
            doVisitLiterals(e.rhs)
        }
        is Eq -> {
            if (e.lhs is NumLiteral && e.rhs is MVar) {
                assert(if (fixedParams[e.rhs] != null) fixedParams[e.rhs] == e.lhs else true)
                { "Problem has no solution ${e.rhs} would have value ${e.lhs} and ${fixedParams[e.rhs]} at the same time." }
                fixedParams[e.rhs] = e.lhs
            }
            if (e.rhs is NumLiteral && e.lhs is MVar) {
                assert(if (fixedParams[e.lhs] != null) fixedParams[e.lhs] == e.rhs else true)
                { "Problem has no solution ${e.lhs} would have value ${e.lhs} and ${fixedParams[e.lhs]} at the same time." }
                fixedParams[e.lhs] = e.rhs
            }
            Unit
        }
        else -> Unit
    }

    private fun doVisit(e: MBoolExpr): Unit = when (e) {
        is And -> {
            doVisit(e.lhs)
            doVisit(e.rhs)
        }
        is Eq -> {
            if (e.lhs is MVar && e.rhs is MVar) {
                val lhs = e.lhs
                val rhs = e.rhs
                graph.addVertex(lhs)
                graph.addVertex(rhs)

                if (graph.getAncestors(lhs).filter { fixedParams.contains(it) }.any() || fixedParams.contains(lhs))
                    graph.addEdge(rhs, lhs)
                else
                    graph.addEdge(lhs, rhs)
            }
            Unit
        }
        else -> Unit
    }


    private fun doReplace(e: MBoolExpr): MBoolExpr = when (e) {
        is Not -> Not(doReplace(e.element))
        is And -> And(doReplace(e.lhs), doReplace(e.rhs))
        is Or -> Or(doReplace(e.lhs), doReplace(e.rhs))
        is Impl -> Impl(doReplace(e.lhs), doReplace(e.rhs))
        is Leq -> Leq(doReplace(e.lhs), doReplace(e.rhs))
        is Geq -> Geq(doReplace(e.lhs), doReplace(e.rhs))
        is Eq -> Eq(doReplace(e.lhs), doReplace(e.rhs))
    }

    private fun doReplace(e: MNumExpr): MNumExpr = when (e) {
        is MVar -> getReplacement(e)
        is IteNumExpr -> IteNumExpr(doReplace(e.i), doReplace(e.t), doReplace(e.e))
        is Mult -> Mult(e.lhs, doReplace(e.rhs))
        is Add -> Add(doReplace(e.lhs), doReplace(e.rhs))
        is Subtract -> Subtract(doReplace(e.lhs), doReplace(e.rhs))
        is Neg -> Neg(doReplace(e.e))
        is Max -> Max(doReplace(e.lhs), doReplace(e.rhs))
        is Min -> Min(doReplace(e.lhs), doReplace(e.rhs))
        is IntLit -> e
        is DblLit -> e
    }

    private fun inCvar(mvar: MVar): Boolean = ctx.mC.vars.values.contains(mvar)

    private fun inRef(mvar: MVar): Boolean = ctx.mRefs.map { it.value.vars.values }.flatten().contains(mvar)

    private fun getReplacement(mvar: MVar): MNumExpr {

        val param = getFixedParam(mvar)
        return when {
            param != null -> {
                // add eq condition for mC and mRef such that information is not lost
                if (ctx.mC.vars.values.contains(mvar) || ctx.mRefs.values.map { it.vars.values }.flatten().contains(mvar))
                    tmpConds.add(mvar eq param)
                param
            }
            else -> mvar
        }
    }

    private fun getFixedParam(mvar: MVar): NumLiteral? =
            (if (graph.containsVertex(mvar))
                listOf(mvar) + graph.getAncestors(mvar)
            else listOf(mvar))
                    .filter { fixedParams.contains(it) }
                    .map { fixedParams[it] }
                    .toSet()
                    .let {
                        assert(it.size <= 1)
                        { "Variable is required to be equal to two different values at the same time. Unsatisfiable condition," };it
                    }.firstOrNull()


}