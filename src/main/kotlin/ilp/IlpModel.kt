package ilp

import org.cardygan.ilp.internal.expr.ExprPrettyPrinter

typealias IlpModel = org.cardygan.ilp.api.model.Model
typealias IlpBoolExpr = org.cardygan.ilp.api.model.bool.BoolExpr
typealias IlpNot = org.cardygan.ilp.api.model.bool.Not
typealias IlpAnd = org.cardygan.ilp.api.model.bool.And
typealias IlpOr = org.cardygan.ilp.api.model.bool.Or
typealias IlpImpl = org.cardygan.ilp.api.model.bool.Impl
typealias IlpLeq = org.cardygan.ilp.api.model.bool.Leq
typealias IlpGeq = org.cardygan.ilp.api.model.bool.Geq
typealias IlpEq = org.cardygan.ilp.api.model.bool.Eq
typealias IlpNumExpr = org.cardygan.ilp.api.model.ArithExpr
typealias DoubleParam = org.cardygan.ilp.api.model.DoubleParam
typealias IlpMult = org.cardygan.ilp.api.model.Mult
typealias IlpSum = org.cardygan.ilp.api.model.Sum
typealias IlpUnary = org.cardygan.ilp.api.model.ArithUnaryExpr
typealias IlpNeg = org.cardygan.ilp.api.model.Neg
typealias IlpIntVar = org.cardygan.ilp.api.model.IntVar
typealias IlpDoubleVar = org.cardygan.ilp.api.model.DoubleVar
typealias IlpParam = org.cardygan.ilp.api.model.Param


fun IlpBoolExpr.prettyPrint(): String = this.accept(ExprPrettyPrinter())

fun IlpNumExpr.prettyPrint(): String = this.accept(ExprPrettyPrinter())

fun p(e: Double): DoubleParam {
    return DoubleParam(e)
}

fun p(e: Int): DoubleParam {
    return DoubleParam(e.toDouble())
}

fun not(e: IlpBoolExpr): IlpNot {
    return IlpNot(e)
}

infix fun IlpBoolExpr.and(rhs: IlpBoolExpr): IlpAnd {
    return IlpAnd(listOf(this, rhs))
}

infix fun IlpBoolExpr.or(rhs: IlpBoolExpr): IlpOr {
    return IlpOr(listOf(this, rhs))
}

infix fun IlpBoolExpr.impl(rhs: IlpBoolExpr): IlpImpl {
    return IlpImpl(this, rhs)
}

infix fun IlpNumExpr.leq(rhs: IlpNumExpr): IlpLeq {
    return IlpLeq(this, rhs)
}

infix fun IlpNumExpr.geq(rhs: IlpNumExpr): IlpGeq {
    return IlpGeq(this, rhs)
}

infix fun IlpNumExpr.eq(rhs: IlpNumExpr): IlpEq {
    return IlpEq(this, rhs)
}

operator fun IlpNumExpr.unaryMinus(): IlpNeg {
    return IlpNeg(this)
}

operator fun IlpNumExpr.plus(rhs: IlpNumExpr): IlpSum {
    return IlpSum(listOf(this, rhs))
}

operator fun IlpNumExpr.minus(rhs: IlpNumExpr): IlpSum {
    return IlpSum(listOf(this, -rhs))
}

operator fun IlpUnary.times(rhs: IlpUnary): IlpMult {
    return IlpMult(this, rhs)
}