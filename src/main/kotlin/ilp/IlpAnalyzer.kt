package ilp

import ilp.transform.IlpContext
import ilp.transform.transformToIlp
import ir.IrGraph
import ir.Node
import mset.transform.MContext
import mset.transform.transformToMset
import mu.KotlinLogging
import org.cardygan.ilp.api.Result
import org.cardygan.ilp.api.model.ArithExpr
import org.cardygan.ilp.api.model.Model
import org.cardygan.ilp.api.model.Sum
import org.cardygan.ilp.api.solver.CplexSolver
import org.cardygan.ilp.api.solver.GurobiSolver
import org.cardygan.ilp.api.util.ExprDsl.eq
import org.cardygan.ilp.api.util.ExprDsl.sum
import org.cardygan.ilp.internal.expr.ExprPrettyPrinter
import org.omg.CORBA.TIMEOUT
import java.lang.RuntimeException
import java.util.*
import java.util.concurrent.TimeoutException


class IlpAnalyzer(val graph: IrGraph, private val solverType: SolverType = SolverType.GUROBI, val timeoutSeconds: Long = 300) {

    val mCtx = transformToMset(graph)
    val ilpCtx = transformToIlp(mCtx)

    private val cache = mutableMapOf<AnalysisType, MutableMap<AnalysisTarget, MutableMap<Node, AnalysisResult>>>()
    private val logger = KotlinLogging.logger {}

    enum class SolverType {
        GUROBI, CPLEX
    }

    data class Interval(val lb: BoundResult, val ub: BoundResult)


    data class AnalysisResult(val interval: Interval,
                              val stats: ModelStats,
                              val node: Node,
                              val target: AnalysisTarget,
                              val analysisType: AnalysisType,
                              val anomalyTypes: List<AnomalyType>)

    abstract class BoundResult
    data class Feasible(val value: Int) : BoundResult()
    class Unbounded : BoundResult()
    class Infeasible : BoundResult()

    data class ModelStats(val noVariables: Int, val noCstr: Int, val runtime: Long)

    enum class AnalysisType {
        SCOPE, BOUND
    }


    enum class AnalysisTarget {
        CLAFER_MULT, GROUP_MULT, REF
    }

    fun analyzeInterval(node: Node, target: AnalysisTarget) = analyze(node, target, AnalysisType.BOUND)

    fun analyzeScope(node: Node) = analyze(node, AnalysisTarget.CLAFER_MULT, AnalysisType.SCOPE)

    fun analyze(node: Node, target: AnalysisTarget, analysisType: AnalysisType): AnalysisResult {

        val cached = getFromCache(node, target, analysisType)

        if (cached.isPresent) {
            logger.info { "Previously computed $target for ${node.name}. Load result from cache" }
            return cached.get()
        }

        logger.info { "Analyzing $target on for clafer ${node.name}." }

//        val mCtx = transformToMset(graph)
//        val ilpCtx = transformToIlp(mCtx)

        val ilp = ilpCtx.ilp.copy()

        // fix parent constraint if bound analysis
        if (analysisType == AnalysisType.BOUND && target != AnalysisTarget.REF) {
            val fixedNode = when (target) {
                AnalysisTarget.CLAFER_MULT -> graph.getParent(node)
                AnalysisTarget.GROUP_MULT -> node
                else -> error("Should not be here.")
            }

            if (fixedNode != null) {
                logger.info { "Compute minimal scope for parent clafer ${fixedNode.name}." }
                val res = analyze(fixedNode, AnalysisTarget.CLAFER_MULT, AnalysisType.SCOPE)
                val (lb, ub) = res.interval
                // fixed node is infeasible
                if (lb is Infeasible) {
                    val stats = ModelStats(ilp.vars.size, ilp.constraints.size, res.stats.runtime)
                    logger.info { "Analysis of parent clafer yields infeasible result. Assuming child clafer to be infeasible as well => INCONSISTENT SPEC" }
                    return AnalysisResult(Interval(Infeasible(), Infeasible()), stats, node, target, analysisType, listOf(AnomalyType.INCONSISTENT_SPEC))
                }
                // fixed clafer has no instances
                if (lb is Feasible && ub is Feasible && lb.value == 0 && ub.value == 0)
                    logger.warn { "Scope of parent clafer ${fixedNode.name} is 0." }
                else {
                    val parentDecVar = ilpCtx.mvars[mCtx.mCVar(fixedNode)]!!

                    if (lb is Feasible && lb.value == 0 && ((ub is Feasible && ub.value >= 1) || ub is Unbounded)) {
                        ilp.newConstraint("fix_${fixedNode.name}", eq(parentDecVar, p(1.0)))
                    } else if (lb is Feasible) {
                        ilp.newConstraint("fix_${fixedNode.name}", eq(parentDecVar, p(lb.value.toDouble())))
                    }
                }
            }
        }

        val aCtx = AContext(node, mCtx, ilpCtx, target, analysisType)

        return putInCache(when (target) {
            AnalysisTarget.CLAFER_MULT -> solveClafer(aCtx)
            AnalysisTarget.GROUP_MULT -> solveGroupMult(aCtx)
            AnalysisTarget.REF -> solveRef(aCtx)
        }, node, target, analysisType)
    }

    private fun putInCache(res: AnalysisResult, node: Node, target: AnalysisTarget, analysisType: AnalysisType): AnalysisResult =
            cache.getOrPut(analysisType) { mutableMapOf() }.getOrPut(target) { mutableMapOf() }.getOrPut(node) { res }


    private fun getFromCache(node: Node, target: AnalysisTarget, analysisType: AnalysisType): Optional<AnalysisResult> =
            Optional.ofNullable(cache.get(analysisType)?.get(target)?.get(node))


    private fun solveRef(aCtx: AContext): AnalysisResult {
        val (node, mCtx, ilpCtx, _, _) = aCtx

        val decVar = when {
            mCtx.intRefs.contains(node) -> ilpCtx.intRefVars[mCtx.intRef(node)]!!
            mCtx.dblRefs.contains(node) -> ilpCtx.dblRefVars[mCtx.dblRef(node)]!!
            else -> error("Trying to analyse reference of $node, however there does no reference exist.")
        }

        return solveIlp(decVar, aCtx)
    }

    private fun solveGroupMult(aCtx: AContext): AnalysisResult {
        val (node, mCtx, ilpCtx, _, _) = aCtx

        val childDecVars = graph.getNested(node).map { ilpCtx.mvars[mCtx.mCVar(it)]!! }
        return solveIlp(sum(childDecVars), aCtx)
    }

    private fun solveClafer(aCtx: AContext): AnalysisResult {
        val (node, mCtx, ilpCtx, _, _) = aCtx
        val decVar = ilpCtx.mvars[mCtx.mCVar(node)]!!

        return solveIlp(decVar, aCtx)
    }

    private fun solveIlp(obj: ArithExpr, aCtx: AContext): AnalysisResult {
        val (node, _, ilpCtx, target, analysisType) = aCtx

        logger.info { "Running solver for objective function ${obj.accept(ExprPrettyPrinter())}" }
        val ilp = ilpCtx.ilp
        ilp.newObjective(false, obj)

        val resultLb = solveIlp(ilp)

        logger.info { "Computed result for lower bound after ${resultLb.statistics.duration} ms: feasible=${resultLb.statistics.status}" }


        // ub
        ilp.newObjective(true, obj)
        val resultUb = solveIlp(ilp)

        logger.info { "Computed result for upper bound after ${resultUb.statistics.duration} ms: feasible=${resultUb.statistics.status}" }

        val lb = when (resultLb.statistics.status) {
            Result.SolverStatus.OPTIMAL -> Feasible(resultLb.objVal.get().toInt())
            Result.SolverStatus.INFEASIBLE -> Infeasible()
            Result.SolverStatus.UNBOUNDED -> if (aCtx.target == AnalysisTarget.REF) Unbounded() else
                error("Solver status ${resultLb.statistics.status} should not occur for lower bound.")
            Result.SolverStatus.INF_OR_UNBD -> {
                if (aCtx.target == AnalysisTarget.REF) {
                    logger.info { "ILP solver found solution to be either infeasible or unbounded. In Case of Attribute: Unbounded" }
                    Unbounded()
//                    // create empty objective
//                    ilp.newObjective(false, Sum())
//                    val tmpRes = solveIlp(ilp, false)
//                    when {
//                        tmpRes.statistics.status == Result.SolverStatus.INFEASIBLE -> {
//                            logger.info { "ILP solver found solution to be infeasible." }
//                            Infeasible()
//                        }
//                        tmpRes.statistics.status == Result.SolverStatus.OPTIMAL -> {
//                            logger.info { "ILP solver found solution to be feasible." }
//                            Unbounded()
//                        }
//                        else -> error("Undefined status ${tmpRes.statistics.status} after rerun.")
//                    }
                } else error("Solver status ${resultLb.statistics.status} should not occur for lower bound.")
            }
            Result.SolverStatus.TIME_OUT -> throw SolverTimeoutException()
        }

        val ub = when (resultUb.statistics.status) {
            Result.SolverStatus.UNBOUNDED -> {
                logger.info { "Determined unbounded result for upper bound." }
                Unbounded()
            }
            Result.SolverStatus.INF_OR_UNBD -> {
                if (aCtx.target == AnalysisTarget.REF) {
                    logger.info { "ILP solver found solution to be either infeasible or unbounded. In Case of Attribute: Unbounded" }
                    Unbounded()
                } else {
                    logger.info { "ILP solver found solution to be either infeasible or unbounded. Rerun without presolve." }

                    // create empty objective
                    ilp.newObjective(true, Sum())
                    val tmpRes = solveIlp(ilp, false, 1)
                    when {
                        tmpRes.statistics.status == Result.SolverStatus.INFEASIBLE -> {
                            logger.info { "ILP solver found solution to be infeasible." }
                            Infeasible()
                        }
                        tmpRes.statistics.status == Result.SolverStatus.OPTIMAL -> {
                            logger.info { "ILP solver found solution to be feasible." }
                            Unbounded()
                        }
                        else -> error("Undefined status ${tmpRes.statistics.status} after rerun.")
                    }
                }
            }
            Result.SolverStatus.INFEASIBLE -> {
                logger.info { "ILP solver found NO solution to be feasible." }
                Infeasible()
            }
            Result.SolverStatus.OPTIMAL -> {
                logger.info { "ILP solver found feasible solution with objective value ${resultUb.objVal.get().toInt()}." }
                Feasible(resultUb.objVal.get().toInt())
            }
            Result.SolverStatus.TIME_OUT -> throw SolverTimeoutException()
            else -> error("Result for upper bound must not be null")
        }

        assert((lb is Feasible && (ub is Feasible || ub is Unbounded)) || (lb is Infeasible && ub is Infeasible))
        { "Unexpected combination of solver results lb=$lb ub=$ub." }


        val (interval, anomalyTypes) = determineAnomalyType(resultLb, resultUb, Interval(lb, ub), aCtx)
        logger.info { "Result of ${analysisType.name} analysis: lb=${interval.lb} ub=${interval.ub}" }

        logger.info { "Determined anomaly types: ${anomalyTypes.joinToString(", ")}" }

        val stats = ModelStats(ilp.vars.size, ilp.constraints.size, resultLb.statistics.duration + resultUb.statistics.duration)

        return AnalysisResult(interval, stats, node, target, analysisType, anomalyTypes)
    }

    fun solveIlp(ilp: Model, withPresolve: Boolean = true, solLimit: Int? = null) = ilp.solve(
            when (solverType) {
                SolverType.CPLEX -> {
                    val builder = CplexSolver.create().withPresolve(withPresolve).withTimeOut(timeoutSeconds)
                    if (solLimit != null)
                        builder.withFeasibleSolLimit(solLimit.toLong()).build()
                    else builder.build()
                }
                SolverType.GUROBI -> {
                    val builder = GurobiSolver.create().withPresolve(withPresolve).withTimeOut(timeoutSeconds)
                    if (solLimit != null)
                        builder.withFeasibleSolLimit(solLimit).build()
                    else builder.build()
                }
            })


    private fun determineAnomalyType(lbRes: Result, ubRes: Result, interval: Interval,
                                     aCtx: AContext): Pair<Interval, List<AnomalyType>> {
        val (node, _, _, target, analysisType) = aCtx


        // Determine anomaly type only if BOUND analysis is performed
        if (analysisType == AnalysisType.SCOPE)
            return Pair(interval, listOf(AnomalyType.NOT_CHECKED))

        val (lb, ub) = interval
        if (lb is Infeasible || ub is Infeasible)
            return Pair(interval, listOf(AnomalyType.INCONSISTENT_SPEC))


        // Anomalies at multiplicities
        return when (target) {
            AnalysisTarget.CLAFER_MULT -> determineAnomalyTypeCmult(interval, aCtx, lbRes, ubRes)
            AnalysisTarget.GROUP_MULT -> determineAnomalyTypeGmult(interval, aCtx, lbRes, ubRes)
            AnalysisTarget.REF -> determineAnomalyTypeRef(interval, node)
        }
    }

    private fun determineAnomalyTypeCmult(interval: Interval, aCtx: AContext,
                                          lbRes: Result, ubRes: Result): Pair<Interval, List<AnomalyType>> {
        val (node, mCtx, ilpCtx, _, _) = aCtx

        val ret = mutableListOf<AnomalyType>()
        val (lb, ub) = interval
        val parent = graph.getParent(node)
        val (parentLb, parentUb) = if (parent != null) {
            val parentDecVar = ilpCtx.mvars[mCtx.mCVar(parent)]
            val lbVal = lbRes.solutions[parentDecVar]!!.toInt()
            val ubVal = if (ub !is Unbounded) ubRes.solutions[parentDecVar]!!.toInt() else (-1)

            Pair(lbVal, ubVal)
        } else Pair(1, 1)

        val (specLb, specUb) = graph.cmult(node)

        val expLb = if (lb is Feasible) if (parentLb == 0) Feasible(0) else Feasible((lb.value / parentLb)) else error("should not be here")
        val expUb = when (ub) {
            is Feasible -> if (parentUb == 0) Feasible(0) else Feasible((ub.value / parentUb + ub.value % parentUb))
            is Unbounded -> Unbounded()
            else -> error("should not be here")
        }

        // Dead clafer
        if (expLb.value == 0 && expUb is Feasible && expUb.value == 0) {
            ret.add(AnomalyType.DEAD_CLAFER)
        }
        // Unbounded clafer
        if (ub is Unbounded) {
            ret.add(AnomalyType.UNBOUNDED_CLAFER)
        }

        // False lower bound
        if (specLb < expLb.value)
            ret.add(AnomalyType.FALSE_LB_CMULT)

        // False upper bound
        if (specUb != -1 && expUb is Feasible && expUb.value < specUb) {
            ret.add(AnomalyType.FALSE_UB_CMULT)
        }
        // Falsely unbounded
        if (ub is Feasible && specUb == -1) {
            ret.add(AnomalyType.FALSE_UNBOUNDED_CMULT)
        }

        if (ret.isEmpty())
            ret.add(AnomalyType.NONE)

        return Pair(Interval(expLb, expUb), ret)
    }

    private fun determineAnomalyTypeGmult(interval: Interval, aCtx: AContext, lbRes: Result, ubRes: Result): Pair<Interval, List<AnomalyType>> {
        val (node, mCtx, ilpCtx, _, _) = aCtx

        val ret = mutableListOf<AnomalyType>()
        val (lb, ub) = interval

        val nodeDecVar = ilpCtx.mvars[mCtx.mCVar(node)]
        val lbValNode = lbRes.solutions[nodeDecVar]!!.toInt()
        val ubValNode = if (ub !is Unbounded) ubRes.solutions[nodeDecVar]!!.toInt() else (-1)

        val (nodeLb, nodeUb) = Pair(lbValNode, ubValNode)

        val (specLb, specUb) = graph.gmult(node)

        val expLb = if (lb is Feasible) if (nodeLb == 0) Feasible(0) else Feasible((lb.value / nodeLb)) else error("should not be here")
        val expUb = when (ub) {
            is Feasible -> if (nodeUb == 0) Feasible(0) else Feasible((ub.value / nodeUb + ub.value % nodeUb))
            is Unbounded -> Unbounded()
            else -> error("should not be here")
        }

        val lbVal = lb.value
        // False lower bound
        if (nodeLb > 0 && specLb < lbVal / nodeLb) {
            if (graph.getNested(node).isNotEmpty()) ret.add(AnomalyType.FALSE_LB_GMULT)
        }
        // False upper bound
        if (nodeUb != 0 && ub is Feasible && specUb != -1 && ub.value / nodeUb + ub.value % nodeUb < specUb) {
            if (graph.getNested(node).isNotEmpty()) ret.add(AnomalyType.FALSE_UB_GMULT)
        }
        // Falsely unbounded
        if (ub is Feasible && specUb == -1) {
            if (graph.getNested(node).isNotEmpty()) ret.add(AnomalyType.FALSE_UNBOUNDED_GMULT)
        }

        if (ret.isEmpty())
            ret.add(AnomalyType.NONE)

        return Pair(Interval(expLb, expUb), ret)
    }

    private fun determineAnomalyTypeRef(interval: Interval,
                                        node: Node): Pair<Interval, List<AnomalyType>> {
        val ret = mutableListOf<AnomalyType>()
        val (lb, ub) = interval
        if (lb is Feasible)
            ret.add(AnomalyType.LOWER_BOUNDED_ATTR)
        if (ub is Feasible)
            ret.add(AnomalyType.UPPER_BOUNDED_ATTR)
        if (ub is Unbounded || lb is Unbounded)
            ret.add(AnomalyType.UNBOUNDED_ATTR)

        if (ret.isEmpty())
            ret.add(AnomalyType.NONE)

        return Pair(interval, ret)
    }

    enum class AnomalyType {
        NOT_CHECKED, NONE, INCONSISTENT_SPEC, DEAD_CLAFER, UNBOUNDED_CLAFER, FALSE_LB_CMULT, FALSE_UB_CMULT, FALSE_UNBOUNDED_CMULT,
        FALSE_LB_GMULT, FALSE_UB_GMULT, FALSE_UNBOUNDED_GMULT, UPPER_BOUNDED_ATTR, LOWER_BOUNDED_ATTR, UNBOUNDED_ATTR
    }


    class SolverTimeoutException : RuntimeException()


    /**
     * Objects of this class represent context objects of analysis
     */
    data class AContext(val node: Node, val mCtx: MContext, val ilpCtx: IlpContext, val target: AnalysisTarget, val analysisType: AnalysisType)

}