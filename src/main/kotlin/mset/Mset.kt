package mset


import ir.Node
import java.util.concurrent.atomic.AtomicLong


sealed class MElement {
    abstract fun <R> accept(visitor: MsetAstElementVisitor<R>): R
}


sealed class MBoolExpr : MElement() {
    abstract fun <R> accept(visitor: BoolExprVisitor<R>): R
}

sealed class MNumExpr : MElement() {
    abstract fun <R> accept(visitor: NumExprVisitor<R>): R
}

class CFunc(val vars: Map<Node, MVar>) {
    constructor(set: Set<Node>) : this(set.map { it to MVar(it) }.toMap())

    val set: Set<Node>
        get() = vars.keys

    fun getVar(node: Node) = vars.getValue(node)

    override fun toString(): String {
        return "(${vars.keys}, ${vars.map { "${it.key} -> ${prettyPrint(it.value)}" }.joinToString(", ")})"
    }
}

private val NEXT_ID = AtomicLong(0)

data class MVar(val id: Long, val element: Node) : MNumExpr() {

    constructor(path: Node) : this(NEXT_ID.getAndIncrement(), path)

    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return "${element.name}[$id]"
    }
}


data class Not(val element: MBoolExpr) : MBoolExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: BoolExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class And(val lhs: MBoolExpr, val rhs: MBoolExpr) : MBoolExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: BoolExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Or(val lhs: MBoolExpr, val rhs: MBoolExpr) : MBoolExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: BoolExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Impl(val lhs: MBoolExpr, val rhs: MBoolExpr) : MBoolExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: BoolExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Leq(val lhs: MNumExpr, val rhs: MNumExpr) : MBoolExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: BoolExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Geq(val lhs: MNumExpr, val rhs: MNumExpr) : MBoolExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: BoolExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Eq(val lhs: MNumExpr, val rhs: MNumExpr) : MBoolExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: BoolExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

fun not(element: MBoolExpr): Not {
    return Not(element)
}

fun and(conds: List<MBoolExpr>): MBoolExpr =
        conds.reduce { rem, next -> rem and next }

infix fun MBoolExpr.and(rhs: MBoolExpr): And {
    return And(this, rhs)
}

infix fun MBoolExpr.or(rhs: MBoolExpr): Or {
    return Or(this, rhs)
}

infix fun MBoolExpr.impl(rhs: MBoolExpr): Impl {
    return Impl(this, rhs)
}

infix fun MNumExpr.leq(rhs: MNumExpr): Leq {
    return Leq(this, rhs)
}

infix fun MNumExpr.geq(rhs: MNumExpr): Geq {
    return Geq(this, rhs)
}

infix fun MNumExpr.eq(rhs: MNumExpr): Eq {
    return Eq(this, rhs)
}

/*
 * DSL for numeric expression in multisets
 */


data class IteNumExpr(val i: MBoolExpr, val t: MNumExpr, val e: MNumExpr) : MNumExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Mult(val lhs: NumLiteral, val rhs: MNumExpr) : MNumExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Add(val lhs: MNumExpr, val rhs: MNumExpr) : MNumExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Subtract(val lhs: MNumExpr, val rhs: MNumExpr) : MNumExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Neg(val e: MNumExpr) : MNumExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class Max(val lhs: MNumExpr, val rhs: MNumExpr) : MNumExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }
}

data class Min(val lhs: MNumExpr, val rhs: MNumExpr) : MNumExpr() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }
}

sealed class NumLiteral : MNumExpr()
data class IntLit(val value: Int) : NumLiteral() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}

data class DblLit(val value: Double) : NumLiteral() {
    override fun <R> accept(visitor: MsetAstElementVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun <R> accept(visitor: NumExprVisitor<R>): R {
        return visitor.visit(this)
    }

    override fun toString(): String {
        return prettyPrint(this)
    }
}


fun card(cFunc: CFunc): MNumExpr {
    return cFunc.vars.values.reduce { sum: MNumExpr, next: MVar -> sum + next }
}

operator fun MNumExpr.unaryMinus(): Neg {
    return Neg(this)
}

operator fun MNumExpr.plus(rhs: MNumExpr): Add {
    return Add(this, rhs)
}

operator fun MNumExpr.minus(rhs: MNumExpr): Subtract {
    return Subtract(this, rhs)
}

operator fun NumLiteral.times(rhs: MNumExpr): Mult {
    return Mult(this, rhs)
}

interface MsetAstElementVisitor<out R> {
    fun visit(e: Not): R
    fun visit(e: And): R
    fun visit(e: Or): R
    fun visit(e: Impl): R
    fun visit(e: Leq): R
    fun visit(e: Geq): R
    fun visit(e: Eq): R
    fun visit(e: Mult): R
    fun visit(e: Add): R
    fun visit(e: Subtract): R
    fun visit(e: Neg): R
    fun visit(e: IntLit): R
    fun visit(e: DblLit): R
    fun visit(e: MVar): R
    fun visit(e: IteNumExpr): R
    fun visit(e: Max): R
    fun visit(e: Min): R
}

interface BoolExprVisitor<out R> {
    fun visit(e: Not): R
    fun visit(e: And): R
    fun visit(e: Or): R
    fun visit(e: Impl): R
    fun visit(e: Leq): R
    fun visit(e: Geq): R
    fun visit(e: Eq): R
}

interface NumExprVisitor<out R> {
    fun visit(e: Mult): R
    fun visit(e: Add): R
    fun visit(e: Subtract): R
    fun visit(e: Neg): R
    fun visit(e: IntLit): R
    fun visit(e: DblLit): R
    fun visit(e: MVar): R
    fun visit(e: IteNumExpr): R
    fun visit(e: Max): R
    fun visit(e: Min): R
}

fun prettyPrint(expr: MElement): String {
    return expr.accept(PrettyPrinter())
}

private class PrettyPrinter : MsetAstElementVisitor<String> {
    override fun visit(e: Not): String {
        return "!${e.element.accept(this)}"
    }

    override fun visit(e: And): String {
        return "(${e.lhs.accept(this)} && ${e.rhs.accept(this)})"
    }

    override fun visit(e: Or): String {
        return "(${e.lhs.accept(this)} || ${e.rhs.accept(this)})"
    }

    override fun visit(e: Impl): String {
        return "${e.lhs.accept(this)} => ${e.rhs.accept(this)}"
    }

    override fun visit(e: Leq): String {
        return "${e.lhs.accept(this)} <= ${e.rhs.accept(this)}"
    }

    override fun visit(e: Geq): String {
        return "${e.lhs.accept(this)} >= ${e.rhs.accept(this)}"
    }

    override fun visit(e: Eq): String {
        return "${e.lhs.accept(this)} == ${e.rhs.accept(this)}"
    }

    override fun visit(e: IteNumExpr): String {
        return "if (${e.i.accept(this)} then ${e.t.accept(this)} else ${e.e.accept(this)}"
    }

    override fun visit(e: Mult): String {
        return "${e.lhs.accept(this)} * ${e.rhs.accept(this)}"
    }

    override fun visit(e: Add): String {
        return "${e.lhs.accept(this)} + ${e.rhs.accept(this)}"
    }

    override fun visit(e: Subtract): String {
        return "${e.lhs.accept(this)} - ${e.rhs.accept(this)}"
    }

    override fun visit(e: Neg): String {
        return "-(${e.e.accept(this)})"
    }

    override fun visit(e: IntLit): String {
        return "${e.value}"
    }

    override fun visit(e: DblLit): String {
        return "${e.value}"
    }


    override fun visit(e: Max): String {
        return "max(${e.lhs.accept(this)}, ${e.rhs.accept(this)})"
    }

    override fun visit(e: Min): String {
        return "min(${e.lhs.accept(this)}, ${e.rhs.accept(this)})"
    }

    override fun visit(e: MVar): String {
        return e.toString()
    }

}