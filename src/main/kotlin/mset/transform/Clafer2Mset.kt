package mset.transform

import ilp.optimize.MsetOptimizer
import ir.IrGraph
import ir.Node
import ir.constraints.CstrNormalizer
import ir.constraints.NNFTrans
import ir.constraints.SetExpr
import mset.*


fun transformToMset(graph: IrGraph): MContext {
    // create M_c
    val mC = CFunc(graph.nodes)
    val ctx = MContext(mC)
    ctx.mAll.add(mC)

    // create conditions for nesting hierarchy
    createNestingConds(ctx, graph)

    // create conditions for set references and init msetRef
    createSetRefConds(ctx, graph)

    // create variables for integer or double references and init numRef
    createNumRefs(ctx, graph)

    // normalize constraints
    graph.cstrs.replaceAll { _, cstrs -> cstrs.map { CstrNormalizer(it).doTransform() } }

    // transform constraints to negation normal form
    graph.cstrs.replaceAll { _, cstrs -> cstrs.map { NNFTrans(it).doTransform() } }

    // check context dependency and transform constraints
    // TODO if optimization at later step does not work properly

    // transform constraints
    graph.cstrs.forEach { ctxNode, cstrs ->
        cstrs.forEachIndexed { index, expr ->
            //                    ctx.mCVar(ctxNode) geq IntLit(1) impl
            ctx.add(BoolExpr2Mset(expr, ctxNode, ctx, graph).doTransform())
        }
    }

    // TODO Do optimization

    return ctx
}


data class MContext(val mC: CFunc,
                    val mAll: MutableList<CFunc> = mutableListOf(),
                    val conds: MutableList<MBoolExpr> = mutableListOf(),
                    val mRefs: MutableMap<Node, CFunc> = mutableMapOf(),
                    val intRefs: MutableMap<Node, MVar> = mutableMapOf(),
                    val dblRefs: MutableMap<Node, MVar> = mutableMapOf()
) {

    fun mCVar(node: Node): MVar = mC.vars.getValue(node)

    fun add(cond: MBoolExpr): MBoolExpr {
        conds.add(cond)
        return cond
    }

    fun add(cFunc: CFunc): CFunc {
        mAll.add(cFunc)
        return cFunc
    }

    fun mRef(n: Node): CFunc = mRefs[n] ?:
    error("Could not resolve to multiset for node $n")

    fun intRef(n: Node): MVar = intRefs[n] ?: error("Could not resolve to variable for node $n")

    fun intRefs(): List<MVar> = intRefs.values.toList()

    fun dblRef(n: Node): MVar = dblRefs[n] ?: error("Could not resolve to variable for node $n")

    fun dblRefs(): List<MVar> = dblRefs.values.toList()

    fun addMRef(c: Node, mRef: CFunc) {
        mRefs[c] = mRef
    }

    fun addIntRef(n: Node, mVar: MVar) {
        intRefs[n] = mVar
    }

    fun addDblRef(n: Node, mVar: MVar) {
        dblRefs[n] = mVar
    }
}


fun createNestingConds(ctx: MContext, graph: IrGraph) {
    for (node in graph.nodes.filter { it.type == Node.NodeType.STD }) {
        assert(graph.claferMult.contains(node))

        val mC = ctx.mCVar(node)

        // add conditions for clafer multiplicities
        if (node == graph.cRoot) {
            ctx.add(mC eq IntLit(1))
        } else {
            val mP = ctx.mCVar(graph.getParent(node)!!)

            val (lb, ub) = graph.cmult(node)

            if (ub >= 0) {
                ctx.add((IntLit(lb) * mP leq mC) and (mC leq IntLit(ub) * mP))
            } else {
                ctx.add(IntLit(lb) * mP leq mC)
            }
        }

        // add conditions for group cardinality
        if (graph.getNested(node).isNotEmpty()) {
            val children = graph.getNested(node)

            val sum = children.map { ctx.mCVar(it) as MNumExpr }
                    .reduce { sum, next -> sum + next }

            assert(graph.groupMult.contains(node))

            val (lb, ub) = graph.gmult(node)

            if (ub >= 0) {
                ctx.add((IntLit(lb) * mC leq sum) and (sum leq IntLit(ub) * mC))
            } else {
                ctx.add(IntLit(lb) * mC leq sum)
            }
        }
    }
}

fun createSetRefConds(ctx: MContext, graph: IrGraph) {
    // TODO drop bag refs...
    graph.setRefs.forEach { c, setExpr -> createSetRefCond(c, setExpr, ctx, graph) }
}

fun createSetRefCond(c: Node, setExpr: SetExpr, ctx: MContext, graph: IrGraph) {
    val p = graph.getParent(c)!!

    val mC = ctx.mCVar(c)
    val mRef = SetExpr2Mset(setExpr, c, ctx, graph, true).doTransform()

    // b1 := |M_ref| \leq m_C(c) \leq m_C(c') * |M_ref| with c' <>-- c
    val lMax = graph.lamdaMax(p)
    val b1 = if (lMax != null)
        (mC leq IntLit(lMax) * card(mRef))
//        (card(mRef) leq mC) and (mC leq IntLit(lMax) * card(mRef))
    else
        card(mRef) leq mC

    ctx.add(b1)

    // b2 := for all c in supp(M_ref) : m_ref(c) \leq m_C(c)
    mRef.vars.map { (c, mVar) -> mVar leq ctx.mCVar(c) }.forEach { ctx.add(it) }

    ctx.addMRef(c, mRef)
}

fun createNumRefs(ctx: MContext, graph: IrGraph) {
    // create integer refs
    graph.nodes.filter { it.type == Node.NodeType.INT }
            .map { graph.getRefsTo(it) }
            .flatten()
            .forEach {
                ctx.addIntRef(it, MVar(it))
            }

    // create double refs
    graph.nodes.filter { it.type == Node.NodeType.DBL }
            .map { graph.getRefsTo(it) }
            .flatten()
            .forEach {
                ctx.addDblRef(it, MVar(it))
            }
}
