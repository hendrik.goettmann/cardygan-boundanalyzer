package mset.transform

import ir.IrGraph
import ir.Node
import ir.constraints.*
import mset.*

class SimpleQuant2Mset(private val expr: SimpleQuantifiedExpr, private val ctxNode: Node,
                       val ctx: MContext,
                       val graph: IrGraph) {

    fun doTransform(): MBoolExpr {
        val mP = SetExpr2Mset(expr.set, ctxNode, ctx, graph).doTransform()
        return when (expr) {
            is Lone -> card(mP) leq IntLit(1)
            is One -> card(mP) eq IntLit(1)
            is No -> card(mP) eq IntLit(0)
            is Some -> card(mP) geq IntLit(1)
        }
    }
}