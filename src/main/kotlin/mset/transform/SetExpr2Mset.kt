package mset.transform

import ir.IrGraph
import ir.Node
import ir.constraints.*
import mset.*
import mset.Max
import mset.Min


class SetExpr2Mset(val setExpr: SetExpr, private val ctxNode: Node, val ctx: MContext, val graph: IrGraph, private val inRef: Boolean = false) {

    fun doTransform(): CFunc = setExpr.accept(SetExprVisitor(inRef, ctxNode, ctx, graph))


    private class SetExprVisitor(val inRef: Boolean, val ctxNode: Node, val ctx: MContext, val graph: IrGraph) : AstVisitor<CFunc>() {

        override fun visit(element: AstElement): CFunc = when (element) {
            is Union -> {
                val mQ = element.lhs.accept(this)
                val mP = element.rhs.accept(this)

                val setQ = mQ.set
                val setP = mP.set
                val setR = setQ + setP
                val mR = CFunc(setR)

                for (c in mR.set) {
                    val varR = mR.vars.getValue(c)
                    val varQ = mQ.vars[c]
                    val varP = mP.vars[c]

                    if (varP != null && varQ != null) {
                        // approximation for lower bound:
                        // for all c in Q: max(count_Q(c), count_P(c)) <= count_R(c)
                        val varC = ctx.mCVar(c)

                        val lowerBound = Max(varQ, varP) leq varR
                        ctx.add(lowerBound)

                        // approximation for upper bound:
                        // for all c in Q: count_R(c) <= min(count_Q(c) + count_P(c), count_C(c))
                        val upperBound = varR leq Min(varQ + varP, varC)
                        ctx.add(upperBound)
                    } else if ((varQ != null && varP == null)) {
                        // if element c is  not in underlying set (P) but in underlying set (Q) of mset_Q, mset_R hast no elements of Q
                        val bound = varR eq varQ
                        ctx.add(bound)
                    } else if (varQ == null && varP != null) {
                        // if element c is  not in underlying set (Q) but in underlying set (P) of mset_P, mset_R hast no elements of P
                        val bound = varR eq varP
                        ctx.add(bound)
                    } else {
                        error("This should not occur since multiset mset_R was created from P and Q")
                    }
                }

                ctx.add(mR)
            }
            is Intersect -> {
                val mQ = element.lhs.accept(this)
                val mP = element.rhs.accept(this)

                val setQ = mQ.set
                val setP = mP.set
                val setR = setQ + setP
                val mR = CFunc(setR)

                for (c in mR.set) {
                    val varR = mR.vars.getValue(c)
                    val varQ = mQ.vars[c]
                    val varP = mP.vars[c]

                    if (varP != null && varQ != null) {
                        // approximation for lower bound:
                        // for all c in Q: max(0, count_Q(c) + count_P(c) - count_C(c)) <= count_R(c)
                        val count_C = ctx.mCVar(c)

                        val lowerBound = Max(IntLit(0), (varQ + varP) - count_C) leq varR
                        ctx.add(lowerBound)

                        // approximation for upper bound:
                        // for all c in Q: count_R(c) <= min(count_Q(c), count_P(c))

                        val upperBound = varR leq Min(varQ, varP)
                        ctx.add(upperBound)
                    } else if ((varQ != null && varP == null) || (varQ == null && varP != null)) {
                        // if element c is either not in underlying set (P) of mset_P or underlying set (Q) of mset_Q, mset_R hast no elements for c
                        val bound = varR eq IntLit(0)
                        ctx.add(bound)
                    } else {
                        error("This should not occur since multiset mset_R was created from P and Q")
                    }
                }

                ctx.add(mR)
            }
            is Removal -> {
                val mQ = element.lhs.accept(this)
                val mP = element.rhs.accept(this)

                val setQ = mQ.set
                val mR = CFunc(setQ)


                for (c in mR.set) {
                    assert(mQ.vars.containsKey(c)) { "Element be contained since we created msetR from msetQ." }
                    val varR = mR.vars.getValue(c)
                    val varQ = mQ.vars.getValue(c)
                    val varP = mP.vars[c]

                    if (varP != null) {
                        // approximation for lower bound:
                        // for all c in Q: max(0, count_Q(c) - count_P(c)) <= count_R(c)
                        val lowerBound = Max(IntLit(0), varQ - varP) leq varR
                        ctx.add(lowerBound)

                        // approximation for upper bound:
                        // for all c in Q: count_R(c) <= count_Q(c) - max(0, count_Q(c)+count_P(c)-count_C(c) )
                        val count_C = ctx.mCVar(c)
                        val upperBound = varR leq varQ - Max(IntLit(0), ((varQ + varP) - count_C))
                        ctx.add(upperBound)
                    } else {
                        // if element c is not in underlying set (P) of mset_P, mset_R just the number of instance of mset_Q
                        val bound = varR eq varQ
                        ctx.add(bound)
                    }
                }

                ctx.add(mR)
            }
            is SetElement -> PathExpr2Mset(element.path, ctxNode, ctx, graph, inRef).doTransform()

            is IteSet -> {
                TODO()
            }
            is StringSetElement -> TODO()

            else -> {
                error("Should be visited from a set expression.")
            }
        }
    }


}