package mset.transform

import ir.IrGraph
import ir.Node
import ir.constraints.*
import ir.constraints.Eq
import ir.constraints.Geq
import ir.constraints.Leq
import mset.*

class NumComp2Mset(private val expr: NumComp, private val ctxNode: Node,
                   val ctx: MContext,
                   val graph: IrGraph) {

    fun doTransform(): MBoolExpr {
        val lhs = NumExpr2Mset(expr.lhs, ctxNode, ctx, graph).doTransform()
        val rhs = NumExpr2Mset(expr.rhs, ctxNode, ctx, graph).doTransform()

        return when (expr) {
            is Eq -> lhs eq rhs
            is Neq -> not(lhs eq rhs)
            is Leq -> lhs leq rhs
            is Geq -> lhs geq rhs
            is Lt -> not(lhs geq rhs)
            is Gt -> not(lhs leq rhs)
        }
    }
}