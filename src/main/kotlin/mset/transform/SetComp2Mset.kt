package mset.transform

import ir.IrGraph
import ir.Node
import ir.constraints.*
import mset.*
import mset.And

class SetComp2Mset(private val expr: SetComp, private val ctxNode: Node,
                   val ctx: MContext,
                   val graph: IrGraph) {

    fun doTransform(): MBoolExpr = visit(expr)

    fun visit(elem: SetComp): MBoolExpr {
        val mP = SetExpr2Mset(elem.lhs, ctxNode, ctx, graph).doTransform()
        val mQ = SetExpr2Mset(elem.rhs, ctxNode, ctx, graph).doTransform()

        return when (elem) {
            is InSet -> {
                val conds: MutableList<MBoolExpr> = mutableListOf()

                for (c in mP.set) {
                    val varP = mP.getVar(c)
                    if (c !in mQ.set) {
                        conds.add(varP eq IntLit(0))
                    } else {
                        val varQ = mQ.getVar(c)
                        conds.add(varP leq varQ)
                    }
                }
                and(conds)
            }
            is EqSet -> And(visit(InSet(elem.lhs, elem.rhs)), visit(InSet(elem.rhs, elem.rhs)))
            is NotInSet -> {
//                val conds: MutableList<MBoolExpr> = mutableListOf()

//                for (c in mP.set) {
//                    val varP = mP.getVar(c)
//                    if (c !in mQ.set) {
//                        conds.add(not(varP eq IntLit(0)))
//                    } else {
//                        val varQ = mQ.getVar(c)
                        // m_P(c) = 0 => m_Q(c) != 0
//                        conds.add((varP eq IntLit(0)) impl not(varQ eq IntLit(0)))
                        // m_Q(c) = 0 => m_P(c) != 0
//                        conds.add((varQ eq IntLit(0)) impl not(varP eq IntLit(0)))
//                    }
//                }
                // TODO dummy rule. Discard not eq constraints!!
                and(listOf(ctx.mCVar(graph.cRoot) eq IntLit(1)))
            }
            is NotEqSet -> And(visit(NotInSet(elem.lhs, elem.rhs)), visit(NotInSet(elem.rhs, elem.rhs)))
        }

    }

}