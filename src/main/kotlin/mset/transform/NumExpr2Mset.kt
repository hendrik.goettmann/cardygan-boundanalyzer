package mset.transform

import ir.IrGraph
import ir.Node
import ir.constraints.*
import ir.constraints.Add
import ir.constraints.Neg
import ir.constraints.NumLiteral
import mset.*

class NumExpr2Mset(private val expr: NumExpr, private val ctxNode: Node,
                   val ctx: MContext,
                   val graph: IrGraph) {

    fun doTransform(): MNumExpr = visit(expr)

    private fun visit(elem: NumExpr): MNumExpr = when (elem) {
        is Neg -> -(visit(elem.e))
        is Multiply -> {
            val lhs = if (elem.lhs is NumLiteral)
                visit(elem.lhs) as mset.NumLiteral
            else
            //try to compute value, assumes that AST only contains NumLiterals
                DblLit(simplify(elem.lhs))
            lhs * visit(elem.rhs)
        }
        is Add -> visit(elem.lhs) + visit(elem.rhs)
        is Minus -> visit(elem.lhs) - visit(elem.rhs)
        is Card -> card(SetExpr2Mset(elem.set, ctxNode, ctx, graph).doTransform())
        is NumVar -> {
            val mV = PathExpr2Mset(elem.path, ctxNode, ctx, graph).doTransform()
            assert(mV.set.size == 1) { "Expecting a singleton multiset here." }
            mV.getVar(mV.set.first())
        }
        is IntVal -> IntLit(elem.v)
        is DoubleVal -> DblLit(elem.v)
        else -> error("$elem is not supported and should not occur in AST.")
        // TODO compute SUM,MAX,MIN for context-independent arguments
    }

    /**
     * Simplify MNumExpr if the AST only contains NumLiteral objects
     */
    private fun simplify(e: NumExpr): Double = when (e) {
        is Neg -> -simplify(e.e)
        is Multiply -> simplify(e.lhs) * simplify(e.rhs)
        is Div -> simplify(e.lhs) / simplify(e.rhs)
        is Add -> simplify(e.lhs) + simplify(e.rhs)
        is Minus -> simplify(e.lhs) - simplify(e.rhs)
        is Modulo -> simplify(e.lhs) % simplify(e.rhs)
        is IntVal -> e.v.toDouble()
        is DoubleVal -> e.v
        else -> error("$e is not supported and should not be contained in expression.")
    }

}