package mset.transform

import ir.IrGraph
import ir.Node
import ir.constraints.*
import mset.MBoolExpr
import mset.and
import mset.not
import mset.or

class BoolExpr2Mset(private val boolExpr: BoolExpr, private val ctxNode: Node,
                    val ctx: MContext,
                    val graph: IrGraph) {

    fun doTransform(): MBoolExpr = visit(boolExpr)

    private fun visit(elem: BoolExpr): MBoolExpr = when (elem) {
        is Not -> not(visit(elem.e))
        is And -> visit(elem.lhs) and visit(elem.rhs)
        is Or -> visit(elem.lhs) or visit(elem.rhs)
        is Eq -> NumComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is Neq -> NumComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is Leq -> NumComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is Geq -> NumComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is Lt -> NumComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is Gt -> NumComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is Lone -> SimpleQuant2Mset(elem, ctxNode, ctx, graph).doTransform()
        is One -> SimpleQuant2Mset(elem, ctxNode, ctx, graph).doTransform()
        is No -> SimpleQuant2Mset(elem, ctxNode, ctx, graph).doTransform()
        is Some -> SimpleQuant2Mset(elem, ctxNode, ctx, graph).doTransform()
        is EqSet -> SetComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is NotEqSet -> SetComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is InSet -> SetComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        is NotInSet -> SetComp2Mset(elem, ctxNode, ctx, graph).doTransform()
        else -> error("Element $elem should not be present")
    }

}