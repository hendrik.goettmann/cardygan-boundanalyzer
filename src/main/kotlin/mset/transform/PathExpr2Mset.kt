package mset.transform

import ir.*
import ir.constraints.DrefIdentfier
import ir.constraints.ParentIdentfier
import ir.constraints.Path
import ir.constraints.ThisIdentifier
import mset.*

class PathExpr2Mset(private val pathExpr: Path, private val ctxNode: Node,
                    val ctx: MContext,
                    val graph: IrGraph, private val inRef: Boolean = false) {

    fun doTransform(): CFunc {
        val path = pathExpr.asList()
        val evalGraph = graph.retrieveEvalGraph(path, ctxNode)
        val evalSteps = transEvalGraph(evalGraph)

        if (evalSteps.isEmpty() && path.size == 1 && path.first() == "this")
            return transThis(ctxNode)

        val numRef = evalSteps.last().edges.first().edge.type == Edge.EdgeType.R
                && evalSteps.last().edges.first().edge.trg.type != Node.NodeType.STD


        val ctxDep = pathExpr.elements.first() == ThisIdentifier

        fun visitPath(remPath: List<String>, remSteps: List<EvalStep>): CFunc {
            val nextElem = remPath.lastOrNull()
            val nextStep = remSteps.lastOrNull()

            if (nextElem != null && nextStep != null || (nextElem == ThisIdentifier.id && nextStep == null)) {
                return when (nextElem) {
                    ThisIdentifier.id -> transThis(ctxNode)
                    DrefIdentfier.id -> transDref(visitPath(remPath.dropLast(1), remSteps.dropLast(1)), nextStep!!, isDis(remPath), ctxDep)
                    ParentIdentfier.id -> transParent(visitPath(remPath.dropLast(1), remSteps.dropLast(1)), nextStep!!, isDis(remPath))
                    else -> if (remPath.size == 1)
                        transId(nextStep!!, inRef)
                    else transId(nextElem, visitPath(remPath.dropLast(1), remSteps.dropLast(1)), nextStep!!, isDis(remPath), ctxDep, numRef)
                }
            } else error("Path seems not to be well-formed. We should never be here!")
        }

        return visitPath(path, evalSteps)
    }

    /**
     * Checks if given path is potentially violating property that path is referencing disjunctive instance set.
     */
    fun isDis(path: List<String>): Boolean = path.dropLast(1).none { it == DrefIdentfier.id }

    private fun add(cond: MBoolExpr) {
        // (c_ctx > 0) => cond
        val varC = ctx.mCVar(ctxNode)
//        ctx.add(varC geq IntLit(1) impl cond)
        //TODO
        ctx.add(cond)
    }


    /**
     * Transform path element with this keyword
     */
    fun transThis(cCtx: Node): CFunc {
//        assert(step.edges.map { it.edge.src }.toSet().size == 1)
//        { "All edges must have same source node." }

//        val cCtx = step.edges.first().edge.src
        val mP = CFunc(setOf(cCtx))
        val varP = mP.getVar(cCtx)

        // b_l := m_p(c_ctx) = 1
//        add(varP eq IntLit(1))
        //TODO
        val varC = ctx.mCVar(ctxNode)
        ctx.add(varC geq IntLit(1) impl (varP eq IntLit(1)))

        // b_g := m_p(c_ctx) <= m_c(c_ctx)
//        add(varP leq ctx.mCVar(cCtx))

        return ctx.add(mP)
    }

    /**
     * Transform path element with global ID
     */
    fun transId(step: EvalStep, inRef: Boolean): CFunc {
        assert(step.edges.map { it.edge.src }.toSet().size == 1)
        { "All edges must have same source node." }

        val c = step.edges.first().edge.src
        val mP = CFunc(setOf(c))
        val varP = mP.getVar(c)

        val (lb, ub) = graph.cmult(c)


        if (!inRef)
        // b_l := l <= m_p(c) <= u
            if (ub >= 0)
                add(ctx.mCVar(c) geq IntLit(1) impl ((IntLit(lb) leq varP) and (varP leq IntLit(ub))))
            else
                add(ctx.mCVar(c) geq IntLit(1) impl (IntLit(lb) leq varP))
        else
            if (ub >= 0)
                add(varP leq IntLit(ub))

        // Create following condition only if path expression is not created within reference
        // In reference we do not want to bind mP to global mC.
        if (!inRef)
//         b_g := (m_C(c) >= 1) => (m_p(c) = m_C(c))
            add(ctx.mCVar(ctxNode) geq IntLit(1) impl (varP eq ctx.mCVar(c)))
//            add(ctx.mCVar(c) geq IntLit(1) impl (varP eq ctx.mCVar(c)))

        return ctx.add(mP)
    }

    /**
     * Transform sub path with ID in the general case
     */
    fun transId(id: String, mQ: CFunc, step: EvalStep, isDis: Boolean, ctxDep: Boolean, numRef: Boolean): CFunc =
            if (isDis) transIdDis(id, mQ, step, ctxDep, numRef) else transIdNonDis(id, mQ, step)


    /**
     * Transform sub path with ID in case of referenced disjunctive instance set
     */
    fun transIdDis(id: String, mQ: CFunc, step: EvalStep, ctxDep: Boolean, numRef: Boolean): CFunc {
        assert(step.edges.all { it.edge.type == Edge.EdgeType.P && it.isInverted }
                && step.edges.size == 1) { "Must contain exactly one nesting edge." }
        assert(mQ.set.size == 1) { "M_Q must have one element." }
        assert(step.edges.map { it.edge.trg }.toSet() == mQ.set) { "Eval step does not fit to M_Q" }

        val c = step.edges.first().edge.src
        val (lb, ub) = graph.cmult(c)
        val mP = CFunc(setOf(c))
        val varP = mP.getVar(c)
        val varQ = mQ.getVar(mQ.set.first())
        val varC = ctx.mCVar(c)

        // b_l := m_Q(c_q) >= 1 => (l * m_Q(c_q) <= m_P(c) <= u * m_Q(c_q))
        if (ub >= 0)
            add(varQ geq IntLit(1) impl ((IntLit(lb) * varQ leq varP) and (varP leq IntLit(ub) * varQ)))
        else
            add(varQ geq IntLit(1) impl (IntLit(lb) * varQ leq varP))

        if (ctxDep) {
            val lambdaMin = graph.lamdaMin(c)
            // b_g := varQ >= 1 => (m_P(c) <= m_C(c) - (lambda_min(c) - l * m_Q(c_q))
            add(varQ geq IntLit(1) impl (varP leq varC - (IntLit(lambdaMin) - IntLit(lb) * varQ)))
//            add(varP leq varC)
        } else {
            // b_g := varQ >= 1 => m_P(c) = m_C(c)
            add(varQ geq IntLit(1) impl (varP eq varC))
        }

        return ctx.add(mP)
    }

    /**
     * Transform sub path with ID in the non-disjunctive case
     */
    fun transIdNonDis(id: String, mQ: CFunc, step: EvalStep): CFunc {
        assert(step.edges.all { it.isInverted && it.edge.type == Edge.EdgeType.P })
        { "All edges must have same edge type." }
        assert(step.edges.map { it.edge.src.name }.toSet().size == 1)
        { "All edges must point to nodes with equal ids." }
        assert(step.edges.map { it.edge.trg }.toSet() == mQ.set) { "Eval step does not fit to M_Q" }

        val mP = CFunc(step.edges.map { it.edge.src }.toSet())

        val childrenLb = mP.set.map { graph.cmult(it).first }.toSet()
        assert(childrenLb.size == 1) { "All lower bound multiplicities of children are expected to be equal" }

        val childrenUb = mP.set.map { graph.cmult(it).second }.toSet()
        assert(childrenUb.size == 1) { "All lower bound multiplicities of children are expected to be equal" }

        val lb = childrenLb.first()
        val ub = childrenUb.first()


        if (ub < 0)
            add(card(mQ) geq IntLit(1) impl (IntLit(lb) leq card(mP)))
        else
            add(card(mQ) geq IntLit(1) impl (IntLit(lb) leq card(mP)) and (card(mP) leq IntLit(ub) * card(mQ)))


        for (c in mP.set) {
            val varP = mP.getVar(c)
            val varC = ctx.mCVar(c)

            // b_g := m_P(c) <= m_C(c)
            add(varP leq varC)
        }

        return ctx.add(mP)
    }


    /**
     * Transform sub path with parent keyword in the general case
     */
    fun transParent(mQ: CFunc, step: EvalStep, isDis: Boolean): CFunc =
            if (isDis) transParentDis(mQ, step) else transParentNonDis(mQ, step)


    /**
     * Transform sub path with parent keyword in case of referenced disjunctive instance set
     */
    fun transParentDis(mQ: CFunc, step: EvalStep): CFunc {
        assert(step.edges.all { it.edge.type == Edge.EdgeType.P && !it.isInverted }
                && step.edges.size == 1) { "Must contain exactly one nesting edge." }
        assert(mQ.set.size == 1) { "M_Q must have one element." }
        assert(step.edges.map { it.edge.src }.toSet() == mQ.set) { "Eval step does not fit to M_Q" }

        val c = step.edges.first().edge.trg
        val (lb, ub) = graph.cmult(c)
        val mP = CFunc(setOf(c))
        val varP = mP.getVar(c)
        val varQ = mQ.getVar(mQ.set.first())
        val varC = ctx.mCVar(c)

        // b_l := l * m_P(c) <= m_Q(c_q) <= m_P(c)
        if (ub >= 0)
            add(card(mQ) geq IntLit(1) impl (IntLit(lb) * varP leq varQ) and (varQ leq varP))
        else
            add(card(mQ) geq IntLit(1) impl (IntLit(lb) * varP leq varQ))

        // b_g := m_P(c) <= m_C(c)
        add(varP leq varC)

        return ctx.add(mP)
    }

    /**
     * Transform sub path with parent keyword in the non-disjunctive case
     */
    fun transParentNonDis(mQ: CFunc, step: EvalStep): CFunc {
        assert(step.edges.all { !it.isInverted && it.edge.type == Edge.EdgeType.P })
        { "All edges must have same edge type." }
        assert(step.edges.map { it.edge.src }.toSet() == mQ.set) { "Eval step does not fit to M_Q" }

        val mP = CFunc(step.edges.map { it.edge.trg }.toSet())

        for (e in step.edges) {
            val c = e.edge.src
            val cq = e.edge.trg
            val varP = mP.getVar(c)
            val varQ = mQ.getVar(cq)

            // b_l := 1 <= m_P(c) <= u * m_Q(c_q)
            add(card(mQ) geq IntLit(1) impl (IntLit(1) leq varP) and (varP leq varQ))
        }

        for (c in mP.set) {
            val varP = mP.getVar(c)
            val varC = ctx.mCVar(c)

            // b_g := m_P(c) <= m_C(c)
            add(varP leq varC)
        }

        return ctx.add(mP)
    }


    /**
     * Transform sub path with dref keyword in the general case
     */
    fun transDref(mQ: CFunc, step: EvalStep, isDis: Boolean, ctxDep: Boolean): CFunc =
            when {
                refsNumeric(step) -> transDrefNum(mQ, step)
                isDis -> transDrefDis(mQ, step, ctxDep)
                else -> transDrefNonDis(mQ, step)
            }

    private fun refsNumeric(step: EvalStep): Boolean =
            step.edges.first().edge.trg.type == Node.NodeType.DBL
                    || step.edges.first().edge.trg.type == Node.NodeType.INT

    /**
     * Transform sub path with dref in case of numeric variable
     */
    fun transDrefNum(mQ: CFunc, step: EvalStep): CFunc {
        assert(step.edges.size == 1) { "Eval graph should have exactly one edge after flattening." }
        val refEdge = step.edges.first().edge
        assert(refEdge.type == Edge.EdgeType.R &&
                (refEdge.trg.type == Node.NodeType.DBL) || refEdge.trg.type == Node.NodeType.INT)

        // make sure that path exists
        //TODO check
//        ctx.add(card(mQ) geq IntLit(1))

        val refNode = refEdge.src

        return if (refEdge.trg.type == Node.NodeType.INT)
            CFunc(mapOf(refNode to ctx.intRef(refNode)))
        else
            CFunc(mapOf(refNode to ctx.dblRef(refNode)))

    }


    /**
     * Transform sub path with dref keyword in case of referenced disjunctive instance set
     */
    fun transDrefDis(mQ: CFunc, step: EvalStep, ctxDep: Boolean): CFunc {
        assert(step.edges.map { it.edge.src }.toSet().size == 1) { "All edges must have same source node." }
        assert(step.edges.all { it.edge.type == Edge.EdgeType.R }) { "All edges must be reference edge." }
        assert(step.edges.map { it.edge.src }.toSet() == mQ.set) { "Eval step does not fit to M_Q" }
        assert(step.edges.map { it.edge.trg }.all { it.type == Node.NodeType.STD })
        { "No int, real or string edges must be referenced." }

        val cq = mQ.set.first()
        val cs = graph.getParent(cq)!!
        val mP = CFunc(step.edges.map { it.edge.trg }.toSet())

        if (ctxDep) {
            // |M_P| <= |msetRef(c_q)|
            add(card(mP) leq card(ctx.mRef(cq)))
        } else {
            // M_P = msetRef(c_q)
            return ctx.mRef(cq)
        }

        if (cs == graph.cRoot) {
            val varCq = ctx.mCVar(cq)

            // b_l := m_C(c_q) = |M_P|
            add(varCq eq card(mP))
        } else {
            val varCs = ctx.mCVar(cs)
            val varQq = mQ.getVar(cq)

            // ( m_Q(c_q) >= 1 ) => ( m_C(c_s) <= |M_P| <= m_Q(c_q) )
            add(varQq geq IntLit(1) impl (varCs leq card(mP)) and (card(mP) leq varQq))
        }

        return ctx.add(mP)
    }

    /**
     * Transform sub path with dref keyword in the non-disjunctive case
     */
    fun transDrefNonDis(mQ: CFunc, step: EvalStep): CFunc {
        assert(step.edges.all { it.edge.type == Edge.EdgeType.R }) { "All edges must be reference edge." }
        assert(step.edges.map { it.edge.src }.toSet() == mQ.set) { "Eval step does not fit to M_Q" }
        assert(step.edges.map { it.edge.trg }.all { it.type == Node.NodeType.STD })
        { "No int, real or string edges must be referenced." }

        val mP = CFunc(step.edges.map { it.edge.trg }.toSet())

        for (e in step.edges) {
            val c = e.edge.trg
            val cq = e.edge.src
            val varP = mP.getVar(c)
            val varQ = mQ.getVar(cq)

            // b_l := m_Q(c_q) >= 1 <= |M_P|
            add(varQ geq IntLit(1) impl (IntLit(1) leq card(mP)))

            val varRef = ctx.mRef(cq).getVar(c)
            // b_g := m_P(c) <= m_Ref(c)
            add(varP leq varRef)
        }
        // |M_P| <= |M_Q|
        add(card(mP) leq card(mQ))
        return ctx.add(mP)
    }

    /**
     * Transform eval graph (list of paths) to eval step (list of edges, each entry corresponds to a path.
     */
    fun transEvalGraph(evalGraph: EvalGraph): List<EvalStep> {
        val evalSteps = mutableListOf<MutableSet<EvalGraphElem>>()

        evalGraph.edges.forEach {
            it.forEachIndexed { i, elem ->
                val curStep = evalSteps.getOrNull(i)
                if (curStep == null)
                    evalSteps.add(i, mutableSetOf(elem))
                else
                    curStep.add(elem)
            }
        }
        return evalSteps.map { EvalStep(it) }
    }

    /**
     * Represents an evaluation step for traversing a path. Consists of the edges which are currently traversed.
     */
    data class EvalStep(val edges: Set<EvalGraphElem>)

}

