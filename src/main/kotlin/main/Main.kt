package main

import com.google.gson.*
import ilp.IlpAnalyzer
//import model.*
import org.apache.commons.cli.*
import java.io.File
import java.io.FileInputStream
import java.lang.reflect.Type
import org.cardygan.ilp.api.model.Model
import java.io.FileWriter


fun main(args: Array<String>) {
    TODO()
}
//    val options = Options()
//
//    val input = Option("i", "input", true, "input Clafer file pathExpr")
//    input.setRequired(true)
//    options.addOption(input)
//
//    val output = Option("o", "output", true, "output CSV file for statistics")
//    output.setRequired(false)
//    options.addOption(output)
//
//    val claferOption = Option("c", "clafer", true,
//            "pathExpr to clafer to be analyzed or ALL for all clafers")
//    claferOption.setRequired(false)
//    options.addOption(claferOption)
//
//    val kindOption = Option("k", "kind", true, "kind of analysis; either BOUND or SCOPE")
//    kindOption.setRequired(false)
//    options.addOption(kindOption)
//
//    val targetOption = Option("t", "target", true,
//            "target of analysis, available options: REF, GMULT, CMULT or ALL")
//    targetOption.setRequired(false)
//    options.addOption(targetOption)
//
//    val base = Option("b", "base", true, "Transform to base language and output to file.")
//    base.setRequired(false)
//    options.addOption(base)
//
//    val flatten = Option("f", "flatten", true, "Perform flattening and output to file.")
//    flatten.setRequired(false)
//    options.addOption(flatten)
//
//    val parser = DefaultParser()
//    val formatter = HelpFormatter()
//    val cmd: CommandLine
//
//    try {
//        cmd = parser.parse(options, args)
//    } catch (e: ParseException) {
//        System.out.println(e.message)
//        formatter.printHelp("boundanalyzer", options)
//
//        System.exit(1)
//        return
//    }
//
//    val module = if (File(cmd.getOptionValue("input")).exists())
//        Module.createFromStream(FileInputStream(File(cmd.getOptionValue("input"))))
//    else throw IllegalStateException("Could not read module.")
//
//    if (cmd.hasOption("base")) {
//        File(cmd.getOptionValue("base")).writeText(
//                prettyPrint(simplifyIds(dropUnsupportedConstraints(preCheckAst(module)))).trimIndent()
//        )
//    }
//
//    if (cmd.hasOption("flatten")) {
//        File(cmd.getOptionValue("flatten")).writeText(
//                prettyPrint(simplifyIds(flattenTreeLegacy(dropUnsupportedConstraints(preCheckAst(module))))).trimIndent()
//        )
//    }
//
//    // Do analysis
//    if (cmd.hasOption("clafer") && cmd.hasOption("kind") && cmd.hasOption("target") && cmd.hasOption("output")) {
//
//        val specName = File(cmd.getOptionValue("input")).nameWithoutExtension
//
//        val paths = when {
//            cmd.getOptionValue("clafer") == "ALL" -> {
//                val ret = mutableListOf<Path>()
//                visitAst(flattenTreeLegacy(dropUnsupportedConstraints(normalizeCstr(module))), {
//                    it is Module || (it is Clafer && !it.isAbstract)
//                }, VisitorParams(visitClafer = {
//                    if (!it.isAbstract) ret.add(simplifyIds(it.getAbsolutePath()))
//                }))
//                ret
//            }
//        // get first non abstract clafer
//            cmd.getOptionValue("clafer") == "ROOT" ->
//                listOf(module.clafers.filter { !it.isAbstract }.first().getAbsolutePath())
//            else -> listOf(module.context.getClaferByPath(Path(cmd.getOptionValue("clafer")))!!.getAbsolutePath())
//        }
//
//        val outputFilePath = cmd.getOptionValue("output")
//
//        val type = when {
//            cmd.getOptionValue("kind") == IlpAnalyzer.AnalysisType.SCOPE.name -> IlpAnalyzer.AnalysisType.SCOPE
//            cmd.getOptionValue("kind") == IlpAnalyzer.AnalysisType.BOUND.name -> IlpAnalyzer.AnalysisType.BOUND
//            else -> throw IllegalStateException("Unknown analysis type. Must either be SCOPE or BOUND.")
//        }
//
//        val targets = when {
//            cmd.getOptionValue("target") == "CMULT" -> listOf(IlpAnalyzer.AnalysisTarget.CLAFER_MULT)
//            cmd.getOptionValue("target") == "GMULT" -> listOf(IlpAnalyzer.AnalysisTarget.GROUP_MULT)
//            cmd.getOptionValue("target") == "REF" -> listOf(IlpAnalyzer.AnalysisTarget.GROUP_MULT)
//            cmd.getOptionValue("target") == "ALL" -> listOf(IlpAnalyzer.AnalysisTarget.CLAFER_MULT,
//                    IlpAnalyzer.AnalysisTarget.GROUP_MULT, IlpAnalyzer.AnalysisTarget.REF)
//            else -> throw IllegalStateException("Unknown target. Must either be CMULT, GMULT, or ALL")
//        }
//
//
//        val results = analyzeWorkflow(specName, module, paths, targets, type)
//
//        val gsonBuilder = GsonBuilder()
//        gsonBuilder.registerTypeAdapter(Path::class.java, PathSerializer())
//        gsonBuilder.registerTypeAdapter(Model::class.java, IlpModelSerializer())
//
//        val timestamp = (System.currentTimeMillis() / 1000).toString()
//
//        val file = File(outputFilePath)
//        val outputFile = if (file.isDirectory && file.canWrite())
//            "$outputFilePath/$specName-$timestamp.json"
//        else if (file.parentFile.canWrite()) {
//            if (file.exists()) {
//                file.renameTo(File(file.parentFile, "${file.nameWithoutExtension}_${timestamp}.${file.extension}"))
//            }
//            outputFilePath
//        } else
//            throw RuntimeException("Output file/dir not accessible.")
//
//        FileWriter(outputFile).use { writer ->
//            val gson = gsonBuilder.setPrettyPrinting().create()
//            gson.toJson(results, writer)
//        }
//
//    }
//}
//
//
//private class IlpModelSerializer : JsonSerializer<Model> {
//    override fun serialize(src: Model, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
//        val obj = JsonObject()
//        obj.addProperty("noDecVars", src.vars.size)
//        obj.addProperty("noConstraints", src.constraints.size)
//        return obj
//    }
//}
//
//private class PathSerializer : JsonSerializer<Path> {
//    override fun serialize(src: Path, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
//        return JsonPrimitive(prettyPrint(src))
//    }
//}
//
//fun analyzeWorkflow(specName: String,
//                    module: Module,
//                    paths: List<Path>,
//                    targets: List<IlpAnalyzer.AnalysisTarget>,
//                    analysisType: IlpAnalyzer.AnalysisType): Statistics {
//    val normalized = normalizeCstr(module)
//    val preChecked = preCheckAst(normalized)
//    val baseLang = dropUnsupportedConstraints(preChecked)
//    val flattened = flattenTreeLegacy(baseLang)
//    val unnested = unnestConstraints(flattened)
//    val analyzer = IlpAnalyzer(unnested)
//
//    val results = mutableListOf<IlpAnalyzer.AnalysisResult>()
//    for (path in paths) {
//        for (target in targets) {
//            val clafer = unnested.context.getClaferByPath(path)!!
//            // only run for ref if int or double reference is available
//            if (target == IlpAnalyzer.AnalysisTarget.CLAFER_MULT || target == IlpAnalyzer.AnalysisTarget.GROUP_MULT || (target == IlpAnalyzer.AnalysisTarget.REF &&
//                            clafer.ref.isPresent && (clafer.ref.get() is IntSetReference || clafer.ref.get() is DoubleSetReference)))
//                results.add(analyzer.analyze(path, target, analysisType))
//        }
//    }
//
//    return Statistics(
//            specName,
//            analyzeElement(module),
//            analyzeElement(baseLang),
//            analyzeElement(flattened),
//            analyzeElement(unnested),
//            results
//    )
//}
//
//data class Statistics(val specificationName: String,
//                      val originalStats: AstAnalyzerStats,
//                      val afterConstraintDropStats: AstAnalyzerStats,
//                      val afterFlattenigStats: AstAnalyzerStats,
//                      val afterUnnestingStats: AstAnalyzerStats,
//                      val analysisResults: List<IlpAnalyzer.AnalysisResult>
//)