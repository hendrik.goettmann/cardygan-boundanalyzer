package main

import ilp.IlpAnalyzer
import ir.constraints.Path
import ir.IrGraph

/**
 * BoundAnalyzer is used for analysis of bounds of
 * cardinality intervals in Clafer models.
 */
class BoundAnalyzer {

    /**
     * Analyzes (global) scope of given clafer.
     *
     * @param path pathExpr to clafer to be analyzed.
     * @param graph      the Clafer model which
     * contains the `clafer`.
     * @return the scope of the given clafer
     *
     * Throws IllegalArgumentException if clafer is not contained in Clafer model.
     */
    fun analyzeScope(path: Path, graph: IrGraph): IlpAnalyzer.Interval {
        try {
            graph.resolvePath(path.asList(), graph.cRoot).single()
        } catch (e: Exception) {
            throw IllegalArgumentException("Could not resolve path $path in clafer specification.")
        }

        TODO()
//        val stats = analyzeWorkflow("",
//                graph,
//                listOf(path),
//                listOf(IlpAnalyzer.AnalysisTarget.CLAFER_MULT),
//                IlpAnalyzer.AnalysisType.SCOPE
//        )
//        assert(stats.analysisResults.size == 1, { "Must contain exactly one result." })
//
//        return stats.analysisResults[0].interval
    }

    /**
     * Analyzes local bounds of interval of given clafer.
     *
     * @param path pathExpr to clafer to be analyzed.
     * @param target the target interval to be analyzed.
     * @param graph      the Clafer model which
     * contains the `clafer`.
     * @return the analysis results for the given interval.
     *
     * Throws IllegalArgumentException if clafer is not contained in Clafer model.
     */
    fun analyzeBounds(path: Path, target: IlpAnalyzer.AnalysisTarget, graph: IrGraph): IlpAnalyzer.AnalysisResult {
//        module.context.getClaferByPath(pathExpr)

        TODO()
//        val stats = analyzeWorkflow("",
//                graph,
//                listOf(path),
//                listOf(target),
//                IlpAnalyzer.AnalysisType.BOUND
//        )
//        assert(stats.analysisResults.size == 1, { "Must contain exactly one result." })
//
//        return stats.analysisResults[0]
    }

}