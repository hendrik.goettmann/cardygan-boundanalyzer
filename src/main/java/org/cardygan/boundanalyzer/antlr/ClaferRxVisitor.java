// Generated from ClaferRx.g4 by ANTLR 4.6
package org.cardygan.boundanalyzer.antlr;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ClaferRxParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ClaferRxVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#module}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModule(ClaferRxParser.ModuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#topLevelElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTopLevelElement(ClaferRxParser.TopLevelElementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement(ClaferRxParser.ElementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#clafer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClafer(ClaferRxParser.ClaferContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#abstr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAbstr(ClaferRxParser.AbstrContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#parent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParent(ClaferRxParser.ParentContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#reference}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReference(ClaferRxParser.ReferenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#setRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetRef(ClaferRxParser.SetRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#stringSetRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringSetRef(ClaferRxParser.StringSetRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#stringBagRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringBagRef(ClaferRxParser.StringBagRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#intSetRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntSetRef(ClaferRxParser.IntSetRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#doubleSetRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoubleSetRef(ClaferRxParser.DoubleSetRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#bagRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBagRef(ClaferRxParser.BagRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#intBagRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntBagRef(ClaferRxParser.IntBagRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#doubleBagRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoubleBagRef(ClaferRxParser.DoubleBagRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#childElements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChildElements(ClaferRxParser.ChildElementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#empty}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmpty(ClaferRxParser.EmptyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#giMult}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGiMult(ClaferRxParser.GiMultContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#fiMult}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFiMult(ClaferRxParser.FiMultContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#mult}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMult(ClaferRxParser.MultContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#lb}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLb(ClaferRxParser.LbContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#ub}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUb(ClaferRxParser.UbContext ctx);
	/**
	 * Visit a parse tree produced by the {@code minObj}
	 * labeled alternative in {@link ClaferRxParser#objective}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinObj(ClaferRxParser.MinObjContext ctx);
	/**
	 * Visit a parse tree produced by the {@code maxObj}
	 * labeled alternative in {@link ClaferRxParser#objective}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaxObj(ClaferRxParser.MaxObjContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#assertion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssertion(ClaferRxParser.AssertionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstraint(ClaferRxParser.ConstraintContext ctx);
	/**
	 * Visit a parse tree produced by the {@code quantified}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQuantified(ClaferRxParser.QuantifiedContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Not}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(ClaferRxParser.NotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Impl}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImpl(ClaferRxParser.ImplContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(ClaferRxParser.OrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setComparison}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetComparison(ClaferRxParser.SetComparisonContext ctx);
	/**
	 * Visit a parse tree produced by the {@code simpleQuantified}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleQuantified(ClaferRxParser.SimpleQuantifiedContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolBrackets}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolBrackets(ClaferRxParser.BoolBracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code And}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(ClaferRxParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Rel}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRel(ClaferRxParser.RelContext ctx);
	/**
	 * Visit a parse tree produced by the {@code XOr}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitXOr(ClaferRxParser.XOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code iteBool}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIteBool(ClaferRxParser.IteBoolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BiImpl}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBiImpl(ClaferRxParser.BiImplContext ctx);
	/**
	 * Visit a parse tree produced by the {@code loneSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoneSimpleQuant(ClaferRxParser.LoneSimpleQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oneSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOneSimpleQuant(ClaferRxParser.OneSimpleQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code someSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSomeSimpleQuant(ClaferRxParser.SomeSimpleQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoSimpleQuant(ClaferRxParser.NoSimpleQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code allDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllDisjQuant(ClaferRxParser.AllDisjQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code allQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllQuant(ClaferRxParser.AllQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oneDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOneDisjQuant(ClaferRxParser.OneDisjQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oneQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOneQuant(ClaferRxParser.OneQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code someDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSomeDisjQuant(ClaferRxParser.SomeDisjQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code someQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSomeQuant(ClaferRxParser.SomeQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoDisjQuant(ClaferRxParser.NoDisjQuantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoQuant(ClaferRxParser.NoQuantContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#localDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLocalDecl(ClaferRxParser.LocalDeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ge}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGe(ClaferRxParser.GeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code gt}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGt(ClaferRxParser.GtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code le}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLe(ClaferRxParser.LeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lt}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLt(ClaferRxParser.LtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eq}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEq(ClaferRxParser.EqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code neq}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeq(ClaferRxParser.NeqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setEq}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetEq(ClaferRxParser.SetEqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setUnEq}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetUnEq(ClaferRxParser.SetUnEqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setIn}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetIn(ClaferRxParser.SetInContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setNotIn}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetNotIn(ClaferRxParser.SetNotInContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringElement}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringElement(ClaferRxParser.StringElementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setIntersec}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetIntersec(ClaferRxParser.SetIntersecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code iteSet}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIteSet(ClaferRxParser.IteSetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setBrackets}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetBrackets(ClaferRxParser.SetBracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code join}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin(ClaferRxParser.JoinContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setUnion}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetUnion(ClaferRxParser.SetUnionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setDiff}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetDiff(ClaferRxParser.SetDiffContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#setElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetElement(ClaferRxParser.SetElementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#path}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPath(ClaferRxParser.PathContext ctx);
	/**
	 * Visit a parse tree produced by the {@code add}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(ClaferRxParser.AddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParentheses(ClaferRxParser.ParenthesesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code product}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProduct(ClaferRxParser.ProductContext ctx);
	/**
	 * Visit a parse tree produced by the {@code mod}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMod(ClaferRxParser.ModContext ctx);
	/**
	 * Visit a parse tree produced by the {@code max}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMax(ClaferRxParser.MaxContext ctx);
	/**
	 * Visit a parse tree produced by the {@code iteNumeric}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIteNumeric(ClaferRxParser.IteNumericContext ctx);
	/**
	 * Visit a parse tree produced by the {@code var}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar(ClaferRxParser.VarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code sum}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSum(ClaferRxParser.SumContext ctx);
	/**
	 * Visit a parse tree produced by the {@code substr}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubstr(ClaferRxParser.SubstrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code realLit}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealLit(ClaferRxParser.RealLitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code div}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiv(ClaferRxParser.DivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code neg}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeg(ClaferRxParser.NegContext ctx);
	/**
	 * Visit a parse tree produced by the {@code min}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMin(ClaferRxParser.MinContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intLit}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntLit(ClaferRxParser.IntLitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setCard}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetCard(ClaferRxParser.SetCardContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiply}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiply(ClaferRxParser.MultiplyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#numVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumVar(ClaferRxParser.NumVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(ClaferRxParser.IdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#claferName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClaferName(ClaferRxParser.ClaferNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#localIdentifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLocalIdentifier(ClaferRxParser.LocalIdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(ClaferRxParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#drefClafer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrefClafer(ClaferRxParser.DrefClaferContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#thisClafer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThisClafer(ClaferRxParser.ThisClaferContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#parentClafer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParentClafer(ClaferRxParser.ParentClaferContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#realSet}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealSet(ClaferRxParser.RealSetContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#stringSet}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringSet(ClaferRxParser.StringSetContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#intSet}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntSet(ClaferRxParser.IntSetContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#realLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealLiteral(ClaferRxParser.RealLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#intLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntLiteral(ClaferRxParser.IntLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link ClaferRxParser#stringExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringExpr(ClaferRxParser.StringExprContext ctx);
}