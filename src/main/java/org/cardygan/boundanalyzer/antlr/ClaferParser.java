package org.cardygan.boundanalyzer.antlr;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class ClaferParser {
    public static ParseTree parseFromFile(File file) throws IOException {
        return parseFromString(ResourceUtil.transformStreamToString(new FileInputStream(file)));
    }

    public static ParseTree parseFromInputStream(InputStream file) throws IOException {
        return parseFromString(ResourceUtil.transformStreamToString(file));
    }

    public static ParseTree parseFromString(String compiledClaferModel, List<ANTLRErrorListener> listeners) {
        ClaferRxLexer lexer = new ClaferRxLexer(new ANTLRInputStream(compiledClaferModel));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ClaferRxParser parser = new ClaferRxParser(tokens);

        for (ANTLRErrorListener listener : listeners) {
            parser.addErrorListener(listener);
        }
        ParseTree tree = parser.module();
        return tree;
    }

    public static ParseTree parseFromString(String compiledClaferModel) {
        return parseFromString(compiledClaferModel, Collections.emptyList());

    }

}