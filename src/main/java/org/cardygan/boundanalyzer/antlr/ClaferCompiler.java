package org.cardygan.boundanalyzer.antlr;


import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ClaferCompiler {

    private static final String CLAFER_COMPILER_PATH = "CLAFER_COMPILER_PATH";


    public enum OutputType {
        CLAFER, ALLOY, JS
    }

    public static String compile(String claferModel) {
        return compile(claferModel, OutputType.CLAFER);
    }

    public static String compile(String claferModel, OutputType outputType) {
        String compilerPath = System.getenv(CLAFER_COMPILER_PATH);
        ;
        if (compilerPath == null) {
            throw new IllegalStateException("Could not read Clafer compiler from pathExpr. Environment variable " + CLAFER_COMPILER_PATH + " not set.");
        }


        File tempFile = null;
        try {
            tempFile = File.createTempFile("prefix-", ".cfr");
            tempFile.deleteOnExit();

            try (PrintWriter out = new PrintWriter(tempFile)) {
                out.println(claferModel);
            }

            String fileName = tempFile.toString();

            long startCompilerProcess = 0;
            long endCompilerProcess = 0;

            String fileSuffix = ".des.cfr";
            try {
                startCompilerProcess = System.nanoTime();
                String command = null;
                switch (outputType) {
                    case ALLOY:
                        command = compilerPath + " -k -m alloy --no-stats " + fileName;
                        fileSuffix = ".als";
                        break;
                    case CLAFER:
                        command = compilerPath + " -k -m clafer " + fileName;
                        fileSuffix = ".des.cfr";
                        break;
                    case JS:
                        command = compilerPath + " -k -m choco " + fileName;
                        fileSuffix = ".js";
                        break;
                }
                Process compilerProcess = Runtime.getRuntime().exec(command);
                compilerProcess.waitFor();
                endCompilerProcess = System.nanoTime();

                if (compilerProcess.exitValue() != 0) {
                    System.out.println("Clafer compilation error: make sure your model is correct. Aborting...");
                    System.exit(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Abnormal Clafer compiler termination. Aborting...");
                System.exit(1);
            }

            long runtimeCompiler = (endCompilerProcess - startCompilerProcess) / 1000000;

            int extPos = fileName.lastIndexOf(".");
            if (extPos != -1) {
                fileName = fileName.substring(0, extPos) + fileSuffix;
            }

            // FOR DEBUGGING
            byte[] encoded = Files.readAllBytes(Paths.get(fileName));
            String tmp = new String(encoded, Charset.defaultCharset());
//            System.out.println(tmp);

            return tmp;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException("Compiler terminated unexpectedly.");
    }
}
