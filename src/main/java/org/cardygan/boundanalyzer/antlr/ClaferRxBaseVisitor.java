// Generated from ClaferRx.g4 by ANTLR 4.6
package org.cardygan.boundanalyzer.antlr;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

/**
 * This class provides an empty implementation of {@link ClaferRxVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class ClaferRxBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements ClaferRxVisitor<T> {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitModule(ClaferRxParser.ModuleContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitTopLevelElement(ClaferRxParser.TopLevelElementContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitElement(ClaferRxParser.ElementContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitClafer(ClaferRxParser.ClaferContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitAbstr(ClaferRxParser.AbstrContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitParent(ClaferRxParser.ParentContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitReference(ClaferRxParser.ReferenceContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetRef(ClaferRxParser.SetRefContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitStringSetRef(ClaferRxParser.StringSetRefContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitStringBagRef(ClaferRxParser.StringBagRefContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIntSetRef(ClaferRxParser.IntSetRefContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitDoubleSetRef(ClaferRxParser.DoubleSetRefContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitBagRef(ClaferRxParser.BagRefContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIntBagRef(ClaferRxParser.IntBagRefContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitDoubleBagRef(ClaferRxParser.DoubleBagRefContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitChildElements(ClaferRxParser.ChildElementsContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitEmpty(ClaferRxParser.EmptyContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitGiMult(ClaferRxParser.GiMultContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitFiMult(ClaferRxParser.FiMultContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitMult(ClaferRxParser.MultContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitLb(ClaferRxParser.LbContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitUb(ClaferRxParser.UbContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitMinObj(ClaferRxParser.MinObjContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitMaxObj(ClaferRxParser.MaxObjContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitAssertion(ClaferRxParser.AssertionContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitConstraint(ClaferRxParser.ConstraintContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitQuantified(ClaferRxParser.QuantifiedContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitNot(ClaferRxParser.NotContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitImpl(ClaferRxParser.ImplContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitOr(ClaferRxParser.OrContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetComparison(ClaferRxParser.SetComparisonContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSimpleQuantified(ClaferRxParser.SimpleQuantifiedContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitBoolBrackets(ClaferRxParser.BoolBracketsContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitAnd(ClaferRxParser.AndContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitRel(ClaferRxParser.RelContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitXOr(ClaferRxParser.XOrContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIteBool(ClaferRxParser.IteBoolContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitBiImpl(ClaferRxParser.BiImplContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitLoneSimpleQuant(ClaferRxParser.LoneSimpleQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitOneSimpleQuant(ClaferRxParser.OneSimpleQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSomeSimpleQuant(ClaferRxParser.SomeSimpleQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitNoSimpleQuant(ClaferRxParser.NoSimpleQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitAllDisjQuant(ClaferRxParser.AllDisjQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitAllQuant(ClaferRxParser.AllQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitOneDisjQuant(ClaferRxParser.OneDisjQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitOneQuant(ClaferRxParser.OneQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSomeDisjQuant(ClaferRxParser.SomeDisjQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSomeQuant(ClaferRxParser.SomeQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitNoDisjQuant(ClaferRxParser.NoDisjQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitNoQuant(ClaferRxParser.NoQuantContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitLocalDecl(ClaferRxParser.LocalDeclContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitGe(ClaferRxParser.GeContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitGt(ClaferRxParser.GtContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitLe(ClaferRxParser.LeContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitLt(ClaferRxParser.LtContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitEq(ClaferRxParser.EqContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitNeq(ClaferRxParser.NeqContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetEq(ClaferRxParser.SetEqContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetUnEq(ClaferRxParser.SetUnEqContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetIn(ClaferRxParser.SetInContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetNotIn(ClaferRxParser.SetNotInContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitStringElement(ClaferRxParser.StringElementContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetIntersec(ClaferRxParser.SetIntersecContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIteSet(ClaferRxParser.IteSetContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetBrackets(ClaferRxParser.SetBracketsContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitJoin(ClaferRxParser.JoinContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetUnion(ClaferRxParser.SetUnionContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetDiff(ClaferRxParser.SetDiffContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetElement(ClaferRxParser.SetElementContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitPath(ClaferRxParser.PathContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitAdd(ClaferRxParser.AddContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitParentheses(ClaferRxParser.ParenthesesContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitProduct(ClaferRxParser.ProductContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitMod(ClaferRxParser.ModContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitMax(ClaferRxParser.MaxContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIteNumeric(ClaferRxParser.IteNumericContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitVar(ClaferRxParser.VarContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSum(ClaferRxParser.SumContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSubstr(ClaferRxParser.SubstrContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitRealLit(ClaferRxParser.RealLitContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitDiv(ClaferRxParser.DivContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitNeg(ClaferRxParser.NegContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitMin(ClaferRxParser.MinContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIntLit(ClaferRxParser.IntLitContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitSetCard(ClaferRxParser.SetCardContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitMultiply(ClaferRxParser.MultiplyContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitNumVar(ClaferRxParser.NumVarContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIdentifier(ClaferRxParser.IdentifierContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitClaferName(ClaferRxParser.ClaferNameContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitLocalIdentifier(ClaferRxParser.LocalIdentifierContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitName(ClaferRxParser.NameContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitDrefClafer(ClaferRxParser.DrefClaferContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitThisClafer(ClaferRxParser.ThisClaferContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitParentClafer(ClaferRxParser.ParentClaferContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitRealSet(ClaferRxParser.RealSetContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitStringSet(ClaferRxParser.StringSetContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIntSet(ClaferRxParser.IntSetContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitRealLiteral(ClaferRxParser.RealLiteralContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitIntLiteral(ClaferRxParser.IntLiteralContext ctx) { return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code mCtx}.</p>
	 */
	@Override public T visitStringExpr(ClaferRxParser.StringExprContext ctx) { return visitChildren(ctx); }
}