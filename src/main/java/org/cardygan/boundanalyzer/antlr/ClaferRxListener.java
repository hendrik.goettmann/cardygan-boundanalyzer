// Generated from ClaferRx.g4 by ANTLR 4.6
package org.cardygan.boundanalyzer.antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ClaferRxParser}.
 */
public interface ClaferRxListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#module}.
	 * @param ctx the parse tree
	 */
	void enterModule(ClaferRxParser.ModuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#module}.
	 * @param ctx the parse tree
	 */
	void exitModule(ClaferRxParser.ModuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#topLevelElement}.
	 * @param ctx the parse tree
	 */
	void enterTopLevelElement(ClaferRxParser.TopLevelElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#topLevelElement}.
	 * @param ctx the parse tree
	 */
	void exitTopLevelElement(ClaferRxParser.TopLevelElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#element}.
	 * @param ctx the parse tree
	 */
	void enterElement(ClaferRxParser.ElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#element}.
	 * @param ctx the parse tree
	 */
	void exitElement(ClaferRxParser.ElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#clafer}.
	 * @param ctx the parse tree
	 */
	void enterClafer(ClaferRxParser.ClaferContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#clafer}.
	 * @param ctx the parse tree
	 */
	void exitClafer(ClaferRxParser.ClaferContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#abstr}.
	 * @param ctx the parse tree
	 */
	void enterAbstr(ClaferRxParser.AbstrContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#abstr}.
	 * @param ctx the parse tree
	 */
	void exitAbstr(ClaferRxParser.AbstrContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#parent}.
	 * @param ctx the parse tree
	 */
	void enterParent(ClaferRxParser.ParentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#parent}.
	 * @param ctx the parse tree
	 */
	void exitParent(ClaferRxParser.ParentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#reference}.
	 * @param ctx the parse tree
	 */
	void enterReference(ClaferRxParser.ReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#reference}.
	 * @param ctx the parse tree
	 */
	void exitReference(ClaferRxParser.ReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#setRef}.
	 * @param ctx the parse tree
	 */
	void enterSetRef(ClaferRxParser.SetRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#setRef}.
	 * @param ctx the parse tree
	 */
	void exitSetRef(ClaferRxParser.SetRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#stringSetRef}.
	 * @param ctx the parse tree
	 */
	void enterStringSetRef(ClaferRxParser.StringSetRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#stringSetRef}.
	 * @param ctx the parse tree
	 */
	void exitStringSetRef(ClaferRxParser.StringSetRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#stringBagRef}.
	 * @param ctx the parse tree
	 */
	void enterStringBagRef(ClaferRxParser.StringBagRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#stringBagRef}.
	 * @param ctx the parse tree
	 */
	void exitStringBagRef(ClaferRxParser.StringBagRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#intSetRef}.
	 * @param ctx the parse tree
	 */
	void enterIntSetRef(ClaferRxParser.IntSetRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#intSetRef}.
	 * @param ctx the parse tree
	 */
	void exitIntSetRef(ClaferRxParser.IntSetRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#doubleSetRef}.
	 * @param ctx the parse tree
	 */
	void enterDoubleSetRef(ClaferRxParser.DoubleSetRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#doubleSetRef}.
	 * @param ctx the parse tree
	 */
	void exitDoubleSetRef(ClaferRxParser.DoubleSetRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#bagRef}.
	 * @param ctx the parse tree
	 */
	void enterBagRef(ClaferRxParser.BagRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#bagRef}.
	 * @param ctx the parse tree
	 */
	void exitBagRef(ClaferRxParser.BagRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#intBagRef}.
	 * @param ctx the parse tree
	 */
	void enterIntBagRef(ClaferRxParser.IntBagRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#intBagRef}.
	 * @param ctx the parse tree
	 */
	void exitIntBagRef(ClaferRxParser.IntBagRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#doubleBagRef}.
	 * @param ctx the parse tree
	 */
	void enterDoubleBagRef(ClaferRxParser.DoubleBagRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#doubleBagRef}.
	 * @param ctx the parse tree
	 */
	void exitDoubleBagRef(ClaferRxParser.DoubleBagRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#childElements}.
	 * @param ctx the parse tree
	 */
	void enterChildElements(ClaferRxParser.ChildElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#childElements}.
	 * @param ctx the parse tree
	 */
	void exitChildElements(ClaferRxParser.ChildElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#empty}.
	 * @param ctx the parse tree
	 */
	void enterEmpty(ClaferRxParser.EmptyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#empty}.
	 * @param ctx the parse tree
	 */
	void exitEmpty(ClaferRxParser.EmptyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#giMult}.
	 * @param ctx the parse tree
	 */
	void enterGiMult(ClaferRxParser.GiMultContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#giMult}.
	 * @param ctx the parse tree
	 */
	void exitGiMult(ClaferRxParser.GiMultContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#fiMult}.
	 * @param ctx the parse tree
	 */
	void enterFiMult(ClaferRxParser.FiMultContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#fiMult}.
	 * @param ctx the parse tree
	 */
	void exitFiMult(ClaferRxParser.FiMultContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#mult}.
	 * @param ctx the parse tree
	 */
	void enterMult(ClaferRxParser.MultContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#mult}.
	 * @param ctx the parse tree
	 */
	void exitMult(ClaferRxParser.MultContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#lb}.
	 * @param ctx the parse tree
	 */
	void enterLb(ClaferRxParser.LbContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#lb}.
	 * @param ctx the parse tree
	 */
	void exitLb(ClaferRxParser.LbContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#ub}.
	 * @param ctx the parse tree
	 */
	void enterUb(ClaferRxParser.UbContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#ub}.
	 * @param ctx the parse tree
	 */
	void exitUb(ClaferRxParser.UbContext ctx);
	/**
	 * Enter a parse tree produced by the {@code minObj}
	 * labeled alternative in {@link ClaferRxParser#objective}.
	 * @param ctx the parse tree
	 */
	void enterMinObj(ClaferRxParser.MinObjContext ctx);
	/**
	 * Exit a parse tree produced by the {@code minObj}
	 * labeled alternative in {@link ClaferRxParser#objective}.
	 * @param ctx the parse tree
	 */
	void exitMinObj(ClaferRxParser.MinObjContext ctx);
	/**
	 * Enter a parse tree produced by the {@code maxObj}
	 * labeled alternative in {@link ClaferRxParser#objective}.
	 * @param ctx the parse tree
	 */
	void enterMaxObj(ClaferRxParser.MaxObjContext ctx);
	/**
	 * Exit a parse tree produced by the {@code maxObj}
	 * labeled alternative in {@link ClaferRxParser#objective}.
	 * @param ctx the parse tree
	 */
	void exitMaxObj(ClaferRxParser.MaxObjContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#assertion}.
	 * @param ctx the parse tree
	 */
	void enterAssertion(ClaferRxParser.AssertionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#assertion}.
	 * @param ctx the parse tree
	 */
	void exitAssertion(ClaferRxParser.AssertionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#constraint}.
	 * @param ctx the parse tree
	 */
	void enterConstraint(ClaferRxParser.ConstraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#constraint}.
	 * @param ctx the parse tree
	 */
	void exitConstraint(ClaferRxParser.ConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code quantified}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterQuantified(ClaferRxParser.QuantifiedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code quantified}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitQuantified(ClaferRxParser.QuantifiedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Not}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterNot(ClaferRxParser.NotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Not}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitNot(ClaferRxParser.NotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Impl}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterImpl(ClaferRxParser.ImplContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Impl}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitImpl(ClaferRxParser.ImplContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Or}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterOr(ClaferRxParser.OrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitOr(ClaferRxParser.OrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setComparison}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterSetComparison(ClaferRxParser.SetComparisonContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setComparison}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitSetComparison(ClaferRxParser.SetComparisonContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleQuantified}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterSimpleQuantified(ClaferRxParser.SimpleQuantifiedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleQuantified}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitSimpleQuantified(ClaferRxParser.SimpleQuantifiedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolBrackets}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterBoolBrackets(ClaferRxParser.BoolBracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolBrackets}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitBoolBrackets(ClaferRxParser.BoolBracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code And}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterAnd(ClaferRxParser.AndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code And}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitAnd(ClaferRxParser.AndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Rel}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterRel(ClaferRxParser.RelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Rel}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitRel(ClaferRxParser.RelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code XOr}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterXOr(ClaferRxParser.XOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code XOr}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitXOr(ClaferRxParser.XOrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code iteBool}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterIteBool(ClaferRxParser.IteBoolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code iteBool}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitIteBool(ClaferRxParser.IteBoolContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BiImpl}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void enterBiImpl(ClaferRxParser.BiImplContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BiImpl}
	 * labeled alternative in {@link ClaferRxParser#boolExpr}.
	 * @param ctx the parse tree
	 */
	void exitBiImpl(ClaferRxParser.BiImplContext ctx);
	/**
	 * Enter a parse tree produced by the {@code loneSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterLoneSimpleQuant(ClaferRxParser.LoneSimpleQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code loneSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitLoneSimpleQuant(ClaferRxParser.LoneSimpleQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oneSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterOneSimpleQuant(ClaferRxParser.OneSimpleQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oneSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitOneSimpleQuant(ClaferRxParser.OneSimpleQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code someSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterSomeSimpleQuant(ClaferRxParser.SomeSimpleQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code someSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitSomeSimpleQuant(ClaferRxParser.SomeSimpleQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterNoSimpleQuant(ClaferRxParser.NoSimpleQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noSimpleQuant}
	 * labeled alternative in {@link ClaferRxParser#simpleQuantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitNoSimpleQuant(ClaferRxParser.NoSimpleQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code allDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterAllDisjQuant(ClaferRxParser.AllDisjQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code allDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitAllDisjQuant(ClaferRxParser.AllDisjQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code allQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterAllQuant(ClaferRxParser.AllQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code allQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitAllQuant(ClaferRxParser.AllQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oneDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterOneDisjQuant(ClaferRxParser.OneDisjQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oneDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitOneDisjQuant(ClaferRxParser.OneDisjQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oneQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterOneQuant(ClaferRxParser.OneQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oneQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitOneQuant(ClaferRxParser.OneQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code someDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterSomeDisjQuant(ClaferRxParser.SomeDisjQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code someDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitSomeDisjQuant(ClaferRxParser.SomeDisjQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code someQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterSomeQuant(ClaferRxParser.SomeQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code someQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitSomeQuant(ClaferRxParser.SomeQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterNoDisjQuant(ClaferRxParser.NoDisjQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noDisjQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitNoDisjQuant(ClaferRxParser.NoDisjQuantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void enterNoQuant(ClaferRxParser.NoQuantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noQuant}
	 * labeled alternative in {@link ClaferRxParser#quantifiedExpr}.
	 * @param ctx the parse tree
	 */
	void exitNoQuant(ClaferRxParser.NoQuantContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#localDecl}.
	 * @param ctx the parse tree
	 */
	void enterLocalDecl(ClaferRxParser.LocalDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#localDecl}.
	 * @param ctx the parse tree
	 */
	void exitLocalDecl(ClaferRxParser.LocalDeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ge}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void enterGe(ClaferRxParser.GeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ge}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void exitGe(ClaferRxParser.GeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gt}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void enterGt(ClaferRxParser.GtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gt}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void exitGt(ClaferRxParser.GtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code le}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void enterLe(ClaferRxParser.LeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code le}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void exitLe(ClaferRxParser.LeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lt}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void enterLt(ClaferRxParser.LtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lt}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void exitLt(ClaferRxParser.LtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eq}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void enterEq(ClaferRxParser.EqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eq}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void exitEq(ClaferRxParser.EqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code neq}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void enterNeq(ClaferRxParser.NeqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code neq}
	 * labeled alternative in {@link ClaferRxParser#numericComp}.
	 * @param ctx the parse tree
	 */
	void exitNeq(ClaferRxParser.NeqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setEq}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 */
	void enterSetEq(ClaferRxParser.SetEqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setEq}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 */
	void exitSetEq(ClaferRxParser.SetEqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setUnEq}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 */
	void enterSetUnEq(ClaferRxParser.SetUnEqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setUnEq}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 */
	void exitSetUnEq(ClaferRxParser.SetUnEqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setIn}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 */
	void enterSetIn(ClaferRxParser.SetInContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setIn}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 */
	void exitSetIn(ClaferRxParser.SetInContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setNotIn}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 */
	void enterSetNotIn(ClaferRxParser.SetNotInContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setNotIn}
	 * labeled alternative in {@link ClaferRxParser#setComp}.
	 * @param ctx the parse tree
	 */
	void exitSetNotIn(ClaferRxParser.SetNotInContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringElement}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void enterStringElement(ClaferRxParser.StringElementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringElement}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void exitStringElement(ClaferRxParser.StringElementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setIntersec}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void enterSetIntersec(ClaferRxParser.SetIntersecContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setIntersec}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void exitSetIntersec(ClaferRxParser.SetIntersecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code iteSet}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void enterIteSet(ClaferRxParser.IteSetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code iteSet}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void exitIteSet(ClaferRxParser.IteSetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setBrackets}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void enterSetBrackets(ClaferRxParser.SetBracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setBrackets}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void exitSetBrackets(ClaferRxParser.SetBracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code join}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void enterJoin(ClaferRxParser.JoinContext ctx);
	/**
	 * Exit a parse tree produced by the {@code join}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void exitJoin(ClaferRxParser.JoinContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setUnion}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void enterSetUnion(ClaferRxParser.SetUnionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setUnion}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void exitSetUnion(ClaferRxParser.SetUnionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setDiff}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void enterSetDiff(ClaferRxParser.SetDiffContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setDiff}
	 * labeled alternative in {@link ClaferRxParser#setExpr}.
	 * @param ctx the parse tree
	 */
	void exitSetDiff(ClaferRxParser.SetDiffContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#setElement}.
	 * @param ctx the parse tree
	 */
	void enterSetElement(ClaferRxParser.SetElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#setElement}.
	 * @param ctx the parse tree
	 */
	void exitSetElement(ClaferRxParser.SetElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#path}.
	 * @param ctx the parse tree
	 */
	void enterPath(ClaferRxParser.PathContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#path}.
	 * @param ctx the parse tree
	 */
	void exitPath(ClaferRxParser.PathContext ctx);
	/**
	 * Enter a parse tree produced by the {@code add}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterAdd(ClaferRxParser.AddContext ctx);
	/**
	 * Exit a parse tree produced by the {@code add}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitAdd(ClaferRxParser.AddContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterParentheses(ClaferRxParser.ParenthesesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitParentheses(ClaferRxParser.ParenthesesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code product}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterProduct(ClaferRxParser.ProductContext ctx);
	/**
	 * Exit a parse tree produced by the {@code product}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitProduct(ClaferRxParser.ProductContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mod}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterMod(ClaferRxParser.ModContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mod}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitMod(ClaferRxParser.ModContext ctx);
	/**
	 * Enter a parse tree produced by the {@code max}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterMax(ClaferRxParser.MaxContext ctx);
	/**
	 * Exit a parse tree produced by the {@code max}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitMax(ClaferRxParser.MaxContext ctx);
	/**
	 * Enter a parse tree produced by the {@code iteNumeric}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterIteNumeric(ClaferRxParser.IteNumericContext ctx);
	/**
	 * Exit a parse tree produced by the {@code iteNumeric}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitIteNumeric(ClaferRxParser.IteNumericContext ctx);
	/**
	 * Enter a parse tree produced by the {@code var}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterVar(ClaferRxParser.VarContext ctx);
	/**
	 * Exit a parse tree produced by the {@code var}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitVar(ClaferRxParser.VarContext ctx);
	/**
	 * Enter a parse tree produced by the {@code sum}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterSum(ClaferRxParser.SumContext ctx);
	/**
	 * Exit a parse tree produced by the {@code sum}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitSum(ClaferRxParser.SumContext ctx);
	/**
	 * Enter a parse tree produced by the {@code substr}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterSubstr(ClaferRxParser.SubstrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code substr}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitSubstr(ClaferRxParser.SubstrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code realLit}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterRealLit(ClaferRxParser.RealLitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code realLit}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitRealLit(ClaferRxParser.RealLitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code div}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterDiv(ClaferRxParser.DivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code div}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitDiv(ClaferRxParser.DivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code neg}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterNeg(ClaferRxParser.NegContext ctx);
	/**
	 * Exit a parse tree produced by the {@code neg}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitNeg(ClaferRxParser.NegContext ctx);
	/**
	 * Enter a parse tree produced by the {@code min}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterMin(ClaferRxParser.MinContext ctx);
	/**
	 * Exit a parse tree produced by the {@code min}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitMin(ClaferRxParser.MinContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intLit}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterIntLit(ClaferRxParser.IntLitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intLit}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitIntLit(ClaferRxParser.IntLitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setCard}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterSetCard(ClaferRxParser.SetCardContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setCard}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitSetCard(ClaferRxParser.SetCardContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiply}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void enterMultiply(ClaferRxParser.MultiplyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiply}
	 * labeled alternative in {@link ClaferRxParser#numericExpr}.
	 * @param ctx the parse tree
	 */
	void exitMultiply(ClaferRxParser.MultiplyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#numVar}.
	 * @param ctx the parse tree
	 */
	void enterNumVar(ClaferRxParser.NumVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#numVar}.
	 * @param ctx the parse tree
	 */
	void exitNumVar(ClaferRxParser.NumVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(ClaferRxParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(ClaferRxParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#claferName}.
	 * @param ctx the parse tree
	 */
	void enterClaferName(ClaferRxParser.ClaferNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#claferName}.
	 * @param ctx the parse tree
	 */
	void exitClaferName(ClaferRxParser.ClaferNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#localIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterLocalIdentifier(ClaferRxParser.LocalIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#localIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitLocalIdentifier(ClaferRxParser.LocalIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(ClaferRxParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(ClaferRxParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#drefClafer}.
	 * @param ctx the parse tree
	 */
	void enterDrefClafer(ClaferRxParser.DrefClaferContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#drefClafer}.
	 * @param ctx the parse tree
	 */
	void exitDrefClafer(ClaferRxParser.DrefClaferContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#thisClafer}.
	 * @param ctx the parse tree
	 */
	void enterThisClafer(ClaferRxParser.ThisClaferContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#thisClafer}.
	 * @param ctx the parse tree
	 */
	void exitThisClafer(ClaferRxParser.ThisClaferContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#parentClafer}.
	 * @param ctx the parse tree
	 */
	void enterParentClafer(ClaferRxParser.ParentClaferContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#parentClafer}.
	 * @param ctx the parse tree
	 */
	void exitParentClafer(ClaferRxParser.ParentClaferContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#realSet}.
	 * @param ctx the parse tree
	 */
	void enterRealSet(ClaferRxParser.RealSetContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#realSet}.
	 * @param ctx the parse tree
	 */
	void exitRealSet(ClaferRxParser.RealSetContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#stringSet}.
	 * @param ctx the parse tree
	 */
	void enterStringSet(ClaferRxParser.StringSetContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#stringSet}.
	 * @param ctx the parse tree
	 */
	void exitStringSet(ClaferRxParser.StringSetContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#intSet}.
	 * @param ctx the parse tree
	 */
	void enterIntSet(ClaferRxParser.IntSetContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#intSet}.
	 * @param ctx the parse tree
	 */
	void exitIntSet(ClaferRxParser.IntSetContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#realLiteral}.
	 * @param ctx the parse tree
	 */
	void enterRealLiteral(ClaferRxParser.RealLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#realLiteral}.
	 * @param ctx the parse tree
	 */
	void exitRealLiteral(ClaferRxParser.RealLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#intLiteral}.
	 * @param ctx the parse tree
	 */
	void enterIntLiteral(ClaferRxParser.IntLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#intLiteral}.
	 * @param ctx the parse tree
	 */
	void exitIntLiteral(ClaferRxParser.IntLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link ClaferRxParser#stringExpr}.
	 * @param ctx the parse tree
	 */
	void enterStringExpr(ClaferRxParser.StringExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ClaferRxParser#stringExpr}.
	 * @param ctx the parse tree
	 */
	void exitStringExpr(ClaferRxParser.StringExprContext ctx);
}