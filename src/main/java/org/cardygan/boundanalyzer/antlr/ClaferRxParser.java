// Generated from ClaferRx.g4 by ANTLR 4.6
package org.cardygan.boundanalyzer.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ClaferRxParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		LEFT_CURLY=53, RIGHT_CURLY=54, ABSTR=55, INF=56, STRING_KEYWORD=57, INTEGER_KEYWORD=58, 
		REAL_KEYWORD=59, DOUBLE_KEYWORD=60, DREF_KEYWORD=61, THIS_KEYWORD=62, 
		PARENT_KEYWORD=63, CLAFER_ID=64, ID=65, STRING=66, NUMBER=67, INTEGER=68, 
		WS=69, COMMENT=70, LINE_COMMENT=71;
	public static final int
		RULE_module = 0, RULE_topLevelElement = 1, RULE_element = 2, RULE_clafer = 3, 
		RULE_abstr = 4, RULE_parent = 5, RULE_reference = 6, RULE_setRef = 7, 
		RULE_stringSetRef = 8, RULE_stringBagRef = 9, RULE_intSetRef = 10, RULE_doubleSetRef = 11, 
		RULE_bagRef = 12, RULE_intBagRef = 13, RULE_doubleBagRef = 14, RULE_childElements = 15, 
		RULE_empty = 16, RULE_giMult = 17, RULE_fiMult = 18, RULE_mult = 19, RULE_lb = 20, 
		RULE_ub = 21, RULE_objective = 22, RULE_assertion = 23, RULE_constraint = 24, 
		RULE_boolExpr = 25, RULE_simpleQuantifiedExpr = 26, RULE_quantifiedExpr = 27, 
		RULE_localDecl = 28, RULE_numericComp = 29, RULE_setComp = 30, RULE_setExpr = 31, 
		RULE_setElement = 32, RULE_path = 33, RULE_numericExpr = 34, RULE_numVar = 35, 
		RULE_identifier = 36, RULE_claferName = 37, RULE_localIdentifier = 38, 
		RULE_name = 39, RULE_drefClafer = 40, RULE_thisClafer = 41, RULE_parentClafer = 42, 
		RULE_realSet = 43, RULE_stringSet = 44, RULE_intSet = 45, RULE_realLiteral = 46, 
		RULE_intLiteral = 47, RULE_stringExpr = 48;
	public static final String[] ruleNames = {
		"module", "topLevelElement", "element", "clafer", "abstr", "parent", "reference", 
		"setRef", "stringSetRef", "stringBagRef", "intSetRef", "doubleSetRef", 
		"bagRef", "intBagRef", "doubleBagRef", "childElements", "empty", "giMult", 
		"fiMult", "mult", "lb", "ub", "objective", "assertion", "constraint", 
		"boolExpr", "simpleQuantifiedExpr", "quantifiedExpr", "localDecl", "numericComp", 
		"setComp", "setExpr", "setElement", "pathExpr", "numericExpr", "numVar", "identifier",
		"claferName", "localIdentifier", "name", "drefClafer", "thisClafer", "parentClafer", 
		"realSet", "stringSet", "intSet", "realLiteral", "intLiteral", "stringExpr"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "':'", "'->'", "'->>'", "'..'", "'<<'", "'minimize'", "'>>'", "'maximize'", 
		"'assert'", "'['", "']'", "'if'", "'then'", "'else'", "'('", "')'", "'&&'", 
		"'||'", "'xor'", "'<=>'", "'=>'", "'!'", "'lone'", "'one'", "'some'", 
		"'no'", "'not'", "'all'", "'disj'", "'|'", "';'", "'>='", "'>'", "'<='", 
		"'<'", "'='", "'!='", "'in'", "'not in'", "'++'", "'--'", "'**'", "'.'", 
		"'/'", "'+'", "'-'", "'%'", "'sum'", "'product'", "'#'", "'min'", "'max'", 
		"'{'", "'}'", "'abstract'", "'*'", "'string'", null, "'real'", "'double'", 
		"'dref'", "'this'", "'parent'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "LEFT_CURLY", "RIGHT_CURLY", "ABSTR", "INF", 
		"STRING_KEYWORD", "INTEGER_KEYWORD", "REAL_KEYWORD", "DOUBLE_KEYWORD", 
		"DREF_KEYWORD", "THIS_KEYWORD", "PARENT_KEYWORD", "CLAFER_ID", "ID", "STRING", 
		"NUMBER", "INTEGER", "WS", "COMMENT", "LINE_COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "ClaferRx.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ClaferRxParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ModuleContext extends ParserRuleContext {
		public List<TopLevelElementContext> topLevelElement() {
			return getRuleContexts(TopLevelElementContext.class);
		}
		public TopLevelElementContext topLevelElement(int i) {
			return getRuleContext(TopLevelElementContext.class,i);
		}
		public ModuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_module; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterModule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitModule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitModule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModuleContext module() throws RecognitionException {
		ModuleContext _localctx = new ModuleContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_module);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__8 - 5)) | (1L << (T__9 - 5)) | (1L << (ABSTR - 5)) | (1L << (NUMBER - 5)))) != 0)) {
				{
				{
				setState(98);
				topLevelElement();
				}
				}
				setState(103);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TopLevelElementContext extends ParserRuleContext {
		public ElementContext element() {
			return getRuleContext(ElementContext.class,0);
		}
		public ObjectiveContext objective() {
			return getRuleContext(ObjectiveContext.class,0);
		}
		public AssertionContext assertion() {
			return getRuleContext(AssertionContext.class,0);
		}
		public TopLevelElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_topLevelElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterTopLevelElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitTopLevelElement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitTopLevelElement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TopLevelElementContext topLevelElement() throws RecognitionException {
		TopLevelElementContext _localctx = new TopLevelElementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_topLevelElement);
		try {
			setState(107);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__9:
			case ABSTR:
			case NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(104);
				element();
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 2);
				{
				setState(105);
				objective();
				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 3);
				{
				setState(106);
				assertion();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementContext extends ParserRuleContext {
		public ClaferContext clafer() {
			return getRuleContext(ClaferContext.class,0);
		}
		public ConstraintContext constraint() {
			return getRuleContext(ConstraintContext.class,0);
		}
		public ElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_element; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitElement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitElement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElementContext element() throws RecognitionException {
		ElementContext _localctx = new ElementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_element);
		try {
			setState(111);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ABSTR:
			case NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(109);
				clafer();
				}
				break;
			case T__9:
				enterOuterAlt(_localctx, 2);
				{
				setState(110);
				constraint();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClaferContext extends ParserRuleContext {
		public GiMultContext giMult() {
			return getRuleContext(GiMultContext.class,0);
		}
		public ClaferNameContext claferName() {
			return getRuleContext(ClaferNameContext.class,0);
		}
		public FiMultContext fiMult() {
			return getRuleContext(FiMultContext.class,0);
		}
		public ChildElementsContext childElements() {
			return getRuleContext(ChildElementsContext.class,0);
		}
		public AbstrContext abstr() {
			return getRuleContext(AbstrContext.class,0);
		}
		public ParentContext parent() {
			return getRuleContext(ParentContext.class,0);
		}
		public ReferenceContext reference() {
			return getRuleContext(ReferenceContext.class,0);
		}
		public ClaferContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_clafer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterClafer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitClafer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitClafer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClaferContext clafer() throws RecognitionException {
		ClaferContext _localctx = new ClaferContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_clafer);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ABSTR) {
				{
				setState(113);
				abstr();
				}
			}

			setState(116);
			giMult();
			setState(117);
			claferName();
			setState(119);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(118);
				parent();
				}
			}

			setState(122);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1 || _la==T__2) {
				{
				setState(121);
				reference();
				}
			}

			setState(124);
			fiMult();
			setState(125);
			childElements();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AbstrContext extends ParserRuleContext {
		public TerminalNode ABSTR() { return getToken(ClaferRxParser.ABSTR, 0); }
		public AbstrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abstr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterAbstr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitAbstr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitAbstr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AbstrContext abstr() throws RecognitionException {
		AbstrContext _localctx = new AbstrContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_abstr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			match(ABSTR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParentContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ParentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterParent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitParent(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitParent(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParentContext parent() throws RecognitionException {
		ParentContext _localctx = new ParentContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_parent);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(129);
			match(T__0);
			setState(130);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReferenceContext extends ParserRuleContext {
		public IntSetRefContext intSetRef() {
			return getRuleContext(IntSetRefContext.class,0);
		}
		public DoubleSetRefContext doubleSetRef() {
			return getRuleContext(DoubleSetRefContext.class,0);
		}
		public SetRefContext setRef() {
			return getRuleContext(SetRefContext.class,0);
		}
		public IntBagRefContext intBagRef() {
			return getRuleContext(IntBagRefContext.class,0);
		}
		public DoubleBagRefContext doubleBagRef() {
			return getRuleContext(DoubleBagRefContext.class,0);
		}
		public BagRefContext bagRef() {
			return getRuleContext(BagRefContext.class,0);
		}
		public StringSetRefContext stringSetRef() {
			return getRuleContext(StringSetRefContext.class,0);
		}
		public StringBagRefContext stringBagRef() {
			return getRuleContext(StringBagRefContext.class,0);
		}
		public ReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitReference(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitReference(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReferenceContext reference() throws RecognitionException {
		ReferenceContext _localctx = new ReferenceContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_reference);
		try {
			setState(140);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(132);
				intSetRef();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(133);
				doubleSetRef();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(134);
				setRef();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(135);
				intBagRef();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(136);
				doubleBagRef();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(137);
				bagRef();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(138);
				stringSetRef();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(139);
				stringBagRef();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetRefContext extends ParserRuleContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public SetRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetRef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetRefContext setRef() throws RecognitionException {
		SetRefContext _localctx = new SetRefContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_setRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(142);
			match(T__1);
			setState(143);
			setExpr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringSetRefContext extends ParserRuleContext {
		public StringSetContext stringSet() {
			return getRuleContext(StringSetContext.class,0);
		}
		public StringSetRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringSetRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterStringSetRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitStringSetRef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitStringSetRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringSetRefContext stringSetRef() throws RecognitionException {
		StringSetRefContext _localctx = new StringSetRefContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_stringSetRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(145);
			match(T__1);
			setState(146);
			stringSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringBagRefContext extends ParserRuleContext {
		public StringSetContext stringSet() {
			return getRuleContext(StringSetContext.class,0);
		}
		public StringBagRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringBagRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterStringBagRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitStringBagRef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitStringBagRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringBagRefContext stringBagRef() throws RecognitionException {
		StringBagRefContext _localctx = new StringBagRefContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_stringBagRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(148);
			match(T__2);
			setState(149);
			stringSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntSetRefContext extends ParserRuleContext {
		public IntSetContext intSet() {
			return getRuleContext(IntSetContext.class,0);
		}
		public IntSetRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intSetRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIntSetRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIntSetRef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIntSetRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntSetRefContext intSetRef() throws RecognitionException {
		IntSetRefContext _localctx = new IntSetRefContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_intSetRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(151);
			match(T__1);
			setState(152);
			intSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoubleSetRefContext extends ParserRuleContext {
		public RealSetContext realSet() {
			return getRuleContext(RealSetContext.class,0);
		}
		public DoubleSetRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_doubleSetRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterDoubleSetRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitDoubleSetRef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitDoubleSetRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DoubleSetRefContext doubleSetRef() throws RecognitionException {
		DoubleSetRefContext _localctx = new DoubleSetRefContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_doubleSetRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154);
			match(T__1);
			setState(155);
			realSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BagRefContext extends ParserRuleContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public BagRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bagRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterBagRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitBagRef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitBagRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BagRefContext bagRef() throws RecognitionException {
		BagRefContext _localctx = new BagRefContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_bagRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(T__2);
			setState(158);
			setExpr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntBagRefContext extends ParserRuleContext {
		public IntSetContext intSet() {
			return getRuleContext(IntSetContext.class,0);
		}
		public IntBagRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intBagRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIntBagRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIntBagRef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIntBagRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntBagRefContext intBagRef() throws RecognitionException {
		IntBagRefContext _localctx = new IntBagRefContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_intBagRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			match(T__2);
			setState(161);
			intSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoubleBagRefContext extends ParserRuleContext {
		public RealSetContext realSet() {
			return getRuleContext(RealSetContext.class,0);
		}
		public DoubleBagRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_doubleBagRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterDoubleBagRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitDoubleBagRef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitDoubleBagRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DoubleBagRefContext doubleBagRef() throws RecognitionException {
		DoubleBagRefContext _localctx = new DoubleBagRefContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_doubleBagRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(163);
			match(T__2);
			setState(164);
			realSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChildElementsContext extends ParserRuleContext {
		public EmptyContext empty() {
			return getRuleContext(EmptyContext.class,0);
		}
		public TerminalNode LEFT_CURLY() { return getToken(ClaferRxParser.LEFT_CURLY, 0); }
		public TerminalNode RIGHT_CURLY() { return getToken(ClaferRxParser.RIGHT_CURLY, 0); }
		public List<ElementContext> element() {
			return getRuleContexts(ElementContext.class);
		}
		public ElementContext element(int i) {
			return getRuleContext(ElementContext.class,i);
		}
		public ChildElementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_childElements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterChildElements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitChildElements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitChildElements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ChildElementsContext childElements() throws RecognitionException {
		ChildElementsContext _localctx = new ChildElementsContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_childElements);
		int _la;
		try {
			setState(175);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(166);
				empty();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(167);
				match(LEFT_CURLY);
				setState(169); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(168);
					element();
					}
					}
					setState(171); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((((_la - 10)) & ~0x3f) == 0 && ((1L << (_la - 10)) & ((1L << (T__9 - 10)) | (1L << (ABSTR - 10)) | (1L << (NUMBER - 10)))) != 0) );
				setState(173);
				match(RIGHT_CURLY);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EmptyContext extends ParserRuleContext {
		public TerminalNode LEFT_CURLY() { return getToken(ClaferRxParser.LEFT_CURLY, 0); }
		public TerminalNode RIGHT_CURLY() { return getToken(ClaferRxParser.RIGHT_CURLY, 0); }
		public EmptyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_empty; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterEmpty(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitEmpty(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitEmpty(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EmptyContext empty() throws RecognitionException {
		EmptyContext _localctx = new EmptyContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_empty);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(177);
			match(LEFT_CURLY);
			setState(178);
			match(RIGHT_CURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GiMultContext extends ParserRuleContext {
		public MultContext mult() {
			return getRuleContext(MultContext.class,0);
		}
		public GiMultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_giMult; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterGiMult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitGiMult(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitGiMult(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GiMultContext giMult() throws RecognitionException {
		GiMultContext _localctx = new GiMultContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_giMult);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			mult();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FiMultContext extends ParserRuleContext {
		public MultContext mult() {
			return getRuleContext(MultContext.class,0);
		}
		public FiMultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fiMult; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterFiMult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitFiMult(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitFiMult(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FiMultContext fiMult() throws RecognitionException {
		FiMultContext _localctx = new FiMultContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_fiMult);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(182);
			mult();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultContext extends ParserRuleContext {
		public LbContext lb() {
			return getRuleContext(LbContext.class,0);
		}
		public UbContext ub() {
			return getRuleContext(UbContext.class,0);
		}
		public MultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mult; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterMult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitMult(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitMult(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultContext mult() throws RecognitionException {
		MultContext _localctx = new MultContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_mult);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(184);
			lb();
			setState(185);
			match(T__3);
			setState(186);
			ub();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LbContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(ClaferRxParser.NUMBER, 0); }
		public LbContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lb; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterLb(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitLb(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitLb(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LbContext lb() throws RecognitionException {
		LbContext _localctx = new LbContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_lb);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(188);
			match(NUMBER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UbContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(ClaferRxParser.NUMBER, 0); }
		public TerminalNode INF() { return getToken(ClaferRxParser.INF, 0); }
		public UbContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ub; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterUb(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitUb(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitUb(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UbContext ub() throws RecognitionException {
		UbContext _localctx = new UbContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_ub);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			_la = _input.LA(1);
			if ( !(_la==INF || _la==NUMBER) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectiveContext extends ParserRuleContext {
		public ObjectiveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objective; }
	 
		public ObjectiveContext() { }
		public void copyFrom(ObjectiveContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MaxObjContext extends ObjectiveContext {
		public NumericExprContext numericExpr() {
			return getRuleContext(NumericExprContext.class,0);
		}
		public MaxObjContext(ObjectiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterMaxObj(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitMaxObj(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitMaxObj(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinObjContext extends ObjectiveContext {
		public NumericExprContext numericExpr() {
			return getRuleContext(NumericExprContext.class,0);
		}
		public MinObjContext(ObjectiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterMinObj(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitMinObj(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitMinObj(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObjectiveContext objective() throws RecognitionException {
		ObjectiveContext _localctx = new ObjectiveContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_objective);
		try {
			setState(202);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new MinObjContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(192);
				match(T__4);
				setState(193);
				match(T__5);
				setState(194);
				numericExpr(0);
				setState(195);
				match(T__6);
				}
				break;
			case 2:
				_localctx = new MaxObjContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(197);
				match(T__4);
				setState(198);
				match(T__7);
				setState(199);
				numericExpr(0);
				setState(200);
				match(T__6);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssertionContext extends ParserRuleContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public AssertionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assertion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterAssertion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitAssertion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitAssertion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssertionContext assertion() throws RecognitionException {
		AssertionContext _localctx = new AssertionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_assertion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(204);
			match(T__8);
			setState(205);
			match(T__9);
			setState(206);
			boolExpr(0);
			setState(207);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstraintContext extends ParserRuleContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public ConstraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterConstraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitConstraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitConstraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstraintContext constraint() throws RecognitionException {
		ConstraintContext _localctx = new ConstraintContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_constraint);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(209);
			match(T__9);
			setState(210);
			boolExpr(0);
			setState(211);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolExprContext extends ParserRuleContext {
		public BoolExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolExpr; }
	 
		public BoolExprContext() { }
		public void copyFrom(BoolExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class QuantifiedContext extends BoolExprContext {
		public QuantifiedExprContext quantifiedExpr() {
			return getRuleContext(QuantifiedExprContext.class,0);
		}
		public QuantifiedContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterQuantified(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitQuantified(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitQuantified(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotContext extends BoolExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public NotContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterNot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitNot(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitNot(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ImplContext extends BoolExprContext {
		public List<BoolExprContext> boolExpr() {
			return getRuleContexts(BoolExprContext.class);
		}
		public BoolExprContext boolExpr(int i) {
			return getRuleContext(BoolExprContext.class,i);
		}
		public ImplContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterImpl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitImpl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitImpl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OrContext extends BoolExprContext {
		public List<BoolExprContext> boolExpr() {
			return getRuleContexts(BoolExprContext.class);
		}
		public BoolExprContext boolExpr(int i) {
			return getRuleContext(BoolExprContext.class,i);
		}
		public OrContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitOr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetComparisonContext extends BoolExprContext {
		public SetCompContext setComp() {
			return getRuleContext(SetCompContext.class,0);
		}
		public SetComparisonContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetComparison(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetComparison(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SimpleQuantifiedContext extends BoolExprContext {
		public SimpleQuantifiedExprContext simpleQuantifiedExpr() {
			return getRuleContext(SimpleQuantifiedExprContext.class,0);
		}
		public SimpleQuantifiedContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSimpleQuantified(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSimpleQuantified(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSimpleQuantified(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolBracketsContext extends BoolExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public BoolBracketsContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterBoolBrackets(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitBoolBrackets(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitBoolBrackets(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndContext extends BoolExprContext {
		public List<BoolExprContext> boolExpr() {
			return getRuleContexts(BoolExprContext.class);
		}
		public BoolExprContext boolExpr(int i) {
			return getRuleContext(BoolExprContext.class,i);
		}
		public AndContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterAnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitAnd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RelContext extends BoolExprContext {
		public NumericCompContext numericComp() {
			return getRuleContext(NumericCompContext.class,0);
		}
		public RelContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterRel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitRel(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitRel(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class XOrContext extends BoolExprContext {
		public List<BoolExprContext> boolExpr() {
			return getRuleContexts(BoolExprContext.class);
		}
		public BoolExprContext boolExpr(int i) {
			return getRuleContext(BoolExprContext.class,i);
		}
		public XOrContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterXOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitXOr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitXOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IteBoolContext extends BoolExprContext {
		public List<BoolExprContext> boolExpr() {
			return getRuleContexts(BoolExprContext.class);
		}
		public BoolExprContext boolExpr(int i) {
			return getRuleContext(BoolExprContext.class,i);
		}
		public IteBoolContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIteBool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIteBool(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIteBool(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BiImplContext extends BoolExprContext {
		public List<BoolExprContext> boolExpr() {
			return getRuleContexts(BoolExprContext.class);
		}
		public BoolExprContext boolExpr(int i) {
			return getRuleContext(BoolExprContext.class,i);
		}
		public BiImplContext(BoolExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterBiImpl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitBiImpl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitBiImpl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolExprContext boolExpr() throws RecognitionException {
		return boolExpr(0);
	}

	private BoolExprContext boolExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BoolExprContext _localctx = new BoolExprContext(_ctx, _parentState);
		BoolExprContext _prevctx = _localctx;
		int _startState = 50;
		enterRecursionRule(_localctx, 50, RULE_boolExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(231);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				{
				_localctx = new IteBoolContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(214);
				match(T__11);
				setState(215);
				boolExpr(0);
				setState(216);
				match(T__12);
				setState(217);
				boolExpr(0);
				setState(218);
				match(T__13);
				setState(219);
				boolExpr(12);
				}
				break;
			case 2:
				{
				_localctx = new QuantifiedContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(221);
				quantifiedExpr();
				}
				break;
			case 3:
				{
				_localctx = new SimpleQuantifiedContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(222);
				simpleQuantifiedExpr();
				}
				break;
			case 4:
				{
				_localctx = new BoolBracketsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(223);
				match(T__14);
				setState(224);
				boolExpr(0);
				setState(225);
				match(T__15);
				}
				break;
			case 5:
				{
				_localctx = new NotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(227);
				match(T__21);
				setState(228);
				boolExpr(3);
				}
				break;
			case 6:
				{
				_localctx = new RelContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(229);
				numericComp();
				}
				break;
			case 7:
				{
				_localctx = new SetComparisonContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(230);
				setComp();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(250);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(248);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
					case 1:
						{
						_localctx = new AndContext(new BoolExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolExpr);
						setState(233);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(234);
						match(T__16);
						setState(235);
						boolExpr(9);
						}
						break;
					case 2:
						{
						_localctx = new OrContext(new BoolExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolExpr);
						setState(236);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(237);
						match(T__17);
						setState(238);
						boolExpr(8);
						}
						break;
					case 3:
						{
						_localctx = new XOrContext(new BoolExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolExpr);
						setState(239);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(240);
						match(T__18);
						setState(241);
						boolExpr(7);
						}
						break;
					case 4:
						{
						_localctx = new BiImplContext(new BoolExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolExpr);
						setState(242);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(243);
						match(T__19);
						setState(244);
						boolExpr(6);
						}
						break;
					case 5:
						{
						_localctx = new ImplContext(new BoolExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolExpr);
						setState(245);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(246);
						match(T__20);
						setState(247);
						boolExpr(5);
						}
						break;
					}
					} 
				}
				setState(252);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SimpleQuantifiedExprContext extends ParserRuleContext {
		public SimpleQuantifiedExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleQuantifiedExpr; }
	 
		public SimpleQuantifiedExprContext() { }
		public void copyFrom(SimpleQuantifiedExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SomeSimpleQuantContext extends SimpleQuantifiedExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public SomeSimpleQuantContext(SimpleQuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSomeSimpleQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSomeSimpleQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSomeSimpleQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LoneSimpleQuantContext extends SimpleQuantifiedExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public LoneSimpleQuantContext(SimpleQuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterLoneSimpleQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitLoneSimpleQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitLoneSimpleQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoSimpleQuantContext extends SimpleQuantifiedExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public NoSimpleQuantContext(SimpleQuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterNoSimpleQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitNoSimpleQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitNoSimpleQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OneSimpleQuantContext extends SimpleQuantifiedExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public OneSimpleQuantContext(SimpleQuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterOneSimpleQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitOneSimpleQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitOneSimpleQuant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleQuantifiedExprContext simpleQuantifiedExpr() throws RecognitionException {
		SimpleQuantifiedExprContext _localctx = new SimpleQuantifiedExprContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_simpleQuantifiedExpr);
		int _la;
		try {
			setState(261);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__22:
				_localctx = new LoneSimpleQuantContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(253);
				match(T__22);
				setState(254);
				setExpr(0);
				}
				break;
			case T__23:
				_localctx = new OneSimpleQuantContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(255);
				match(T__23);
				setState(256);
				setExpr(0);
				}
				break;
			case T__24:
				_localctx = new SomeSimpleQuantContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(257);
				match(T__24);
				setState(258);
				setExpr(0);
				}
				break;
			case T__25:
			case T__26:
				_localctx = new NoSimpleQuantContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(259);
				_la = _input.LA(1);
				if ( !(_la==T__25 || _la==T__26) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(260);
				setExpr(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuantifiedExprContext extends ParserRuleContext {
		public QuantifiedExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quantifiedExpr; }
	 
		public QuantifiedExprContext() { }
		public void copyFrom(QuantifiedExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NoQuantContext extends QuantifiedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<LocalDeclContext> localDecl() {
			return getRuleContexts(LocalDeclContext.class);
		}
		public LocalDeclContext localDecl(int i) {
			return getRuleContext(LocalDeclContext.class,i);
		}
		public NoQuantContext(QuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterNoQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitNoQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitNoQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SomeQuantContext extends QuantifiedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<LocalDeclContext> localDecl() {
			return getRuleContexts(LocalDeclContext.class);
		}
		public LocalDeclContext localDecl(int i) {
			return getRuleContext(LocalDeclContext.class,i);
		}
		public SomeQuantContext(QuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSomeQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSomeQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSomeQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoDisjQuantContext extends QuantifiedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<LocalDeclContext> localDecl() {
			return getRuleContexts(LocalDeclContext.class);
		}
		public LocalDeclContext localDecl(int i) {
			return getRuleContext(LocalDeclContext.class,i);
		}
		public NoDisjQuantContext(QuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterNoDisjQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitNoDisjQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitNoDisjQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AllQuantContext extends QuantifiedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<LocalDeclContext> localDecl() {
			return getRuleContexts(LocalDeclContext.class);
		}
		public LocalDeclContext localDecl(int i) {
			return getRuleContext(LocalDeclContext.class,i);
		}
		public AllQuantContext(QuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterAllQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitAllQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitAllQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OneQuantContext extends QuantifiedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<LocalDeclContext> localDecl() {
			return getRuleContexts(LocalDeclContext.class);
		}
		public LocalDeclContext localDecl(int i) {
			return getRuleContext(LocalDeclContext.class,i);
		}
		public OneQuantContext(QuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterOneQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitOneQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitOneQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OneDisjQuantContext extends QuantifiedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<LocalDeclContext> localDecl() {
			return getRuleContexts(LocalDeclContext.class);
		}
		public LocalDeclContext localDecl(int i) {
			return getRuleContext(LocalDeclContext.class,i);
		}
		public OneDisjQuantContext(QuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterOneDisjQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitOneDisjQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitOneDisjQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AllDisjQuantContext extends QuantifiedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<LocalDeclContext> localDecl() {
			return getRuleContexts(LocalDeclContext.class);
		}
		public LocalDeclContext localDecl(int i) {
			return getRuleContext(LocalDeclContext.class,i);
		}
		public AllDisjQuantContext(QuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterAllDisjQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitAllDisjQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitAllDisjQuant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SomeDisjQuantContext extends QuantifiedExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<LocalDeclContext> localDecl() {
			return getRuleContexts(LocalDeclContext.class);
		}
		public LocalDeclContext localDecl(int i) {
			return getRuleContext(LocalDeclContext.class,i);
		}
		public SomeDisjQuantContext(QuantifiedExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSomeDisjQuant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSomeDisjQuant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSomeDisjQuant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QuantifiedExprContext quantifiedExpr() throws RecognitionException {
		QuantifiedExprContext _localctx = new QuantifiedExprContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_quantifiedExpr);
		int _la;
		try {
			setState(339);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				_localctx = new AllDisjQuantContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(263);
				match(T__27);
				setState(264);
				match(T__28);
				setState(266); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(265);
					localDecl();
					}
					}
					setState(268); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(270);
				match(T__29);
				setState(271);
				boolExpr(0);
				}
				break;
			case 2:
				_localctx = new AllQuantContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(273);
				match(T__27);
				setState(275); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(274);
					localDecl();
					}
					}
					setState(277); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(279);
				match(T__29);
				setState(280);
				boolExpr(0);
				}
				break;
			case 3:
				_localctx = new OneDisjQuantContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(282);
				match(T__23);
				setState(283);
				match(T__28);
				setState(285); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(284);
					localDecl();
					}
					}
					setState(287); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(289);
				match(T__29);
				setState(290);
				boolExpr(0);
				}
				break;
			case 4:
				_localctx = new OneQuantContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(292);
				match(T__23);
				setState(294); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(293);
					localDecl();
					}
					}
					setState(296); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(298);
				match(T__29);
				setState(299);
				boolExpr(0);
				}
				break;
			case 5:
				_localctx = new SomeDisjQuantContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(301);
				match(T__24);
				setState(302);
				match(T__28);
				setState(304); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(303);
					localDecl();
					}
					}
					setState(306); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(308);
				match(T__29);
				setState(309);
				boolExpr(0);
				}
				break;
			case 6:
				_localctx = new SomeQuantContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(311);
				match(T__24);
				setState(313); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(312);
					localDecl();
					}
					}
					setState(315); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(317);
				match(T__29);
				setState(318);
				boolExpr(0);
				}
				break;
			case 7:
				_localctx = new NoDisjQuantContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(320);
				match(T__25);
				setState(321);
				match(T__28);
				setState(323); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(322);
					localDecl();
					}
					}
					setState(325); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(327);
				match(T__29);
				setState(328);
				boolExpr(0);
				}
				break;
			case 8:
				_localctx = new NoQuantContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(330);
				match(T__25);
				setState(332); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(331);
					localDecl();
					}
					}
					setState(334); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(336);
				match(T__29);
				setState(337);
				boolExpr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalDeclContext extends ParserRuleContext {
		public List<LocalIdentifierContext> localIdentifier() {
			return getRuleContexts(LocalIdentifierContext.class);
		}
		public LocalIdentifierContext localIdentifier(int i) {
			return getRuleContext(LocalIdentifierContext.class,i);
		}
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public LocalDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_localDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterLocalDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitLocalDecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitLocalDecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LocalDeclContext localDecl() throws RecognitionException {
		LocalDeclContext _localctx = new LocalDeclContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_localDecl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(341);
			localIdentifier();
			setState(346);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__30) {
				{
				{
				setState(342);
				match(T__30);
				setState(343);
				localIdentifier();
				}
				}
				setState(348);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(349);
			match(T__0);
			setState(350);
			setExpr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericCompContext extends ParserRuleContext {
		public NumericCompContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericComp; }
	 
		public NumericCompContext() { }
		public void copyFrom(NumericCompContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LtContext extends NumericCompContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public LtContext(NumericCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterLt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitLt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitLt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LeContext extends NumericCompContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public LeContext(NumericCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterLe(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitLe(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitLe(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NeqContext extends NumericCompContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public NeqContext(NumericCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterNeq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitNeq(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitNeq(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqContext extends NumericCompContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public EqContext(NumericCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterEq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitEq(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitEq(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GtContext extends NumericCompContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public GtContext(NumericCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterGt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitGt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitGt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GeContext extends NumericCompContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public GeContext(NumericCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterGe(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitGe(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitGe(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumericCompContext numericComp() throws RecognitionException {
		NumericCompContext _localctx = new NumericCompContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_numericComp);
		try {
			setState(376);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				_localctx = new GeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(352);
				numericExpr(0);
				setState(353);
				match(T__31);
				setState(354);
				numericExpr(0);
				}
				break;
			case 2:
				_localctx = new GtContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(356);
				numericExpr(0);
				setState(357);
				match(T__32);
				setState(358);
				numericExpr(0);
				}
				break;
			case 3:
				_localctx = new LeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(360);
				numericExpr(0);
				setState(361);
				match(T__33);
				setState(362);
				numericExpr(0);
				}
				break;
			case 4:
				_localctx = new LtContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(364);
				numericExpr(0);
				setState(365);
				match(T__34);
				setState(366);
				numericExpr(0);
				}
				break;
			case 5:
				_localctx = new EqContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(368);
				numericExpr(0);
				setState(369);
				match(T__35);
				setState(370);
				numericExpr(0);
				}
				break;
			case 6:
				_localctx = new NeqContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(372);
				numericExpr(0);
				setState(373);
				match(T__36);
				setState(374);
				numericExpr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetCompContext extends ParserRuleContext {
		public SetCompContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setComp; }
	 
		public SetCompContext() { }
		public void copyFrom(SetCompContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SetInContext extends SetCompContext {
		public List<SetExprContext> setExpr() {
			return getRuleContexts(SetExprContext.class);
		}
		public SetExprContext setExpr(int i) {
			return getRuleContext(SetExprContext.class,i);
		}
		public SetInContext(SetCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetIn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetIn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetIn(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetUnEqContext extends SetCompContext {
		public List<SetExprContext> setExpr() {
			return getRuleContexts(SetExprContext.class);
		}
		public SetExprContext setExpr(int i) {
			return getRuleContext(SetExprContext.class,i);
		}
		public SetUnEqContext(SetCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetUnEq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetUnEq(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetUnEq(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetNotInContext extends SetCompContext {
		public List<SetExprContext> setExpr() {
			return getRuleContexts(SetExprContext.class);
		}
		public SetExprContext setExpr(int i) {
			return getRuleContext(SetExprContext.class,i);
		}
		public SetNotInContext(SetCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetNotIn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetNotIn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetNotIn(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetEqContext extends SetCompContext {
		public List<SetExprContext> setExpr() {
			return getRuleContexts(SetExprContext.class);
		}
		public SetExprContext setExpr(int i) {
			return getRuleContext(SetExprContext.class,i);
		}
		public SetEqContext(SetCompContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetEq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetEq(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetEq(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetCompContext setComp() throws RecognitionException {
		SetCompContext _localctx = new SetCompContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_setComp);
		try {
			setState(394);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				_localctx = new SetEqContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(378);
				setExpr(0);
				setState(379);
				match(T__35);
				setState(380);
				setExpr(0);
				}
				break;
			case 2:
				_localctx = new SetUnEqContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(382);
				setExpr(0);
				setState(383);
				match(T__36);
				setState(384);
				setExpr(0);
				}
				break;
			case 3:
				_localctx = new SetInContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(386);
				setExpr(0);
				setState(387);
				match(T__37);
				setState(388);
				setExpr(0);
				}
				break;
			case 4:
				_localctx = new SetNotInContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(390);
				setExpr(0);
				setState(391);
				match(T__38);
				setState(392);
				setExpr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetExprContext extends ParserRuleContext {
		public SetExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setExpr; }
	 
		public SetExprContext() { }
		public void copyFrom(SetExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StringElementContext extends SetExprContext {
		public StringExprContext stringExpr() {
			return getRuleContext(StringExprContext.class,0);
		}
		public StringElementContext(SetExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterStringElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitStringElement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitStringElement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetIntersecContext extends SetExprContext {
		public List<SetExprContext> setExpr() {
			return getRuleContexts(SetExprContext.class);
		}
		public SetExprContext setExpr(int i) {
			return getRuleContext(SetExprContext.class,i);
		}
		public SetIntersecContext(SetExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetIntersec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetIntersec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetIntersec(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IteSetContext extends SetExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<SetExprContext> setExpr() {
			return getRuleContexts(SetExprContext.class);
		}
		public SetExprContext setExpr(int i) {
			return getRuleContext(SetExprContext.class,i);
		}
		public IteSetContext(SetExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIteSet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIteSet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIteSet(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetBracketsContext extends SetExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public SetBracketsContext(SetExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetBrackets(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetBrackets(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetBrackets(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class JoinContext extends SetExprContext {
		public SetElementContext setElement() {
			return getRuleContext(SetElementContext.class,0);
		}
		public JoinContext(SetExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterJoin(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitJoin(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitJoin(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetUnionContext extends SetExprContext {
		public List<SetExprContext> setExpr() {
			return getRuleContexts(SetExprContext.class);
		}
		public SetExprContext setExpr(int i) {
			return getRuleContext(SetExprContext.class,i);
		}
		public SetUnionContext(SetExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetUnion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetUnion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetUnion(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetDiffContext extends SetExprContext {
		public List<SetExprContext> setExpr() {
			return getRuleContexts(SetExprContext.class);
		}
		public SetExprContext setExpr(int i) {
			return getRuleContext(SetExprContext.class,i);
		}
		public SetDiffContext(SetExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetDiff(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetDiff(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetDiff(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetExprContext setExpr() throws RecognitionException {
		return setExpr(0);
	}

	private SetExprContext setExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SetExprContext _localctx = new SetExprContext(_ctx, _parentState);
		SetExprContext _prevctx = _localctx;
		int _startState = 62;
		enterRecursionRule(_localctx, 62, RULE_setExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(410);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__14:
				{
				_localctx = new SetBracketsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(397);
				match(T__14);
				setState(398);
				setExpr(0);
				setState(399);
				match(T__15);
				}
				break;
			case T__11:
				{
				_localctx = new IteSetContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(401);
				match(T__11);
				setState(402);
				boolExpr(0);
				setState(403);
				match(T__12);
				setState(404);
				setExpr(0);
				setState(405);
				match(T__13);
				setState(406);
				setExpr(3);
				}
				break;
			case DREF_KEYWORD:
			case THIS_KEYWORD:
			case PARENT_KEYWORD:
			case CLAFER_ID:
			case ID:
				{
				_localctx = new JoinContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(408);
				setElement();
				}
				break;
			case STRING:
				{
				_localctx = new StringElementContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(409);
				stringExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(423);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(421);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
					case 1:
						{
						_localctx = new SetUnionContext(new SetExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_setExpr);
						setState(412);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(413);
						match(T__39);
						setState(414);
						setExpr(7);
						}
						break;
					case 2:
						{
						_localctx = new SetDiffContext(new SetExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_setExpr);
						setState(415);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(416);
						match(T__40);
						setState(417);
						setExpr(6);
						}
						break;
					case 3:
						{
						_localctx = new SetIntersecContext(new SetExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_setExpr);
						setState(418);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(419);
						match(T__41);
						setState(420);
						setExpr(5);
						}
						break;
					}
					} 
				}
				setState(425);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SetElementContext extends ParserRuleContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public SetElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetElement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetElement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetElementContext setElement() throws RecognitionException {
		SetElementContext _localctx = new SetElementContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_setElement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(426);
			path();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public PathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_path; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitPath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitPath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PathContext path() throws RecognitionException {
		PathContext _localctx = new PathContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_path);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(428);
			identifier();
			setState(433);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(429);
					match(T__42);
					setState(430);
					identifier();
					}
					} 
				}
				setState(435);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericExprContext extends ParserRuleContext {
		public NumericExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericExpr; }
	 
		public NumericExprContext() { }
		public void copyFrom(NumericExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AddContext extends NumericExprContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public AddContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterAdd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitAdd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenthesesContext extends NumericExprContext {
		public NumericExprContext numericExpr() {
			return getRuleContext(NumericExprContext.class,0);
		}
		public ParenthesesContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterParentheses(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitParentheses(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitParentheses(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ProductContext extends NumericExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public ProductContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterProduct(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitProduct(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitProduct(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ModContext extends NumericExprContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public ModContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterMod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitMod(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitMod(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MaxContext extends NumericExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public MaxContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterMax(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitMax(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitMax(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IteNumericContext extends NumericExprContext {
		public BoolExprContext boolExpr() {
			return getRuleContext(BoolExprContext.class,0);
		}
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public IteNumericContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIteNumeric(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIteNumeric(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIteNumeric(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarContext extends NumericExprContext {
		public NumVarContext numVar() {
			return getRuleContext(NumVarContext.class,0);
		}
		public VarContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitVar(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SumContext extends NumericExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public SumContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSum(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSum(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSum(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubstrContext extends NumericExprContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public SubstrContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSubstr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSubstr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSubstr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealLitContext extends NumericExprContext {
		public RealLiteralContext realLiteral() {
			return getRuleContext(RealLiteralContext.class,0);
		}
		public RealLitContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterRealLit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitRealLit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitRealLit(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DivContext extends NumericExprContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public DivContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterDiv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitDiv(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NegContext extends NumericExprContext {
		public NumericExprContext numericExpr() {
			return getRuleContext(NumericExprContext.class,0);
		}
		public NegContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterNeg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitNeg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitNeg(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinContext extends NumericExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public MinContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterMin(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitMin(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitMin(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntLitContext extends NumericExprContext {
		public IntLiteralContext intLiteral() {
			return getRuleContext(IntLiteralContext.class,0);
		}
		public IntLitContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIntLit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIntLit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIntLit(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetCardContext extends NumericExprContext {
		public SetExprContext setExpr() {
			return getRuleContext(SetExprContext.class,0);
		}
		public SetCardContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterSetCard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitSetCard(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitSetCard(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiplyContext extends NumericExprContext {
		public List<NumericExprContext> numericExpr() {
			return getRuleContexts(NumericExprContext.class);
		}
		public NumericExprContext numericExpr(int i) {
			return getRuleContext(NumericExprContext.class,i);
		}
		public MultiplyContext(NumericExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterMultiply(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitMultiply(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitMultiply(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumericExprContext numericExpr() throws RecognitionException {
		return numericExpr(0);
	}

	private NumericExprContext numericExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		NumericExprContext _localctx = new NumericExprContext(_ctx, _parentState);
		NumericExprContext _prevctx = _localctx;
		int _startState = 68;
		enterRecursionRule(_localctx, 68, RULE_numericExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(463);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__11:
				{
				_localctx = new IteNumericContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(437);
				match(T__11);
				setState(438);
				boolExpr(0);
				setState(439);
				match(T__12);
				setState(440);
				numericExpr(0);
				setState(441);
				match(T__13);
				setState(442);
				numericExpr(16);
				}
				break;
			case T__14:
				{
				_localctx = new ParenthesesContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(444);
				match(T__14);
				setState(445);
				numericExpr(0);
				setState(446);
				match(T__15);
				}
				break;
			case T__45:
				{
				_localctx = new NegContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(448);
				match(T__45);
				setState(449);
				numericExpr(9);
				}
				break;
			case T__47:
				{
				_localctx = new SumContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(450);
				match(T__47);
				setState(451);
				setExpr(0);
				}
				break;
			case T__48:
				{
				_localctx = new ProductContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(452);
				match(T__48);
				setState(453);
				setExpr(0);
				}
				break;
			case T__49:
				{
				_localctx = new SetCardContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(454);
				match(T__49);
				setState(455);
				setExpr(0);
				}
				break;
			case T__50:
				{
				_localctx = new MinContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(456);
				match(T__50);
				setState(457);
				setExpr(0);
				}
				break;
			case T__51:
				{
				_localctx = new MaxContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(458);
				match(T__51);
				setState(459);
				setExpr(0);
				}
				break;
			case DREF_KEYWORD:
			case THIS_KEYWORD:
			case PARENT_KEYWORD:
			case CLAFER_ID:
			case ID:
				{
				_localctx = new VarContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(460);
				numVar();
				}
				break;
			case INTEGER:
				{
				_localctx = new IntLitContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(461);
				intLiteral();
				}
				break;
			case NUMBER:
				{
				_localctx = new RealLitContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(462);
				realLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(482);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(480);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
					case 1:
						{
						_localctx = new MultiplyContext(new NumericExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_numericExpr);
						setState(465);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(466);
						match(INF);
						setState(467);
						numericExpr(16);
						}
						break;
					case 2:
						{
						_localctx = new DivContext(new NumericExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_numericExpr);
						setState(468);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(469);
						match(T__43);
						setState(470);
						numericExpr(15);
						}
						break;
					case 3:
						{
						_localctx = new AddContext(new NumericExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_numericExpr);
						setState(471);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(472);
						match(T__44);
						setState(473);
						numericExpr(14);
						}
						break;
					case 4:
						{
						_localctx = new SubstrContext(new NumericExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_numericExpr);
						setState(474);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(475);
						match(T__45);
						setState(476);
						numericExpr(13);
						}
						break;
					case 5:
						{
						_localctx = new ModContext(new NumericExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_numericExpr);
						setState(477);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(478);
						match(T__46);
						setState(479);
						numericExpr(12);
						}
						break;
					}
					} 
				}
				setState(484);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class NumVarContext extends ParserRuleContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public TerminalNode DREF_KEYWORD() { return getToken(ClaferRxParser.DREF_KEYWORD, 0); }
		public NumVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterNumVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitNumVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitNumVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumVarContext numVar() throws RecognitionException {
		NumVarContext _localctx = new NumVarContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_numVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(485);
			path();
			setState(486);
			match(T__42);
			setState(487);
			match(DREF_KEYWORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public DrefClaferContext drefClafer() {
			return getRuleContext(DrefClaferContext.class,0);
		}
		public ThisClaferContext thisClafer() {
			return getRuleContext(ThisClaferContext.class,0);
		}
		public ParentClaferContext parentClafer() {
			return getRuleContext(ParentClaferContext.class,0);
		}
		public ClaferNameContext claferName() {
			return getRuleContext(ClaferNameContext.class,0);
		}
		public LocalIdentifierContext localIdentifier() {
			return getRuleContext(LocalIdentifierContext.class,0);
		}
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_identifier);
		try {
			setState(494);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DREF_KEYWORD:
				enterOuterAlt(_localctx, 1);
				{
				setState(489);
				drefClafer();
				}
				break;
			case THIS_KEYWORD:
				enterOuterAlt(_localctx, 2);
				{
				setState(490);
				thisClafer();
				}
				break;
			case PARENT_KEYWORD:
				enterOuterAlt(_localctx, 3);
				{
				setState(491);
				parentClafer();
				}
				break;
			case CLAFER_ID:
				enterOuterAlt(_localctx, 4);
				{
				setState(492);
				claferName();
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 5);
				{
				setState(493);
				localIdentifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClaferNameContext extends ParserRuleContext {
		public TerminalNode CLAFER_ID() { return getToken(ClaferRxParser.CLAFER_ID, 0); }
		public ClaferNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_claferName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterClaferName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitClaferName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitClaferName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClaferNameContext claferName() throws RecognitionException {
		ClaferNameContext _localctx = new ClaferNameContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_claferName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(496);
			match(CLAFER_ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalIdentifierContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(ClaferRxParser.ID, 0); }
		public LocalIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_localIdentifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterLocalIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitLocalIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitLocalIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LocalIdentifierContext localIdentifier() throws RecognitionException {
		LocalIdentifierContext _localctx = new LocalIdentifierContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_localIdentifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(498);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(ClaferRxParser.ID, 0); }
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(500);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DrefClaferContext extends ParserRuleContext {
		public TerminalNode DREF_KEYWORD() { return getToken(ClaferRxParser.DREF_KEYWORD, 0); }
		public DrefClaferContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_drefClafer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterDrefClafer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitDrefClafer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitDrefClafer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DrefClaferContext drefClafer() throws RecognitionException {
		DrefClaferContext _localctx = new DrefClaferContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_drefClafer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(502);
			match(DREF_KEYWORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ThisClaferContext extends ParserRuleContext {
		public TerminalNode THIS_KEYWORD() { return getToken(ClaferRxParser.THIS_KEYWORD, 0); }
		public ThisClaferContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_thisClafer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterThisClafer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitThisClafer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitThisClafer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ThisClaferContext thisClafer() throws RecognitionException {
		ThisClaferContext _localctx = new ThisClaferContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_thisClafer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(504);
			match(THIS_KEYWORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParentClaferContext extends ParserRuleContext {
		public TerminalNode PARENT_KEYWORD() { return getToken(ClaferRxParser.PARENT_KEYWORD, 0); }
		public ParentClaferContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parentClafer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterParentClafer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitParentClafer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitParentClafer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParentClaferContext parentClafer() throws RecognitionException {
		ParentClaferContext _localctx = new ParentClaferContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_parentClafer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(506);
			match(PARENT_KEYWORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealSetContext extends ParserRuleContext {
		public TerminalNode REAL_KEYWORD() { return getToken(ClaferRxParser.REAL_KEYWORD, 0); }
		public TerminalNode DOUBLE_KEYWORD() { return getToken(ClaferRxParser.DOUBLE_KEYWORD, 0); }
		public RealSetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realSet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterRealSet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitRealSet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitRealSet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RealSetContext realSet() throws RecognitionException {
		RealSetContext _localctx = new RealSetContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_realSet);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(508);
			_la = _input.LA(1);
			if ( !(_la==REAL_KEYWORD || _la==DOUBLE_KEYWORD) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringSetContext extends ParserRuleContext {
		public TerminalNode STRING_KEYWORD() { return getToken(ClaferRxParser.STRING_KEYWORD, 0); }
		public StringSetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringSet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterStringSet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitStringSet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitStringSet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringSetContext stringSet() throws RecognitionException {
		StringSetContext _localctx = new StringSetContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_stringSet);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(510);
			match(STRING_KEYWORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntSetContext extends ParserRuleContext {
		public TerminalNode INTEGER_KEYWORD() { return getToken(ClaferRxParser.INTEGER_KEYWORD, 0); }
		public IntSetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intSet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIntSet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIntSet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIntSet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntSetContext intSet() throws RecognitionException {
		IntSetContext _localctx = new IntSetContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_intSet);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(512);
			match(INTEGER_KEYWORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealLiteralContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(ClaferRxParser.NUMBER, 0); }
		public RealLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterRealLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitRealLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitRealLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RealLiteralContext realLiteral() throws RecognitionException {
		RealLiteralContext _localctx = new RealLiteralContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_realLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(514);
			match(NUMBER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntLiteralContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(ClaferRxParser.INTEGER, 0); }
		public IntLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterIntLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitIntLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitIntLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntLiteralContext intLiteral() throws RecognitionException {
		IntLiteralContext _localctx = new IntLiteralContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_intLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(516);
			match(INTEGER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringExprContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(ClaferRxParser.STRING, 0); }
		public StringExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).enterStringExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ClaferRxListener ) ((ClaferRxListener)listener).exitStringExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ClaferRxVisitor ) return ((ClaferRxVisitor<? extends T>)visitor).visitStringExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringExprContext stringExpr() throws RecognitionException {
		StringExprContext _localctx = new StringExprContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_stringExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(518);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 25:
			return boolExpr_sempred((BoolExprContext)_localctx, predIndex);
		case 31:
			return setExpr_sempred((SetExprContext)_localctx, predIndex);
		case 34:
			return numericExpr_sempred((NumericExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean boolExpr_sempred(BoolExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 7);
		case 2:
			return precpred(_ctx, 6);
		case 3:
			return precpred(_ctx, 5);
		case 4:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean setExpr_sempred(SetExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 6);
		case 6:
			return precpred(_ctx, 5);
		case 7:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean numericExpr_sempred(NumericExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 8:
			return precpred(_ctx, 15);
		case 9:
			return precpred(_ctx, 14);
		case 10:
			return precpred(_ctx, 13);
		case 11:
			return precpred(_ctx, 12);
		case 12:
			return precpred(_ctx, 11);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3I\u020b\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\3\2\7\2f\n\2\f\2"+
		"\16\2i\13\2\3\3\3\3\3\3\5\3n\n\3\3\4\3\4\5\4r\n\4\3\5\5\5u\n\5\3\5\3\5"+
		"\3\5\5\5z\n\5\3\5\5\5}\n\5\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\5\b\u008f\n\b\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3"+
		"\13\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3"+
		"\20\3\21\3\21\3\21\6\21\u00ac\n\21\r\21\16\21\u00ad\3\21\3\21\5\21\u00b2"+
		"\n\21\3\22\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\25\3\25\3\26\3\26"+
		"\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\5\30\u00cd"+
		"\n\30\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\5\33\u00ea\n\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\3\33\3\33\7\33\u00fb\n\33\f\33\16\33\u00fe\13\33\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\5\34\u0108\n\34\3\35\3\35\3\35\6\35\u010d"+
		"\n\35\r\35\16\35\u010e\3\35\3\35\3\35\3\35\3\35\6\35\u0116\n\35\r\35\16"+
		"\35\u0117\3\35\3\35\3\35\3\35\3\35\3\35\6\35\u0120\n\35\r\35\16\35\u0121"+
		"\3\35\3\35\3\35\3\35\3\35\6\35\u0129\n\35\r\35\16\35\u012a\3\35\3\35\3"+
		"\35\3\35\3\35\3\35\6\35\u0133\n\35\r\35\16\35\u0134\3\35\3\35\3\35\3\35"+
		"\3\35\6\35\u013c\n\35\r\35\16\35\u013d\3\35\3\35\3\35\3\35\3\35\3\35\6"+
		"\35\u0146\n\35\r\35\16\35\u0147\3\35\3\35\3\35\3\35\3\35\6\35\u014f\n"+
		"\35\r\35\16\35\u0150\3\35\3\35\3\35\5\35\u0156\n\35\3\36\3\36\3\36\7\36"+
		"\u015b\n\36\f\36\16\36\u015e\13\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u017b\n\37\3 \3 \3 \3 \3 \3 \3 \3"+
		" \3 \3 \3 \3 \3 \3 \3 \3 \5 \u018d\n \3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3"+
		"!\3!\3!\3!\5!\u019d\n!\3!\3!\3!\3!\3!\3!\3!\3!\3!\7!\u01a8\n!\f!\16!\u01ab"+
		"\13!\3\"\3\"\3#\3#\3#\7#\u01b2\n#\f#\16#\u01b5\13#\3$\3$\3$\3$\3$\3$\3"+
		"$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\5$\u01d2"+
		"\n$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\7$\u01e3\n$\f$\16$\u01e6"+
		"\13$\3%\3%\3%\3%\3&\3&\3&\3&\3&\5&\u01f1\n&\3\'\3\'\3(\3(\3)\3)\3*\3*"+
		"\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\62\2\5"+
		"\64@F\63\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668"+
		":<>@BDFHJLNPRTVXZ\\^`b\2\5\4\2::EE\3\2\34\35\3\2=>\u022a\2g\3\2\2\2\4"+
		"m\3\2\2\2\6q\3\2\2\2\bt\3\2\2\2\n\u0081\3\2\2\2\f\u0083\3\2\2\2\16\u008e"+
		"\3\2\2\2\20\u0090\3\2\2\2\22\u0093\3\2\2\2\24\u0096\3\2\2\2\26\u0099\3"+
		"\2\2\2\30\u009c\3\2\2\2\32\u009f\3\2\2\2\34\u00a2\3\2\2\2\36\u00a5\3\2"+
		"\2\2 \u00b1\3\2\2\2\"\u00b3\3\2\2\2$\u00b6\3\2\2\2&\u00b8\3\2\2\2(\u00ba"+
		"\3\2\2\2*\u00be\3\2\2\2,\u00c0\3\2\2\2.\u00cc\3\2\2\2\60\u00ce\3\2\2\2"+
		"\62\u00d3\3\2\2\2\64\u00e9\3\2\2\2\66\u0107\3\2\2\28\u0155\3\2\2\2:\u0157"+
		"\3\2\2\2<\u017a\3\2\2\2>\u018c\3\2\2\2@\u019c\3\2\2\2B\u01ac\3\2\2\2D"+
		"\u01ae\3\2\2\2F\u01d1\3\2\2\2H\u01e7\3\2\2\2J\u01f0\3\2\2\2L\u01f2\3\2"+
		"\2\2N\u01f4\3\2\2\2P\u01f6\3\2\2\2R\u01f8\3\2\2\2T\u01fa\3\2\2\2V\u01fc"+
		"\3\2\2\2X\u01fe\3\2\2\2Z\u0200\3\2\2\2\\\u0202\3\2\2\2^\u0204\3\2\2\2"+
		"`\u0206\3\2\2\2b\u0208\3\2\2\2df\5\4\3\2ed\3\2\2\2fi\3\2\2\2ge\3\2\2\2"+
		"gh\3\2\2\2h\3\3\2\2\2ig\3\2\2\2jn\5\6\4\2kn\5.\30\2ln\5\60\31\2mj\3\2"+
		"\2\2mk\3\2\2\2ml\3\2\2\2n\5\3\2\2\2or\5\b\5\2pr\5\62\32\2qo\3\2\2\2qp"+
		"\3\2\2\2r\7\3\2\2\2su\5\n\6\2ts\3\2\2\2tu\3\2\2\2uv\3\2\2\2vw\5$\23\2"+
		"wy\5L\'\2xz\5\f\7\2yx\3\2\2\2yz\3\2\2\2z|\3\2\2\2{}\5\16\b\2|{\3\2\2\2"+
		"|}\3\2\2\2}~\3\2\2\2~\177\5&\24\2\177\u0080\5 \21\2\u0080\t\3\2\2\2\u0081"+
		"\u0082\79\2\2\u0082\13\3\2\2\2\u0083\u0084\7\3\2\2\u0084\u0085\5J&\2\u0085"+
		"\r\3\2\2\2\u0086\u008f\5\26\f\2\u0087\u008f\5\30\r\2\u0088\u008f\5\20"+
		"\t\2\u0089\u008f\5\34\17\2\u008a\u008f\5\36\20\2\u008b\u008f\5\32\16\2"+
		"\u008c\u008f\5\22\n\2\u008d\u008f\5\24\13\2\u008e\u0086\3\2\2\2\u008e"+
		"\u0087\3\2\2\2\u008e\u0088\3\2\2\2\u008e\u0089\3\2\2\2\u008e\u008a\3\2"+
		"\2\2\u008e\u008b\3\2\2\2\u008e\u008c\3\2\2\2\u008e\u008d\3\2\2\2\u008f"+
		"\17\3\2\2\2\u0090\u0091\7\4\2\2\u0091\u0092\5@!\2\u0092\21\3\2\2\2\u0093"+
		"\u0094\7\4\2\2\u0094\u0095\5Z.\2\u0095\23\3\2\2\2\u0096\u0097\7\5\2\2"+
		"\u0097\u0098\5Z.\2\u0098\25\3\2\2\2\u0099\u009a\7\4\2\2\u009a\u009b\5"+
		"\\/\2\u009b\27\3\2\2\2\u009c\u009d\7\4\2\2\u009d\u009e\5X-\2\u009e\31"+
		"\3\2\2\2\u009f\u00a0\7\5\2\2\u00a0\u00a1\5@!\2\u00a1\33\3\2\2\2\u00a2"+
		"\u00a3\7\5\2\2\u00a3\u00a4\5\\/\2\u00a4\35\3\2\2\2\u00a5\u00a6\7\5\2\2"+
		"\u00a6\u00a7\5X-\2\u00a7\37\3\2\2\2\u00a8\u00b2\5\"\22\2\u00a9\u00ab\7"+
		"\67\2\2\u00aa\u00ac\5\6\4\2\u00ab\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad"+
		"\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00b0\78"+
		"\2\2\u00b0\u00b2\3\2\2\2\u00b1\u00a8\3\2\2\2\u00b1\u00a9\3\2\2\2\u00b2"+
		"!\3\2\2\2\u00b3\u00b4\7\67\2\2\u00b4\u00b5\78\2\2\u00b5#\3\2\2\2\u00b6"+
		"\u00b7\5(\25\2\u00b7%\3\2\2\2\u00b8\u00b9\5(\25\2\u00b9\'\3\2\2\2\u00ba"+
		"\u00bb\5*\26\2\u00bb\u00bc\7\6\2\2\u00bc\u00bd\5,\27\2\u00bd)\3\2\2\2"+
		"\u00be\u00bf\7E\2\2\u00bf+\3\2\2\2\u00c0\u00c1\t\2\2\2\u00c1-\3\2\2\2"+
		"\u00c2\u00c3\7\7\2\2\u00c3\u00c4\7\b\2\2\u00c4\u00c5\5F$\2\u00c5\u00c6"+
		"\7\t\2\2\u00c6\u00cd\3\2\2\2\u00c7\u00c8\7\7\2\2\u00c8\u00c9\7\n\2\2\u00c9"+
		"\u00ca\5F$\2\u00ca\u00cb\7\t\2\2\u00cb\u00cd\3\2\2\2\u00cc\u00c2\3\2\2"+
		"\2\u00cc\u00c7\3\2\2\2\u00cd/\3\2\2\2\u00ce\u00cf\7\13\2\2\u00cf\u00d0"+
		"\7\f\2\2\u00d0\u00d1\5\64\33\2\u00d1\u00d2\7\r\2\2\u00d2\61\3\2\2\2\u00d3"+
		"\u00d4\7\f\2\2\u00d4\u00d5\5\64\33\2\u00d5\u00d6\7\r\2\2\u00d6\63\3\2"+
		"\2\2\u00d7\u00d8\b\33\1\2\u00d8\u00d9\7\16\2\2\u00d9\u00da\5\64\33\2\u00da"+
		"\u00db\7\17\2\2\u00db\u00dc\5\64\33\2\u00dc\u00dd\7\20\2\2\u00dd\u00de"+
		"\5\64\33\16\u00de\u00ea\3\2\2\2\u00df\u00ea\58\35\2\u00e0\u00ea\5\66\34"+
		"\2\u00e1\u00e2\7\21\2\2\u00e2\u00e3\5\64\33\2\u00e3\u00e4\7\22\2\2\u00e4"+
		"\u00ea\3\2\2\2\u00e5\u00e6\7\30\2\2\u00e6\u00ea\5\64\33\5\u00e7\u00ea"+
		"\5<\37\2\u00e8\u00ea\5> \2\u00e9\u00d7\3\2\2\2\u00e9\u00df\3\2\2\2\u00e9"+
		"\u00e0\3\2\2\2\u00e9\u00e1\3\2\2\2\u00e9\u00e5\3\2\2\2\u00e9\u00e7\3\2"+
		"\2\2\u00e9\u00e8\3\2\2\2\u00ea\u00fc\3\2\2\2\u00eb\u00ec\f\n\2\2\u00ec"+
		"\u00ed\7\23\2\2\u00ed\u00fb\5\64\33\13\u00ee\u00ef\f\t\2\2\u00ef\u00f0"+
		"\7\24\2\2\u00f0\u00fb\5\64\33\n\u00f1\u00f2\f\b\2\2\u00f2\u00f3\7\25\2"+
		"\2\u00f3\u00fb\5\64\33\t\u00f4\u00f5\f\7\2\2\u00f5\u00f6\7\26\2\2\u00f6"+
		"\u00fb\5\64\33\b\u00f7\u00f8\f\6\2\2\u00f8\u00f9\7\27\2\2\u00f9\u00fb"+
		"\5\64\33\7\u00fa\u00eb\3\2\2\2\u00fa\u00ee\3\2\2\2\u00fa\u00f1\3\2\2\2"+
		"\u00fa\u00f4\3\2\2\2\u00fa\u00f7\3\2\2\2\u00fb\u00fe\3\2\2\2\u00fc\u00fa"+
		"\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\65\3\2\2\2\u00fe\u00fc\3\2\2\2\u00ff"+
		"\u0100\7\31\2\2\u0100\u0108\5@!\2\u0101\u0102\7\32\2\2\u0102\u0108\5@"+
		"!\2\u0103\u0104\7\33\2\2\u0104\u0108\5@!\2\u0105\u0106\t\3\2\2\u0106\u0108"+
		"\5@!\2\u0107\u00ff\3\2\2\2\u0107\u0101\3\2\2\2\u0107\u0103\3\2\2\2\u0107"+
		"\u0105\3\2\2\2\u0108\67\3\2\2\2\u0109\u010a\7\36\2\2\u010a\u010c\7\37"+
		"\2\2\u010b\u010d\5:\36\2\u010c\u010b\3\2\2\2\u010d\u010e\3\2\2\2\u010e"+
		"\u010c\3\2\2\2\u010e\u010f\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0111\7 "+
		"\2\2\u0111\u0112\5\64\33\2\u0112\u0156\3\2\2\2\u0113\u0115\7\36\2\2\u0114"+
		"\u0116\5:\36\2\u0115\u0114\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0115\3\2"+
		"\2\2\u0117\u0118\3\2\2\2\u0118\u0119\3\2\2\2\u0119\u011a\7 \2\2\u011a"+
		"\u011b\5\64\33\2\u011b\u0156\3\2\2\2\u011c\u011d\7\32\2\2\u011d\u011f"+
		"\7\37\2\2\u011e\u0120\5:\36\2\u011f\u011e\3\2\2\2\u0120\u0121\3\2\2\2"+
		"\u0121\u011f\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0123\3\2\2\2\u0123\u0124"+
		"\7 \2\2\u0124\u0125\5\64\33\2\u0125\u0156\3\2\2\2\u0126\u0128\7\32\2\2"+
		"\u0127\u0129\5:\36\2\u0128\u0127\3\2\2\2\u0129\u012a\3\2\2\2\u012a\u0128"+
		"\3\2\2\2\u012a\u012b\3\2\2\2\u012b\u012c\3\2\2\2\u012c\u012d\7 \2\2\u012d"+
		"\u012e\5\64\33\2\u012e\u0156\3\2\2\2\u012f\u0130\7\33\2\2\u0130\u0132"+
		"\7\37\2\2\u0131\u0133\5:\36\2\u0132\u0131\3\2\2\2\u0133\u0134\3\2\2\2"+
		"\u0134\u0132\3\2\2\2\u0134\u0135\3\2\2\2\u0135\u0136\3\2\2\2\u0136\u0137"+
		"\7 \2\2\u0137\u0138\5\64\33\2\u0138\u0156\3\2\2\2\u0139\u013b\7\33\2\2"+
		"\u013a\u013c\5:\36\2\u013b\u013a\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u013b"+
		"\3\2\2\2\u013d\u013e\3\2\2\2\u013e\u013f\3\2\2\2\u013f\u0140\7 \2\2\u0140"+
		"\u0141\5\64\33\2\u0141\u0156\3\2\2\2\u0142\u0143\7\34\2\2\u0143\u0145"+
		"\7\37\2\2\u0144\u0146\5:\36\2\u0145\u0144\3\2\2\2\u0146\u0147\3\2\2\2"+
		"\u0147\u0145\3\2\2\2\u0147\u0148\3\2\2\2\u0148\u0149\3\2\2\2\u0149\u014a"+
		"\7 \2\2\u014a\u014b\5\64\33\2\u014b\u0156\3\2\2\2\u014c\u014e\7\34\2\2"+
		"\u014d\u014f\5:\36\2\u014e\u014d\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u014e"+
		"\3\2\2\2\u0150\u0151\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0153\7 \2\2\u0153"+
		"\u0154\5\64\33\2\u0154\u0156\3\2\2\2\u0155\u0109\3\2\2\2\u0155\u0113\3"+
		"\2\2\2\u0155\u011c\3\2\2\2\u0155\u0126\3\2\2\2\u0155\u012f\3\2\2\2\u0155"+
		"\u0139\3\2\2\2\u0155\u0142\3\2\2\2\u0155\u014c\3\2\2\2\u01569\3\2\2\2"+
		"\u0157\u015c\5N(\2\u0158\u0159\7!\2\2\u0159\u015b\5N(\2\u015a\u0158\3"+
		"\2\2\2\u015b\u015e\3\2\2\2\u015c\u015a\3\2\2\2\u015c\u015d\3\2\2\2\u015d"+
		"\u015f\3\2\2\2\u015e\u015c\3\2\2\2\u015f\u0160\7\3\2\2\u0160\u0161\5@"+
		"!\2\u0161;\3\2\2\2\u0162\u0163\5F$\2\u0163\u0164\7\"\2\2\u0164\u0165\5"+
		"F$\2\u0165\u017b\3\2\2\2\u0166\u0167\5F$\2\u0167\u0168\7#\2\2\u0168\u0169"+
		"\5F$\2\u0169\u017b\3\2\2\2\u016a\u016b\5F$\2\u016b\u016c\7$\2\2\u016c"+
		"\u016d\5F$\2\u016d\u017b\3\2\2\2\u016e\u016f\5F$\2\u016f\u0170\7%\2\2"+
		"\u0170\u0171\5F$\2\u0171\u017b\3\2\2\2\u0172\u0173\5F$\2\u0173\u0174\7"+
		"&\2\2\u0174\u0175\5F$\2\u0175\u017b\3\2\2\2\u0176\u0177\5F$\2\u0177\u0178"+
		"\7\'\2\2\u0178\u0179\5F$\2\u0179\u017b\3\2\2\2\u017a\u0162\3\2\2\2\u017a"+
		"\u0166\3\2\2\2\u017a\u016a\3\2\2\2\u017a\u016e\3\2\2\2\u017a\u0172\3\2"+
		"\2\2\u017a\u0176\3\2\2\2\u017b=\3\2\2\2\u017c\u017d\5@!\2\u017d\u017e"+
		"\7&\2\2\u017e\u017f\5@!\2\u017f\u018d\3\2\2\2\u0180\u0181\5@!\2\u0181"+
		"\u0182\7\'\2\2\u0182\u0183\5@!\2\u0183\u018d\3\2\2\2\u0184\u0185\5@!\2"+
		"\u0185\u0186\7(\2\2\u0186\u0187\5@!\2\u0187\u018d\3\2\2\2\u0188\u0189"+
		"\5@!\2\u0189\u018a\7)\2\2\u018a\u018b\5@!\2\u018b\u018d\3\2\2\2\u018c"+
		"\u017c\3\2\2\2\u018c\u0180\3\2\2\2\u018c\u0184\3\2\2\2\u018c\u0188\3\2"+
		"\2\2\u018d?\3\2\2\2\u018e\u018f\b!\1\2\u018f\u0190\7\21\2\2\u0190\u0191"+
		"\5@!\2\u0191\u0192\7\22\2\2\u0192\u019d\3\2\2\2\u0193\u0194\7\16\2\2\u0194"+
		"\u0195\5\64\33\2\u0195\u0196\7\17\2\2\u0196\u0197\5@!\2\u0197\u0198\7"+
		"\20\2\2\u0198\u0199\5@!\5\u0199\u019d\3\2\2\2\u019a\u019d\5B\"\2\u019b"+
		"\u019d\5b\62\2\u019c\u018e\3\2\2\2\u019c\u0193\3\2\2\2\u019c\u019a\3\2"+
		"\2\2\u019c\u019b\3\2\2\2\u019d\u01a9\3\2\2\2\u019e\u019f\f\b\2\2\u019f"+
		"\u01a0\7*\2\2\u01a0\u01a8\5@!\t\u01a1\u01a2\f\7\2\2\u01a2\u01a3\7+\2\2"+
		"\u01a3\u01a8\5@!\b\u01a4\u01a5\f\6\2\2\u01a5\u01a6\7,\2\2\u01a6\u01a8"+
		"\5@!\7\u01a7\u019e\3\2\2\2\u01a7\u01a1\3\2\2\2\u01a7\u01a4\3\2\2\2\u01a8"+
		"\u01ab\3\2\2\2\u01a9\u01a7\3\2\2\2\u01a9\u01aa\3\2\2\2\u01aaA\3\2\2\2"+
		"\u01ab\u01a9\3\2\2\2\u01ac\u01ad\5D#\2\u01adC\3\2\2\2\u01ae\u01b3\5J&"+
		"\2\u01af\u01b0\7-\2\2\u01b0\u01b2\5J&\2\u01b1\u01af\3\2\2\2\u01b2\u01b5"+
		"\3\2\2\2\u01b3\u01b1\3\2\2\2\u01b3\u01b4\3\2\2\2\u01b4E\3\2\2\2\u01b5"+
		"\u01b3\3\2\2\2\u01b6\u01b7\b$\1\2\u01b7\u01b8\7\16\2\2\u01b8\u01b9\5\64"+
		"\33\2\u01b9\u01ba\7\17\2\2\u01ba\u01bb\5F$\2\u01bb\u01bc\7\20\2\2\u01bc"+
		"\u01bd\5F$\22\u01bd\u01d2\3\2\2\2\u01be\u01bf\7\21\2\2\u01bf\u01c0\5F"+
		"$\2\u01c0\u01c1\7\22\2\2\u01c1\u01d2\3\2\2\2\u01c2\u01c3\7\60\2\2\u01c3"+
		"\u01d2\5F$\13\u01c4\u01c5\7\62\2\2\u01c5\u01d2\5@!\2\u01c6\u01c7\7\63"+
		"\2\2\u01c7\u01d2\5@!\2\u01c8\u01c9\7\64\2\2\u01c9\u01d2\5@!\2\u01ca\u01cb"+
		"\7\65\2\2\u01cb\u01d2\5@!\2\u01cc\u01cd\7\66\2\2\u01cd\u01d2\5@!\2\u01ce"+
		"\u01d2\5H%\2\u01cf\u01d2\5`\61\2\u01d0\u01d2\5^\60\2\u01d1\u01b6\3\2\2"+
		"\2\u01d1\u01be\3\2\2\2\u01d1\u01c2\3\2\2\2\u01d1\u01c4\3\2\2\2\u01d1\u01c6"+
		"\3\2\2\2\u01d1\u01c8\3\2\2\2\u01d1\u01ca\3\2\2\2\u01d1\u01cc\3\2\2\2\u01d1"+
		"\u01ce\3\2\2\2\u01d1\u01cf\3\2\2\2\u01d1\u01d0\3\2\2\2\u01d2\u01e4\3\2"+
		"\2\2\u01d3\u01d4\f\21\2\2\u01d4\u01d5\7:\2\2\u01d5\u01e3\5F$\22\u01d6"+
		"\u01d7\f\20\2\2\u01d7\u01d8\7.\2\2\u01d8\u01e3\5F$\21\u01d9\u01da\f\17"+
		"\2\2\u01da\u01db\7/\2\2\u01db\u01e3\5F$\20\u01dc\u01dd\f\16\2\2\u01dd"+
		"\u01de\7\60\2\2\u01de\u01e3\5F$\17\u01df\u01e0\f\r\2\2\u01e0\u01e1\7\61"+
		"\2\2\u01e1\u01e3\5F$\16\u01e2\u01d3\3\2\2\2\u01e2\u01d6\3\2\2\2\u01e2"+
		"\u01d9\3\2\2\2\u01e2\u01dc\3\2\2\2\u01e2\u01df\3\2\2\2\u01e3\u01e6\3\2"+
		"\2\2\u01e4\u01e2\3\2\2\2\u01e4\u01e5\3\2\2\2\u01e5G\3\2\2\2\u01e6\u01e4"+
		"\3\2\2\2\u01e7\u01e8\5D#\2\u01e8\u01e9\7-\2\2\u01e9\u01ea\7?\2\2\u01ea"+
		"I\3\2\2\2\u01eb\u01f1\5R*\2\u01ec\u01f1\5T+\2\u01ed\u01f1\5V,\2\u01ee"+
		"\u01f1\5L\'\2\u01ef\u01f1\5N(\2\u01f0\u01eb\3\2\2\2\u01f0\u01ec\3\2\2"+
		"\2\u01f0\u01ed\3\2\2\2\u01f0\u01ee\3\2\2\2\u01f0\u01ef\3\2\2\2\u01f1K"+
		"\3\2\2\2\u01f2\u01f3\7B\2\2\u01f3M\3\2\2\2\u01f4\u01f5\7C\2\2\u01f5O\3"+
		"\2\2\2\u01f6\u01f7\7C\2\2\u01f7Q\3\2\2\2\u01f8\u01f9\7?\2\2\u01f9S\3\2"+
		"\2\2\u01fa\u01fb\7@\2\2\u01fbU\3\2\2\2\u01fc\u01fd\7A\2\2\u01fdW\3\2\2"+
		"\2\u01fe\u01ff\t\4\2\2\u01ffY\3\2\2\2\u0200\u0201\7;\2\2\u0201[\3\2\2"+
		"\2\u0202\u0203\7<\2\2\u0203]\3\2\2\2\u0204\u0205\7E\2\2\u0205_\3\2\2\2"+
		"\u0206\u0207\7F\2\2\u0207a\3\2\2\2\u0208\u0209\7D\2\2\u0209c\3\2\2\2$"+
		"gmqty|\u008e\u00ad\u00b1\u00cc\u00e9\u00fa\u00fc\u0107\u010e\u0117\u0121"+
		"\u012a\u0134\u013d\u0147\u0150\u0155\u015c\u017a\u018c\u019c\u01a7\u01a9"+
		"\u01b3\u01d1\u01e2\u01e4\u01f0";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}