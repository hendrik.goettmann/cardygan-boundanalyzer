#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
# set -x

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

scratch_mvn=$(mktemp -d -t tmp.XXXXXXXXXX)
local_dry_run=false

echo "Maven repo dir: $scratch_mvn"

function cleanUp() {
    if [[ -d "$scratch_mvn" ]]
    then
        rm -rf "$scratch_mvn"
    fi

    if [ "$local_dry_run" = false ]; then
    	touch ~/.ssh/id_rsa
    	rm ~/.ssh/id_rsa
	fi
}
 
trap cleanUp EXIT

main() {
	git_acc="$1"
	mvn_repo_private_key="$2"
	mvn_repo_url="$3"
	project_dir="${__root}/"


	# config git
	if [ "$local_dry_run" = false ]; then
		mkdir -p ~/.ssh/
		touch ~/.ssh/config
		echo "Host github.com\n\tStrictHostKeyChecking no\n" > ~/.ssh/config
		git config --global user.email "$git_acc"
		git config --global user.name "gitlab-ci"
    fi


  	mkdir -p target/mvn-repo
  	
  	# build
  	gradle publish -x generateGrammarSource

  	echo "Build succeeded"

  	deployToMaven "$mvn_repo_private_key" "$mvn_repo_url"

	echo "Deployment succeeded."
}

deployToMaven(){
	echo "Deploying to maven repository..."
	local privateKey="$1"
	local repo_url="$2"

	# init ssh for mvn-repo git
	if [ "$local_dry_run" = false ]; then
		setPrivateKey "$privateKey"
	fi

  	# deploy to mvn
  	initDirAsRepo "$scratch_mvn" cardygan-mvn "$repo_url"
  	
  	rsync -av ${project_dir}/artifacts/ $scratch_mvn

  	pushChangesToGit $scratch_mvn cardygan-mvn
  	
}

setPrivateKey() {

	touch ~/.ssh/known_hosts
	ssh-keyscan -t rsa github.com > ~/.ssh/known_hosts

	echo "Setting private key."
	local privateKey="$1"
	echo "$privateKey" > ~/id_rsa
	chmod 700 ~/id_rsa
	mv ~/id_rsa ~/.ssh/
	eval `ssh-agent -s`
	ssh-add ~/.ssh/id_rsa

}

mkdir_cd(){
	local dir_path="$1"

	if [ ! -d $dir_path ]; then
		mkdir $dir_path
	fi
	cd $dir_path
}

initDirAsRepo() {
	echo "Initializing git repository."
	local repo="$1"
	local git_remote_name="$2"
	local git_remote_url="$3"
	# check if repo is empty
	if [[ "$(ls -A ${repo})" ]]; then
		echo "ERROR: git cannot be initialized in a non-empty directory."
		exit 1
	fi

	cd $repo
	git init
	git remote add $git_remote_name $git_remote_url
	git pull $git_remote_url master
}

pushChangesToGit(){
	echo "Pushing changes to git..."
	local repo="$1"
	local git_remote_name="$2"

	cd $repo
	git add .
	git checkout --ours .
	git add -u
	git commit -m"updated releases."


	if [ "$local_dry_run" = false ]; then
		git push $git_remote_name master
	fi
}

main "$@"
